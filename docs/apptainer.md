# Installing Apptainer

Apptainer (formerly Singularity) is a popular container platform designed for High Performance Computing (HPC) environments. Below are the installation instructions for Arch Linux, MacOS, and Windows.

---

## Arch Linux

### Installation

1. Install Apptainer:
   ```bash
   sudo pacman -Sy apptainer
   ```

2. Verify the installation:
   ```bash
   apptainer version
   ```

---

## MacOS

### Step 1: Install Lima

1. **Install Homebrew** (if you don't have it installed):
   Homebrew is a package manager for macOS. If you haven't installed it, run the following command:
   ```bash
   /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
   ```

2. **Install Lima:**
   Once Homebrew is installed, install Lima using:
   ```bash
   brew install lima
   ```

3. **Initialize a Default Lima VM:**
   After installation, initialize Lima with a default configuration:
   ```bash
   limactl start
   ```

   This will prompt you to download a Linux image and configure the virtual machine. Select `Yes` to proceed.

### Step 2: Connect to the Lima VM

1. **Start the VM:**
   If the VM is not already running, you can start it with:
   ```bash
   limactl start default
   ```

2. **Open a Shell in the VM:**
   To connect to the Linux VM, run:
   ```bash
   lima
   ```

   This opens a shell in the running Linux environment, which you will use to install Apptainer.

### Step 3: Install Apptainer in the Lima VM

Once inside the Lima VM (which is now running a Linux environment), follow these steps to install Apptainer:

1. **Update the Package Lists:**
   Update the package manager to ensure all packages are up to date:
   ```bash
   sudo apt update
   ```

2. **Install Dependencies:**
   Install the necessary build tools and dependencies for Apptainer:
   ```bash
   sudo apt install -y build-essential libseccomp-dev pkg-config squashfs-tools
   ```

3. **Install Go (Golang):**
   Apptainer requires Go (Golang) to build from source:
   ```bash
   sudo apt install -y golang
   ```

4. **Download and Build Apptainer:**
   Clone the Apptainer repository and build it from source:
   ```bash
   git clone https://github.com/apptainer/apptainer.git
   cd apptainer
   ./mconfig
   make -C builddir
   sudo make -C builddir install
   ```

### Step 4: Verify the Installation

1. **Check Apptainer Version:**
   After the installation is complete, verify the installation by checking the version of Apptainer:
   ```bash
   apptainer version
   ```

   If Apptainer was installed successfully, you should see the version number output.

### Step 5: Exiting and Managing Lima

1. **Exit the Lima VM:**
   To exit the VM, simply type:
   ```bash
   exit
   ```

2. **Stopping Lima VM:**
   When you're done using the VM, you can stop it by running:
   ```bash
   limactl stop default
   ```
   
---

## Windows

Apptainer is not natively supported on Windows, but you can use it within a Linux environment through **Windows Subsystem for Linux (WSL)**.

### Prerequisites
1. Install WSL and a Linux distribution (e.g., Ubuntu):
   - Open PowerShell as Administrator and run:
     ```bash
     wsl --install
     ```

2. Once WSL is installed, restart your computer, and set up your Linux distribution (e.g., Ubuntu).

3. After setting up the Linux distribution, install necessary packages:
   ```bash
   sudo apt update
   sudo apt install -y build-essential libseccomp-dev pkg-config squashfs-tools
   ```

### Installation (Inside WSL)
1. Clone and build Apptainer from source:
   ```bash
   git clone https://github.com/apptainer/apptainer.git
   cd apptainer
   ./mconfig
   make -C builddir
   sudo make -C builddir install
   ```

2. Verify the installation:
   ```bash
   apptainer version
   ```

---

## Conclusion

By following the above steps, you should have Apptainer installed on Arch Linux, macOS, or through WSL on Windows. To ensure that Apptainer is working correctly, run the version check and start experimenting with containerizing applications.