cmsml==0.2.6
keras==3.4.1
matplotlib==3.9.0
numpy==1.26.4
scipy==1.13.1
tensorflow-cpu>=2.17.0
tqdm==4.66.4
numba==0.60.0