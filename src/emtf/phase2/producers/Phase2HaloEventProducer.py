import math

import numpy as np

from emtf.core.commons.combinatorics import yield_combinations
from emtf.core.commons.kinematics import propagate_particle
from emtf.core.commons.pairing import reduce_pairs
from emtf.core.entries import Producer


class Phase2HaloEventProducer(Producer):

    def __init__(self):
        self.event_count = 0
        self.invalid_event_count = 0
        self.invalid_muon_event_count = 0
        self.muon_count = 0
        self.invalid_muon_count = 0
        self.invalid_eta = 0

    def provides(self):
        return ['beam_halo_muons']

    def empty_products(self):
        return {
            'beam_halo_muons': list()
        }

    def extract(self, event):
        # Increase Counter
        self.event_count += 1

        # Get Muon LUT
        event_muon_count = len(event.mu_gen_id_to_idx.keys())

        # Short-Circuit: No muons in event
        if event_muon_count == 0:
            self.invalid_event_count += 1
            return self.empty_products()

        # Increase Counter
        self.muon_count += event_muon_count

        # Build muon collection
        muon_cadidates = list()

        for gen_id, gen_idx in event.mu_gen_id_to_idx.items():
            # Get GEN
            gen = event.gen_particles[gen_idx]

            pos_st2_mom, pos_st2_pos = propagate_particle(
                gen.q, gen.p3, gen.pos, z_star=850.
            )

            pos_st4_mom, pos_st4_pos = propagate_particle(
                gen.q, gen.p3, gen.pos, z_star=1050.
            )

            neg_st2_mom, neg_st2_pos = propagate_particle(
                gen.q, gen.p3, gen.pos, z_star=-850.
            )

            neg_st4_mom, neg_st4_pos = propagate_particle(
                gen.q, gen.p3, gen.pos, z_star=-1050.
            )

            # Short-Circuit: Only muons with valid eta in station 2
            eta_cond = ((1.2 <= abs(pos_st2_pos.Eta()) <= 2.4) and (1.2 <= abs(pos_st4_pos.Eta()) <= 2.4))
            eta_cond |= ((1.2 <= abs(neg_st2_pos.Eta()) <= 2.4) and (1.2 <= abs(neg_st4_pos.Eta()) <= 2.4))

            if not eta_cond:
                self.invalid_muon_count += 1
                self.invalid_eta += 1
                continue

            # Add Candidate
            muon_cadidates.append(gen)

        # Short-Circuit: No muons found
        if len(muon_cadidates) == 0:
            self.invalid_muon_event_count += 1
            return self.empty_products()

        # Associate tracks to muons
        associate_tracks(event, muon_cadidates)

        # Return
        return {
            'beam_halo_muons': muon_cadidates
        }

    def summary(self):
        valid_event_count = self.event_count - self.invalid_event_count - self.invalid_muon_event_count
        valid_muon_count = self.muon_count - self.invalid_muon_count

        print('Event Count: %d' % self.event_count)
        print('Invalid Event Count: %d' % self.invalid_event_count)
        print('Invalid Muon Event Count: %d' % self.invalid_muon_event_count)
        print('Valid Event Count: %d' % valid_event_count)
        print('Muon Count: %d' % self.muon_count)
        print('Invalid Muon Count: %d' % self.invalid_muon_count)
        print('Invalid Eta: %d' % self.invalid_eta)
        print('Valid Muon Count: %d' % valid_muon_count)


def associate_tracks(event, muon_candidates):
    prompt_col = list()
    disp_col = list()

    for gen, track in yield_combinations(muon_candidates, event.emtf_tracks):
        # Short-Circuit: Only use valid tracks
        if not track.valid:
            continue

        # Short-Circuit: Must be in zone
        if track.zone != gen.zone:
            continue

        # Check dR
        dR = math.hypot(
            gen.phi_st2 - np.deg2rad(track.model_phi),
            gen.eta_st2 - track.model_eta
        )

        # Select tracks
        if track.unconstrained:
            disp_col.append((gen.idx, track.idx, dR))
        else:
            prompt_col.append((gen.idx, track.idx, dR))

    # Reduce pairs
    prompt_pairs = reduce_pairs(prompt_col, value_fn=lambda entry: entry[2])
    disp_pairs = reduce_pairs(disp_col, value_fn=lambda entry: entry[2])

    # Associate
    for pair in prompt_pairs:
        gen = event.gen_particles[pair[0]]
        track = event.emtf_tracks[pair[1]]
        gen.best_prompt_track = track

    for pair in disp_pairs:
        gen = event.gen_particles[pair[0]]
        track = event.emtf_tracks[pair[1]]
        gen.best_disp_track = track
