import math

import numpy as np

from emtf.core.commons.combinatorics import yield_combinations
from emtf.core.commons.pairing import reduce_pairs
from emtf.core.entries import Producer
from emtf.phase2.commons.emtf_model import num_sectors


class Phase2MuonEventProducer(Producer):

    def __init__(self, multiplicity):
        self.multiplicity = multiplicity

        self.event_count = 0
        self.invalid_event_count = 0
        self.invalid_muon_event_count = 0
        self.muon_count = 0
        self.invalid_muon_count = 0
        self.invalid_endsec = 0
        self.invalid_zone = 0
        self.invalid_lxy = 0
        self.invalid_lz = 0

    def provides(self):
        return ['muons']

    def empty_products(self):
        return {
            'muons': list()
        }

    def extract(self, event):
        # Increase Counter
        self.event_count += 1

        # Get Muon LUT
        event_muon_count = len(event.mu_tp_id_to_idx.keys())

        # Short-Circuit: No muons in event
        if event_muon_count == 0:
            self.invalid_event_count += 1
            return self.empty_products()

        # Short-Circuit: Should only have N gen muons
        if (self.multiplicity > -1) and (event_muon_count != self.multiplicity):
            self.invalid_event_count += 1
            return self.empty_products()

        # Increase Counter
        self.muon_count += event_muon_count

        # Build muon collection
        muon_cadidates = list()

        for tp_id, tp_idx in event.mu_tp_id_to_idx.items():
            # Get TP
            tp = event.tracking_particles[tp_idx]

            # Short-Circuit: Must be in valid sector and zone
            associate_endsec(event, tp)

            if tp.endsec is None:
                self.invalid_muon_count += 1
                self.invalid_endsec += 1
                continue

            # Short-Circuit: Must be before muon system in lxy
            if not (tp.lxy <= 300):
                self.invalid_muon_count += 1
                self.invalid_lxy += 1
                continue

            # Short-Circuit: Must be before muon system in lz
            if not (abs(tp.vz) <= 500):
                self.invalid_muon_count += 1
                self.invalid_lz += 1
                continue

            # Short-Circuit: In valid eta region
            if tp.zone is None:
                self.invalid_muon_count += 1
                self.invalid_zone += 1
                continue

            # Add Candidate
            muon_cadidates.append(tp)

        # Short-Circuit: No muons found
        if len(muon_cadidates) == 0:
            self.invalid_muon_event_count += 1
            return self.empty_products()

        # Associate tracks to muons
        associate_tracks(event, muon_cadidates)

        # Associate hits and segments
        for tp in muon_cadidates:
            associate_hits(event, tp)
            associate_segments(event, tp)

        # Return
        return {
            'muons': muon_cadidates
        }

    def summary(self):
        valid_event_count = self.event_count - self.invalid_event_count - self.invalid_muon_event_count
        valid_muon_count = self.muon_count - self.invalid_muon_count
        expected_muon_count = valid_event_count * self.multiplicity
        lost_muon_count = max(expected_muon_count - valid_muon_count, 0)

        if self.multiplicity == -1:
            expected_muon_count = -1
            lost_muon_count = -1

        print('Event Count: %d' % self.event_count)
        print('Invalid Event Count: %d' % self.invalid_event_count)
        print('Invalid Muon Event Count: %d' % self.invalid_muon_event_count)
        print('Valid Event Count: %d' % valid_event_count)
        print('Muon Count: %d' % self.muon_count)
        print('Invalid Muon Count: %d' % self.invalid_muon_count)
        print('Invalid Endsec: %d' % self.invalid_endsec)
        print('Invalid Zone: %d' % self.invalid_zone)
        print('Invalid Lxy: %d' % self.invalid_lxy)
        print('Invalid Lz: %d' % self.invalid_lz)
        print('Expected Muon Count: %d' % expected_muon_count)
        print('Valid Muon Count: %d' % valid_muon_count)
        print('Lost Muon Count: %d' % (lost_muon_count))


def associate_endsec(event, tp):
    # Init Modes
    activations = np.zeros(num_sectors, dtype=np.int32)

    # Loop Hits
    for hit in event.emtf_hits:
        # Short-Circuit: Only keep tp hits
        if tp.id not in hit.sim_tp:
            continue

        # Check endsec
        assert (hit.endsec < num_sectors)

        # Pack bits
        bits = np.zeros(8, dtype=np.bool_)
        bits[0] = (hit.emtf_host == 18)  # GE0
        bits[1] = (hit.emtf_host == 0)  # ME1/1
        bits[2] = (hit.emtf_host in (1, 2))  # ME1/2, ME1/3
        bits[3] = (hit.emtf_host in (3, 4))  # ME2/1, ME2/2
        bits[4] = (hit.emtf_host in (5, 6))  # ME3/1, ME3/2
        bits[5] = (hit.emtf_host in (7, 8))  # ME4/1, ME4/2
        bits[6] = (hit.emtf_host in (9, 10, 11, 12, 13))  # GE1/1, RE1/2, RE1/3, GE2/1, RE2/2
        bits[7] = (hit.emtf_host in (14, 15, 16, 17))  # RE3/1, RE3/2, RE4/1, RE4/2
        activation = np.packbits(bits)  # pack 8 booleans into an uint8

        # OR Activation
        activations[hit.endsec] |= activation

    # Get Best
    best_endsec = np.argmax(activations)
    best_activation = activations[best_endsec]

    if best_activation == 0:
        return None

    # Associate
    tp.endsec = best_endsec


def associate_tracks(event, muon_candidates):
    prompt_col = list()
    disp_col = list()

    for tp, track in yield_combinations(muon_candidates, event.emtf_tracks):
        # Short-Circuit: Only use valid tracks
        if not track.valid:
            continue

        # Short-Circuit: Doesn't match best endsec
        if track.endsec != tp.endsec:
            continue

        # Short-Circuit: Must be in zone
        if track.zone != tp.zone:
            continue

        # Check dR
        dR = math.hypot(
            tp.phi_st2 - np.deg2rad(track.model_phi),
            tp.eta_st2 - track.model_eta
        )

        # Select tracks
        if track.unconstrained:
            disp_col.append((tp.idx, track.idx, dR))
        else:
            prompt_col.append((tp.idx, track.idx, dR))

    # Reduce pairs
    prompt_pairs = reduce_pairs(prompt_col, value_fn=lambda entry: entry[2])
    disp_pairs = reduce_pairs(disp_col, value_fn=lambda entry: entry[2])

    # Associate
    for pair in prompt_pairs:
        tp = event.tracking_particles[pair[0]]
        track = event.emtf_tracks[pair[1]]
        tp.best_prompt_track = track

    for pair in disp_pairs:
        tp = event.tracking_particles[pair[0]]
        track = event.emtf_tracks[pair[1]]
        tp.best_disp_track = track


def associate_hits(event, tp):
    endsec_hits = set()
    tp_hits = set()

    # Loop all hits regardless if they were input into track building or not
    for hit in event.emtf_hits:
        # Short-Circuit: Doesn't match best endsec
        if hit.endsec != tp.endsec:
            continue

        # Collect
        endsec_hits.add(hit.idx)

        # Short-Circuit: Isn't from muon
        if tp.id not in hit.sim_tp:
            continue

        # Collect
        tp_hits.add(hit.idx)

    # Associate
    tp.endsec_hits = [event.emtf_hits[idx] for idx in endsec_hits]
    tp.hits = [event.emtf_hits[idx] for idx in tp_hits]


def associate_segments(event, tp):
    endsec_segs = set()
    tp_segs = set()

    # Loop Inputs
    for input in event.emtf_inputs:
        # Short-Circuit: Doesn't match endsec
        if input.endsec != tp.endsec:
            continue

        # Collect segments
        for hit_idx in input.hits:
            # Get Hit
            hit = event.emtf_hits[hit_idx]

            # Collect
            endsec_segs.add(hit_idx)

            # Short-Circuit: Isn't from muon
            if tp.id not in hit.sim_tp:
                continue

            # Collect
            tp_segs.add(hit_idx)

    # Associate
    tp.endsec_segs = [event.emtf_hits[idx] for idx in endsec_segs]
    tp.segs = [event.emtf_hits[idx] for idx in tp_segs]
