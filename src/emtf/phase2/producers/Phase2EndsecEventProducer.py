from emtf.core.entries import Producer


class Phase2EndsecEventProducer(Producer):

    def __init__(self):
        self.event_count = 0
        self.endsec_event_count = 0

    def provides(self):
        return ['endsec']

    def empty_products(self):
        return {
            'endsec': dict()
        }

    def extract(self, event):
        self.event_count += 1

        # Short-Circuit: No tp received
        if len(event.tp_id_to_idx.keys()) == 0:
            return self.empty_products()

        # Build Endsec
        # Loop through all available hits and collect hits and tp per endsec
        endsec_entries = dict()

        for hit in event.emtf_hits:
            # Make sure hit is in a valid BX
            if hit.bx not in [-2, -1, 0, 1, 2, 3]:
                continue

            # Checkout Endsec Info
            endsec_entry = endsec_entries.get(hit.endsec, None)

            if endsec_entry is None:
                endsec_entry = {
                    'endsec': hit.endsec,
                    'tp': set(),
                    'hits': set(),
                    'segs': set(),
                    'tracks': set(),
                }

                endsec_entries[hit.endsec] = endsec_entry

            # Unpack Endsec Info
            endsec_tp = endsec_entry['tp']
            endsec_hits = endsec_entry['hits']

            # Append hits
            endsec_hits.add(hit.idx)

            # Unpack hit
            for tp_id in hit.sim_tp:
                tp_idx = event.tp_id_to_idx.get(tp_id, -1)

                # Short-Circuit: Invalid tracking particle
                if tp_idx < 0:
                    continue

                # Add Tracking Particle
                endsec_tp.add(tp_idx)

        # Short-Circuit: No entries
        if len(endsec_entries.keys()) == 0:
            return self.empty_products()

        # Find Endsec Information
        for endsec_id, endsec_entry in endsec_entries.items():
            # Convert Indexes to Objects
            endsec_entry['tp'] = [event.tracking_particles[idx] for idx in endsec_entry['tp']]
            endsec_entry['hits'] = [event.emtf_hits[idx] for idx in endsec_entry['hits']]

            # Find Tracks and Segments
            endsec_entry['segs'] = find_segments(event, endsec_id)
            endsec_entry['tracks'] = find_tracks(event, endsec_id)

        # Count
        self.endsec_event_count += len(endsec_entries.keys())

        # Return Endsecs
        return {
            'endsec': endsec_entries
        }

    def summary(self):
        print('Event Count: %d' % self.event_count)
        print('Endsec Event Count: %d' % self.endsec_event_count)


def find_tracks(event, endsec):
    tracks = list()

    # Loop tracks
    for track in event.emtf_tracks:
        # Short-Circuit: Only use valid tracks
        if not track.valid:
            continue

        # Short-Circuit: Doesn't match endsec
        if track.endsec != endsec:
            continue

        # Add
        tracks.append(track)

    # Return
    return tracks


def find_segments(event, endsec):
    segs = set()

    # Loop Inputs
    for input in event.emtf_inputs:
        # Short-Circuit: Doesn't match best endsec
        if input.endsec != endsec:
            continue

        # Collect segments
        for hit_idx in input.hits:
            segs.add(hit_idx)

    # Return
    return [event.emtf_hits[idx] for idx in segs]
