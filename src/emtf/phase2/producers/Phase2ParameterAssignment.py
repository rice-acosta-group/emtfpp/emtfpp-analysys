from emtf.core.entries import Producer
from emtf.phase2.producers.Phase2Protobuf import Phase2Protobuf


class Phase2ParameterAssignment(Producer):

    def __init__(self, prompt_pbf=None, disp_pbf=None):

        self.prompt_pb = None
        self.disp_pb = None

        if prompt_pbf is not None:
            self.prompt_pb = Phase2Protobuf(prompt_pbf, False)

        if disp_pbf is not None:
            self.disp_pb = Phase2Protobuf(disp_pbf, True)

    def provides(self):
        return ['trk_parameters_emtf', 'trk_parameters_calib', 'trk_parameters_model']

    def extract(self, event):
        # Init Collections
        trk_parameters_emtf = dict()
        trk_parameters_calib = dict()
        trk_parameters_model = dict()

        # Collect Original Values
        for track in event.emtf_tracks:
            # Parameters
            trk_parameters_emtf[track.idx] = (
                track.emtf_q * track.emtf_pt,
                track.emtf_rels,
                track.emtf_dxy
            )

            trk_parameters_calib[track.idx] = (
                track.emtf_q * track.calib_pt,
                track.calib_rels,
                track.calib_dxy
            )

            trk_parameters_model[track.idx] = (
                track.emtf_q * track.model_pt,
                track.model_rels,
                track.model_dxy
            )

        # Short-Circuit: No pb provided
        if (self.prompt_pb is None) and (self.disp_pb is None):
            return {
                'trk_parameters_emtf': trk_parameters_emtf,
                'trk_parameters_calib': trk_parameters_calib,
                'trk_parameters_model': trk_parameters_model,
            }

        # Evaluate
        params = trk_parameters_emtf, trk_parameters_calib, trk_parameters_model

        if self.prompt_pb is not None:
            params = self.prompt_pb.eval(params, event)

        if self.disp_pb is not None:
            params = self.disp_pb.eval(params, event)

        # Unpack new results
        trk_parameters_emtf, trk_parameters_calib, trk_parameters_model = params

        # Return
        return {
            'trk_parameters_emtf': trk_parameters_emtf,
            'trk_parameters_calib': trk_parameters_calib,
            'trk_parameters_model': trk_parameters_model,
        }

    def summary(self):
        pass
