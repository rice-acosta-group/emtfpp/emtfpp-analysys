import numpy as np

from emtf.core.commons.arbitrary_precision import convert_int_to_uint, clip_int
from emtf.core.commons.ml_models import load_protobuf_model
from emtf.phase2.commons.emtf_constants import emtf_dxy_eps, emtf_pt_eps, emtf_rels_eps
from emtf.phase2.luts.calibrated_luts import disp_pt_calibrated_lut, prompt_pt_calibrated_lut, \
    dxy_calibrated_lut
from emtf.phase2.luts.emtf_luts import disp_pt_emtf_lut, prompt_pt_emtf_lut, \
    dxy_emtf_lut
from emtf.phase2.luts.model_luts import disp_pt_model_lut, dxy_model_lut, prompt_pt_model_lut, rels_model_lut


class Phase2Protobuf(object):

    def __init__(self, protobuf_file, displaced_en=False):
        self.displaced_en = displaced_en

        self.pb_graph, self.pb_session = None, None
        self.pb_input_tensor = None
        self.pb_output_tensor = None

        if protobuf_file is not None:
            self.pb_graph, self.pb_session = load_protobuf_model(protobuf_file)
            self.pb_input_tensor = self.pb_graph.get_tensor_by_name('inputs:0')
            self.pb_output_tensor = self.pb_graph.get_tensor_by_name('Identity:0')

    def eval(self, params, event):
        # Init Collections
        trk_parameters_emtf, trk_parameters_calib, trk_parameters_model = params

        # Find Tracks
        trk_idx_col = list()
        trk_var_col = list()

        for track in event.emtf_tracks:
            # Short-Circuit: Only use valid tracks
            if not track.valid:
                continue

            # Short-Circuit: Only relevant tracks
            if self.displaced_en != track.unconstrained:
                continue

            # Append
            trk_idx_col.append(track.idx)
            trk_var_col.append(track.features)

        # Short-Circuit: No tracks found
        if len(trk_idx_col) == 0:
            return trk_parameters_emtf, trk_parameters_calib, trk_parameters_model

        # Predict Parameters
        pb_predictions = self.pb_session.run(self.pb_output_tensor, feed_dict={self.pb_input_tensor: trk_var_col})

        if self.displaced_en:
            pred_qpt = pb_predictions[:, 0]
            pred_rels = pb_predictions[:, 1]
            pred_dxy = pb_predictions[:, 2]
        else:
            pred_qpt = pb_predictions[:, 0]
            pred_rels = pb_predictions[:, 1]
            pred_dxy = np.zeros((len(trk_idx_col),))

        # Unpack Predictions
        for evaluation_id, trk_idx in enumerate(trk_idx_col):
            # Get QPt parameter
            if self.displaced_en:
                quantized_x = int(pred_qpt[evaluation_id])
                quantized_x = clip_int(quantized_x, 10)
                bin_x = convert_int_to_uint(quantized_x, 10)

                trk_model_pt = disp_pt_model_lut[bin_x] * emtf_pt_eps
                trk_model_q = 1 if bin_x <= (1 << 9) else -1
                trk_model_qpt = trk_model_q * trk_model_pt
                trk_calib_qpt = trk_model_q * disp_pt_calibrated_lut[bin_x] * emtf_pt_eps
                trk_emtf_qpt = trk_model_q * disp_pt_emtf_lut[bin_x] * emtf_pt_eps
            else:
                quantized_x = int(pred_qpt[evaluation_id])
                quantized_x = clip_int(quantized_x, 10)
                bin_x = convert_int_to_uint(quantized_x, 10)

                trk_model_pt = prompt_pt_model_lut[bin_x] * emtf_pt_eps
                trk_model_q = 1 if bin_x <= (1 << 9) else -1
                trk_model_qpt = trk_model_q * trk_model_pt
                trk_calib_qpt = trk_model_q * prompt_pt_calibrated_lut[bin_x] * emtf_pt_eps
                trk_emtf_qpt = trk_model_q * prompt_pt_emtf_lut[bin_x] * emtf_pt_eps

            # Get Relevance parameter
            quantized_x = int(pred_rels[evaluation_id])
            quantized_x = clip_int(quantized_x, 10)
            bin_x = convert_int_to_uint(quantized_x, 10)

            trk_rels = rels_model_lut[bin_x] * emtf_rels_eps

            # Get Dxy parameter
            quantized_x = int(pred_dxy[evaluation_id])
            quantized_x = clip_int(quantized_x, 10)
            bin_x = convert_int_to_uint(quantized_x, 10)

            trk_model_dxy = dxy_model_lut[bin_x] * emtf_dxy_eps
            trk_calib_dxy = dxy_calibrated_lut[bin_x] * emtf_dxy_eps
            trk_emtf_dxy = dxy_emtf_lut[bin_x] * emtf_dxy_eps

            # Pack parameters
            trk_parameters_model[trk_idx] = (trk_model_qpt, trk_rels, trk_model_dxy)
            trk_parameters_calib[trk_idx] = (trk_calib_qpt, trk_rels, trk_calib_dxy)
            trk_parameters_emtf[trk_idx] = (trk_emtf_qpt, trk_rels, trk_emtf_dxy)

        # Return
        return trk_parameters_emtf, trk_parameters_calib, trk_parameters_model
