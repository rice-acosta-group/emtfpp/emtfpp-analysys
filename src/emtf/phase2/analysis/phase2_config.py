from emtf.core.entries import SimpleProducer
from emtf.phase2.commons.ntuple_luts import get_mu_tp_id_to_idx_lut, get_tp_id_to_idx_lut, get_gen_id_to_idx_lut, \
    get_mu_gen_id_to_idx_lut, get_hscp_tp_id_to_idx_lut
from emtf.phase2.commons.ntuple_primitives import get_emtf_tracks, get_tracking_particles, get_emtf_inputs, \
    get_emtf_hits, get_gen_particles

branch_aliases = {
    'trk_model_theta': 'trk_model_eta'
}

base_producers = [
    SimpleProducer('gen_id_to_idx', lambda tree: get_gen_id_to_idx_lut(tree)),
    SimpleProducer('mu_gen_id_to_idx', lambda tree: get_mu_gen_id_to_idx_lut(tree)),
    SimpleProducer('tp_id_to_idx', lambda tree: get_tp_id_to_idx_lut(tree)),
    SimpleProducer('mu_tp_id_to_idx', lambda tree: get_mu_tp_id_to_idx_lut(tree)),
    SimpleProducer('hscp_tp_id_to_idx', lambda tree: get_hscp_tp_id_to_idx_lut(tree)),
    SimpleProducer('gen_particles', lambda tree: get_gen_particles(tree)),
    SimpleProducer('tracking_particles', lambda tree: get_tracking_particles(tree)),
    SimpleProducer('emtf_inputs', lambda tree: get_emtf_inputs(tree)),
    SimpleProducer('emtf_hits', lambda tree: get_emtf_hits(tree)),
    SimpleProducer('emtf_tracks', lambda tree: get_emtf_tracks(tree))
]
