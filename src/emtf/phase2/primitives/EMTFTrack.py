import numpy as np

from emtf.core.commons.emtf import calc_theta_deg_from_int, calc_phi_glob_deg_from_loc, calc_phi_loc_deg_from_int, \
    get_trigger_endsec
from emtf.core.commons.kinematics import calc_eta_from_theta_deg
from emtf.core.primitives import VectorPrimitive
from emtf.phase2.commons.emtf_constants import emtf_pt_eps, emtf_rels_eps, emtf_dxy_eps
from emtf.phase2.commons.emtf_eta_ranges import emtf_z0_eta_start, emtf_z0_eta_stop, emtf_z1_eta_stop, \
    emtf_z1_eta_start, emtf_z2_eta_start, emtf_z2_eta_stop
from emtf.phase2.commons.emtf_modes import calc_emtf_mode_v1, calc_emtf_mode_v2
from emtf.phase2.commons.emtf_quality import calc_emtf_quality
from emtf.phase2.luts.calibrated_luts import prompt_pt_calibrated_lut, disp_pt_calibrated_lut, \
    dxy_calibrated_lut
from emtf.phase2.luts.emtf_luts import prompt_pt_emtf_lut, disp_pt_emtf_lut, dxy_emtf_lut
from emtf.phase2.luts.model_luts import rels_model_lut, prompt_pt_model_lut, disp_pt_model_lut, dxy_model_lut


class IdealConditions(object):

    def __init__(self, valid, nhits, model_qual, emtf_mode_v1, emtf_mode_v2):
        self.nhits = nhits
        self.emtf_mode_v1 = emtf_mode_v1
        self.emtf_mode_v2 = emtf_mode_v2
        self.emtf_quality = calc_emtf_quality(
            valid,
            model_qual,
            emtf_mode_v1,
            emtf_mode_v2
        )


class EMTFTrack(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'trk_')

    def get_endsec(self):
        return get_trigger_endsec(self.endcap, self.sector)

    def get_zone(self):
        if emtf_z0_eta_start <= abs(self.model_eta) < emtf_z0_eta_stop:
            return 0
        elif emtf_z1_eta_start <= abs(self.model_eta) < emtf_z1_eta_stop:
            return 1
        elif emtf_z2_eta_start <= abs(self.model_eta) < emtf_z2_eta_stop:
            return 2

        return None

    def get_emtf_quality(self):
        return calc_emtf_quality(
            self.valid,
            self.model_qual,
            self.emtf_mode_v1,
            self.emtf_mode_v2
        )

    def get_emtf_q(self):
        emtf_q = self.get_from_entry('emtf_q')

        return -1 if (emtf_q == 1) else 1

    def get_emtf_pt(self):
        address = self.model_pt_address

        if self.unconstrained:
            quantized_pt = disp_pt_emtf_lut[address]
        else:
            quantized_pt = prompt_pt_emtf_lut[address]

        return emtf_pt_eps * quantized_pt

    def get_emtf_rels(self):
        address = self.model_rels_address
        quantized_rels = rels_model_lut[address]
        return emtf_rels_eps * quantized_rels

    def get_emtf_dxy(self):
        # Prompt dxy is 0
        if not self.unconstrained:
            return 0

        # Lookup
        address = self.model_dxy_address
        quantized_dxy = dxy_emtf_lut[address]
        return emtf_dxy_eps * quantized_dxy

    def get_calib_pt(self):
        address = self.model_pt_address

        if self.unconstrained:
            quantized_pt = disp_pt_calibrated_lut[address]
        else:
            quantized_pt = prompt_pt_calibrated_lut[address]

        return emtf_pt_eps * quantized_pt

    def get_calib_rels(self):
        address = self.model_rels_address
        quantized_rels = rels_model_lut[address]
        return emtf_rels_eps * quantized_rels

    def get_calib_dxy(self):
        # Prompt dxy is 0
        if not self.unconstrained:
            return 0

        # Lookup
        address = self.model_dxy_address
        quantized_dxy = dxy_calibrated_lut[address]
        return emtf_dxy_eps * quantized_dxy

    def get_model_pt(self):
        address = self.model_pt_address

        if self.unconstrained:
            quantized_pt = disp_pt_model_lut[address]
        else:
            quantized_pt = prompt_pt_model_lut[address]

        return emtf_pt_eps * quantized_pt

    def get_model_rels(self):
        address = self.model_rels_address
        quantized_rels = rels_model_lut[address]
        return emtf_rels_eps * quantized_rels

    def get_model_dxy(self):
        # Prompt dxy is 0
        if not self.unconstrained:
            return 0

        # Lookup
        address = self.model_dxy_address
        quantized_dxy = dxy_model_lut[address]
        return emtf_dxy_eps * quantized_dxy

    def get_model_eta(self):
        model_theta = self.get_from_entry('model_theta')

        return calc_eta_from_theta_deg(
            calc_theta_deg_from_int(model_theta)
        ) * self.endcap

    def get_model_phi(self):
        model_phi = self.get_from_entry('model_phi')

        return calc_phi_glob_deg_from_loc(
            calc_phi_loc_deg_from_int(model_phi),
            self.sector
        )

    def get_ideal(self):
        trk_mask = self.site_mask
        trk_rm_mask = self.site_rm_mask

        # Check ideal modes
        ideal_mask = [0 for i in range(12)]

        for site_id in range(12):
            site_bit = ord(trk_mask[site_id])
            site_rm_bit = ord(trk_rm_mask[site_id])
            ideal_mask[site_id] = (site_bit | site_rm_bit)

        ideal_mode_v1 = calc_emtf_mode_v1(ideal_mask)
        ideal_mode_v2 = calc_emtf_mode_v2(ideal_mask)

        return IdealConditions(
            self.valid,
            np.sum(ideal_mask),
            self.model_qual,
            ideal_mode_v1,
            ideal_mode_v2
        )

    def get_features(self):
        feature_sites = [
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
            0, 1, 2, 3, 4, 11,
            0, 1, 2, 3, 4, 11,
            -1, -1, -1, -1
        ]

        trk_features_out = list()

        trk_masked_features = self.model_features

        for feature_id in range(40):
            feature = trk_masked_features[feature_id]
            feature_site_id = feature_sites[feature_id]

            if feature_site_id == -1:
                trk_features_out.append(feature)
                continue

            feature_site_bit = ord(self.site_mask[feature_site_id])

            if feature_site_bit == 1:
                trk_features_out.append(feature)
            else:
                trk_features_out.append(0)

        return np.asarray(trk_features_out)
