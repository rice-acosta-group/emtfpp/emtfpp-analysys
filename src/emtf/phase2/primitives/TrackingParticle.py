import math

import numpy as np
from ROOT import TLorentzVector
from ROOT import TVector3

from emtf.core.commons.kinematics import calc_d0, calc_qinvpt_from_qpt, \
    calc_dxy, kCSC, kGE0, calc_z0_cmssw, propagate_particle, calc_z_star
from emtf.core.primitives import VectorPrimitive
from emtf.phase2.commons.emtf_eta_ranges import emtf_z0_eta_start, emtf_z1_eta_start, emtf_z2_eta_start, \
    emtf_z0_eta_stop, emtf_z1_eta_stop, emtf_z2_eta_stop


class TrackingParticle(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'trk_part_')

        # Constants
        self.endsec = None
        self.endsec_hits = set()
        self.endsec_segs = set()

        self.hits = set()
        self.segs = set()

        self.best_prompt_track = None
        self.best_disp_track = None

        self.get_is_training_quality = False

    def get_endcap(self):
        eta_st2 = self.mom_pos_st2[1].Eta()

        if eta_st2 >= 0:
            return 1

        return -1

    def get_zone(self):
        eta_st2 = self.mom_pos_st2[1].Eta()

        if emtf_z0_eta_start <= abs(eta_st2) < emtf_z0_eta_stop:
            return 0
        elif emtf_z1_eta_start <= abs(eta_st2) < emtf_z1_eta_stop:
            return 1
        elif emtf_z2_eta_start <= abs(eta_st2) < emtf_z2_eta_stop:
            return 2

        return None

    def get_pos(self):
        return TVector3(self.vx, self.vy, self.vz)

    def get_p4(self):
        p4 = TLorentzVector()
        p4.SetPtEtaPhiE(self.pt, self.eta, self.phi, self.energy)
        return p4

    def get_p3(self):
        p3 = TVector3()
        p3.SetPtEtaPhi(self.pt, self.eta, self.phi)
        return p3

    def get_lxy(self):
        return math.hypot(self.vx, self.vy)

    def get_lxyz(self):
        return math.hypot(self.lxy, self.vz)

    def get_invpt(self):
        return calc_qinvpt_from_qpt(self.pt)

    def get_d0(self):
        return calc_d0(
            self.q, self.pt, self.phi,
            self.vx, self.vy
        )

    def get_dxy(self):
        return calc_dxy(
            self.q, self.pt, self.phi,
            self.vx, self.vy
        )

    def get_udxy(self):
        return abs(self.dxy)

    def get_z0(self):
        return calc_z0_cmssw(
            self.pt, self.eta, self.phi,
            self.vx, self.vy, self.vz
        )

    def get_mom_pos_st2(self):
        return propagate_particle(
            self.q, self.p3, self.pos
        )

    def get_mom_pos_st4(self):
        return propagate_particle(
            self.q, self.p3, self.pos,
            z_star=calc_z_star(self.eta, self.vz, 1050.)
        )

    def get_phi_st2(self):
        return self.mom_pos_st2[0].Phi()

    def get_phi_st4(self):
        return self.mom_pos_st4[0].Phi()

    def get_eta_st2(self):
        return self.mom_pos_st2[0].Eta()

    def get_eta_st4(self):
        return self.mom_pos_st4[0].Eta()

    def get_dphidz(self):
        pos_st2, mom_st2 = self.mom_pos_st2
        pos_st4, mom_st4 = self.mom_pos_st4

        dphi = np.rad2deg(pos_st4.Phi() - pos_st2.Phi())
        dz = pos_st4.z() - pos_st2.z()

        return dphi / dz

    def get_drdz(self):
        pos_st2, mom_st2 = self.mom_pos_st2
        pos_st4, mom_st4 = self.mom_pos_st4

        dr = pos_st4.Perp() - pos_st2.Perp()
        dz = pos_st4.z() - pos_st2.z()

        return dr / dz

    def get_endsec_stations(self):
        return {
            hit.station
            for hit in self.endsec_hits
        }

    def get_endsec_subsystems(self):
        return {
            hit.subsystem
            for hit in self.endsec_hits
        }

    def get_is_training_quality(self):
        # Endsec should have at least two hits
        if len(self.endsec_hits) < 2:
            return False

        # Check Stations and Subsystems
        min_station = min(self.endsec_stations) if len(self.endsec_stations) else 5
        max_station = max(self.endsec_stations) if len(self.endsec_stations) else 0
        both_ge0_me1 = (kGE0 in self.endsec_subsystems) and (kCSC in self.endsec_subsystems)

        endsec_has_two_stations = ((min_station <= 1 and max_station >= 2)
                                   or (min_station == 2 and max_station >= 3)
                                   or (max_station == 1 and both_ge0_me1))

        if not endsec_has_two_stations:
            return False

        return True
