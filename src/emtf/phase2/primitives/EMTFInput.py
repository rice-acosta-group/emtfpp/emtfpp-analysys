from emtf.core.commons.emtf import get_trigger_endsec
from emtf.core.primitives import VectorPrimitive


class EMTFInput(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'in_')

    def get_endsec(self):
        return get_trigger_endsec(self.endcap, self.sector)
