import math

import numpy as np
from ROOT import TLorentzVector
from ROOT import TVector3

from emtf.core.commons.kinematics import calc_d0, calc_qinvpt_from_qpt, \
    calc_dxy, calc_z0_cmssw, propagate_particle, calc_z_star
from emtf.core.primitives import VectorPrimitive
from emtf.phase2.commons.emtf_eta_ranges import emtf_z0_eta_start, emtf_z1_eta_start, emtf_z2_eta_start, \
    emtf_z0_eta_stop, emtf_z1_eta_stop, emtf_z2_eta_stop


class GENParticle(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'gen_part_')

        # Constants
        self.best_prompt_track = None
        self.best_disp_track = None

    def get_endcap(self):
        eta_st2 = self.mom_pos_st2[1].Eta()

        if eta_st2 >= 0:
            return 1

        return -1

    def get_zone(self):
        eta_st2 = self.mom_pos_st2[1].Eta()

        if emtf_z0_eta_start <= abs(eta_st2) < emtf_z0_eta_stop:
            return 0
        elif emtf_z1_eta_start <= abs(eta_st2) < emtf_z1_eta_stop:
            return 1
        elif emtf_z2_eta_start <= abs(eta_st2) < emtf_z2_eta_stop:
            return 2

        return None

    def get_pos(self):
        return TVector3(self.vx, self.vy, self.vz)

    def get_p4(self):
        p4 = TLorentzVector()
        p4.SetPtEtaPhiE(self.pt, self.eta, self.phi, self.energy)
        return p4

    def get_p3(self):
        p3 = TVector3()
        p3.SetPtEtaPhi(self.pt, self.eta, self.phi)
        return p3

    def get_lxy(self):
        return math.hypot(self.vx, self.vy)

    def get_lxyz(self):
        return math.hypot(self.lxy, self.vz)

    def get_invpt(self):
        return calc_qinvpt_from_qpt(self.pt)

    def get_d0(self):
        return calc_d0(
            self.q, self.pt, self.phi,
            self.vx, self.vy
        )

    def get_dxy(self):
        return calc_dxy(
            self.q, self.pt, self.phi,
            self.vx, self.vy
        )

    def get_udxy(self):
        return abs(self.dxy)

    def get_z0(self):
        return calc_z0_cmssw(
            self.pt, self.eta, self.phi,
            self.vx, self.vy, self.vz
        )

    def get_mom_pos_st2(self):
        return propagate_particle(
            self.q, self.p3, self.pos
        )

    def get_mom_pos_st4(self):
        return propagate_particle(
            self.q, self.p3, self.pos,
            z_star=calc_z_star(self.eta, self.vz, 1050.)
        )

    def get_phi_st2(self):
        return self.mom_pos_st2[0].Phi()

    def get_phi_st4(self):
        return self.mom_pos_st4[0].Phi()

    def get_eta_st2(self):
        return self.mom_pos_st2[0].Eta()

    def get_eta_st4(self):
        return self.mom_pos_st4[0].Eta()

    def get_dphidz(self):
        mom_st2, pos_st2 = self.mom_pos_st2
        mom_st4, pos_st4 = self.mom_pos_st4

        dphi = np.rad2deg(pos_st4.Phi() - pos_st2.Phi())
        dz = pos_st4.z() - pos_st2.z()

        return dphi / dz

    def get_drdz(self):
        mom_st2, pos_st2 = self.mom_pos_st2
        mom_st4, pos_st4 = self.mom_pos_st4

        dr = pos_st4.Perp() - pos_st2.Perp()
        dz = pos_st4.z() - pos_st2.z()

        return dr / dz
