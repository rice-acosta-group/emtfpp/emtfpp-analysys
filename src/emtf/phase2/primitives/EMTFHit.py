from emtf.core.commons.emtf import get_trigger_endsec
from emtf.core.primitives import VectorPrimitive
from emtf.phase2.commons.emtf_model import get_host_bx_to_timezone_lut


class EMTFHit(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'hit_')

    def get_sim_tp(self):
        return {self.sim_tp1, self.sim_tp2}

    def get_endsec(self):
        return get_trigger_endsec(self.endcap, self.sector)

    def get_emtf_zones(self):
        # Get Zones Word
        word = self.get_from_entry('emtf_zones')

        # Collect Zones
        zones = list()

        for zone_id in range(3):
            mask = (1 << zone_id)

            if (word & mask) != mask:
                continue

            zones.append(zone_id)

        return zones

    def get_emtf_timezones(self):
        return get_host_bx_to_timezone_lut()(self.emtf_host, self.bx)

    def get_emtf_thetas(self):
        return [self.emtf_theta1, self.emtf_theta2]
