def calc_emtf_quality(valid, model_quality, emtf_mode_v1, emtf_mode_v2):
    # Short-Circuit: Invalid Track
    if valid == 0:
        return 0

    # Short-Circuit: Single Station
    if emtf_mode_v1 in [0, 1, 2, 4, 8]:
        return 0

    # Calculate Quality Based on ModeV2
    if emtf_mode_v2 == 0:
        if 0 <= model_quality <= 3:
            return 1
        elif 4 <= model_quality <= 7:
            return 2
        elif model_quality > 7:
            return 3

        return 0
    elif emtf_mode_v2 == 4:
        if emtf_mode_v1 in [3, 5, 6, 7]:
            if 8 <= model_quality <= 11:
                return 5
            elif 12 <= model_quality <= 15:
                return 6
            elif model_quality > 15:
                return 7

        return 4
    elif emtf_mode_v2 == 8:
        if emtf_mode_v1 in [9, 10, 12]:
            if 16 <= model_quality <= 23:
                return 9
            elif 24 <= model_quality <= 31:
                return 10
            elif model_quality > 31:
                return 11

        return 8
    elif emtf_mode_v2 == 12:
        if emtf_mode_v1 in [11, 13, 14, 15]:
            if 32 <= model_quality <= 39:
                return 13
            elif 40 <= model_quality <= 51:
                return 14
            elif model_quality > 51:
                return 15

        return 12

    return 0
