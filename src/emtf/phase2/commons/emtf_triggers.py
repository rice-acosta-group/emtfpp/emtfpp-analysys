import math

# Prompt Single Muon
sm_prompt_min_pt = 20
sm_prompt_min_rels = 1
sm_prompt_min_qual = 14
sm_prompt_eta_start = 1.2
sm_prompt_eta_stop = 2.4


# # Prompt Single Muon for Fine-Tuning Theta Window
# sm_prompt_min_pt = 0
# sm_prompt_min_rels = 0
# sm_prompt_min_qual = 0
# sm_prompt_eta_start = 1.2
# sm_prompt_eta_stop = 2.4

def get_sm_prompt_eta_start(eta):
    return max(sm_prompt_eta_start, eta)


def get_sm_prompt_eta_stop(eta):
    return min(sm_prompt_eta_stop, eta)


# Displaced Single Muon
sm_disp_min_pt = 20
sm_disp_min_rels = 1
sm_disp_min_dxy = 8
sm_disp_min_qual = 15
sm_disp_eta_start = 1.2
sm_disp_eta_stop = 2.0


# Displaced Single Muon for Fine-Tuning Theta Window
# sm_disp_min_pt = 0
# sm_disp_min_rels = 0
# sm_disp_min_dxy = 0
# sm_disp_min_qual = 0
# sm_disp_eta_start = 1.2
# sm_disp_eta_stop = 2.4


def get_sm_disp_eta_start(eta):
    return max(sm_disp_eta_start, eta)


def get_sm_disp_eta_stop(eta):
    return min(sm_disp_eta_stop, eta)


# Factories
def new_trk_condition(
        unconstrained=None,
        modes_v1=None, modes_v2=None, min_qual=None,
        min_eta=None, max_eta=None,
        min_pt=None, min_rels=None, min_dxy=None,
        charge=None
):
    def condition(entry):
        trk_unconstrained = entry.get('trk_unconstrained', 0)
        trk_mode_v1 = entry.get('trk_mode_v1', 0)
        trk_mode_v2 = entry.get('trk_mode_v2', 0)
        trk_qual = entry.get('trk_qual', 0)
        trk_q = entry.get('trk_q', 0)
        trk_pt = entry.get('trk_pt', 0)
        trk_rels = entry.get('trk_rels', 1)
        trk_dxy = entry.get('trk_dxy', 0)
        trk_eta = entry.get('trk_eta', 0)

        if unconstrained is not None and not (trk_unconstrained == unconstrained):
            return False

        if modes_v1 is not None and not (trk_mode_v1 in modes_v1):
            return False

        if modes_v2 is not None and not (trk_mode_v2 in modes_v2):
            return False

        if min_qual is not None and not (min_qual <= trk_qual):
            return False

        if charge is not None and not (trk_q == charge):
            return False

        if min_pt is not None and not (min_pt <= abs(trk_pt)):
            return False

        if min_rels is not None and not (min_rels <= trk_rels):
            return False

        if (trk_unconstrained == 1) and min_dxy is not None and not (min_dxy <= abs(trk_dxy)):
            return False

        if min_eta is not None and not (min_eta <= abs(trk_eta)):
            return False

        if max_eta is not None and not (abs(trk_eta) < max_eta):
            return False

        return True

    return condition


def new_gen_condition(
        min_eta=None, max_eta=None,
        min_pt=None, max_pt=None,
        min_dxy=None, max_dxy=None,
        min_lxy=None, max_lxy=None,
        min_lz=None, max_lz=None,
        min_lxyz=None, max_lxyz=None,
        charge=None, d0_sign=1, dxy_sign=None
):
    def condition(entry):
        gen_bx = entry.get('gen_bx', 0)
        gen_q = entry.get('gen_q', 0)
        gen_pt = entry.get('gen_pt', 0)
        gen_d0 = entry.get('gen_d0', 0)
        gen_d0_sign = -1 if (gen_d0 < 0) else 1
        gen_dxy = entry.get('gen_dxy', 0)
        gen_dxy_sign = -1 if (gen_dxy < 0) else 1
        gen_lxy = entry.get('gen_lxy', 0)
        gen_lz = entry.get('gen_lz', 0)
        gen_eta_st2 = entry.get('gen_eta_st2', 0)

        gen_lxyz = math.hypot(gen_lz, gen_lxy)

        if gen_bx != 0:
            return False

        if min_pt is not None and not (min_pt <= abs(gen_pt)):
            return False

        if charge is not None and not (gen_q == charge):
            return False

        if max_pt is not None and not (abs(gen_pt) < max_pt):
            return False

        if min_dxy is not None and not (min_dxy <= abs(gen_d0)):
            return False

        if max_dxy is not None and not (abs(gen_d0) < max_dxy):
            return False

        if d0_sign is not None and not (gen_d0_sign == d0_sign):
            return False

        if dxy_sign is not None and not (gen_dxy_sign == dxy_sign):
            return False

        if min_lxy is not None and not (min_lxy <= gen_lxy):
            return False

        if max_lxy is not None and not (gen_lxy < max_lxy):
            return False

        if min_lz is not None and not (min_lz <= abs(gen_lz)):
            return False

        if max_lz is not None and not (abs(gen_lz) < max_lz):
            return False

        if min_lxyz is not None and not (min_lxyz <= gen_lxyz):
            return False

        if max_lxyz is not None and not (gen_lxyz < max_lxyz):
            return False

        if min_eta is not None and not (min_eta <= abs(gen_eta_st2)):
            return False

        if max_eta is not None and not (abs(gen_eta_st2) < max_eta):
            return False

        return True

    return condition


# Base Trigger
def sm_base_trg(entry):
    return False

# sm_base_trg = new_trk_condition(
#     unconstrained=False, min_qual=sm_prompt_min_qual,
#     min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
#     min_pt=sm_prompt_min_pt, min_rels=sm_prompt_min_rels,
# )
