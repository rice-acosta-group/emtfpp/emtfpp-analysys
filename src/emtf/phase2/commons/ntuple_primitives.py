from emtf.core.commons.caching import CachedVector
from emtf.phase2.primitives.EMTFHit import EMTFHit
from emtf.phase2.primitives.EMTFInput import EMTFInput
from emtf.phase2.primitives.EMTFTrack import EMTFTrack
from emtf.phase2.primitives.GENParticle import GENParticle
from emtf.phase2.primitives.TrackingParticle import TrackingParticle


def get_gen_particles(entry):
    return CachedVector(entry.vsize_gen_part, lambda idx: GENParticle(entry, idx))


def get_tracking_particles(entry):
    return CachedVector(entry.vsize_trk_part, lambda idx: TrackingParticle(entry, idx))


def get_emtf_inputs(entry):
    return CachedVector(entry.vsize_in, lambda idx: EMTFInput(entry, idx))


def get_emtf_hits(entry):
    return CachedVector(entry.vsize_hit, lambda idx: EMTFHit(entry, idx))


def get_emtf_tracks(entry):
    return CachedVector(entry.vsize_trk, lambda idx: EMTFTrack(entry, idx))
