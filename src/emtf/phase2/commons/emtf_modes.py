def calc_emtf_mode_v1(x):
    mode = 0

    if x[0] or x[9] or x[1] or x[5] or x[11]:  # ME1/1, GE1/1, ME1/2, RE1/2, GE0
        mode |= (1 << 3)

    if x[2] or x[10] or x[6]:  # ME2, GE2/1, RE2/2
        mode |= (1 << 2)

    if x[3] or x[7]:  # ME3, RE3
        mode |= (1 << 1)

    if x[4] or x[8]:  # ME4, RE4
        mode |= (1 << 0)

    return mode


# SingleMu (12)
# - at least one station-1 segment (ME1/1, GE1/1, ME1/2, RE1/2, GE0)
#   with one of the following requirements on stations 2,3,4
#   a. if there is ME1/2 or RE1/2,
#      i.  if there is ME1/2, require 1 more CSC station
#      ii. else, require 1 more CSC station + 1 more station
#   b. if there is ME1/1 or GE1/1,
#      i.  if there is ME1/1, require 1 more CSC station + 1 more station
#      ii. else, require 2 more CSC stations
#   c. if there is GE0,
#      i.  if there is ME1/1, require 1 more station in stations 3,4
#      ii. else, require 1 more CSC station + 1 more station
#
# DoubleMu (8)
# - at least one station-1 segment (ME1/1, GE1/1, ME1/2, RE1/2, GE0)
#   with one of the following requirements on stations 2,3,4
#   a. if there is ME1/1 or ME1/2, require 1 more station
#   b. if there is GE1/1 or RE1/2, require 1 more CSC station
#   c. if there is GE0,
#      i.  if there is ME1/1, require 1 more station
#      ii. else, require 1 more CSC station
#
# TripleMu (4)
# - at least two stations
#   a. if there is ME1/1 or ME1/2, require 1 more station
#   b. if there is GE1/1 or RE1/2, require 1 more CSC station
#   c. if there is GE0,
#      i.  if there is ME1/1, require 1 more station
#      ii. else, require 1 more CSC station
#   d. else, require 2 more CSC stations
#
# SingleHit (0)
# - at least one station
#
# Note that SingleMu, DoubleMu, TripleMu, SingleHit are mutually-exclusive categories. 
def calc_emtf_mode_v2(x):
    cnt_ye11 = x[0] + x[9]  # ME1/1, GE1/1
    cnt_ye12 = x[1] + x[5]  # ME1/2, RE1/2
    cnt_ye22 = x[2] + x[10] + x[6]  # ME2, GE2/1, RE2/2
    cnt_ye23 = x[3] + x[7]  # ME3, RE3
    cnt_ye24 = x[4] + x[8]  # ME4, RE4
    cnt_ye2a = (cnt_ye22 != 0) + (cnt_ye23 != 0) + (cnt_ye24 != 0)  #
    cnt_ye2b = (cnt_ye23 != 0) + (cnt_ye24 != 0)  #
    cnt_me11 = x[0]  # ME1/1 only
    cnt_me12 = x[1]  # ME1/2 only
    cnt_me14 = x[11]  # GE0 only
    cnt_me2a = (x[2] != 0) + (x[3] != 0) + (x[4] != 0)  #

    def is_single_mu():
        rule_a_i = (cnt_me12 != 0) and (cnt_me2a >= 1)
        rule_a_ii = (cnt_ye12 != 0) and (cnt_me2a >= 1) and (cnt_ye2a >= 2)
        rule_b_i = (cnt_me11 != 0) and (cnt_me2a >= 1) and (cnt_ye2a >= 2)
        rule_b_ii = (cnt_ye11 != 0) and (cnt_me2a >= 2)
        rule_c_i = (cnt_me14 != 0) and (cnt_me11 != 0) and (cnt_ye2b >= 1)
        rule_c_ii = (cnt_me14 != 0) and (cnt_me2a >= 1) and (cnt_ye2a >= 2)

        if rule_a_i or rule_a_ii or rule_b_i or rule_b_ii or rule_c_i or rule_c_ii:
            return True

        return False

    # DoubleMu(8)
    def is_double_mu():
        rule_a_i = (cnt_me12 != 0) and (cnt_ye2a >= 1)
        rule_a_ii = (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_b_i = (cnt_ye12 != 0) and (cnt_me2a >= 1)
        rule_b_ii = (cnt_ye11 != 0) and (cnt_me2a >= 1)
        rule_c_i = (cnt_me14 != 0) and (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_c_ii = (cnt_me14 != 0) and (cnt_me2a >= 1)

        if rule_a_i or rule_a_ii or rule_b_i or rule_b_ii or rule_c_i or rule_c_ii:
            return True

    # TripleMu(4)
    def is_triple_mu():
        rule_a_i = (cnt_me12 != 0) and (cnt_ye2a >= 1)
        rule_a_ii = (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_b_i = (cnt_ye12 != 0) and (cnt_me2a >= 1)
        rule_b_ii = (cnt_ye11 != 0) and (cnt_me2a >= 1)
        rule_c_i = (cnt_me14 != 0) and (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_c_ii = (cnt_me14 != 0) and (cnt_me2a >= 1)
        rule_d = (cnt_me2a >= 2)

        if rule_a_i or rule_a_ii or rule_b_i or rule_b_ii or rule_c_i or rule_c_ii or rule_d:
            return True

        return False

    # Get Mode
    if is_single_mu():
        return 12
    elif is_double_mu():
        return 8
    elif is_triple_mu():
        return 4
    else:
        return 0
