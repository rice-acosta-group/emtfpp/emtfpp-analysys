def get_gen_id_to_idx_lut(event, only_muons=False):
    gen_id_to_idx = dict()

    for gen_part_id in range(event.vsize_gen_part):
        gen_id = event.trk_part_id[gen_part_id]

        if only_muons and abs(event.gen_part_pdgid[gen_part_id]) != 13:
            continue

        gen_id_to_idx[gen_id] = gen_part_id

    return gen_id_to_idx


def get_mu_gen_id_to_idx_lut(event):
    gen_id_to_idx = dict()

    for gen_idx in range(event.vsize_gen_part):
        if event.gen_part_status[gen_idx] != 1:
            continue

        if abs(event.gen_part_pdgid[gen_idx]) != 13:
            continue

        gen_id_to_idx[event.gen_part_id[gen_idx]] = gen_idx

    return gen_id_to_idx


def get_tp_id_to_idx_lut(event, only_muons=False):
    tp_id_to_idx = dict()

    for trk_part_id in range(event.vsize_trk_part):
        tp_id = event.trk_part_id[trk_part_id]

        if only_muons and abs(event.trk_part_pdgid[trk_part_id]) != 13:
            continue

        tp_id_to_idx[tp_id] = trk_part_id

    return tp_id_to_idx


def get_mu_tp_id_to_idx_lut(event):
    tp_id_to_idx = dict()

    for tp_idx in range(event.vsize_trk_part):
        if event.trk_part_status[tp_idx] != 1:
            continue

        if abs(event.trk_part_pdgid[tp_idx]) != 13:
            continue

        tp_id_to_idx[event.trk_part_id[tp_idx]] = tp_idx

    return tp_id_to_idx


def get_hscp_tp_id_to_idx_lut(event):
    tp_id_to_idx = dict()

    for tp_idx in range(event.vsize_trk_part):
        if event.trk_part_status[tp_idx] != 1:
            continue

        if abs(event.trk_part_pdgid[tp_idx]) != 17:
            continue

        tp_id_to_idx[event.trk_part_id[tp_idx]] = tp_idx

    return tp_id_to_idx
