emtf_pt_bw = 13
emtf_pt_eps = (2 ** -5)

emtf_rels_bw = 7
emtf_rels_eps = 1 / ((2 ** 7) - 1)

emtf_dxy_bw = 7
emtf_dxy_eps = 3.85

emtf_max_pt = 256
emtf_max_rels = 1
emtf_max_d0 = 246.4

img_row_labels = [
    ['GE0', 'GE1/1', 'ME1/1', 'GE2/1', 'ME2/1', 'ME3/1', 'RE3/1', 'MRE4/1'],
    ['GE1/1', 'ME1/1', 'MRE1/2', 'GE2/1', 'ME2/1', 'ME3', 'RE3', 'MRE4'],
    ['ME1/2', 'RE1/2', 'RE2/2', 'ME2/2', 'ME3/2', 'RE3/2', 'ME4/2', 'RE4/2'],
]

site_labels = ['ME1/1', 'ME1/2', 'ME2', 'ME3', 'ME4', 'RE1', 'RE2', 'RE3', 'RE4', 'GE1/1', 'GE2/1', 'GE0']

# Full range of emtf_phi is assumed to be 0..5040 (0..84 deg).
# 84 deg from 60 (native) + 20 (neighbor) + 2 (tolerance, left) + 2 (tolerance, right).
coarse_strip = 8 * 2  # 'doublestrip' unit
min_strip = (315 - 288) * coarse_strip  # 7.2 deg
max_emtf_strip = (315 - 0) * coarse_strip  # 84 deg
