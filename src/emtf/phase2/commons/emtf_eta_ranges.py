emtf_eta_start = 1.2
emtf_eta_stop = 2.4
emtf_z2_eta_start = 1.2
emtf_z2_eta_stop = 1.6
emtf_z1_eta_start = 1.6
emtf_z1_eta_stop = 2.0
emtf_z0_eta_start = 2.0
emtf_z0_eta_stop = 2.4

emtf_extended_zone_eta_ranges = [
    (emtf_z0_eta_start, emtf_z0_eta_stop),
    (emtf_z1_eta_start, emtf_z1_eta_stop),
    (emtf_z2_eta_start, emtf_z2_eta_stop),
    (emtf_eta_start, emtf_eta_stop),
]

emtf_zone_eta_ranges = [
    (emtf_z0_eta_start, emtf_z0_eta_stop),
    (emtf_z1_eta_start, emtf_z1_eta_stop),
    (emtf_z2_eta_start, emtf_z2_eta_stop),
]
