import numpy as np

from emtf.core.commons.arrays import multi_index_get, multi_index_set
from emtf.core.commons.caching import Cached

# Original: https://github.com/jiafulow/L1TMuonSimulations-Jun2021/blob/master/Analyzers/workspace/emtf_algos.py

##################################################################################
# Constants
##################################################################################

# 3 zones and 3 timezones per sector.
num_sectors = 12

num_zones = 3
num_timezones = 3

num_hosts = 19
num_sites = 12
num_rsites = 5


##################################################################################
# Hosts
# Total: 19 (9 from CSC + 9 from GEM & RPC + 1 from GE0)
##################################################################################

# Generates detid to host lut
# Total: 19 (9 from CSC + 9 from GEM & RPC + 1 from GE0)
# (subsystem, station, ring) -> host
@Cached
def get_detid_to_host_lut():
    default_value = -99

    lut = np.full((5, 5, 5), default_value, dtype=np.int32)
    lut[1, 1, 4] = 0  # ME1/1a
    lut[1, 1, 1] = 0  # ME1/1b
    lut[1, 1, 2] = 1  # ME1/2
    lut[1, 1, 3] = 2  # ME1/3
    lut[1, 2, 1] = 3  # ME2/1
    lut[1, 2, 2] = 4  # ME2/2
    lut[1, 3, 1] = 5  # ME3/1
    lut[1, 3, 2] = 6  # ME3/2
    lut[1, 4, 1] = 7  # ME4/1
    lut[1, 4, 2] = 8  # ME4/2
    lut[3, 1, 1] = 9  # GE1/1
    lut[2, 1, 2] = 10  # RE1/2
    lut[2, 1, 3] = 11  # RE1/3
    lut[3, 2, 1] = 12  # GE2/1
    lut[2, 2, 2] = 13  # RE2/2
    lut[2, 2, 3] = 13  # RE2/3
    lut[2, 3, 1] = 14  # RE3/1
    lut[2, 3, 2] = 15  # RE3/2
    lut[2, 3, 3] = 15  # RE3/3
    lut[2, 4, 1] = 16  # RE4/1
    lut[2, 4, 2] = 17  # RE4/2
    lut[2, 4, 3] = 17  # RE4/3
    lut[4, 1, 4] = 18  # GE0

    return lut


# Used to generate host to site lut
# Total: 12 sites (5 from CSC + 4 from RPC + 3 from GEM)
# host -> site
@Cached
def get_host_to_site_lut():
    default_value = -99

    host_to_site_lut = np.full((num_hosts,), default_value, dtype=np.int32)
    host_to_site_lut[0] = 0  # ME1/1a + ME1/1b
    host_to_site_lut[1] = 1  # ME1/2
    host_to_site_lut[2] = 1  # ME1/3
    host_to_site_lut[3] = 2  # ME2/1
    host_to_site_lut[4] = 2  # ME2/2
    host_to_site_lut[5] = 3  # ME3/1
    host_to_site_lut[6] = 3  # ME3/2
    host_to_site_lut[7] = 4  # ME4/1
    host_to_site_lut[8] = 4  # ME4/2
    host_to_site_lut[9] = 9  # GE1/1
    host_to_site_lut[10] = 5  # RE1/2
    host_to_site_lut[11] = 5  # RE1/3
    host_to_site_lut[12] = 10  # GE2/1
    host_to_site_lut[13] = 6  # RE2/2 + RE2/3
    host_to_site_lut[14] = 7  # RE3/1
    host_to_site_lut[15] = 7  # RE3/2 + RE3/3
    host_to_site_lut[16] = 8  # RE4/1
    host_to_site_lut[17] = 8  # RE4/2 + RE4/3
    host_to_site_lut[18] = 11  # GE0

    return host_to_site_lut


##################################################################################
# Sites
# Total: 12 sites (5 from CSC + 4 from RPC + 3 from GEM)
##################################################################################

# Generate detid to site lut
# Defaults to Jia Fu's host to site mapping (See get_host_to_site_lut)
# Total: 12 sites (5 from CSC + 4 from RPC + 3 from GEM)
# (subsystem, station, ring) -> site
@Cached
def get_detid_to_site_lut():
    host_to_site_lut = get_host_to_site_lut()
    detid_to_host_lut = get_detid_to_host_lut()
    detid_count = np.size(detid_to_host_lut)

    detid_to_site_lut = detid_to_host_lut.copy()

    for flat_id in range(detid_count):
        detid = np.unravel_index(flat_id, shape=detid_to_host_lut.shape)
        host = multi_index_get(detid_to_host_lut, detid)

        if host > -99:
            site = host_to_site_lut[host]
            multi_index_set(site, detid_to_site_lut, detid)

    return detid_to_site_lut


@Cached
def get_site_pos_to_rsite_lut():
    default_value = -99

    jagged_lut = [
        [0, 9, 1, 5],  # ME1/1, GE1/1, ME1/2, RE1/2
        [2, 10, 6],  # ME2, GE2/1, RE2/2
        [3, 7],  # ME3, RE3
        [4, 8],  # ME4, RE4
        [11],  # GE0
    ]

    lut = np.full((num_rsites, 4), default_value, dtype=np.int32)

    for row_idx in range(len(jagged_lut)):
        row = jagged_lut[row_idx]

        for col_idx in range(len(row)):
            lut[row_idx, col_idx] = jagged_lut[row_idx][col_idx]

    return lut


@Cached
def get_site_pos_to_ch_lut():
    default_value = -99

    jagged_lut = [
        [0, 1, 2, 9, 10, 11, 45],  # ME1/1
        [3, 4, 5, 12, 13, 14, 46],  # ME1/2
        [18, 19, 20, 48, 21, 22, 23, 24, 25, 26, 49],  # ME2/1 + ME2/2
        [27, 28, 29, 50, 30, 31, 32, 33, 34, 35, 51],  # ME3/1 + ME3/2
        [36, 37, 38, 52, 39, 40, 41, 42, 43, 44, 53],  # ME4/1 + ME4/2
        [57, 58, 59, 66, 67, 68, 100],  # RE1/2
        [75, 76, 77, 78, 79, 80, 103],  # RE2/2
        [81, 82, 83, 104, 84, 85, 86, 87, 88, 89, 105],  # RE3/1 + RE3/2
        [90, 91, 92, 106, 93, 94, 95, 96, 97, 98, 107],  # RE4/1 + RE4/2
        [54, 55, 56, 63, 64, 65, 99],  # GE1/1
        [72, 73, 74, 102],  # GE2/1
        [108, 109, 110, 111, 112, 113, 114]  # GE0
    ]

    lut = np.full((num_sites, 11), default_value, dtype=np.int32)

    for row_idx in range(len(jagged_lut)):
        row = jagged_lut[row_idx]

        for col_idx in range(len(row)):
            lut[row_idx, col_idx] = jagged_lut[row_idx][col_idx]

    return lut


@Cached
def get_site_to_ph_ord_lut():
    lut = np.array([
        0, 0, 2, 2, 2, 0, 0, 2, 2, 0, 1, 0
    ], dtype=np.int32)

    return lut


@Cached
def get_ph_ord_pos_to_pos_lut():
    default_value = -99

    jagged_lut = [
        [6, 0, 1, 2, 3, 4, 5],
        [3, 0, 1, 2],
        [3, 10, 0, 4, 5, 1, 6, 7, 2, 8, 9]
    ]

    lut = np.full((num_zones, 11), default_value, dtype=np.int32)

    for row_idx in range(len(jagged_lut)):
        row = jagged_lut[row_idx]

        for col_idx in range(len(row)):
            lut[row_idx, col_idx] = jagged_lut[row_idx][col_idx]

    return lut


@Cached
def get_site_to_ch_ord_lut():
    site_to_ph_ord_lut = get_site_to_ph_ord_lut()
    ph_ord_pos_to_pos_lut = get_ph_ord_pos_to_pos_lut()
    site_pos_to_ch_lut = get_site_pos_to_ch_lut()

    default_value = -99

    site_to_ch_ord_lut = np.full((num_sites, 11), default_value, dtype=np.int32)

    for site_id in range(num_sites):
        ph_ord = site_to_ph_ord_lut[site_id]

        for pos in range(11):
            ord_pos = ph_ord_pos_to_pos_lut[ph_ord, pos]

            if ord_pos > -99:
                ch = site_pos_to_ch_lut[site_id][ord_pos]
                site_to_ch_ord_lut[site_id, pos] = ch

    return site_to_ch_ord_lut


##################################################################################
# Chambers
##################################################################################

# Generate detid to chamber number lut
# Total: 115 (6*9*2 + 7)
# (subsystem, station, cscid, subsector) -> chamber
@Cached
def get_detid_to_ch_lut():
    default_value = -99

    lut = np.full((5, 5, 10, 4), default_value, dtype=np.int32)
    lut[1, 1, 1, 1] = 0  # ME1/1 sub 1
    lut[1, 1, 2, 1] = 1  # ME1/1 sub 1
    lut[1, 1, 3, 1] = 2  # ME1/1 sub 1
    lut[1, 1, 4, 1] = 3  # ME1/2 sub 1
    lut[1, 1, 5, 1] = 4  # ME1/2 sub 1
    lut[1, 1, 6, 1] = 5  # ME1/2 sub 1
    lut[1, 1, 7, 1] = 6  # ME1/3 sub 1
    lut[1, 1, 8, 1] = 7  # ME1/3 sub 1
    lut[1, 1, 9, 1] = 8  # ME1/3 sub 1
    lut[1, 1, 1, 2] = 9  # ME1/1 sub 2
    lut[1, 1, 2, 2] = 10  # ME1/1 sub 2
    lut[1, 1, 3, 2] = 11  # ME1/1 sub 2
    lut[1, 1, 4, 2] = 12  # ME1/2 sub 2
    lut[1, 1, 5, 2] = 13  # ME1/2 sub 2
    lut[1, 1, 6, 2] = 14  # ME1/2 sub 2
    lut[1, 1, 7, 2] = 15  # ME1/3 sub 2
    lut[1, 1, 8, 2] = 16  # ME1/3 sub 2
    lut[1, 1, 9, 2] = 17  # ME1/3 sub 2
    lut[1, 2, 1, 0] = 18  # ME2/1
    lut[1, 2, 2, 0] = 19  # ME2/1
    lut[1, 2, 3, 0] = 20  # ME2/1
    lut[1, 2, 4, 0] = 21  # ME2/2
    lut[1, 2, 5, 0] = 22  # ME2/2
    lut[1, 2, 6, 0] = 23  # ME2/2
    lut[1, 2, 7, 0] = 24  # ME2/2
    lut[1, 2, 8, 0] = 25  # ME2/2
    lut[1, 2, 9, 0] = 26  # ME2/2
    lut[1, 3, 1, 0] = 27  # ME3/1
    lut[1, 3, 2, 0] = 28  # ME3/1
    lut[1, 3, 3, 0] = 29  # ME3/1
    lut[1, 3, 4, 0] = 30  # ME3/2
    lut[1, 3, 5, 0] = 31  # ME3/2
    lut[1, 3, 6, 0] = 32  # ME3/2
    lut[1, 3, 7, 0] = 33  # ME3/2
    lut[1, 3, 8, 0] = 34  # ME3/2
    lut[1, 3, 9, 0] = 35  # ME3/2
    lut[1, 4, 1, 0] = 36  # ME4/1
    lut[1, 4, 2, 0] = 37  # ME4/1
    lut[1, 4, 3, 0] = 38  # ME4/1
    lut[1, 4, 4, 0] = 39  # ME4/2
    lut[1, 4, 5, 0] = 40  # ME4/2
    lut[1, 4, 6, 0] = 41  # ME4/2
    lut[1, 4, 7, 0] = 42  # ME4/2
    lut[1, 4, 8, 0] = 43  # ME4/2
    lut[1, 4, 9, 0] = 44  # ME4/2
    lut[1, 1, 3, 3] = 45  # ME1/1 neigh
    lut[1, 1, 6, 3] = 46  # ME1/2 neigh
    lut[1, 1, 9, 3] = 47  # ME1/3 neigh
    lut[1, 2, 3, 3] = 48  # ME2/1 neigh
    lut[1, 2, 9, 3] = 49  # ME2/2 neigh
    lut[1, 3, 3, 3] = 50  # ME3/1 neigh
    lut[1, 3, 9, 3] = 51  # ME3/2 neigh
    lut[1, 4, 3, 3] = 52  # ME4/1 neigh
    lut[1, 4, 9, 3] = 53  # ME4/2 neigh
    # GEM + RPC Chambers
    lut[3, 1, 1, 1] = 54  # GE1/1 sub 1
    lut[3, 1, 2, 1] = 55  # GE1/1 sub 1
    lut[3, 1, 3, 1] = 56  # GE1/1 sub 1
    lut[2, 1, 4, 1] = 57  # RE1/2 sub 1
    lut[2, 1, 5, 1] = 58  # RE1/2 sub 1
    lut[2, 1, 6, 1] = 59  # RE1/2 sub 1
    lut[2, 1, 7, 1] = 60  # RE1/3 sub 1
    lut[2, 1, 8, 1] = 61  # RE1/3 sub 1
    lut[2, 1, 9, 1] = 62  # RE1/3 sub 1
    lut[3, 1, 1, 2] = 63  # GE1/1 sub 2
    lut[3, 1, 2, 2] = 64  # GE1/1 sub 2
    lut[3, 1, 3, 2] = 65  # GE1/1 sub 2
    lut[2, 1, 4, 2] = 66  # RE1/2 sub 2
    lut[2, 1, 5, 2] = 67  # RE1/2 sub 2
    lut[2, 1, 6, 2] = 68  # RE1/2 sub 2
    lut[2, 1, 7, 2] = 69  # RE1/3 sub 2
    lut[2, 1, 8, 2] = 70  # RE1/3 sub 2
    lut[2, 1, 9, 2] = 71  # RE1/3 sub 2
    lut[3, 2, 1, 0] = 72  # GE2/1
    lut[3, 2, 2, 0] = 73  # GE2/1
    lut[3, 2, 3, 0] = 74  # GE2/1
    lut[2, 2, 4, 0] = 75  # RE2/2
    lut[2, 2, 5, 0] = 76  # RE2/2
    lut[2, 2, 6, 0] = 77  # RE2/2
    lut[2, 2, 7, 0] = 78  # RE2/2
    lut[2, 2, 8, 0] = 79  # RE2/2
    lut[2, 2, 9, 0] = 80  # RE2/2
    lut[2, 3, 1, 0] = 81  # RE3/1
    lut[2, 3, 2, 0] = 82  # RE3/1
    lut[2, 3, 3, 0] = 83  # RE3/1
    lut[2, 3, 4, 0] = 84  # RE3/2
    lut[2, 3, 5, 0] = 85  # RE3/2
    lut[2, 3, 6, 0] = 86  # RE3/2
    lut[2, 3, 7, 0] = 87  # RE3/2
    lut[2, 3, 8, 0] = 88  # RE3/2
    lut[2, 3, 9, 0] = 89  # RE3/2
    lut[2, 4, 1, 0] = 90  # RE4/1
    lut[2, 4, 2, 0] = 91  # RE4/1
    lut[2, 4, 3, 0] = 92  # RE4/1
    lut[2, 4, 4, 0] = 93  # RE4/2
    lut[2, 4, 5, 0] = 94  # RE4/2
    lut[2, 4, 6, 0] = 95  # RE4/2
    lut[2, 4, 7, 0] = 96  # RE4/2
    lut[2, 4, 8, 0] = 97  # RE4/2
    lut[2, 4, 9, 0] = 98  # RE4/2
    lut[3, 1, 3, 3] = 99  # GE1/1 neigh
    lut[2, 1, 6, 3] = 100  # RE1/2 neigh
    lut[2, 1, 9, 3] = 101  # RE1/3 neigh
    lut[3, 2, 3, 3] = 102  # GE2/1 neigh
    lut[2, 2, 9, 3] = 103  # RE2/2 neigh
    lut[2, 3, 3, 3] = 104  # RE3/1 neigh
    lut[2, 3, 9, 3] = 105  # RE3/2 neigh
    lut[2, 4, 3, 3] = 106  # RE4/1 neigh
    lut[2, 4, 9, 3] = 107  # RE4/2 neigh
    # GE0 Chambers
    lut[4, 1, 1, 1] = 108  # GE0 sub 1
    lut[4, 1, 2, 1] = 109  # GE0 sub 1
    lut[4, 1, 3, 1] = 110  # GE0 sub 1
    lut[4, 1, 1, 2] = 111  # GE0 sub 2
    lut[4, 1, 2, 2] = 112  # GE0 sub 2
    lut[4, 1, 3, 2] = 113  # GE0 sub 2
    lut[4, 1, 1, 3] = 114  # GE0 neigh
    lut[4, 1, 2, 3] = 114  # GE0 neigh
    lut[4, 1, 3, 3] = 114  # GE0 neigh

    return lut


@Cached
def get_ch_to_host_lut():
    ch_to_host_lut = np.array([
        0, 0, 0, 1, 1, 1, 2, 2, 2,  # ME1/1 sub 1, ME1/2 sub 1, ME1/3 sub 1
        0, 0, 0, 1, 1, 1, 2, 2, 2,  # ME1/1 sub 2, ME1/2 sub 2, ME1/3 sub 2
        3, 3, 3, 4, 4, 4, 4, 4, 4,  # ME2/1, ME2/2
        5, 5, 5, 6, 6, 6, 6, 6, 6,  # ME3/1, ME3/2
        7, 7, 7, 8, 8, 8, 8, 8, 8,  # ME4/1, ME4/2
        0, 1, 2, 3, 4, 5, 6, 7, 8,  # neigh
        #
        9, 9, 9, 10, 10, 10, 11, 11, 11,  # GE1/1 sub 1, RE1/2 sub 1, RE1/3 sub 1
        9, 9, 9, 10, 10, 10, 11, 11, 11,  # GE1/1 sub 2, RE1/2 sub 2, RE1/3 sub 2
        12, 12, 12, 13, 13, 13, 13, 13, 13,  # GE2/1, RE2/2
        14, 14, 14, 15, 15, 15, 15, 15, 15,  # RE3/1, RE3/2
        16, 16, 16, 17, 17, 17, 17, 17, 17,  # RE4/1, RE4/2
        9, 10, 11, 12, 13, 14, 15, 16, 17,  # neigh
        #
        18, 18, 18, 18, 18, 18, 18,  # GE0
    ], dtype=np.int32)

    return ch_to_host_lut


@Cached
def get_ch_to_site_lut():
    default_value = -99

    host_to_site_lut = get_host_to_site_lut()
    ch_to_host_lut = get_ch_to_host_lut()
    ch_count = np.size(ch_to_host_lut)

    ch_to_site_lut = np.full((ch_count,), default_value, dtype=np.int32)

    for flat_id in range(ch_count):
        ch = np.unravel_index(flat_id, shape=ch_to_host_lut.shape)
        host = multi_index_get(ch_to_host_lut, ch)
        site = host_to_site_lut[host]

        if site > -99:
            multi_index_set(site, ch_to_site_lut, (ch,))

    return ch_to_site_lut


@Cached
def get_ch_blacklist():
    blacklist = np.array([
        6, 7, 8, 15, 16, 17, 47,  # ME1/3
        60, 61, 62, 69, 70, 71, 101,  # RE1/3
    ], dtype=np.int32)

    return blacklist


##################################################################################
# Zones
##################################################################################

# Generate host, zone to theta range
# (host,zone) -> (min_theta,max_theta)
@Cached
def get_host_zone_to_theta_range_lut():
    default_value = -99

    lut = np.full((num_hosts, num_zones, 2), default_value, dtype=np.int32)
    # Zone 0
    lut[0, 0] = 4, 26  # ME1/1
    lut[3, 0] = 4, 25  # ME2/1
    lut[5, 0] = 4, 25  # ME3/1
    lut[7, 0] = 4, 25  # ME4/1
    lut[9, 0] = 17, 26  # GE1/1
    lut[12, 0] = 7, 25  # GE2/1
    lut[14, 0] = 4, 25  # RE3/1
    lut[16, 0] = 4, 25  # RE4/1
    lut[18, 0] = 4, 23  # GE0
    # Zone 1
    lut[0, 1] = 24, 53  # ME1/1
    lut[1, 1] = 46, 54  # ME1/2
    lut[3, 1] = 23, 49  # ME2/1
    lut[5, 1] = 23, 41  # ME3/1
    lut[6, 1] = 44, 54  # ME3/2
    lut[7, 1] = 23, 35  # ME4/1
    lut[8, 1] = 38, 54  # ME4/2
    lut[9, 1] = 24, 52  # GE1/1
    lut[10, 1] = 52, 56  # RE1/2
    lut[12, 1] = 23, 46  # GE2/1
    lut[14, 1] = 23, 36  # RE3/1
    lut[15, 1] = 40, 52  # RE3/2
    lut[16, 1] = 23, 31  # RE4/1
    lut[17, 1] = 35, 54  # RE4/2
    # Zone 2
    lut[1, 2] = 52, 88  # ME1/2
    lut[4, 2] = 52, 88  # ME2/2
    lut[6, 2] = 50, 88  # ME3/2
    lut[8, 2] = 50, 88  # ME4/2
    lut[10, 2] = 52, 84  # RE1/2
    lut[13, 2] = 52, 88  # RE2/2
    lut[15, 2] = 48, 84  # RE3/2
    lut[17, 2] = 52, 84  # RE4/2

    return lut


# Generate host, theta to zone
# (host, theta) -> (zone)
@Cached
def get_host_theta_to_zone_lut():
    lut = get_host_zone_to_theta_range_lut()

    def lookup(emtf_host, emtf_theta1, emtf_theta2=None):
        emtf_host = np.atleast_1d(np.asarray(emtf_host))
        emtf_theta1 = np.atleast_1d(np.asarray(emtf_theta1))

        if emtf_theta2 is not None:
            emtf_theta2 = np.atleast_1d(np.asarray(emtf_theta2))

        bounds = np.take(lut, emtf_host, axis=0)

        # Create a boolean array representing the bits in a uint8 array. Set the last 3 bits.
        # Then, pack into a uint8 array
        result = np.zeros(emtf_host.shape + (8,), dtype=np.bool_)
        result[..., -3:] = (bounds[..., 0] <= emtf_theta1[:, np.newaxis]) & (
                emtf_theta1[:, np.newaxis] <= bounds[..., 1])

        if emtf_theta2 is not None:
            result[..., -3:] |= (bounds[..., 0] <= emtf_theta2[:, np.newaxis]) & (
                    emtf_theta2[:, np.newaxis] <= bounds[..., 1])

        result = np.packbits(result)

        return np.squeeze(result)

    return lookup


##################################################################################
# Timezones
##################################################################################

# Generate host, timezone to BX range
# (host, timezone) -> (min_bx, max_bx)
@Cached
def get_host_timezone_to_bx_range_lut():
    default_value = -99

    lut = np.full((num_hosts, num_timezones, 2), default_value, dtype=np.int32)
    lut[0, 0] = -1, 0  # ME1/1
    lut[1, 0] = -1, 0  # ME1/2
    lut[2, 0] = -1, 0  # ME1/3
    lut[3, 0] = -1, 0  # ME2/1
    lut[4, 0] = -1, 0  # ME2/2
    lut[5, 0] = -1, 0  # ME3/1
    lut[6, 0] = -1, 0  # ME3/2
    lut[7, 0] = -1, 0  # ME4/1
    lut[8, 0] = -1, 0  # ME4/2
    lut[9, 0] = -1, 0  # GE1/1
    lut[10, 0] = 0, 0  # RE1/2
    lut[11, 0] = 0, 0  # RE1/3
    lut[12, 0] = -1, 0  # GE2/1
    lut[13, 0] = 0, 0  # RE2/2
    lut[14, 0] = 0, 0  # RE3/1
    lut[15, 0] = 0, 0  # RE3/2
    lut[16, 0] = 0, 0  # RE4/1
    lut[17, 0] = 0, 0  # RE4/2
    lut[18, 0] = 0, 0  # GE0
    #
    lut[:, 1] = lut[:, 0] - 1  # timezone 1 = timezone 0 - 1
    lut[:, 2] = lut[:, 1] - 1  # timezone 2 = timezone 1 - 1

    return lut


@Cached
def get_host_bx_to_timezone_lut():
    lut = get_host_timezone_to_bx_range_lut()

    def lookup(host, bx):
        host = np.atleast_1d(np.asarray(host))
        bx = np.atleast_1d(np.asarray(bx))
        bounds = np.take(lut, host, axis=0)

        # Create a boolean array representing the bits in a uint8 array. Set the last 3 bits.
        # Then, pack into a uint8 array
        result = np.zeros(host.shape + (8,), dtype=np.bool_)
        result[..., ::-3] = (bounds[..., 0] <= bx[:, np.newaxis]) & (bx[:, np.newaxis] <= bounds[..., 1])
        result = np.packbits(result)

        return np.squeeze(result)

    return lookup


##################################################################################
# Hitmaps
##################################################################################

# Generate zone, host to hm_row
# (zone, host) -> hm_row
@Cached
def get_zone_host_to_hm_row_lut():
    # From closest to furthest:
    # GE0
    # GE1/1, ME1/1, ME1/2, RE1,
    # GE2/1, RE2, ME2,
    # ME3, RE3,
    # ME4, RE4,
    default_value = -99

    lut = np.full((num_zones, num_hosts), default_value, dtype=np.int32)
    # Zone 0
    lut[0, 0] = 2  # ME1/1
    lut[0, 3] = 4  # ME2/1
    lut[0, 5] = 5  # ME3/1
    lut[0, 7] = 7  # ME4/1
    lut[0, 9] = 1  # GE1/1
    lut[0, 12] = 3  # GE2/1
    lut[0, 14] = 6  # RE3/1
    lut[0, 16] = 7  # RE4/1
    lut[0, 18] = 0  # GE0
    # Zone 1
    lut[1, 0] = 1  # ME1/1
    lut[1, 1] = 2  # ME1/2
    lut[1, 3] = 4  # ME2/1
    lut[1, 5] = 5  # ME3/1
    lut[1, 6] = 5  # ME3/2
    lut[1, 7] = 7  # ME4/1
    lut[1, 8] = 7  # ME4/2
    lut[1, 9] = 0  # GE1/1
    lut[1, 10] = 2  # RE1/2
    lut[1, 12] = 3  # GE2/1
    lut[1, 14] = 6  # RE3/1
    lut[1, 15] = 6  # RE3/2
    lut[1, 16] = 7  # RE4/1
    lut[1, 17] = 7  # RE4/2
    # Zone 2
    lut[2, 1] = 0  # ME1/2
    lut[2, 4] = 3  # ME2/2
    lut[2, 6] = 4  # ME3/2
    lut[2, 8] = 6  # ME4/2
    lut[2, 10] = 1  # RE1/2
    lut[2, 13] = 2  # RE2/2
    lut[2, 15] = 5  # RE3/2
    lut[2, 17] = 7  # RE4/2

    return lut


@Cached
def get_zone_site_to_hm_row_lut():
    default_value = -99

    host_to_site_lut = get_host_to_site_lut()
    zone_host_to_hm_row_lut = get_zone_host_to_hm_row_lut()
    key_count = np.size(zone_host_to_hm_row_lut)

    zone_site_to_hm_row_lut = np.full((num_zones, num_sites), default_value, dtype=np.int32)

    for key_id in range(key_count):
        key = np.unravel_index(key_id, shape=zone_host_to_hm_row_lut.shape)
        hm_row = multi_index_get(zone_host_to_hm_row_lut, key)

        zone = key[0]
        host = key[1]
        site = host_to_site_lut[host]

        if hm_row > -99:
            multi_index_set(hm_row, zone_site_to_hm_row_lut, (zone, site))

    return zone_site_to_hm_row_lut
