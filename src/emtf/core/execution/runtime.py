import logging
import os
from textwrap import dedent

from emtf.core.commons.caching import Cached

project_root = None
project_name = None
debug_en = False


def resource(path):
    return os.path.join(project_root, 'rsc', path)


@Cached
def get_logger():
    # Copied from https://docs.python.org/2/howto/logging.html
    # create logger
    logger = logging.getLogger('run')
    logger.setLevel(logging.DEBUG)

    # Create filter
    def sanitize_msg(record):
        if record.exc_info:
            # Format the exception and add it to the log message
            exception_type = record.exc_info[0].__name__
            exception_message = str(record.exc_info[1])
            record.msg = f"Exception: {exception_type} - {exception_message}"

        if hasattr(record, 'msg'):
            addskip = record.msg.startswith('\n')
            record.msg = dedent(record.msg).strip()

            if addskip:
                record.msg = f'\n{record.msg}'

        return True

    logger.addFilter(sanitize_msg)

    # create file handler which logs even debug messages
    fh = logging.FileHandler('run.log')
    fh.setLevel(logging.DEBUG)

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG if debug_en else logging.INFO)

    # create formatter and add it to the handlers
    formatter = logging.Formatter('%(asctime)s [%(levelname)-8s] %(message)s')
    fh.setFormatter(formatter)

    formatter = logging.Formatter('%(message)s')
    ch.setFormatter(formatter)

    # add the handlers to the logger
    if not len(logger.handlers):
        logger.addHandler(fh)
        logger.addHandler(ch)

    return logger
