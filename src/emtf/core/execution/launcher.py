import importlib
import os
import sys

if __name__ == '__main__':
    # READ LAUNCHER ENV VARIABLES
    project_home = os.environ['PROJECT_HOME']
    project_name = os.environ['PROJECT_NAME']
    project_main_pkg = os.environ['PROJECT_MAIN_PKG']

    # ADD PROJECT TO PATH
    sys.path.append(project_home + '/src')
    print(project_home)

    # ADD ROOT TO PATH
    try:
        import ROOT

        root_is_missing = False
    except ImportError:
        root_is_missing = True

    if root_is_missing:
        possible_root_locations = [
            '/usr/lib/root',
            '/usr/lib64/root',
        ]

        pyversion = sys.version
        pyversion = pyversion.split('.')
        pyversion = 'python' + '.'.join((pyversion[0], pyversion[1]))
        possible_root_locations.append('/usr/lib64/' + pyversion + '/site-packages')

        for candidate in possible_root_locations:
            if os.path.exists(os.path.join(candidate, 'ROOT')):
                sys.path.append(candidate)
                root_is_missing = False
                break

        if root_is_missing:
            raise Exception("Unable to find ROOT. Please make sure it's in the PYTHONPATH.")

    # RUNTIME
    from emtf.core.execution import runtime

    runtime.project_root = project_home
    runtime.project_name = project_name

    # RUN MAIN
    main_module = importlib.import_module(project_main_pkg + '.main')
    main_module.main()
