import ROOT as R
from ROOT import gInterpreter

from emtf.core.commons.lazy import LazyMeta
from emtf.core.execution.runtime import get_logger


class ValueIsMissing(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(ValueIsMissing, cls).__new__(cls)
        return cls.instance


VALUE_IS_MISSING = ValueIsMissing()


class BranchInfo(object):

    def __init__(self, name, enabled):
        self.name = name
        self.entry_id = -1
        self.enabled = enabled
        self.is_scalar = False
        self.value = None


class Entry(object):
    __slots__ = (
        '__dict__',
        '_source_id', '_tree',
        '_entry_id', '_entry_loaded', '_sparse_read_en',
        '_branch_aliases', '_branch_info',
        '_producers', '_producer_stack',
        '_on_tree_change_handlers', '_on_entry_change_handlers'
    )

    def __init__(self, source_id, source_tree, branch_aliases, producers):
        # Init
        self.__dict__ = dict()

        self._source_id = source_id
        self._tree = source_tree

        self._entry_id = None
        self._entry_loaded = False
        self._sparse_read_en = True
        self._branch_aliases = branch_aliases

        self._producers = producers
        self._producer_stack = set()

        # Inspect Branches
        self._branch_info = dict()

        for branch in self._tree.GetListOfBranches():
            branch_name = branch.GetName()
            branch_enabled = bool(self._tree.GetBranchStatus(branch_name))
            self._branch_info[branch_name] = BranchInfo(branch_name, branch_enabled)

        # Asynch
        self._on_tree_change_handlers = list()
        self._on_entry_change_handlers = list()

    def set_tree(self, source_id, tree):
        # Short-Circuit: Nothing to do here
        if self._source_id == source_id:
            return

        # Update tree
        self._source_id = source_id
        self._tree = tree

        # Enable branches and reset branch cache
        for branch_name, branch_info in self._branch_info.items():
            branch_info.entry_id = -1
            branch_info.value = None

            tree.SetBranchStatus(branch_name, branch_info.enabled)

        # Update entry
        self._entry_id = None
        self._entry_loaded = False

        # Clear cache
        self.__dict__.clear()

        # Notify
        for handler in self._on_tree_change_handlers:
            handler(self)

    def set_entry(self, id):
        # Short-Circuit: Nothing to do here
        if self._entry_id == id:
            return

        # Update entry
        self._entry_id = id
        self._entry_loaded = False

        # Clear cache
        self.__dict__.clear()

        # Notify
        for handler in self._on_entry_change_handlers:
            handler(self)

    def enable_branch(self, branch_name):
        # Get branch info
        branch_info = self._branch_info.get(branch_name, None)

        # Short-Circuit: Branch doesn't exist
        if branch_info is None:
            return False

        # Short-Circuit: Not needed
        if branch_info.enabled:
            return False

        # Enable branch
        branch_info.enabled = True
        self._tree.SetBranchStatus(branch_info.name, 1)

        # If entry is already loaded, load the entry data for this branch
        if self._entry_loaded:
            self._tree.GetBranch(branch_info.name).GetEntry(self._entry_id)

        # Branch was enabled
        return True

    def __getattr__(self, field):
        # Try to get from cache
        value = self.get_from_cache(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to produce value
        value = self.get_from_producer(field)

        if value is not VALUE_IS_MISSING:
            return value

        # Try to get from tree
        value = self.get_from_tree(field)

        if value is not VALUE_IS_MISSING:
            return value

        raise AttributeError(f'[Entry] Field does not have a value, producer, nor does it exists in the tree: {field}')

    def get_from_cache(self, field):
        return self.__dict__.get(field, VALUE_IS_MISSING)

    def get_from_producer(self, field):
        producer = self._producers.get(field, None)

        if producer is None:
            return VALUE_IS_MISSING

        if field in self._producer_stack:
            raise RecursionError(f'[Entry] Field has a recursive dependency on itself: {field}')

        # Produce values
        products = VALUE_IS_MISSING

        try:
            self._producer_stack.add(field)
            products = producer.extract(self)
        except Exception as err:
            get_logger().error(f'Error in producer {producer.__class__.__name__} on entry_id={self._entry_id}')
            get_logger().exception(err)
        finally:
            self._producer_stack.remove(field)

        # Short-Circuit: Producer failed
        if products is VALUE_IS_MISSING:
            return VALUE_IS_MISSING

        # Cache products
        self.__dict__.update(products)

        # Return
        return products[field]

    def get_from_tree(self, branch_name):
        # Resolve alias
        branch_name = self._branch_aliases.get(branch_name, branch_name)

        # Get branch info
        branch_info = self._branch_info.get(branch_name, None)

        # Short-Circuit: Branch doesn't exist
        if branch_info is None:
            return VALUE_IS_MISSING

        # Enable branch        
        needs_to_be_enabled = not branch_info.enabled

        if needs_to_be_enabled:
            branch_info.enabled = True
            self._tree.SetBranchStatus(branch_info.name, 1)

        # Read branch data
        if self._sparse_read_en:
            # Prepare the tree for read but don't load anything
            if not self._entry_loaded:
                self._tree.LoadTree(self._entry_id)
                self._entry_loaded = True

            # Only read the data for this branch
            if branch_info.entry_id != self._entry_id:
                branch_info.entry_id = self._entry_id
                self._tree.GetBranch(branch_info.name).GetEntry(self._entry_id)
        else:
            # Load the data for all enabled branches
            # if the entry is already loaded and the branch was just enabled,
            # load the data for this branch
            if not self._entry_loaded:
                self._tree.GetEntry(self._entry_id)
                self._entry_loaded = True
            elif needs_to_be_enabled:
                self._tree.GetBranch(branch_info.name).GetEntry(self._entry_id)

        # Get branch value
        branch_value = branch_info.value

        if branch_value is None:
            branch_datatype = self._tree.GetBranch(branch_name).GetLeaf(branch_name).GetTypeName()

            if 'vector' in branch_datatype:
                # Caching objects requires getattr
                branch_value = getattr(self._tree, branch_name)

                branch_info.is_scalar = False
                branch_info.value = branch_value
            else:
                # Caching primitives requires binding the branch address
                field_name = f't_{branch_name}'
                branch_value = getattr(R, field_name, None)

                if branch_value is None:
                    gInterpreter.Declare(f'std::array<{branch_datatype}, 1> {field_name};')
                    branch_value = getattr(R, field_name)

                self._tree.SetBranchAddress(branch_name, branch_value)
                self._tree.GetBranch(branch_info.name).GetEntry(self._entry_id)

                branch_info.is_scalar = True
                branch_info.value = branch_value

        # Short-Circuit: Scalars are bound to arrays to facilitate binding
        if branch_info.is_scalar:
            return branch_value[0]

        # Return
        return branch_value


class Producer(metaclass=LazyMeta):

    def provides(self):
        raise NotImplementedError()

    def extract(self, entry):
        raise NotImplementedError()

    def get_branches_used(self):
        return set()

    def summary(self):
        pass


class SimpleProducer(Producer, metaclass=LazyMeta):

    def __init__(self, key, func):
        self.key = key
        self.func = func

    def provides(self):
        return [self.key]

    def extract(self, entry):
        return {self.key: self.func(entry)}
