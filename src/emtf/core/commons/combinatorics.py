def yield_pairs(col):
    vsize = len(col)

    for i in range(vsize):
        for j in range(i + 1, vsize):
            yield (col[i], col[j])


def yield_combinations(col1, col2):
    for el1 in col1:
        for el2 in col2:
            yield (el1, el2,)
