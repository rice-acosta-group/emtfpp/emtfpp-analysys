def trg_or(trg1, trg2):
    if trg1 is None:
        return trg2

    if trg2 is None:
        return trg1

    def ntrigger(*arg, **kwargs):
        return trg1(*arg, **kwargs) or trg2(*arg, **kwargs)

    return ntrigger


def trg_and(trg1, trg2):
    if trg1 is None:
        return trg2

    if trg2 is None:
        return trg1

    def ntrigger(*args, **kwargs):
        return trg1(*args, **kwargs) and trg2(*args, **kwargs)

    return ntrigger


class TriggerMenu(object):

    def __init__(self):
        self.triggers = dict()
        self.trigger_states = dict()
        self.all_triggers_passed = False

    def register(self, key, trigger):
        self.triggers[key] = trigger
        self.trigger_states[key] = False

    def reset(self):
        self.all_triggers_passed = False

        for key in self.trigger_states.keys():
            self.trigger_states[key] = False

    def process(self, entry):
        # Short-Circuit: All triggers passed; stop testing them
        if self.all_triggers_passed:
            return

        # Test triggers
        all_triggers_passed = True

        for key, trigger in self.triggers.items():
            if self.trigger_states.get(key, False):
                continue

            if not trigger(entry):
                all_triggers_passed = False
                continue

            self.trigger_states[key] = True

        if all_triggers_passed:
            self.all_triggers_passed = all_triggers_passed
