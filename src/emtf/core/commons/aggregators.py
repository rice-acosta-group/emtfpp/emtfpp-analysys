# Basic
class MinAggregator(object):

    def __init__(self, init_metric, init_value):
        self.cur_value = init_value
        self.cur_metric = init_metric
        self.present = False

    def process(self, metric, value):
        if (not self.present) or metric < self.cur_metric:
            self.cur_value = value
            self.cur_metric = metric
            self.present = True
