import contextlib
import os
import tempfile
from datetime import datetime

from matplotlib import pyplot as plt

from emtf.core.execution.runtime import get_logger


class ModelTrainer(object):

    def __init__(self, model, log_dir='keras_logs', prefix='', suffix='.log'):
        self.model = model
        self.log_dir = log_dir
        self.prefix = prefix
        self.suffix = suffix

    def fit(self, *args, **kwargs):
        if not os.path.exists(self.log_dir):
            os.makedirs(self.log_dir)

        fd, name = tempfile.mkstemp(prefix=self.prefix, suffix=self.suffix, dir=self.log_dir, text=True)

        start_time = datetime.now()

        get_logger().info('Begin training ...')

        # Fit
        with os.fdopen(fd, 'w') as f:
            with contextlib.redirect_stdout(f):
                history = self.model.fit(*args, **kwargs)

        get_logger().info(f'Done training. Time elapsed: {datetime.now() - start_time}')

        # Plot history
        HistoryPlotter(history, prefix=self.prefix).plot()

        return history


class HistoryPlotter(object):

    def __init__(self, history, prefix='', metric='loss'):
        self.history = history
        self.prefix = prefix
        self.metric = metric

    def plot(self):
        train_value = self.history.history[self.metric]
        val_value = self.history.history['val_' + self.metric]
        lr_value = self.history.history['lr']
        tup = (len(self.history.epoch), len(self.history.epoch), self.metric,
               train_value[-1], 'val_' + self.metric, val_value[-1])

        get_logger().info('Epoch {}/{} - {}: {:.4f} - {}: {:.4f}'.format(*tup))

        if len(self.history.epoch) > 10:
            fig, axs = plt.subplots(1, 2, figsize=(6, 6 / 2), tight_layout=True)
            ax = axs[0]
            ax.plot(self.history.epoch, lr_value, color='C0')
            ax.set_xlabel('Epochs')
            ax.set_ylabel('Learning rate')
            ax.semilogy()
            ax.grid(True)
            ax = axs[1]
            ax.plot(self.history.epoch, train_value, label='Train')
            ax.plot(self.history.epoch, val_value, label='Validation')
            ax.set_xlabel('Epochs')
            ax.set_ylabel(self.metric.replace('_', ' ').title())
            ax.semilogy()
            ax.legend(loc='upper right')
            ax.grid(True)
            plt.savefig(self.prefix + 'learning_rate.png')
