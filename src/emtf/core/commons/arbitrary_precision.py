import math

import numpy as np


def clip_uint(int_val, bit_width):
    max = (1 << bit_width) - 1

    return np.clip(int_val, 0, max)


def clip_int(uint_val, bit_width):
    po2 = (1 << (bit_width - 1))

    return np.clip(uint_val, -po2, po2 - 1)


def wrap_uint(val, bit_width):
    po2 = (1 << bit_width)

    if val < 0:
        n_wrap = math.ceil(abs(val) / po2)

        return val + po2 * n_wrap

    n_wrap = math.floor(abs(val) / po2)

    return val - po2 * n_wrap


def convert_uint_to_int(uint_val, bit_width):
    uint_val = wrap_uint(uint_val, bit_width)

    neg_mask = 1 << (bit_width - 1)
    pos_mask = neg_mask - 1

    neg_part = uint_val & neg_mask
    pos_part = uint_val & pos_mask

    return pos_part - neg_part


def convert_int_to_uint(int_val, bit_width):
    neg_mask = 1 << (bit_width - 1)
    pos_mask = neg_mask - 1

    pos_part = int_val & pos_mask

    if int_val < 0:
        uint = neg_mask + pos_part
    else:
        uint = pos_part

    return wrap_uint(uint, bit_width)
