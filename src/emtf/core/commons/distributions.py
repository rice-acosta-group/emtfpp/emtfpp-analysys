import math

import numpy as np
from ROOT import TF1
from ROOT import TH1D
from ROOT import TMath
from scipy.special import iv

from emtf.core.commons.tools import safe_divide


# Methods
def calc_sigma_from_fwhm(fwhm):
    return fwhm / (2 * math.sqrt(2 * math.log(2)))


def find_mpv_fwhm_sigma_1d(func, xmin, xmax, step):
    x = np.arange(xmin, xmax, step)
    y = np.array(list(map(func, x)))
    max_bin = np.argmax(y)

    max_x = x[max_bin]
    max_y = y[max_bin]
    half_max = max_y / 2

    left_x = 0
    right_x = 0

    n_val = len(y)
    found_left = False
    found_right = False

    for i in range(n_val):
        yl = y[i]
        yr = y[n_val - i - 1]

        if not found_left:
            if yl <= half_max:
                left_x = i
            else:
                found_left = True

        if not found_right:
            if yr <= half_max:
                right_x = n_val - i - 1
            else:
                found_right = True

        if found_left and found_right:
            break

    fwhm = (x[right_x] - x[left_x]) / 2

    return max_x, fwhm


# Fits

def fit_right_sided_crystalball(arr, bins):
    plot = TH1D('', '', len(bins) - 1, bins)

    for x in arr:
        plot.Fill(x)

    min_x = min(bins)
    max_x = max(bins)

    fit = TF1(
        'right_sided_crystalball',
        right_sided_crystalball,
        min_x, max_x, 5
    )
    fit.SetParLimits(2, 0, max_x)
    fit.SetParLimits(3, 0, max_x)
    fit.SetParameters(plot.GetMean(), 1, plot.GetMean(), plot.GetStdDev(), plot.GetMaximum())
    res = plot.Fit(fit, 'S Q WL')

    if int(res) > -1:
        mean = res.Parameter(2)
        stddev = res.Parameter(3)

        return mean, stddev
    else:
        return None, None


def fit_landau(arr, bins, step=1e-6):
    plot = TH1D('', '', len(bins) - 1, bins)

    for x in arr:
        plot.Fill(x)

    min_x = min(bins)
    max_x = max(bins)

    bin_with_max = plot.GetMaximumBin()
    mpv_x = plot.GetXaxis().GetBinCenter(bin_with_max)

    fit = TF1(
        'landau', landau_distribution,
        min_x, max_x, 3
    )
    fit.SetParLimits(0, max_x)
    fit.SetParameters(mpv_x, 1, plot.GetMaximum())
    res = plot.Fit(fit, 'S Q WL')

    if int(res) > -1:
        mpv = res.Parameter(0)
        scale = res.Parameter(1)
        norm = res.Parameter(2)

        mpv, fwhm = find_mpv_fwhm_sigma_1d(
            lambda x: landau_distribution([x], [mpv, scale, norm]),
            min_x, max_x, step
        )

        return mpv, fwhm
    else:
        return None, None


def fit_gaus(arr, bins):
    plot = TH1D('', '', len(bins) - 1, bins)

    for x in arr:
        plot.Fill(x)

    res = plot.Fit('gaus', 'S Q')

    if int(res) > -1:
        mean = res.Parameter(1)
        sigma = res.Parameter(2)

        return mean, sigma
    else:
        return None, None


# PDF
def landau_distribution(x, par):
    mpv = par[0]
    shape = par[1]
    N = par[2]

    return N * TMath.Landau(x[0], mpv, shape, 1)


def gaus_distribution(x, par):
    mean = par[0]
    sigma = par[1]
    N = par[2]

    t = safe_divide(x[0] - mean, sigma)

    return N * math.exp(-0.5 * t * t)


def rice_distribution(x, par):
    nu = par[0]
    sigma = par[1]
    N = par[2]

    arg0 = x[0] / sigma / sigma
    arg1 = (x[0] * x[0] + nu * nu) / sigma / sigma
    arg2 = nu * arg0

    return N * arg0 * math.exp(-0.5 * arg1) * iv(0, arg2)


def right_sided_crystalball(x, par):
    alpha_h = par[0]
    n_h = par[1]
    mean = par[2]
    sigma = par[3]

    N = par[4]
    t = safe_divide(x[0] - mean, sigma)

    fact1THihgerAlphaH = safe_divide(alpha_h, n_h)
    fact2THigherAlphaH = safe_divide(n_h, alpha_h) - alpha_h + t

    result = 0

    if t <= alpha_h:
        result = math.exp(-0.5 * t * t)
    elif t > alpha_h:
        result = math.exp(-0.5 * alpha_h * alpha_h) * pow(fact1THihgerAlphaH * fact2THigherAlphaH, -n_h)

    return N * result


def double_sided_crystalball(x, par):
    alpha_l = par[0]
    alpha_h = par[1]
    n_l = par[2]
    n_h = par[3]
    mean = par[4]
    sigma = par[5]

    N = par[6]
    t = safe_divide(x[0] - mean, sigma)

    fact1TLessMinosAlphaL = safe_divide(alpha_l, n_l)
    fact2TLessMinosAlphaL = safe_divide(n_l, alpha_l) - alpha_l - t
    fact1THihgerAlphaH = safe_divide(alpha_h, n_h)
    fact2THigherAlphaH = safe_divide(n_h, alpha_h) - alpha_h + t

    result = 0

    if -alpha_l <= t <= alpha_h:
        result = math.exp(-0.5 * t * t)
    elif t < -alpha_l:
        result = math.exp(-0.5 * alpha_l * alpha_l) * pow(fact1TLessMinosAlphaL * fact2TLessMinosAlphaL, -n_l)
    elif t > alpha_h:
        result = math.exp(-0.5 * alpha_h * alpha_h) * pow(fact1THihgerAlphaH * fact2THigherAlphaH, -n_h)

    return N * result
