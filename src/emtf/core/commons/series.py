def power_series(x, params):
    order = len(params) - 1

    y = 0
    slope = 0

    for power in range(order + 1):
        param = params[order - power]
        y += param * pow(x, power)

        if power > 0:
            slope += power * param * pow(x, power - 1)

    return y, slope
