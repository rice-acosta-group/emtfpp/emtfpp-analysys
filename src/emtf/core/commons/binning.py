import numpy as np


# Bin edges
def uniform_axis(nbins, low, up):
    nedges = int(nbins + 1)
    return np.linspace(low, up, nedges, dtype=np.float_)


def uniform_step_axis(step, low, up, align='left'):
    if align == 'center':
        first_low = low - step * 0.5
        last_up = up + step * 1.5
    elif align == 'left':
        first_low = low
        last_up = up + step
    else:
        raise ValueError("'align' can only be 'center' or 'left'")

    return np.arange(first_low, last_up, step, dtype=np.float_)


def inverted_uniform_step_axis(step, begin, end, align='left', add_negatives=False):
    edges = uniform_step_axis(step, begin, end, align=align)

    # Invert
    edges = 1 / edges

    # Sort Edges and Remove Zero
    edges = np.sort(edges[edges != 0])

    # Combine
    if add_negatives:
        edges = np.asarray([*(edges[::-1] * -1.), *edges[:]])

    return edges


# Digitize
def digitize_inclusive(x, edges):
    bin_edges = np.asarray(edges)
    n_bin_edges = bin_edges.size

    if n_bin_edges < 2:
        raise ValueError('`bin_edges` must have size >= 2')

    if bin_edges.ndim != 1:
        raise ValueError('`bin_edges` must be 1d')

    if np.any(bin_edges[:-1] > bin_edges[1:]):
        raise ValueError('`bin_edges` must increase monotonically')

    x = np.asarray(x)
    x = x.ravel()

    # bin_index has range 0..len(bins)
    bin_index = bin_edges.searchsorted(x, side='right')

    # modified bin_index has range 0..len(bins)-2
    bin_index[bin_index == n_bin_edges] -= 1
    bin_index[bin_index != 0] -= 1
    bin_index = np.squeeze(bin_index)

    return int(bin_index)


# Test
if __name__ == '__main__':
    print(uniform_axis(10, 0, 10))
    print(uniform_step_axis(1, -1, 1, align='center'))
    print(uniform_step_axis(1, 0, 10, align='center'))
    print(uniform_step_axis(1, 0, 10, align='left'))
