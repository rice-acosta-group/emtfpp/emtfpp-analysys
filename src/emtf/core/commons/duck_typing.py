# Please read up on Duck-typing to understand why this is here

class FieldNotDefined(object):
    def __new__(cls):
        if not hasattr(cls, 'instance'):
            cls.instance = super(FieldNotDefined, cls).__new__(cls)
        return cls.instance


FIELD_NOT_DEFINED = FieldNotDefined()


class Duck(object):
    __slots__ = ('__dict__',)

    def __init__(self, init_dict=None):
        self.__dict__ = dict()

        if init_dict is not None:
            self.__dict__.update(init_dict)

    def __getattr__(self, field):
        value = self.__dict__.get(field, FIELD_NOT_DEFINED)

        if value is not FIELD_NOT_DEFINED:
            return value

        raise AttributeError('[Duck] Field is not defined: %s' % field)
