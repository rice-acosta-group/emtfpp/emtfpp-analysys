from ctypes import c_double

import numpy as np
from ROOT import TH1
from ROOT import TH2

from emtf.core.commons.measurements import Measurement


def get_th1_user_ranges(*hist_col, logy=False):
    xmin, xmax, ymin, ymax = None, None, None, None

    for hist in hist_col:
        h_xmin = hist.GetBinLowEdge(1)
        h_xmax = hist.GetBinLowEdge(hist.GetNbinsX())
        h_wxmax = hist.GetBinWidth(hist.GetNbinsX())
        h_xmax += h_wxmax

        if logy:
            h_ymin = max(1, hist.GetMinimum())
            h_ymax = max(1, hist.GetMaximum())

            h_ymin = np.power(10, np.floor(np.log10(h_ymin)) - 1)
            h_ymax = np.power(10, np.ceil(np.log10(h_ymax)) + 1)
        else:
            h_ymin = hist.GetMinimum() * 0.9
            h_ymax = hist.GetMaximum() * 1.1

        xmin = h_xmin if (xmin is None) else min(xmin, h_xmin)
        xmax = h_xmax if (xmax is None) else max(xmax, h_xmax)
        ymin = h_ymin if (ymin is None) else min(ymin, h_ymin)
        ymax = h_ymax if (ymax is None) else max(ymax, h_ymax)

    return xmin, xmax, ymin, ymax


def get_th1_integral(hist):
    integralerr = c_double(0.)
    integral = hist.IntegralAndError(0, hist.GetNbinsX(), integralerr)
    return Measurement(integral, integralerr.value)


# add the content of the underflow and overflow bins to the end bins
def add_flows(hist, overflow=True, underflow=True):
    if overflow:
        add_overflows(hist)

    if underflow:
        add_underflows(hist)


def add_underflows(hist, debug=False):
    assert (isinstance(hist, TH1) or isinstance(hist, TH2)), "Error, incompatible type!"

    xbins = hist.GetNbinsX()

    if isinstance(hist, TH2):
        ybins = hist.GetNbinsY()

        for ixbin in range(0, xbins + 2):
            combine_bins(hist, xbin_src=ixbin, ybin_src=0, ybin_dst=1)

        for iybin in range(0, ybins + 2):
            combine_bins(hist, ybin_src=iybin, xbin_src=0, xbin_dst=1)
    else:
        combine_bins(hist, 0, 1)


def add_overflows(target_hist, debug=False):
    assert (isinstance(target_hist, TH1) or isinstance(target_hist, TH2)), "Error, incompatible type!"

    xbins = target_hist.GetNbinsX()

    if isinstance(target_hist, TH2):
        ybins = target_hist.GetNbinsY()

        for ixbin in range(0, xbins + 2):
            combine_bins(target_hist, xbin_src=ixbin, ybin_src=ybins + 1, ybin_dst=ybins)

        for iybin in range(0, ybins + 2):
            combine_bins(target_hist, ybin_src=iybin, xbin_src=xbins + 1, xbin_dst=xbins)
    else:
        combine_bins(target_hist, xbins + 1, xbins)


def combine_bins(hist, xbin_src, xbin_dst=None, ybin_src=None, ybin_dst=None, debug=False):
    """
    Combines the content of bin1 into bin2 for a histogram H, then
    clears the entries in bin1, and recalculates statistics (nentries, etc)

    Also works with 2d histograms, but arguments must be set correctly
    """
    assert (isinstance(hist, TH1) or isinstance(hist, TH2)), "Error, incompatible type!"

    if debug:
        print("=== Debugging combine bins ===")
        print(hist, xbin_src, xbin_dst, ybin_src, ybin_dst)

    if isinstance(hist, TH2):
        combinex = (xbin_src is not None and xbin_dst is not None and ybin_dst is None)
        combiney = (ybin_src is not None and ybin_dst is not None and xbin_dst is None)

        if debug:
            print("combinex?", combinex)
            print("combiney?", combiney)

        assert (combinex or combiney), "Incorrect arguments for 2D bin combiner!"

        if combiney:
            b1 = Measurement(hist.GetBinContent(xbin_src, ybin_src), hist.GetBinError(xbin_src, ybin_src))
            b2 = Measurement(hist.GetBinContent(xbin_src, ybin_dst), hist.GetBinError(xbin_src, ybin_dst))
            b3 = b1.add(b2)

            if debug:
                print("\tCombine Target Bin: {} - (Content, Error): ({},{})".format(ybin_dst, b2.value, b2.error))
                print("\tClear   Target Bin: {} - (Content, Error): ({},{})".format(ybin_src, b1.value, b1.error))
                print("\tAdded (Content, Error): ({},{})".format(b3.value, b3.error))
                print("\tStarting Entries: {}".format(hist.GetEntries()))

            # set values
            hist.SetBinContent(xbin_src, ybin_dst, b3.value)
            hist.SetBinError(xbin_src, ybin_dst, b3.value)

            # clear old bin
            hist.SetBinContent(xbin_src, ybin_src, 0.)
            hist.SetBinError(xbin_src, ybin_src, 0.)
        else:  # combinex
            b1 = Measurement(hist.GetBinContent(xbin_src, ybin_src), hist.GetBinError(xbin_src, ybin_src))
            b2 = Measurement(hist.GetBinContent(xbin_dst, ybin_src), hist.GetBinError(xbin_dst, ybin_src))
            b3 = b1.add(b2)

            if debug:
                print("\tCombine Target Bin: {} - (Content, Error): ({},{})".format(xbin_dst, b2.value, b2.error))
                print("\tClear   Target Bin: {} - (Content, Error): ({},{})".format(xbin_src, b1.value, b1.error))
                print("\tAdded (Content, Error): ({},{})".format(b3.value, b3.error))
                print("\tStarting Entries: {}".format(hist.GetEntries()))

            # set values
            hist.SetBinContent(xbin_dst, ybin_src, b3.value)
            hist.SetBinError(xbin_dst, ybin_src, b3.error)

            # clear old bin
            hist.SetBinContent(xbin_src, ybin_src, 0.)
            hist.SetBinError(xbin_src, ybin_src, 0.)
    else:
        b1 = Measurement(hist.GetBinContent(xbin_src), hist.GetBinError(xbin_src))
        b2 = Measurement(hist.GetBinContent(xbin_dst), hist.GetBinError(xbin_dst))
        b3 = b1.add(b2)

        if debug:
            print("\tLast  Bin: {} - (Content, Error): ({},{})".format(xbin_dst, b2.value, b2.error))
            print("\tOFlow Bin: {} - (Content, Error): ({},{})".format(xbin_src, b1.value, b1.error))
            print("\tAdded (Content, Error): ({},{})".format(b3.value, b3.error))
            print("\tStarting Entries: {}".format(hist.GetEntries()))

        # set values
        hist.SetBinContent(xbin_dst, b3.value)
        hist.SetBinError(xbin_dst, b3.error)

        # clear old bin
        hist.SetBinContent(xbin_src, 0.)
        hist.SetBinError(xbin_src, 0.)

    # H.ResetStats()
    # reduce entries appropriately (called set content twice)
    hist.SetEntries(hist.GetEntries() - 2)

    if debug:
        print("\tEnding Entries: {}".format(hist.GetEntries()))
