from emtf.core.commons.measurements import Count


class ConfusionMatrix(object):

    def __init__(self, require_pred=True):
        # Parameters
        self.require_pred = require_pred

        # Counters
        self.n_truth = 0
        self.n_truth_pass = 0
        self.n_truth_fail = 0
        self.tp_events = 0
        self.tn_events = 0
        self.fp_events = 0
        self.fn_events = 0

        # Buffers
        self.truth_present = False
        self.truth_pass = False
        self.pred_present = False
        self.pred_pass = False

    def add(self, other):
        self.n_truth += other.n_truth
        self.n_truth_pass += other.n_truth_pass
        self.n_truth_fail += other.n_truth_fail
        self.tp_events += other.tp_events
        self.tn_events += other.tn_events
        self.fp_events += other.fp_events
        self.fn_events += other.fn_events

    def on_truth(self, truth_pass):
        self.truth_present = True
        self.truth_pass = truth_pass
        self.pred_present = False
        self.pred_pass = False

    def on_prediction(self, pred_pass):
        # Short-Circuit: truth isn't present
        if not self.truth_present:
            return

        # Record track
        self.pred_present = True
        self.pred_pass = (self.pred_pass or pred_pass)

    def calc_tpr(self):
        p_events = Count(self.n_truth_pass)
        tp_events = Count(self.tp_events)
        return tp_events.divideb(p_events)

    def calc_fpr(self):
        n_events = Count(self.n_truth_fail)
        fp_events = Count(self.fp_events)
        return fp_events.divideb(n_events)

    def calc_accuracy(self):
        tp_events = Count(self.tp_events)
        tn_events = Count(self.tn_events)
        n_truth = Count(self.n_truth)
        return tp_events.add(tn_events).divideb(n_truth)

    def fill(self):
        # Init temp
        truth_present = self.truth_present
        truth_pass = self.truth_pass
        pred_present = self.pred_present
        pred_pass = self.pred_pass

        # Reset
        self.truth_present = False
        self.truth_pass = False
        self.pred_present = False
        self.pred_pass = False

        # Short-Circuit: truth isn't present
        if not truth_present:
            return

        # Short-Circuit: Ignore if prediction isn't required
        # and there wasn't any prediction
        if (not self.require_pred) and (not pred_present):
            return

        # Increase truth counts
        self.n_truth += 1

        if truth_pass:
            self.n_truth_pass += 1
        else:
            self.n_truth_fail += 1

        # No prediction, means not passed
        if not pred_present:
            pred_pass = False

        # Increase counters
        if pred_pass:
            if truth_pass:
                self.tp_events += 1
            else:
                self.fp_events += 1
        else:
            if truth_pass:
                self.fn_events += 1
            else:
                self.tn_events += 1
