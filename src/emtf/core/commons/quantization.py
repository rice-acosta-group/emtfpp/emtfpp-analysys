import math


def quantize_ceil(value, lsb):
    return math.ceil(value / lsb) * lsb


def quantize_floor(value, lsb):
    return math.floor(value / lsb) * lsb


def quantize_round(value, lsb):
    return math.round(value / lsb) * lsb
