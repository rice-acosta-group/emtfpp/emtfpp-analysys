import csv


def load_trigger_rates(file_path):
    rates = dict()

    with open(file_path) as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')

        is_header = True

        for row in reader:
            # Short-Circuit: Skip header
            if is_header:
                is_header = False
                continue

            # Read
            trigger = row[0].strip()
            rate = float(row[1])
            error = float(row[2])

            rates[trigger] = {'rate': rate, 'error': error}

    return rates
