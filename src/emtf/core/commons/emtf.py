import numpy as np

from emtf.core.commons.kinematics import wrap_phi_deg


def get_trigger_endsec(endcap, sector):
    # endsec is 0-5 in positive endcap, 6-11 in negative endcap
    assert (endcap == 1 or endcap == -1)
    assert (1 <= sector <= 6)

    result = (sector - 1) if endcap == 1 else (sector - 1 + 6)

    return result


def get_trigger_sector(ring, station, chamber):
    result = np.uint32(0)

    if station > 1 and ring > 1:
        # ch 3-8->1, 9-14->2, ... 1,2 -> 6
        result = ((np.uint32(chamber - 3) & 0x7f) // 6) + 1
    elif station == 1:
        # ch 3-8->1, 9-14->2, ... 1,2 -> 6
        result = ((np.uint32(chamber - 3) & 0x7f) // 6) + 1
    else:
        # ch 2-4->1, 5-7->2, ...
        result = ((np.uint32(chamber - 2) & 0x1f) // 3) + 1

    # max sector is 6, some calculations give a value greater than 6 but this is expected.
    result = np.clip(result, 1, 6)

    return result


def get_trigger_subsector(ring, station, chamber):
    # csc_tp_subsector = (tp_station != 1) ? 0 : ((csc_tp_chamber % 6 > 2) ? 1 : 2);
    result = np.uint32(0)

    if station == 1:
        if np.uint32(chamber) % 6 > 2:
            result = result + 1
        else:
            result = result + 2

    return result


def get_trigger_cscid(ring, station, chamber):
    result = np.uint32(0)

    if station == 1:
        result = np.uint32(chamber) % 3 + 1  # 1,2,3
        if ring == 2:
            result = result + 3
        elif ring == 3:
            result = result + 6
    else:
        if ring == 1:
            result = np.uint32(chamber + 1) % 3 + 1  # 1,2,3
        else:
            result = np.uint32(chamber + 3) % 6 + 4  # 4,5,6,7,8,9

    return result


def get_trigger_cscfr(ring, station, chamber):
    result = np.uint32(0)
    is_overlapping = not (station == 1 and ring == 3)
    is_even = (chamber % 2 == 0)

    if is_overlapping:
        if station < 3:
            result = result + is_even
        else:
            result = result + (not is_even)

    return result


def get_trigger_neighid(ring, station, chamber):
    # neighid is 0 for native chamber, 1 for neighbor chamber
    result = np.uint32(0)

    if station == 1:
        if np.uint32(chamber + 3) % 6 + 1 == 6:
            result = result + 1
    else:
        if ring == 1:
            if np.uint32(chamber + 1) % 3 + 1 == 3:
                result = result + 1
        else:
            if np.uint32(chamber + 3) % 6 + 1 == 6:
                result = result + 1

    return result


def get_trigger_cscneighid(station, subsector, cscid, neighid):
    if neighid == 0:
        return (((subsector - 1) if (station == 1) else 0) * 9) + (cscid - 1);
    else:
        return (((cscid - 1) // 3) + 18) if (station == 1) else (((cscid - 1) >= 3) + 9);


# Theta
def calc_theta_deg_from_int(theta_int):
    theta = float(theta_int) * (45.0 - 8.5) / 128. + 8.5

    return theta


def calc_theta_rad_from_int(theta_int):
    theta = np.deg2rad(calc_theta_deg_from_int(theta_int))

    return theta


def calc_theta_int(theta, endcap):
    # theta in deg [0..180], endcap [-1, +1]
    theta = 180. - theta if (endcap == -1) else theta
    theta = (theta - 8.5) * 128. / (45.0 - 8.5)
    theta_int = int(round(theta))
    theta_int = 1 if (theta_int <= 0) else theta_int  # protect against invalid value

    return theta_int


# Phi
def calc_phi_glob_deg_from_loc(loc, sector):
    # loc in deg, sector [1..6]
    glob = loc + 15. + (60. * (sector - 1))

    if glob >= 180.:
        glob -= 360.

    return glob


def calc_phi_glob_rad_from_loc(loc, sector):
    # loc in rad, sector [1..6]
    glob = np.deg2rad(calc_phi_glob_deg_from_loc(np.rad2deg(loc), sector))

    return glob


def calc_phi_loc_deg_from_int(phi_int):
    loc = float(phi_int) / 60. - 22.

    return loc


def calc_phi_loc_rad_from_int(phi_int):
    loc = np.deg2rad(calc_phi_loc_deg_from_int(phi_int))

    return loc


def calc_phi_loc_deg_from_glob(glob, sector):
    # glob in deg [-180..180], sector [1..6]
    glob = wrap_phi_deg(glob)
    loc = glob - 15. - (60. * (sector - 1))

    return loc


def calc_phi_int(glob, sector):
    # glob in deg [-180..180], sector [1..6]
    loc = calc_phi_loc_deg_from_glob(glob, sector)

    if (loc + 22.) < 0.:
        loc += 360.

    loc = (loc + 22.) * 60.
    phi_int = int(round(loc))

    return phi_int
