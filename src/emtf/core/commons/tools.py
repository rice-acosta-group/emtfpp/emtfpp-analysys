import numpy as np
from numba import njit


# Columnar
@njit
def nz_sign(val):
    result = np.sign(val)
    return result + (result == 0)


@njit
def safe_divide(num, denom, zerodiv_result=0):
    # Sanitize
    num, denom = np.asarray(num), np.asarray(denom)

    # Handle scalar denominator
    if denom.ndim == 0:
        if denom == 0:
            return np.ones_like(num) * zerodiv_result

        return num / denom

    # Assert same shape
    if num.ndim > 0 and num.shape != denom.shape:
        raise ValueError(f'Inconsistent shapes: x.shape={num.shape} y.shape={denom.shape}')

    # Zerodiv mask
    zerodiv_mask = (denom == 0)

    # Set numerators to zerodiv if denominator is 0
    num = num - (num + zerodiv_result) * zerodiv_mask

    # Set denominators to 1 if denominator is 0
    denom = denom + 1.0 * zerodiv_mask

    # Return
    return num / denom
