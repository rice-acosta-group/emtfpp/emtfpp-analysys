def reduce_pairs(entries, value_fn=lambda e: e, **kwargs):
    # Select best pairs
    taken_k1 = set()
    taken_k2 = set()

    reduced_entries = list()
    sorted_entries = sorted(entries, key=value_fn, **kwargs)

    for pair in sorted_entries:
        k1, k2 = pair[:2]

        k1_taken = (k1 in taken_k1)
        k2_taken = (k2 in taken_k2)

        if k1_taken or k2_taken:
            continue

        taken_k1.add(k1)
        taken_k2.add(k2)
        reduced_entries.append(pair)

    return reduced_entries
