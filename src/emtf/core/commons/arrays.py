# Nested arrays
def multi_index_get(arr, idx_arr):
    def _get_next(_arr, _idx_arr, i):
        if len(_idx_arr) == i:
            return _arr

        _idx = _idx_arr[i]

        return _get_next(_arr[_idx], _idx_arr, i + 1)

    return _get_next(arr, idx_arr, 0)


def multi_index_set(val, arr, idx_arr):
    def _set_next(_arr, _idx_arr, i):
        _idx = _idx_arr[i]

        if len(_idx_arr) == (i + 1):
            _arr[_idx] = val
        else:
            _set_next(_arr[_idx], _idx_arr, i + 1)

    return _set_next(arr, idx_arr, 0)
