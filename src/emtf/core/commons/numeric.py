def derivative(func, x, step):
    f1 = func(x)
    f2 = func(x + step)
    return (f2 - f1) / step


def find_root(func, xi, tol=1e-3, dx=1e-6, min_deriv=1e-12, max_steps=1000):
    for i_step in range(max_steps):
        fi = func(xi)
        dfdx_i = derivative(func, xi, dx)

        # Short-Circuit: If derivative is 0 found min
        if abs(dfdx_i) < min_deriv:
            break

        # Short-Circuit: If close to solution stop
        if abs(fi) < tol:
            break

        # Calculate next xi
        xi = xi - fi / dfdx_i

    return xi
