import array

import numpy as np
from ROOT import TCanvas
from ROOT import TH1D
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlue

from emtf.core.commons.binning import uniform_step_axis


def plot_var_walls(name, data, weights=None):
    n_entries = data.shape[0]
    n_vars = data.shape[1]

    # Short-Circuit: No entries
    if n_entries == 0:
        return

    # Short-Circuit: No variables
    if n_vars == 0:
        return

    # Get Weights
    if weights is None:
        weights = np.ones((n_entries,))

    nz_weights_mask = (weights != 0)
    weights = weights[nz_weights_mask]
    data = data[nz_weights_mask, :]

    # Short-Circuit: No entries
    n_entries = data.shape[0]

    if n_entries == 0:
        return

    # Build Histograms
    hist_col = list()

    for var_id in range(n_vars):
        # Get values
        var_col = data[:, var_id]

        # Find x range
        min_var = min(var_col)
        max_var = max(var_col)

        if max_var == min_var:
            if max_var == 0:
                min_var = -1
                max_var = 1
            else:
                min_var *= 0.9
                max_var *= 1.1

        # Build one hist per value
        bin_width = (max_var - min_var) / 100.
        bin_edges = uniform_step_axis(bin_width, min_var, max_var, align='center')
        hist = TH1D('', '', len(bin_edges) - 1, bin_edges)
        hist.FillN(
            len(var_col),
            array.array('d', var_col),
            array.array('d', weights)
        )

        # Normalize
        norm = hist.GetSumOfWeights()

        if norm > 0:
            hist.Scale(1 / hist.GetSumOfWeights())

        # Style Hist
        hist.SetLineColor(kBlue)
        hist.SetFillColorAlpha(kBlue, 0.5)
        hist.SetMaximum(1.1)
        hist.SetMinimum(1e-5)

        # Append
        hist_col.append(hist)

    # Plot
    wall_id = 0

    initial_hist_col = hist_col[:]

    while len(initial_hist_col) > 0:
        max_index = min(len(initial_hist_col), 30)
        reduced_hist_col = initial_hist_col[:max_index]
        initial_hist_col = initial_hist_col[max_index:]

        plot_var_wall('%s_%d' % (name, wall_id), reduced_hist_col)

        wall_id += 1


def plot_var_wall(name, hist_col):
    # Build Canvas
    # Split canvas in a 6 by 5 grid
    gStyle.SetOptStat(0)

    canvas = TCanvas(name, '')
    canvas.Divide(6, 5)

    # Allocate histograms
    for var_id in range(len(hist_col)):
        var_hist = hist_col[var_id]

        canvas.cd(var_id + 1)
        gPad.SetLogy(1)

        var_hist.Draw('HIST')
        var_hist.Draw('SAME AXIG')

    # Save
    canvas.SaveAs(name + '.png')
