import numpy as np

from emtf.core.commons.binning import uniform_step_axis, inverted_uniform_step_axis

# Efficiency
emtf_rels_thresholds = [0, 0.99, 1]
emtf_qual_thresholds = [12, 13, 14, 15]
emtf_pt_thresholds = [6, 10, 20, 22, 30, 40]
emtf_dxy_thresholds = [0, 8, 16, 24, 32]

pt_eff_bins = np.asarray([
    0., 2., 4., 6., 8.,
    10., 12., 14., 16., 18.,
    20., 22., 24., 26., 28.,
    30., 32., 34., 36., 38.,
    40., 44., 48.,
    50., 60., 80.,
    100.,
])
udxy_eff_bins = np.asarray([
    0., 4., 8., 12., 16., 20., 30., 40., 50., 60., 70., 80., 90., 100., 110., 120.
])
uz0_eff_bins = np.asarray([
    0., 4., 8., 12., 16., 20., 30., 40., 50., 60., 70., 80., 90., 100., 110., 120.
])
lxy_eff_bins = np.asarray([
    0., 4., 8., 12., 16., 20., 30., 40., 50., 60., 70., 80., 90.,
    100., 110., 120., 150., 200., 250., 300.
])
lz_eff_bins = np.asarray([
    0., 10., 20., 30., 40., 50., 60., 70., 80., 90., 100., 110., 120.,
    150., 200., 250., 300., 350., 400., 450., 500.
])
lxyz_eff_bins = np.asarray([
    0., 10., 20., 30., 40., 50., 60., 70., 80., 90., 100., 110., 120.,
    150., 200., 250., 300., 350., 400., 450., 500.
])
eta_eff_bins = np.arange(1.0, 2.6, 0.05)
phi_deg_eff_bins = uniform_step_axis(5.0, -180., 180.)

# Emulator Validation
emu_val_min_threshold = 1e-5
emu_val_mismatch_threshold = 1e-5

emu_val_q_bins = uniform_step_axis(1, -1, 1)
emu_val_qpt_bins = uniform_step_axis(1, -140, 140)
emu_val_qpt_err_bins = uniform_step_axis(2, -140, 140)
emu_val_qinvpt_bins = inverted_uniform_step_axis(1, 2, 140, add_negatives=True)
emu_val_qinvpt_err_bins = inverted_uniform_step_axis(2, 2, 140, add_negatives=True)

emu_val_pt_bins = uniform_step_axis(1, 0, 140)
emu_val_pt_err_bins = uniform_step_axis(2, -140, 140)
emu_val_pt_rerr_bins = uniform_step_axis(0.05, -1, 2)
emu_val_pt_zoom_bins = uniform_step_axis(1, 0, 20)
emu_val_pt_err_zoom_bins = uniform_step_axis(1, -20, 20)
emu_val_invpt_bins = inverted_uniform_step_axis(1, 2, 140, add_negatives=False)
emu_val_invpt_err_bins = inverted_uniform_step_axis(2, 2, 140, add_negatives=False)
emu_val_rels_bins = uniform_step_axis(0.01, 0, 1)

emu_val_trk_pattern_bins = uniform_step_axis(1, 0, 6)
emu_val_trk_qual_p1_bins = uniform_step_axis(1, 0, 15)
emu_val_trk_qual_p2_bins = uniform_step_axis(1, 0, 15)
emu_val_trk_mode_v1_bins = uniform_step_axis(1, 0, 15)
emu_val_trk_mode_v2_bins = uniform_step_axis(1, 0, 12)
emu_val_trk_site_bins = uniform_step_axis(1, 0, 11)
emu_val_trk_hit_count_bins = uniform_step_axis(1, 1, 12)

emu_val_d0_sign_bins = uniform_step_axis(1, -1, 1, align='center')
emu_val_dxy_sign_bins = uniform_step_axis(1, -1, 1, align='center')
emu_val_d0_bins = uniform_step_axis(10, -140, 140)
emu_val_d0_err_bins = uniform_step_axis(10, -140, 140)
emu_val_dxy_bins = uniform_step_axis(10, -140, 140)
emu_val_dxy_err_bins = uniform_step_axis(10, -140, 140)
emu_val_udxy_bins = uniform_step_axis(10, 0, 140)
emu_val_udxy_err_bins = uniform_step_axis(10, -140, 140)
emu_val_z0_bins = uniform_step_axis(10, -500, 500)
emu_val_uz0_bins = uniform_step_axis(10, 0, 500)

emu_val_lxy_bins = uniform_step_axis(10, 0, 300)
emu_val_lz_bins = uniform_step_axis(10, 0, 500)
emu_val_phi_deg_bins = uniform_step_axis(1, -180, 180)
emu_val_phi_deg_err_bins = uniform_step_axis(2, -180, 180)
emu_val_full_eta_bins = uniform_step_axis(0.1, 0., 5.)
emu_val_eta_bins = uniform_step_axis(0.1, 0.5, 3.30)
emu_val_eta_err_bins = uniform_step_axis(0.2, -2.40, 2.40)
