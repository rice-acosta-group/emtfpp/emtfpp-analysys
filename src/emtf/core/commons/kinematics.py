import math

import numpy as np
from ROOT import TVector3
from numba import njit
from scipy.optimize import brenth

from emtf.core.commons.tools import safe_divide, nz_sign

kDT, kCSC, kRPC, kGEM, kGE0 = 0, 1, 2, 3, 4


@njit
def wrap_phi_rad(x):
    # returns phi in [-pi,pi] rad
    pi = np.pi
    twopi = 2. * pi

    while x >= pi:
        x -= twopi

    while x < -pi:
        x += twopi

    return x


@njit
def wrap_phi_deg(x):
    # returns phi in [-180.,180] deg
    pi = 180.
    twopi = 2. * pi

    while x >= pi:
        x -= twopi

    while x < -pi:
        x += twopi

    return x


@njit
def calc_eta_from_theta_rad(theta_rad):
    arg = np.tan(theta_rad / 2.)
    zeroarg_mask = (arg == 0)

    return np.where(zeroarg_mask, np.inf * zeroarg_mask, -np.log(arg + zeroarg_mask))


@njit
def calc_eta_from_theta_deg(theta_deg):
    return calc_eta_from_theta_rad(np.deg2rad(theta_deg))


@njit
def calc_theta_rad_from_eta(eta):
    eta = np.asarray(eta)
    inf_mask = np.isinf(eta)
    arg = np.where(inf_mask, np.zeros_like(eta), eta)

    return np.where(np.isneginf(eta), np.ones_like(eta) * 180., 2. * np.arctan(np.exp(arg) - inf_mask))


@njit
def calc_theta_deg_from_eta(eta):
    return np.rad2deg(calc_theta_rad_from_eta(eta))


@njit
def calc_qpt_from_qinvpt(qinvpt, max_pt=1e12):
    charge = nz_sign(qinvpt)
    pt = safe_divide(1, abs(qinvpt))
    pt = np.where(qinvpt == 0, np.ones_like(qinvpt) * max_pt, pt)
    return charge * pt


@njit
def calc_qinvpt_from_qpt(qpt, max_invpt=1e12):
    charge = nz_sign(qpt)
    invpt = safe_divide(1, abs(qpt))
    invpt = np.where(qpt == 0, np.ones_like(qpt) * max_invpt, invpt)
    return charge * invpt


@njit
def calc_dxy(charge, pt, phi, xv, yv, B=3.811):
    rg = -pt / (0.003 * charge * B)  # R = -pT/(0.003 q B)  [cm]

    xc = xv - (rg * np.sin(phi))  # xc = xv - R sin(phi)
    yc = yv + (rg * np.cos(phi))  # yc = yv + R cos(phi)
    cg = np.hypot(xc, yc)

    dxy = rg - (np.sign(rg) * cg)

    return dxy


@njit
def calc_d0(charge, pt, phi, xv, yv, B=3.811):
    rg = -pt / (0.003 * charge * B)  # R = -pT/(0.003 q B)  [cm]

    xc = xv - (rg * np.sin(phi))  # xc = xv - R sin(phi)
    yc = yv + (rg * np.cos(phi))  # yc = yv + R cos(phi)

    cg = np.hypot(xc, yc)
    xcg_norm = xc / cg
    ycg_norm = yc / cg

    dxy = cg - nz_sign(rg) * rg
    xd0 = dxy * xcg_norm
    yd0 = dxy * ycg_norm
    udxy = np.hypot(xd0, yd0)

    lxy = np.hypot(xv, yv)
    lxy_dot_d0 = xv * xd0 + yv * yd0
    lxy_d0_ang = math.acos(lxy_dot_d0 / lxy / udxy)

    if lxy_d0_ang == 0:
        d0 = 0
    else:
        if lxy_d0_ang < (np.pi / 2):
            d0_sign = 1
        else:
            d0_sign = -1

        d0 = d0_sign * udxy

    return d0


@njit
def calc_d0_cmssw(phi, vx, vy):
    return vy * np.cos(phi) - vx * np.sin(phi)


@njit
def calc_z0_cmssw(pt, eta, phi, vx, vy, vz):
    return vz - ((vx * np.cos(phi) + vy * np.sin(phi)) / pt) * np.sinh(eta)


def calc_z_star(eta, z0, z_star):
    if eta >= 0:
        if z0 < -z_star:
            return -z_star
        else:
            return z_star
    else:
        if z0 > z_star:
            return z_star
        else:
            return -z_star


def propagate_particle(charge, mom, pos, z_star=None):
    # Define constants
    R_4T = 300.
    z_4T = 650.
    z_max = 1100.

    # Select z_star
    if z_star is None:
        z_star = calc_z_star(mom.Eta(), pos.z(), z_4T)

    # Define position vector
    pos = TVector3(pos)

    # Define momentum vector
    mom = TVector3(mom)

    # Calculate Pz-direction
    pz_is_positive = mom.Pz() > 0

    # Calculate starting region
    if abs(pos.z()) <= z_4T:
        # Particle is inside the solenoid
        region = 1
    else:
        # Particle is outside the solenoid
        region = 2

    # Propagate the particle by regions
    while True:
        # Short-Circuit: Particle left the solenoid in r
        if (pos.Perp() >= R_4T) and region == 1:
            # get_logger().warn(f'Stopped region={region}, r0={math.hypot(x0,y0)}, r={pos.Perp()}, z0={z0}, z={pos.z()}, zstar={z_star}')
            break

        # Short-Circuit: Particle reached z_star
        if pz_is_positive and not (pos.z() < z_star):
            break
        elif not pz_is_positive and not (pos.z() > z_star):
            break

        # Handle Cases
        if region == 1:
            # Calculate next z position
            if pz_is_positive:
                final_z = min(z_star, z_4T)
            else:
                final_z = max(z_star, -z_4T)

            # Propagate
            region = 2
            reached_max_r = propagate_with_mag_field(charge, mom, pos, final_z)

            # Short-Circuit: Particle has left the solenoid
            if reached_max_r:
                # get_logger().warn(f'Stopped at solenoid yolk r={pos.Perp()}, z0={z0}, z={pos.z()}, zstar={z_star}')
                break
        elif region == 2:
            # Calculate next z position
            if pz_is_positive:
                if pos.z() < 0:
                    # Started from the negative endcap moving to solenoid
                    region = 1
                    final_z = min(z_star, -z_4T)
                else:
                    # Started from the positive endcap moving to tunnel
                    region = 0
                    final_z = min(z_star, z_max)
            else:
                if pos.z() < 0:
                    # Started from the negative endcap moving to tunnel
                    region = 0
                    final_z = max(z_star, -z_max)
                else:
                    # Started from the positive endcap moving to solenoid
                    region = 1
                    final_z = max(z_star, z_4T)

            # Propagate
            propagate_without_mag_field(mom, pos, final_z)
        else:
            # Short-Circuit: Invalid Region
            break

    return mom, pos


def propagate_with_mag_field(charge, mom, pos, z_star):
    # Constants
    magnetic_field = 3.811  # Tesla

    # Variables
    x0, y0, z0 = pos.x(), pos.y(), pos.z()
    pt, pz, phi0 = mom.Pt(), mom.Pz(), mom.Phi()
    Rg = -pt / (0.003 * magnetic_field * charge)  # R = -pT/(0.003 q B)  [cm], radius of the circle
    angular_freq = pt / Rg

    # Calculate time to z_star
    delta_z = z_star - z0
    delta_t_for_delta_z = delta_z / pz

    # Ensure we don't leave the solenoid in the r-direction
    if angular_freq >= 0:
        dphi_a, dphi_b = 0, np.pi
    else:
        dphi_a, dphi_b = -np.pi, 0

    dr_at_a = calc_dr_from_toroid(dphi_a, Rg, phi0, x0, y0)
    dr_at_b = calc_dr_from_toroid(dphi_b, Rg, phi0, x0, y0)

    if (dr_at_a * dr_at_b) < 0:
        delta_phi_for_delta_r = brenth(calc_dr_from_toroid, dphi_a, dphi_b, args=(Rg, phi0, x0, y0), rtol=1e-2)
        delta_t_for_delta_r = delta_phi_for_delta_r / angular_freq
    else:
        delta_t_for_delta_r = delta_t_for_delta_z

    # Pick smallest delta time
    reached_max_r = delta_t_for_delta_r < delta_t_for_delta_z

    if reached_max_r:
        delta_t = delta_t_for_delta_r
        final_z = delta_t * pz
    else:
        delta_t = delta_t_for_delta_z
        final_z = z_star

    # Calculate change in positions
    final_phi, final_x, final_y = fast_propagate_with_mag_field(
        Rg, angular_freq, delta_t,
        phi0, x0, y0
    )

    # Update vectors
    pos.SetXYZ(final_x, final_y, final_z)
    mom.SetPhi(final_phi)

    # Return
    return reached_max_r


@njit
def fast_propagate_with_mag_field(Rg, angular_freq, delta_t, phi0, x0, y0):
    dphi = delta_t * angular_freq

    final_phi = phi0 - dphi
    final_x = x0 + Rg * (np.sin(phi0) + np.sin(dphi - phi0))
    final_y = y0 + Rg * (-np.cos(phi0) + np.cos(dphi - phi0))

    return final_phi, final_x, final_y


@njit
def calc_dr_from_toroid(dphi, Rg, phi0, x0, y0):
    R_4T = 300.
    x = x0 + Rg * (np.sin(phi0) + np.sin(dphi - phi0))
    y = y0 + Rg * (-np.cos(phi0) + np.cos(dphi - phi0))
    return np.hypot(x, y) - R_4T


def propagate_without_mag_field(mom, pos, zstar):
    delta_z = zstar - pos.z()
    delta_t = delta_z / mom.Pz()
    delta_x = mom.Px() * delta_t
    delta_y = mom.Py() * delta_t

    pos.SetXYZ(pos.x() + delta_x, pos.y() + delta_y, zstar)
