import numpy as np


def Count(value):
    return Measurement(value, np.sqrt(value))


class Measurement:

    def __init__(self, value, error):
        self.value = value
        self.error = error

    def add(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        v = lhs_value + rhs_value
        e = np.sqrt(np.power(lhs_error, 2) + np.power(rhs_error, 2))

        return Measurement(v, e)

    def subtract(self, rhs, cor=False):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        factor = -1. if cor else 1.

        v = (lhs_value - rhs_value)
        e = np.sqrt(np.power(lhs_error, 2) + factor * np.power(rhs_error, 2))

        return Measurement(v, e)

    def multiply(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        v = lhs_value * rhs_value
        e = abs(np.sqrt(np.power(lhs_error * rhs_value, 2) + np.power(lhs_value * rhs_error, 2)))

        return Measurement(v, e)

    def divide(self, rhs, nan=None):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        if not any([lhs_value, lhs_error, rhs_value, rhs_error]):
            return Measurement(0., 0.)

        if rhs_value == 0.:
            if nan is None:
                raise ValueError("Cannot divide by zero")
            else:
                return Measurement(nan, nan)

        if rhs_value == 0.:
            rhs_value = 1e-9

        v = lhs_value / rhs_value

        if lhs_value == 0.:
            lhs_value = 1e-9

        e = abs(v * np.sqrt(np.power(lhs_error / lhs_value, 2) + np.power(rhs_error / rhs_value, 2)))

        return Measurement(v, e)

    def divideb(self, rhs):
        # Unpack values
        lhs_value = self.value
        lhs_error = self.error
        rhs_value = rhs.value
        rhs_error = rhs.error

        # Calculate
        if not any([lhs_value, lhs_error, rhs_value, rhs_error]):
            return Measurement(0., 0.)

        if rhs_value <= 0.:
            raise ValueError("Cannot binomially divide num <= 0")

        if rhs_value == 0.:
            rhs_value = 1e-9

        v = 1.0 * lhs_value / rhs_value
        e = np.sqrt(((1. - 2. * v) * np.power(lhs_error, 2) + np.power(v * rhs_error, 2))) / rhs_value

        return Measurement(v, e)
