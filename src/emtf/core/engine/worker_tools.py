# States
kOpenNextFile = 0
kProcessBatch = 1
kCloseFile = 2
kDone = 3


# Utils
def calculate_entry_range(offset, batch_size, max_entries):
    # Short-Circuit: Done
    if offset == max_entries:
        return max_entries, max_entries

    # Calculate
    start = offset
    stop = min(max_entries, start + batch_size)
    next_stop = min(max_entries, stop + batch_size)

    if (next_stop - stop) < batch_size:
        stop = next_stop

    return start, stop
