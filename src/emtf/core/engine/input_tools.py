from ROOT import TFile

from emtf.core.execution.runtime import get_logger


def get_input_entries(inputs, tree_name, max_entries):
    input_entries = list()

    # Loop inputs
    total_entries = 0

    for input_path in inputs:
        # Open file
        try:
            root_file = TFile.Open(input_path, 'READ')
        except Exception as err:
            get_logger().error(f"Failed to open file {input_path}: {err}")
            continue

        # Short-Circuit: Corrupt file
        if not (root_file and not root_file.IsZombie()):
            get_logger().warn(f"Skipping corrupt or inaccessible file: {input_path}")
            continue

        # Get entry count
        tree = root_file.Get(tree_name)
        tree_entries = tree.GetEntries()
        root_file.Close('R')

        # Short-Circuit: Empty
        if tree_entries == 0:
            continue

        # Find max entries
        if (max_entries > -1) and (max_entries < (total_entries + tree_entries)):
            reached_max = True
            tree_entries = max_entries - total_entries
            total_entries = max_entries
        else:
            reached_max = False
            total_entries += tree_entries

        # Yield
        input_entries.append((input_path, tree_name, tree_entries))

        # Short-Circuit: Reached max
        if reached_max:
            break

    return input_entries, total_entries
