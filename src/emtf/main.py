import argparse
import os
import sys

import ROOT
import matplotlib
import numpy as np
import tensorflow as tf
from matplotlib import pyplot as plt


def main():
    # CONFIGURE RUN
    if sys.version_info[0] < 3:
        raise AssertionError('Please run this code with Python 3.')

    # Get runtime
    from emtf.core.execution import runtime

    # Get plot style and color map
    plt.style.use(runtime.resource('tdrstyle.mplstyle'))

    # Get logger
    from emtf.core.execution.runtime import get_logger

    get_logger().info('Debug Mode       : {}'.format(runtime.debug_en))
    get_logger().info('Using CMSSW      : {}'.format(os.environ.get('CMSSW_VERSION', 'N/A')))
    get_logger().info('Using python     : {}'.format(sys.version.replace('\n', '')))
    get_logger().info('Using root       : {}'.format(ROOT.__version__))
    get_logger().info('Using numpy      : {}'.format(np.__version__))
    get_logger().info('Using matplotlib : {}'.format(matplotlib.__version__))
    get_logger().info('Using tensorflow : {}'.format(tf.__version__))
    get_logger().info('Using keras      : {}'.format(tf.keras.__version__))
    get_logger().info('Devices          : {}'.format(tf.config.list_physical_devices()))

    # CONFIGURE ROOT
    import emtf.core.root.environment as root_env

    root_env.configure()

    # CONFIGURE PARSER
    parser = argparse.ArgumentParser(prog=runtime.project_name, description='EMTF Analysis')

    parser.add_argument('--debug', dest='debug_en', action='store_const',
                        const=True, default=False,
                        help='Enable debug mode')

    subparsers = parser.add_subparsers(dest='parser', metavar="COMMAND", help='Command to be run')

    # CONFIGURE COMMANDS
    import emtf.commands.phase2.calibrate.command_emtf as calibrate_emtf
    import emtf.commands.phase2.calibrate.command_model as calibrate_model
    import emtf.commands.phase2.check_beamhalo.command as check_beamhalo
    import emtf.commands.phase2.check_sample.command as check_sample
    import emtf.commands.phase2.check_performance.command as check_performance
    import emtf.commands.phase2.check_rates.command as check_rates
    import emtf.commands.general.gen_vertex.llp_command as gen_llp
    import emtf.commands.phase2.gen_ml_samples.command_muon_signal as gen_ml_signal
    import emtf.commands.phase2.gen_ml_samples.command_pileup as gen_ml_pileup
    import emtf.commands.phase2.gen_ml_samples.command_combine as gen_ml_combine
    from emtf.commands.phase2.gen_ml_samples.command_hscp_signal import HSCPSignalSample
    import emtf.commands.phase2.gen_patterns.command_luts as pattern_luts
    import emtf.commands.phase2.gen_patterns.command_pileup_act as pileup_act
    import emtf.commands.phase2.gen_patterns.command_qualities as qualities
    import emtf.commands.phase2.gen_patterns.command_signal as signal_patterns
    import emtf.commands.phase2.gen_patterns.command_signal_act as signal_act
    import emtf.commands.phase2.gen_patterns.command_truth as truth_patterns
    import emtf.commands.phase1.check_performance.command as check_p1_performance
    import emtf.commands.phase2.gen_act_lut_pt.command as gen_act_lut_pt
    import emtf.commands.phase2.gen_act_lut_rels.command as gen_act_lut_rels
    import emtf.commands.phase2.gen_act_lut_d0.command as gen_act_lut_d0
    import emtf.commands.phase1.gen_act_lut_pt.command as gen_p1_act_lut_pt
    import emtf.commands.phase1.gen_act_lut_d0.command as gen_p1_act_lut_dxy
    import emtf.commands.phase1.gen_ml_signal.command_signal as gen_p1_ml_signal
    import emtf.commands.phase1.gen_ml_signal.command_zerobias as gen_p1_ml_zerobias
    import emtf.commands.phase1.check_rates.command as check_p1_rates
    from emtf.core.execution import runtime

    command_entries = {
        # General
        'generate-llp': ('Generate llp distribution', gen_llp),
        # Phase-2
        'calibrate-emtf': ('Phase-2 EMTF calibration', calibrate_emtf),
        'calibrate-model': ('Phase-2 Model calibration', calibrate_model),
        'check-beamhalo': ('Phase-2 check beam halo', check_beamhalo),
        'check-sample': ('Phase-2 check sample', check_sample),
        'check-performance': ('Phase-2 check performance', check_performance),
        'check-rates': ('Phase-2 check rates', check_rates),
        'generate-truth-patterns': ('Phase-2 generate truth patterns', truth_patterns),
        'generate-signal-patterns': ('Phase-2 generate signal patterns', signal_patterns),
        'calculate-signal-activation': ('Phase-2 signal activation', signal_act),
        'calculate-pileup-activation': ('Phase-2 pileup activation', pileup_act),
        'calculate-pattern-qualities': ('Phase-2 pattern qualities', qualities),
        'generate-act-lut-pt': ('Phase-2 qpt activation lut', gen_act_lut_pt),
        'generate-act-lut-rels': ('Phase-2 rels activation lut', gen_act_lut_rels),
        'generate-act-lut-d0': ('Phase-2 d0 activation lut', gen_act_lut_d0),
        'generate-pattern-luts': ('Phase-2 pattern luts', pattern_luts),
        'generate-ml-signal': ('Phase-2 generate machine learning signal sample', gen_ml_signal),
        'generate-ml-signal-hscp': ('Phase-2 generate machine learning signal sample for hscp', HSCPSignalSample()),
        'generate-ml-pileup': ('Phase-2 generate machine learning pileup sample', gen_ml_pileup),
        'generate-ml-combine': ('Phase-2 generate machine learning combined sample', gen_ml_combine),
        # Phase-1
        'check-p1-performance': ('Phase-1 check performance', check_p1_performance),
        'generate-p1-act-lut-pt': ('Phase-1 qpt activation lut', gen_p1_act_lut_pt),
        'generate-p1-act-lut-dxy': ('Phase-1 d0 activation lut', gen_p1_act_lut_dxy),
        'generate-p1-ml-signal': ('Phase-1 generate machine learning signal', gen_p1_ml_signal),
        'generate-p1-ml-zerobias': ('Phase-1 generate machine learning signal', gen_p1_ml_zerobias),
        'check-p1-rates': ('Phase-1 check rates', check_p1_rates),
    }

    for key, command_entry in command_entries.items():
        subparser = subparsers.add_parser(key, help=command_entry[0])
        command_entry[1].configure_parser(subparser)

    args = parser.parse_args()

    # DEBUG MODE
    if args.debug_en:
        runtime.debug_en = True

    # RUN COMMAND
    command_entry = command_entries.get(args.parser)

    if command_entry is None:
        parser.print_help()
        exit(1)

    # RUN
    command_entry[1].run(args)
