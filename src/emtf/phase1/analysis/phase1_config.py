from emtf.core.entries import SimpleProducer
from emtf.phase1.commons.ntuple_primitives import get_emtf_tracks, get_gen_muons, get_gmt_muons, get_emtf_unpacked_tracks

branch_aliases = {
    'emtfUnpTrack_phi_deg': 'emtfUnpTrack_phi',
    'emtfTrack_phi_deg': 'emtfTrack_phi',
    'gmtMuon_pdg_id': 'gmtMuon_ID',
    'gmtMuon_quant_dxy': 'gmtMuon_dxy',
}

base_producers = [
    SimpleProducer('gen_muons', lambda tree: get_gen_muons(tree)),
    SimpleProducer('emtf_unpacked_tracks', lambda tree: get_emtf_unpacked_tracks(tree)),
    SimpleProducer('emtf_tracks', lambda tree: get_emtf_tracks(tree)),
    SimpleProducer('gmt_muons', lambda tree: get_gmt_muons(tree))
]
