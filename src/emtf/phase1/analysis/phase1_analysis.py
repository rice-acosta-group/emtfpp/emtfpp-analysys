import os
from abc import ABC, abstractmethod

from emtf.core.commands import Command
from emtf.phase1.analysis.phase1_engine import phase1_run


class Phase1Analysis(Command, ABC):

    def __init__(self):
        self.analyzers = None
        self.branches_required = None

    def configure(self, args):
        pass

    @abstractmethod
    def create_producers(self):
        raise NotImplementedError()

    @abstractmethod
    def create_analyzers(self):
        raise NotImplementedError()

    def create_analysis(self):
        return self.create_producers(), self.create_analyzers()

    def merge_results(self, analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id, analyzer in enumerate(analyzers):
                self.analyzers[analyzer_id].merge(analyzer)

    def run(self, args):
        # Unpack engine args
        input_files = args.input_files
        max_events = args.max_events
        event_fraction = args.event_fraction

        # Configure
        self.configure(args)

        # Init main analyzers
        self.analyzers = self.create_analyzers()

        # Run MT
        phase1_run(
            input_files, max_events, os.cpu_count() - 5,
            self.create_analysis, self.merge_results,
            event_fraction=event_fraction, branches_required=self.branches_required
        )

        # Consolidate
        self.consolidate()

    def consolidate(self):
        pass
