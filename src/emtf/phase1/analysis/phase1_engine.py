import time
from multiprocessing import Manager, Process
from random import random

import psutil
from ROOT import TFile
from tqdm import tqdm

from emtf.core.engine.input_tools import get_input_entries
from emtf.core.engine.worker_tools import kOpenNextFile, kCloseFile, kProcessBatch, calculate_entry_range, kDone
from emtf.core.entries import Entry
from emtf.core.execution.runtime import get_logger
from emtf.phase1.analysis.phase1_config import base_producers, branch_aliases


def phase1_run(
        inputs, max_entries, worker_count,
        analysis_factory, merge_fn=None,
        event_fraction=1, branches_required=None
):
    # Record start time
    start_time = time.perf_counter()

    # Start engine
    with Manager() as manager:
        # Init Locks
        worker_mutex = manager.Lock()
        progress_condition = manager.Condition()

        # Build Context
        worker_ctx = manager.dict()

        # Probe inputs
        input_entries, total_entries = get_input_entries(inputs, 'EMTFNtuple/tree', max_entries)

        # Calculate batch size
        cache_frac = 0.5
        batch_size = max(1000, total_entries // worker_count // 10)
        cache_size_mb = int(psutil.virtual_memory().available / (1024 ** 2) * cache_frac / worker_count)  # in MB
        get_logger().info(f'Setting batch_size to {batch_size} and cache_size to {cache_size_mb} MB')

        # Init trackers
        worker_ctx['progress'] = 0

        for source_id in range(len(input_entries)):
            worker_ctx[f'done_{source_id}'] = False
            worker_ctx[f'offset_{source_id}'] = 0

        # Launch Processes
        process_col = list()
        worker_analyzers = manager.list()
        worker_count_needed = min(worker_count, total_entries // 1000)
        worker_count_needed = max(worker_count_needed, 1)

        for worker_id in range(worker_count_needed):
            args = (
                worker_id, input_entries,
                worker_mutex, progress_condition,
                worker_ctx, analysis_factory,
                batch_size, cache_size_mb,
                event_fraction, branches_required, worker_analyzers
            )

            process = Process(target=worker_task, args=args)
            process_col.append(process)
            process.start()

        # Monitor progress
        with tqdm(
                desc='Entries', total=int(total_entries * event_fraction),
                unit_scale=True, unit='Entry',
                colour='#00ff00', mininterval=2
        ) as pbar:
            prev_progress = 0

            while True:
                # Check if workers are alive
                all_dead = True

                for process in process_col:
                    process.join(0)

                    if process.is_alive():
                        all_dead = False

                # Short-Circuit: All workers are dead
                if all_dead:
                    break

                # Monitor progress
                with progress_condition:
                    # Get progress
                    progress = worker_ctx['progress']

                    # Update progress
                    if prev_progress < progress:
                        delta_progress = progress - prev_progress
                        prev_progress = progress
                        pbar.update(delta_progress)

                    # Short-Circuit: Work complete
                    if pbar.n == pbar.total:
                        break

                    # Wait at most 10 seconds for a progress update
                    progress_condition.wait(timeout=10)

        # Join Processes
        for process in process_col:
            process.join()

        # Merge Results
        merge_fn(worker_analyzers)

    # Log Performance
    total_time = int(time.perf_counter() - start_time)

    get_logger().info(f"""
            *********************************************************
            Engine Total Time: {total_time} seconds
            *********************************************************
            """)


def worker_task(
        worker_id, input_entries,
        worker_mutex, progress_condition,
        worker_ctx, analysis_factory,
        batch_size, cache_size_mb,
        entry_fraction, branches_required, worker_analyzers
):
    # Init
    user_producers, analyzers = analysis_factory()

    # Build producer dictionary
    producers = dict()

    for producer in [*base_producers, *user_producers]:
        keys = producer.provides()

        for key in keys:
            producers[key] = producer

    # Init progress variables
    partial_progress = 0
    update_interval = 2000
    last_update = time.perf_counter()

    # Run
    state = kOpenNextFile
    source_id = 0
    source_file = None
    source_tree = None
    source_entries = None
    entry = None

    vsize_input_entries = len(input_entries)

    while state != kDone:
        if state == kOpenNextFile:
            # Find a source
            with worker_mutex:
                finished_processing = True

                for source_id in range(source_id, vsize_input_entries):
                    if not worker_ctx[f'done_{source_id}']:
                        finished_processing = False
                        break

            # Short-Circuit: Finished processing files
            if finished_processing:
                state = kDone
                continue

            # Open file
            source_path, source_tree_name, source_entries = input_entries[source_id]
            source_file = TFile.Open(source_path, 'READ')
            source_tree = source_file.Get(source_tree_name)

            # Configure cache
            source_tree.SetCacheSize(cache_size_mb * 1024 * 1024)
            source_tree.SetCacheLearnEntries(max(1000, batch_size // 100))

            # Short-Circuit: Entry already exists, just update the tree
            if entry is not None:
                state = kProcessBatch
                entry.set_tree(source_id, source_tree)
                continue

            # Enable/Disable branches
            if branches_required is not None:
                # Disable all branches
                source_tree.SetBranchStatus('*', 0)

                # Enable branches required
                for branch_name in branches_required:
                    source_tree.SetBranchStatus(branch_name, 1)

            # Create entry
            entry = Entry(source_id, source_tree, branch_aliases, producers)

            # Before run
            for analyzer in analyzers:
                analyzer.before_run(entry)

            # Begin processing
            state = kProcessBatch
        elif state == kProcessBatch:
            # Calculate batch ranges
            with worker_mutex:
                offset_tag = f'offset_{source_id}'
                offset = worker_ctx[offset_tag]
                start, stop = calculate_entry_range(offset, batch_size, source_entries)
                worker_ctx[offset_tag] = stop

            # Log when file is first processed
            if start == 0:
                with progress_condition:
                    get_logger().info(f'Worker {worker_id}: Processing {source_id + 1} of {vsize_input_entries}: {source_path}')

            # Short-Circuit: Done processing file
            if start >= stop:
                state = kCloseFile
                continue

            # Set Cache Range
            source_tree.SetCacheEntryRange(start, stop - 1)

            # Process Batch
            for batch_entry_id in range(stop - start):
                # Calculate next entry id
                entry_id = start + batch_entry_id

                # Short-Circuit: Max entry reached
                if source_entries <= entry_id:
                    state = kCloseFile
                    break

                # Randomly choose to process or not
                if not ((entry_fraction == 1) or (random() < entry_fraction)):
                    continue

                # Analyze
                try:
                    # Load entry
                    entry.set_entry(entry_id)

                    # Process entry
                    for analyzer in analyzers:
                        analyzer.process_entry(entry)
                except Exception as err:
                    with progress_condition:
                        get_logger().error(f'Error occurred on entry_id={entry_id}')
                        get_logger().exception(err)

                    # Fail gracefully
                    return

                # Update partial progress
                partial_progress += 1

                # Short-Circuit: Too early for progress update
                delta_time = (time.perf_counter() - last_update) * 1000

                if delta_time < update_interval:
                    continue

                # Publish progress
                last_update = time.perf_counter()
                flush_progress = partial_progress
                partial_progress = 0

                with progress_condition:
                    worker_ctx['progress'] += flush_progress
                    progress_condition.notify()
        elif state == kCloseFile:
            # Close File
            source_file.Close("R")

            # Flag file as done
            with worker_mutex:
                worker_ctx[f'done_{source_id}'] = True

            # Open next file
            state = kOpenNextFile

    # Publish Progress
    if partial_progress > 0:
        with progress_condition:
            worker_ctx['progress'] += partial_progress
            progress_condition.notify()

    # After run
    for analyzer in analyzers:
        analyzer.after_run()

    # Summary
    with progress_condition:
        get_logger().info(f"""\n
                *********************************************************
                Worker {worker_id} Summaries
                *********************************************************
                """)

        for clazz, producer in producers.items():
            producer.summary()

    # Register
    with worker_mutex:
        worker_analyzers.append(analyzers)
