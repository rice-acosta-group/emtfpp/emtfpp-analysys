from emtf.core.primitives import VectorPrimitive
from emtf.phase1.commons.emtf_eta_ranges import emtf_z0_eta_start, emtf_z0_eta_stop, emtf_z1_eta_stop, \
    emtf_z1_eta_start, emtf_z2_eta_start, emtf_z2_eta_stop


class Run3GMTMuon(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'gmtMuon_')

    def get_endcap(self):
        if self.eta >= 0:
            return 1

        return -1

    def get_zone(self):
        if emtf_z0_eta_start <= abs(self.eta) < emtf_z0_eta_stop:
            return 0
        elif emtf_z1_eta_start <= abs(self.eta) < emtf_z1_eta_stop:
            return 1
        elif emtf_z2_eta_start <= abs(self.eta) < emtf_z2_eta_stop:
            return 2

        return None

    def get_dxy(self):
        return self.quant_dxy * 32.
