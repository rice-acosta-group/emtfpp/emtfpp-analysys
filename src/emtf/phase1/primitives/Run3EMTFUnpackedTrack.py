import numpy as np

from emtf.core.primitives import VectorPrimitive
from emtf.phase1.commons.emtf_eta_ranges import emtf_z0_eta_start, emtf_z0_eta_stop, emtf_z1_eta_stop, \
    emtf_z1_eta_start, emtf_z2_eta_start, emtf_z2_eta_stop


class Run3EMTFUnpackedTrack(VectorPrimitive):

    def __init__(self, event, idx):
        super().__init__(event, idx, 'emtfUnpTrack_')

    def get_zone(self):
        if emtf_z0_eta_start <= abs(self.eta) < emtf_z0_eta_stop:
            return 0
        elif emtf_z1_eta_start <= abs(self.eta) < emtf_z1_eta_stop:
            return 1
        elif emtf_z2_eta_start <= abs(self.eta) < emtf_z2_eta_stop:
            return 2

        return None

    def get_qual(self):
        if self.mode == 15:
            quality = 15
        elif self.mode == 14:
            quality = 14
        elif self.mode == 13:
            quality = 13
        elif self.mode == 12:
            quality = 7
        elif self.mode == 11:
            quality = 12
        elif self.mode == 10:
            quality = 10
        elif self.mode == 9:
            quality = 9
        elif self.mode == 7:
            quality = 11
        elif self.mode == 6:
            quality = 6
        elif self.mode == 5:
            quality = 5
        elif self.mode == 3:
            quality = 4
        else:
            quality = 0

        if abs(self.eta) < 1.19:
            quality = quality // 4

        return quality

    def get_phi(self):
        return np.deg2rad(self.phi_deg)
