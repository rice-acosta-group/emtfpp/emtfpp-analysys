# Prompt Single Muon
sm_prompt_min_qual = 12
sm_prompt_min_pt = 20
sm_prompt_eta_start = 1.2
sm_prompt_eta_stop = 2.4


def get_sm_prompt_eta_start(eta):
    return max(sm_prompt_eta_start, eta)


def get_sm_prompt_eta_stop(eta):
    return min(sm_prompt_eta_stop, eta)


# Displaced Single Muon
sm_disp_min_qual = 12
sm_disp_min_pt_dxy = 10
sm_disp_min_dxy = None
sm_disp_eta_start = 1.24
sm_disp_eta_stop = 2.4


def get_sm_disp_eta_start(eta):
    return max(sm_disp_eta_start, eta)


def get_sm_disp_eta_stop(eta):
    return min(sm_disp_eta_stop, eta)


def new_trk_condition(
        min_qual=None,
        min_eta=None, max_eta=None,
        min_pt=None, min_pt_dxy=None,
        min_dxy=None
):
    def condition(entry):
        trk_qual = entry.get('trk_qual', 0)
        trk_pt = entry.get('trk_pt', 0)
        trk_pt_dxy = entry.get('trk_pt_dxy', 0)
        trk_dxy = entry.get('trk_dxy', 0)
        trk_eta = entry.get('trk_eta', 0)

        if min_qual is not None and not (min_qual <= trk_qual):
            return False

        if min_pt is not None and not (min_pt <= trk_pt):
            return False

        if min_pt_dxy is not None and not (min_pt_dxy <= trk_pt_dxy):
            return False

        if min_dxy is not None and not (min_dxy <= trk_dxy):
            return False

        if min_eta is not None and not (min_eta <= abs(trk_eta)):
            return False

        if max_eta is not None and not (abs(trk_eta) < max_eta):
            return False

        return True

    return condition


def new_gen_condition(
        min_eta=None, max_eta=None,
        min_pt=None, max_pt=None,
        min_dxy=None, max_dxy=None,
        d0_sign=1
):
    def condition(entry):
        gen_bx = entry.get('gen_bx', 0)
        gen_pt = entry.get('gen_pt', 0)
        gen_d0 = entry.get('gen_d0', 0)
        gen_d0_sign = -1 if (gen_d0 < 0) else 1
        gen_eta_st2 = entry.get('gen_eta_st2', 0)

        if gen_bx != 0:
            return False

        if min_pt is not None and not (min_pt <= abs(gen_pt)):
            return False

        if max_pt is not None and not (abs(gen_pt) < max_pt):
            return False

        if min_dxy is not None and not (min_dxy <= abs(gen_d0)):
            return False

        if max_dxy is not None and not (abs(gen_d0) < max_dxy):
            return False

        if d0_sign is not None and not (gen_d0_sign == d0_sign):
            return False

        if min_eta is not None and not (min_eta <= abs(gen_eta_st2)):
            return False

        if max_eta is not None and not (abs(gen_eta_st2) < max_eta):
            return False

        return True

    return condition


# Base Trigger
def sm_base_trg(entry):
    return False

# sm_base_trg = new_trk_condition(
#     min_qual=sm_prompt_min_qual,
#     min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
#     min_pt=sm_prompt_min_pt,
# )
