from emtf.core.commons.caching import CachedVector
from emtf.phase1.primitives.Run3EMTFTrack import Run3EMTFTrack
from emtf.phase1.primitives.Run3EMTFUnpackedTrack import Run3EMTFUnpackedTrack
from emtf.phase1.primitives.Run3GENMuon import Run3GENMuon
from emtf.phase1.primitives.Run3GMTMuon import Run3GMTMuon


def get_gen_muons(event):
    return [
        Run3GENMuon(event, idx)
        for idx in range(len(event.genPart_ID))
        if abs(event.genPart_ID[idx]) == 13
    ]

def get_emtf_unpacked_tracks(event):
    return CachedVector(event.emtfUnpTrack_size, lambda idx: Run3EMTFUnpackedTrack(event, idx))


def get_emtf_tracks(event):
    return CachedVector(event.emtfTrack_size, lambda idx: Run3EMTFTrack(event, idx))


def get_gmt_muons(event):
    return CachedVector(event.gmtMuon_size, lambda idx: Run3GMTMuon(event, idx))
