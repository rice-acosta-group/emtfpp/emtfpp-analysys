import math

from emtf.core.commons.combinatorics import yield_combinations
from emtf.core.commons.pairing import reduce_pairs
from emtf.core.entries import Producer


class Phase1MuonEventBuilder(Producer):

    def __init__(self, multiplicity):
        self.multiplicity = multiplicity

        self.event_count = 0
        self.invalid_event_count = 0
        self.invalid_muon_event_count = 0
        self.muon_count = 0
        self.invalid_muon_count = 0

    def provides(self):
        return ['muons']

    def empty_products(self):
        return {
            'muons': dict()
        }

    def extract(self, event):
        # Increase Counter
        self.event_count += 1

        # Get GEN Muons
        gen_muon_count = len(event.gen_muons)

        # Short-Circuit: Should only have N gen muons
        if (self.multiplicity > -1) and (gen_muon_count != self.multiplicity):
            self.invalid_event_count += 1
            return None

        # Increase Counter
        self.muon_count += gen_muon_count

        # Build muon collection
        muon_cadidates = list()

        for gen_muon in event.gen_muons:
            # Short-Circuit: In valid eta region
            if gen_muon.zone is None:
                self.invalid_muon_count += 1
                continue

            # Save muon
            muon_cadidates.append(gen_muon)

        # Short-Circuit: No muons found
        if len(muon_cadidates) == 0:
            self.invalid_muon_event_count += 1
            return self.empty_products()

        # Match Gen Muons
        associate_tracks_and_gmt(
            event, muon_cadidates
        )

        # Return
        return {
            'muons': muon_cadidates
        }

    def summary(self):
        valid_event_count = self.event_count - self.invalid_event_count - self.invalid_muon_event_count
        valid_muon_count = self.muon_count - self.invalid_muon_count
        expected_muon_count = valid_event_count * self.multiplicity
        lost_muon_count = max(expected_muon_count - valid_muon_count, 0)

        if self.multiplicity == -1:
            expected_muon_count = -1
            lost_muon_count = -1

        print('Event Count: %d' % self.event_count)
        print('Invalid Event Count: %d' % self.invalid_event_count)
        print('Invalid Muon Event Count: %d' % self.invalid_muon_event_count)
        print('Valid Event Count: %d' % valid_event_count)
        print('Muon Count: %d' % self.muon_count)
        print('Invalid Muon Count: %d' % self.invalid_muon_count)
        print('Expected Muon Count: %d' % expected_muon_count)
        print('Valid Muon Count: %d' % valid_muon_count)
        print('Lost Muon Count: %d' % (lost_muon_count))


def associate_tracks_and_gmt(event, gen_muons):
    gen_trk_col = list()
    gen_gmt_col = list()

    for gen_muon, emtf_track in yield_combinations(gen_muons, event.emtf_tracks):
        # Short-Circuit: Only use valid tracks
        if emtf_track.mode == 0:
            continue

        # Short-Circuit: Must be in endcap
        if emtf_track.endcap != gen_muon.endcap:
            continue

        # Short-Circuit: Must be in zone
        if emtf_track.zone != gen_muon.zone:
            continue

        # Calculate dR
        dR = math.hypot(
            gen_muon.phi_st2 - emtf_track.model_phi,
            gen_muon.eta_st2 - emtf_track.model_eta
        )

        # Append pair
        gen_trk_col.append((gen_muon.idx, emtf_track.idx, dR))

    # Loop GMT
    for gen_muon, gmt_muon in yield_combinations(gen_muons, event.gmt_muons):
        # Short-Circuit: Only use valid tracks
        if gmt_muon.qual == 0:
            continue

        # Short-Circuit: Must be in endcap
        if gmt_muon.endcap != gen_muon.endcap:
            continue

        # Short-Circuit: Must be in zone
        if gmt_muon.zone != gen_muon.zone:
            continue

        # Calculate dR
        dR = math.hypot(
            gen_muon.phi_st2 - gmt_muon.phi,
            gen_muon.eta_st2 - gmt_muon.eta
        )

        # Append pair
        gen_gmt_col.append((gen_muon.idx, gmt_muon.idx, dR))

    # Reduce pairs
    gen_trk_col = reduce_pairs(gen_trk_col, value_fn=lambda item: item[2])
    gen_gmt_col = reduce_pairs(gen_gmt_col, value_fn=lambda item: item[2])

    # Associate Tracks to GEN muons
    assoc_muons = set()

    for pair in gen_trk_col:
        gen_muon = event.gen_muons[pair[0]]
        emtf_track = event.emtf_tracks[pair[1]]
        gen_muon.best_emtf_track = emtf_track
        gen_muon.best_emtf_track_dR = pair[2]

        assoc_muons.add(pair[0])

    # Associate GMT muons to GEN Muons
    for pair in gen_gmt_col:
        gen_muon = event.gen_muons[pair[0]]
        gmt_muon = event.gmt_muons[pair[1]]
        gen_muon.best_gmt_muon = gmt_muon
        gen_muon.best_gmt_muon_dR = pair[2]

        assoc_muons.add(pair[0])

    # Associate Tracks to GMT muons
    for gen_idx in assoc_muons:
        gen_muon = event.gen_muons[gen_idx]

        if gen_muon.best_emtf_track is None:
            continue

        if gen_muon.best_gmt_muon is None:
            continue

        gen_muon.best_emtf_track.best_gmt_muon = gen_muon.best_gmt_muon
        gen_muon.best_gmt_muon.best_emtf_track = gen_muon.best_emtf_track
