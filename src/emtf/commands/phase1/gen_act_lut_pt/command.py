import math

import numpy as np

from emtf.commands.phase1.gen_act_lut_pt.plotters import ActivationPlotter
from emtf.core.commons.arbitrary_precision import convert_uint_to_int, convert_int_to_uint, clip_uint
from emtf.core.commons.tools import nz_sign
from emtf.phase1.commons.emtf_constants import emtf_max_pt, emtf_pt_eps, emtf_pt_bw


def configure_parser(parser):
    parser.add_argument('--bitwidth', dest='bitwidth', metavar='BIT_WIDTH',
                        type=int, default=8,
                        help='Input Bit Width')

    parser.add_argument('--signed', dest='signed', metavar='SIGNED',
                        type=bool, nargs='?', const=True, default=False,
                        help='Input Signed')

    parser.add_argument('--eps', dest='eps', metavar='EPS',
                        type=float, default=1,
                        help='Quantization scale')

    parser.add_argument('--param-a', dest='param_a', metavar='PARAM_A',
                        type=float, default=0,
                        help='Parameter a')

    parser.add_argument('--param-b', dest='param_b', metavar='PARAM_B',
                        type=float, default=0,
                        help='Parameter b')

    parser.add_argument('--param-bias', dest='param_bias', metavar='PARAM_BIAS',
                        type=float, default=0,
                        help='Parameter bias')


def run(args):
    bitwidth = args.bitwidth
    signed = args.signed
    eps = args.eps

    a = args.param_a
    bias = args.param_bias

    po2 = 1 << bitwidth
    po2m1 = 1 << (bitwidth - 1)

    lut = np.zeros((po2,), dtype=np.int32)

    def activation_fn(quantized_dense_out):
        if quantized_dense_out == 0:
            out_pt = 1e12
            out_pt = round(out_pt / emtf_pt_eps) * emtf_pt_eps
            return out_pt

        in_charge = -1 if (quantized_dense_out < 0) else 1
        in_invpt = abs(quantized_dense_out * eps)
        in_pt = 0 if (in_invpt == 0) else (1 / in_invpt)

        out_pt = a * in_pt + bias
        out_pt = round(out_pt / emtf_pt_eps) * emtf_pt_eps
        out_qpt = in_charge * out_pt

        return abs(out_qpt)

    for bin_x in range(po2):
        # Find quantized x
        quantized_dense_out = bin_x

        if signed:
            quantized_dense_out = convert_uint_to_int(bin_x, bitwidth)

        # Evaluate Activation
        true_y = activation_fn(quantized_dense_out)

        # Quantize Output
        true_y_sign = nz_sign(true_y)
        quantized_y = true_y_sign * abs(true_y) / emtf_pt_eps

        # Find Output Bin
        bin_y = clip_uint(quantized_y, emtf_pt_bw)

        # Fill LUT
        lut[bin_x] = bin_y

        # Print
        print(bin_x, bin_y, bin_y * emtf_pt_eps, true_y)

    with open('qpt_lut.csv', 'w') as output_file:
        for val in lut:
            output_file.write('%d,' % val)

    # Plot
    plotter = ActivationPlotter(
        'qpt_activation_lut',
        'q #upoint p_{T} LUT', 'True p_{T}', 'LUT p_{T}',
        0, emtf_max_pt,  # Min x, Max x
        0, emtf_max_pt,  # Min y, Max y
        logx=False, logy=False)

    for quantized_dense_out in np.linspace(-po2m1, po2m1 - 1, 10000):
        # Calculate True Value
        true_y = activation_fn(quantized_dense_out)

        # Calculate LUT Value
        sign_quantized_dense_out = -1 if (quantized_dense_out < 0) else 1
        quantized_dense_out = sign_quantized_dense_out * math.floor(abs(quantized_dense_out))

        if signed:
            bin_x = convert_int_to_uint(quantized_dense_out, bitwidth)
        else:
            bin_x = clip_uint(quantized_dense_out, bitwidth)

        lut_y = lut[bin_x] * emtf_pt_eps

        # Fill
        plotter.fill(true_y, lut_y)

    plotter.consolidate()
    plotter.write()
