import numpy as np
from ROOT import TVector3

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.kinematics import calc_qinvpt_from_qpt, calc_d0, calc_dxy, propagate_particle
from emtf.core.commons.plotting import plot_var_walls
from emtf.core.commons.tools import safe_divide

# Constants
invalid_dphi = 8191
invalid_dtheta = 127


class SignalAnalyzer(AbstractAnalyzer):

    def __init__(self, filename, no_gen_en):
        super().__init__()

        self.filename = filename
        self.no_gen_en = no_gen_en

        self.list_trk_variables = list()
        self.list_gen_parameters = list()

        self.muon_count = 0
        self.valid_muon_count = 0

    def process_entry(self, event):
        # Increase Counter
        self.muon_count += 1

        # Init
        trk_dphi = np.zeros((6,))
        trk_dphi_sign = np.zeros((6,))
        trk_dtheta = np.zeros((6,))
        trk_dtheta_sign = np.zeros((6,))
        trk_track_endcap = np.zeros((1,))
        trk_track_sector = np.zeros((1,))
        trk_track_theta = np.zeros((1,))
        trk_pattern_csc = np.zeros((4,))
        trk_fr_emtf = np.zeros((4,))
        trk_chamberid = np.zeros((4,))
        trk_ME11ring = np.zeros((1,))

        gen_q = np.zeros((1,))
        gen_pt = np.zeros((1,))
        gen_qinvpt = np.zeros((1,))
        gen_d0 = np.zeros((1,))
        gen_dxy = np.zeros((1,))
        gen_eta = np.zeros((1,))
        gen_eta_st2 = np.zeros((1,))
        gen_phi = np.zeros((1,))
        gen_vx = np.zeros((1,))
        gen_vy = np.zeros((1,))
        gen_vz = np.zeros((1,))

        # Short-Circuit: No Tracks
        if event.emtfTrack_size == 0:
            return

        # Get GEN Info
        if not self.no_gen_en:
            gen_q[0] = np.asarray(list(event.genPart_q)[0])
            gen_pt[0] = np.asarray(list(event.genPart_pt)[0])
            gen_eta[0] = np.asarray(list(event.genPart_eta)[0])
            gen_phi[0] = np.asarray(list(event.genPart_phi)[0])
            gen_vx[0] = np.asarray(list(event.genPart_vx)[0])
            gen_vy[0] = np.asarray(list(event.genPart_vy)[0])
            gen_vz[0] = np.asarray(list(event.genPart_vz)[0])

            gen_qinvpt[0] = calc_qinvpt_from_qpt(gen_q[0] * gen_pt[0])
            gen_d0[0] = calc_d0(gen_q[0], gen_pt[0], gen_phi[0], gen_vx[0], gen_vy[0])
            gen_dxy[0] = calc_dxy(gen_q[0], gen_pt[0], gen_phi[0], gen_vx[0], gen_vy[0])

            mom, pos = TVector3(), TVector3()
            mom.SetPtEtaPhi(gen_pt[0], gen_eta[0], gen_phi[0])
            pos.SetXYZ(gen_vx[0], gen_vy[0], gen_vz[0])

            gen_pos_st2, gen_mom_st2 = propagate_particle(gen_q[0], mom, pos)
            gen_eta_st2[0] = gen_pos_st2.Eta()

        # Short-Circuit: Negative d0
        if gen_d0[0] < 0:
            return

        # Select Best Track
        trk_id = 0

        if event.emtfTrack_size > 1:
            mode = np.asarray(list(event.emtfTrack_mode))
            mode_max = np.flatnonzero(mode == mode.max())

            if len(mode_max) == 2:
                if list(list(event.emtfTrack_ptLUT_deltaPh)[mode_max[0]]).count(8191) > list(
                        list(event.emtfTrack_ptLUT_deltaPh)[mode_max[1]]).count(8191):
                    trk_id = mode_max[1]
                else:
                    trk_id = mode_max[0]

            elif len(mode_max) > 2:
                deltaPh_count = np.empty((len(mode_max), 1))

                for k in range(0, len(mode_max)):
                    deltaPh_count[k] = list(list(event.emtfTrack_ptLUT_deltaPh)[mode_max[k]]).count(8191)

                trk_id = np.argmin(deltaPh_count)

            else:
                trk_id = mode_max[0]

        # Short-Circuit: Only keep valid mode tracks
        trk_mode = list(event.emtfTrack_mode)[trk_id]

        valid_mode = (trk_mode == 11) or (trk_mode == 13) or \
                     (trk_mode == 14) or (trk_mode == 15)

        if not valid_mode:
            return

        # Calculate dPh and dTh
        dPhi_12 = list(event.emtfTrack_ptLUT_deltaPh)[trk_id][0] if list(event.emtfTrack_ptLUT_deltaPh)[trk_id][
                                                                        0] != invalid_dphi else 0
        dPhi_13 = list(event.emtfTrack_ptLUT_deltaPh)[trk_id][1] if list(event.emtfTrack_ptLUT_deltaPh)[trk_id][
                                                                        1] != invalid_dphi else 0
        dPhi_14 = list(event.emtfTrack_ptLUT_deltaPh)[trk_id][2] if list(event.emtfTrack_ptLUT_deltaPh)[trk_id][
                                                                        2] != invalid_dphi else 0
        dPhi_23 = list(event.emtfTrack_ptLUT_deltaPh)[trk_id][3] if list(event.emtfTrack_ptLUT_deltaPh)[trk_id][
                                                                        3] != invalid_dphi else 0
        dPhi_24 = list(event.emtfTrack_ptLUT_deltaPh)[trk_id][4] if list(event.emtfTrack_ptLUT_deltaPh)[trk_id][
                                                                        4] != invalid_dphi else 0
        dPhi_34 = list(event.emtfTrack_ptLUT_deltaPh)[trk_id][5] if list(event.emtfTrack_ptLUT_deltaPh)[trk_id][
                                                                        5] != invalid_dphi else 0

        dTh_12 = list(event.emtfTrack_ptLUT_deltaTh)[trk_id][0] if list(event.emtfTrack_ptLUT_deltaTh)[trk_id][
                                                                       0] != invalid_dtheta else 0
        dTh_13 = list(event.emtfTrack_ptLUT_deltaTh)[trk_id][1] if list(event.emtfTrack_ptLUT_deltaTh)[trk_id][
                                                                       1] != invalid_dtheta else 0
        dTh_14 = list(event.emtfTrack_ptLUT_deltaTh)[trk_id][2] if list(event.emtfTrack_ptLUT_deltaTh)[trk_id][
                                                                       2] != invalid_dtheta else 0
        dTh_23 = list(event.emtfTrack_ptLUT_deltaTh)[trk_id][3] if list(event.emtfTrack_ptLUT_deltaTh)[trk_id][
                                                                       3] != invalid_dtheta else 0
        dTh_24 = list(event.emtfTrack_ptLUT_deltaTh)[trk_id][4] if list(event.emtfTrack_ptLUT_deltaTh)[trk_id][
                                                                       4] != invalid_dtheta else 0
        dTh_34 = list(event.emtfTrack_ptLUT_deltaTh)[trk_id][5] if list(event.emtfTrack_ptLUT_deltaTh)[trk_id][
                                                                       5] != invalid_dtheta else 0

        sPh_12 = 1 if list(event.emtfTrack_ptLUT_signPh)[trk_id][0] == 1 else 0
        sPh_13 = 1 if list(event.emtfTrack_ptLUT_signPh)[trk_id][1] == 1 else 0
        sPh_14 = 1 if list(event.emtfTrack_ptLUT_signPh)[trk_id][2] == 1 else 0
        sPh_23 = 1 if list(event.emtfTrack_ptLUT_signPh)[trk_id][3] == 1 else 0
        sPh_24 = 1 if list(event.emtfTrack_ptLUT_signPh)[trk_id][4] == 1 else 0
        sPh_34 = 1 if list(event.emtfTrack_ptLUT_signPh)[trk_id][5] == 1 else 0

        sTh_12 = 1 if list(event.emtfTrack_ptLUT_signTh)[trk_id][0] == 1 else 0
        sTh_13 = 1 if list(event.emtfTrack_ptLUT_signTh)[trk_id][1] == 1 else 0
        sTh_14 = 1 if list(event.emtfTrack_ptLUT_signTh)[trk_id][2] == 1 else 0
        sTh_23 = 1 if list(event.emtfTrack_ptLUT_signTh)[trk_id][3] == 1 else 0
        sTh_24 = 1 if list(event.emtfTrack_ptLUT_signTh)[trk_id][4] == 1 else 0
        sTh_34 = 1 if list(event.emtfTrack_ptLUT_signTh)[trk_id][5] == 1 else 0

        trk_dphi[0] = np.asarray(dPhi_12)
        trk_dphi[1] = np.asarray(dPhi_13)
        trk_dphi[2] = np.asarray(dPhi_14)
        trk_dphi[3] = np.asarray(dPhi_23)
        trk_dphi[4] = np.asarray(dPhi_24)
        trk_dphi[5] = np.asarray(dPhi_34)

        trk_dphi_sign[0] = np.asarray(sPh_12)
        trk_dphi_sign[1] = np.asarray(sPh_13)
        trk_dphi_sign[2] = np.asarray(sPh_14)
        trk_dphi_sign[3] = np.asarray(sPh_23)
        trk_dphi_sign[4] = np.asarray(sPh_24)
        trk_dphi_sign[5] = np.asarray(sPh_34)

        trk_dtheta[0] = np.asarray(dTh_12)
        trk_dtheta[1] = np.asarray(dTh_13)
        trk_dtheta[2] = np.asarray(dTh_14)
        trk_dtheta[3] = np.asarray(dTh_23)
        trk_dtheta[4] = np.asarray(dTh_24)
        trk_dtheta[5] = np.asarray(dTh_34)

        trk_dtheta_sign[0] = np.asarray(sTh_12)
        trk_dtheta_sign[1] = np.asarray(sTh_13)
        trk_dtheta_sign[2] = np.asarray(sTh_14)
        trk_dtheta_sign[3] = np.asarray(sTh_23)
        trk_dtheta_sign[4] = np.asarray(sTh_24)
        trk_dtheta_sign[5] = np.asarray(sTh_34)

        # Calculate bend, FR, RPC
        st1 = (list(event.emtfTrack_mode)[trk_id] >= 8)
        st2 = ((list(event.emtfTrack_mode)[trk_id] % 8) >= 4)
        st3 = ((list(event.emtfTrack_mode)[trk_id] % 4) >= 2)
        st4 = ((list(event.emtfTrack_mode)[trk_id] % 2) == 1)

        # Fix dPh and dTh if stations don't exist
        if not st1:
            trk_dphi[0] = np.asarray(-999)
            trk_dphi[1] = np.asarray(-999)
            trk_dphi[2] = np.asarray(-999)

            trk_dtheta[0] = np.asarray(-999)
            trk_dtheta[1] = np.asarray(-999)
            trk_dtheta[2] = np.asarray(-999)

        if not st2:
            trk_dphi[0] = np.asarray(-999)
            trk_dphi[3] = np.asarray(-999)
            trk_dphi[4] = np.asarray(-999)

            trk_dtheta[0] = np.asarray(-999)
            trk_dtheta[3] = np.asarray(-999)
            trk_dtheta[4] = np.asarray(-999)

        if not st3:
            trk_dphi[1] = np.asarray(-999)
            trk_dphi[3] = np.asarray(-999)
            trk_dphi[5] = np.asarray(-999)

            trk_dtheta[1] = np.asarray(-999)
            trk_dtheta[3] = np.asarray(-999)
            trk_dtheta[5] = np.asarray(-999)

        if not st4:
            trk_dphi[2] = np.asarray(-999)
            trk_dphi[4] = np.asarray(-999)
            trk_dphi[5] = np.asarray(-999)

            trk_dtheta[2] = np.asarray(-999)
            trk_dtheta[4] = np.asarray(-999)
            trk_dtheta[5] = np.asarray(-999)

        # Get Pattern
        pat1 = -99
        pat2 = -99
        pat3 = -99
        pat4 = -99

        fr1 = -1
        fr2 = -1
        fr3 = -1
        fr4 = -1

        ci1 = -1
        ci2 = -1
        ci3 = -1
        ci4 = -1

        me11_ring = -1

        if st1:
            pat1 = list(event.emtfTrack_ptLUT_cpattern)[trk_id][0]
            fr1 = list(event.emtfTrack_ptLUT_fr)[trk_id][0]
            ci1 = list(event.emtfTrack_ptLUT_bt_ci)[trk_id][0]
            me11_ring = list(event.emtfTrack_ptLUT_st1_ring2)[trk_id]
        if st2:
            pat2 = list(event.emtfTrack_ptLUT_cpattern)[trk_id][1]
            fr2 = list(event.emtfTrack_ptLUT_fr)[trk_id][1]
            ci2 = list(event.emtfTrack_ptLUT_bt_ci)[trk_id][1]
        if st3:
            pat3 = list(event.emtfTrack_ptLUT_cpattern)[trk_id][2]
            fr3 = list(event.emtfTrack_ptLUT_fr)[trk_id][2]
            ci3 = list(event.emtfTrack_ptLUT_bt_ci)[trk_id][2]
        if st4:
            pat4 = list(event.emtfTrack_ptLUT_cpattern)[trk_id][3]
            fr4 = list(event.emtfTrack_ptLUT_fr)[trk_id][3]
            ci4 = list(event.emtfTrack_ptLUT_bt_ci)[trk_id][3]

        trk_pattern_csc[0] = pat1
        trk_pattern_csc[1] = pat2
        trk_pattern_csc[2] = pat3
        trk_pattern_csc[3] = pat4

        trk_fr_emtf[0] = fr1
        trk_fr_emtf[1] = fr2
        trk_fr_emtf[2] = fr3
        trk_fr_emtf[3] = fr4

        trk_chamberid[0] = ci1
        trk_chamberid[1] = ci2
        trk_chamberid[2] = ci3
        trk_chamberid[3] = ci4

        trk_ME11ring[0] = me11_ring

        # Assign track info
        trk_track_endcap[0] = np.asarray(list(event.emtfTrack_endcap)[trk_id])
        trk_track_sector[0] = np.asarray(list(event.emtfTrack_sector)[trk_id])
        trk_track_theta[0] = np.asarray(list(event.emtfTrack_theta_fp)[trk_id])

        # Pack Track Variables
        trk_variables = np.hstack((
            trk_dphi, trk_dphi_sign,
            trk_dtheta, trk_dtheta_sign,
            trk_pattern_csc, trk_fr_emtf, trk_chamberid, trk_ME11ring,
            trk_track_endcap, trk_track_sector, trk_track_theta,
        ))

        # Pack Gen Parameters
        gen_parameters = np.hstack((
            gen_q * gen_pt, gen_dxy,
            gen_eta_st2, gen_phi,
            gen_vx, gen_vy, gen_vz
        ))

        # Append
        self.list_trk_variables.append(trk_variables)
        self.list_gen_parameters.append(gen_parameters)

        # Increase Counter
        self.valid_muon_count += 1

    def merge(self, other):
        self.muon_count += other.muon_count
        self.valid_muon_count += other.valid_muon_count

        self.list_trk_variables += other.list_trk_variables
        self.list_gen_parameters += other.list_gen_parameters

    def post_production(self):
        # Convert to numpy arrays
        trk_variables = np.asarray(self.list_trk_variables)
        gen_parameters = np.asarray(self.list_gen_parameters)

        # Plot Variables
        plot_var_walls('trk_variables_' + self.filename, trk_variables)
        plot_var_walls('gen_parameters_' + self.filename, gen_parameters)

        # Shuffle
        random_index_array = np.arange(trk_variables.shape[0])
        np.random.shuffle(random_index_array)

        trk_variables = trk_variables[random_index_array]
        gen_parameters = gen_parameters[random_index_array]

        # Split Datasets
        total_tracks = int(trk_variables.shape[0])
        train_tracks = int(0.60 * total_tracks)
        quant_tracks = int(0.10 * total_tracks)
        tune_tracks = int(0.10 * total_tracks)

        test_tracks = int(0.10 * total_tracks)
        qtest_tracks = int(0.05 * total_tracks)
        ttest_tracks = total_tracks - train_tracks - quant_tracks - tune_tracks - test_tracks - qtest_tracks

        from_entry = 0
        to_entry = from_entry + train_tracks
        variables_train = trk_variables[from_entry:to_entry]
        parameters_train = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + quant_tracks
        variables_quant = trk_variables[from_entry:to_entry]
        parameters_quant = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + tune_tracks
        variables_tune = trk_variables[from_entry:to_entry]
        parameters_tune = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + test_tracks
        variables_test = trk_variables[from_entry:to_entry]
        parameters_test = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + qtest_tracks
        variables_qtest = trk_variables[from_entry:to_entry]
        parameters_qtest = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + ttest_tracks
        variables_ttest = trk_variables[from_entry:to_entry]
        parameters_ttest = gen_parameters[from_entry:to_entry]

        # Log
        print('\n*********************************************************')
        print('Signal Sample Summary')
        print('*********************************************************')
        print('Muon Count: %d' % self.muon_count)
        print('Valid Muon Count: %d (%0.2f %%)' % (
            self.valid_muon_count, safe_divide(self.valid_muon_count, self.muon_count) * 100))
        print('\nTotal Tracks: %d' % total_tracks)
        print('Train Tracks: %d' % train_tracks)
        print('Quantization Tracks: %d' % quant_tracks)
        print('Tuning Tracks: %d' % tune_tracks)
        print('Test Tracks: %d' % test_tracks)
        print('QTest Tracks: %d' % qtest_tracks)
        print('TTest Tracks: %d' % ttest_tracks)

        # Save
        np.savez_compressed(self.filename + '.npz',
                            variables_train=variables_train, parameters_train=parameters_train,
                            variables_quant=variables_quant, parameters_quant=parameters_quant,
                            variables_tune=variables_tune, parameters_tune=parameters_tune,
                            variables_test=variables_test, parameters_test=parameters_test,
                            variables_qtest=variables_qtest, parameters_qtest=parameters_qtest,
                            variables_ttest=variables_ttest, parameters_ttest=parameters_ttest)
