import os

from ROOT import TFile

from emtf.commands.phase1.gen_ml_signal.analyzers.signal import SignalAnalyzer
from emtf.phase1.analysis.phase1_engine import phase1_run


def configure_parser(parser):
    parser.add_argument('--filename', dest='filename', metavar='FILENAME',
                        type=str, default='sig_ml_sample',
                        help='Output File Name')

    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--no-gen', dest='no_gen_en', metavar='NO_GEN_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help="Indicates that the input files don't have GEN information")


def run(args):
    # Unpack args
    filename = args.filename
    input_files = args.input_files
    max_events = args.max_events
    no_gen_en = args.no_gen_en

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Build Analysis
    main_analyzers = [SignalAnalyzer(filename, no_gen_en)]

    def analysis_fn():
        return [], [SignalAnalyzer(filename, no_gen_en)]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase1_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
