from array import array

import numpy as np
from ROOT import TCanvas
from ROOT import TGraphErrors
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlack

from emtf.commands.phase1.check_performance.config.resolutions_config import ratio_pt_bins, ratio_udxy_bins, \
    histogram_pt_bins, histogram_udxy_bins, method
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.distributions import fit_right_sided_crystalball, fit_landau, fit_gaus
from emtf.core.commons.stats import Simple2DBin, calc_mad
from emtf.core.plotters.basic import RCrystalBallDist1DPlotter
from emtf.phase1.commons.emtf_triggers import sm_prompt_min_qual, sm_disp_min_qual


class ResolutionAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en):
        super().__init__()

        self.displaced_en = displaced_en

        self.pt_error_stats = [Simple2DBin(y_bins=ratio_pt_bins) for i in range(len(histogram_pt_bins) - 1)]
        self.udxy_error_stats = [Simple2DBin(y_bins=ratio_udxy_bins) for i in range(len(histogram_udxy_bins) - 1)]

    def process_entry(self, event):
        # Loop Muons
        for gen_muon in event.muons:
            # Short-Circuit: Should have a GMT muon
            if gen_muon.best_gmt_muon is None:
                continue

            # Get GMT Muon
            gmt_muon = gen_muon.best_gmt_muon

            # Short-Circuit: Include Quality Cut
            min_qual = sm_prompt_min_qual

            if self.displaced_en:
                min_qual = sm_disp_min_qual

            if gmt_muon.qual < min_qual:
                continue

            # Get Track Parameters
            if self.displaced_en:
                gmt_pt = gmt_muon.pt_dxy
                gmt_udxy = gmt_muon.dxy

                if gmt_muon.best_emtf_track is not None:
                    gmt_udxy = gmt_muon.best_emtf_track.dxy
            else:
                gmt_pt = gmt_muon.pt
                gmt_udxy = 0

            # Find bins
            pt_bin = digitize_inclusive(gen_muon.pt, histogram_pt_bins)
            udxy_bin = digitize_inclusive(gen_muon.udxy, histogram_udxy_bins)

            pt_error_stats = self.pt_error_stats[pt_bin]
            udxy_error_stats = self.udxy_error_stats[udxy_bin]

            # Collect Errors
            pt_error_stats.append(gen_muon.pt, gmt_pt / gen_muon.pt)
            udxy_error_stats.append(gen_muon.udxy, gmt_udxy / gen_muon.udxy)

    def merge(self, other):
        for i in range(len(histogram_pt_bins) - 1):
            self.pt_error_stats[i].add(other.pt_error_stats[i])

        for i in range(len(histogram_udxy_bins) - 1):
            self.udxy_error_stats[i].add(other.udxy_error_stats[i])

    def post_production(self):
        # Extract residuals
        pt_bin = digitize_inclusive(20, histogram_pt_bins)

        residuals_pt_plotter = self.checkout_plotter(
            # LandauDist1DPlotter,
            RCrystalBallDist1DPlotter,
            'residuals_pt',
            'Residuals for %0.1f #leq GEN p_{T} < %0.1f' % (
                histogram_pt_bins[pt_bin], histogram_pt_bins[pt_bin + 1]),
            'L1/GEN', 'a.u.',
            xbins=ratio_pt_bins
        )

        for rresidual in self.pt_error_stats[pt_bin].y:
            residuals_pt_plotter.fill(rresidual)

        # Plot all errors
        pt_stats = np.zeros((len(histogram_pt_bins) - 1, 4), dtype='d')
        udxy_stats = np.zeros((len(histogram_udxy_bins) - 1, 4), dtype='d')

        def get_vals(stats):
            if stats.n == 0:
                return 0, 0, 0, 0

            avg_x, std_x = stats.get_x_pos()

            if method == 'right_sided_crystalball':
                avg_residual, std_residual = fit_right_sided_crystalball(stats.y, stats.y_bins)
            elif method == 'landau':
                avg_residual, std_residual = fit_landau(stats.y, stats.y_bins, 1e-3)
            elif method == 'gaus':
                avg_residual, std_residual = fit_gaus(stats.y, stats.y_bins)
            elif method == 'median':
                avg_residual, std_residual = calc_mad(stats.y)
            else:
                raise ValueError('Invalid resolution method')

            return avg_x, std_x, avg_residual, std_residual

        for i in range(len(histogram_pt_bins) - 1):
            pt_stats[i] = get_vals(self.pt_error_stats[i])

        for i in range(len(histogram_udxy_bins) - 1):
            udxy_stats[i] = get_vals(self.udxy_error_stats[i])

        self.plot_errors(
            pt_stats,
            'err_pt', 'Prediction Error',
            'GEN p_{T} [GeV]', 'L1/GEN',
            2, 70, 0, 2)
        self.plot_res(
            pt_stats,
            'res_pt', 'Prediction Resolution',
            'GEN p_{T} [GeV]', '#sigma_{L1/GEN}',
            2, 70, 0, 1,
            var_id=4)

        self.plot_errors(
            udxy_stats,
            'err_udxy', 'Prediction Error',
            'GEN d_{xy} [cm]', 'L1/GEN',
            0, 70, 0, 2)
        self.plot_res(
            udxy_stats,
            'res_udxy', 'Prediction Resolution',
            'GEN d_{xy} [cm]', '#sigma_{L1/GEN}',
            0, 70, 0, 1,
            var_id=4)

    def plot_errors(
            self, coords,
            name, title,
            xlabel, ylabel,
            xmin, xmax,
            ymin, ymax,
            log_en=False
    ):
        # Clean
        coords = coords[~np.isnan(coords[:, 3]), :]

        if len(coords) == 0:
            return

        # Draw Graphs
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        # plot_canvas.SetLogy(log_en)
        plot_canvas.SetLogx(log_en)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            xmin, ymin, xmax, ymax,
            title
        )
        frame.GetXaxis().SetTitle(xlabel)
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetRangeUser(xmin, xmax)
        frame.GetYaxis().SetTitle(ylabel)
        frame.GetYaxis().SetTitleOffset(1.650)
        frame.GetYaxis().SetMaxDigits(3)
        frame.GetYaxis().SetRangeUser(ymin, ymax)

        # Draw Frame
        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        # Draw Graphs
        graph = TGraphErrors(
            len(coords),
            array('d', coords[:, 0]), array('d', coords[:, 2]),
            array('d', coords[:, 1]), array('d', coords[:, 3])
        )
        graph.SetMarkerSize(1)
        graph.SetMarkerStyle(8)
        graph.SetMarkerColor(kBlack)
        graph.Draw('SAME P')

        # GPad
        gPad.Modified()
        gPad.Update()

        # Save
        plot_canvas.SaveAs('%s.png' % name)

    def plot_res(
            self, coords,
            name, title,
            xlabel, ylabel,
            xmin, xmax,
            ymin, ymax,
            var_id=3,
            log_en=False
    ):
        # Clean
        coords = coords[~np.isnan(coords[:, 3]), :]

        if len(coords) == 0:
            return

        # Draw Graphs
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        # plot_canvas.SetLogy(log_en)
        plot_canvas.SetLogx(log_en)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            xmin, ymin, xmax, ymax,
            title
        )
        frame.GetXaxis().SetTitle(xlabel)
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetRangeUser(xmin, xmax)
        frame.GetYaxis().SetTitle(ylabel)
        frame.GetYaxis().SetTitleOffset(1.650)
        frame.GetYaxis().SetMaxDigits(3)
        frame.GetYaxis().SetRangeUser(ymin, ymax)

        # Draw Frame
        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        # Draw Graphs
        graph = TGraphErrors(
            len(coords),
            array('d', coords[:, 0]), array('d', coords[:, var_id]),
            array('d', coords[:, 1]), array('d', np.zeros_like(coords[:, var_id]))
        )
        graph.SetMarkerSize(1)
        graph.SetMarkerStyle(8)
        graph.SetMarkerColor(kBlack)
        graph.Draw('SAME P')

        # GPad
        gPad.Modified()
        gPad.Update()

        # Save
        plot_canvas.SaveAs('%s.png' % name)
