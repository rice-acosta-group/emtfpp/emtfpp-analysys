from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.kinematics import calc_qinvpt_from_qpt
from emtf.core.commons.tools import safe_divide
from emtf.core.commons.variables import *
from emtf.core.plotters.basic import *
from emtf.phase1.commons.emtf_triggers import sm_prompt_min_qual, sm_disp_min_qual


class CorrelationAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en):
        super().__init__()

        self.displaced_en = displaced_en

        # Plots
        self.vld_q_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_q', 'Emulator Validation',
            'GEN q', 'L1 q',
            xbins=emu_val_q_bins,
            ybins=emu_val_q_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            show_text=True,
            logz=True)

        self.vld_pt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt', 'Emulator Validation',
            'GEN p_{T} [GeV]', 'L1 p_{T} [GeV]',
            xbins=emu_val_pt_bins,
            ybins=emu_val_pt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_pt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_err', 'Emulator Validation',
            'GEN p_{T} [GeV]', 'L1/GEN',
            xbins=emu_val_pt_bins,
            ybins=uniform_step_axis(0.02, 0, 2),
            min_val=emu_val_mismatch_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_invpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_invpt', 'Emulator Validation',
            'GEN 1/p_{T} [GeV^{-1}]', 'L1 1/p_{T} [GeV^{-1}]',
            xbins=emu_val_invpt_bins,
            ybins=emu_val_invpt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logx=True, logy=True, logz=True)

        self.vld_invpt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_invpt_err', 'Emulator Validation',
            'GEN 1/p_{T} [GeV^{-1}]', 'L1/GEN',
            xbins=emu_val_invpt_bins,
            ybins=uniform_step_axis(0.02, 0, 2),
            min_val=emu_val_mismatch_threshold,
            density=False,
            normalize_by='column',
            logx=True, logy=False, logz=True)

        self.vld_qpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt', 'Emulator Validation',
            'GEN q #upoint p_{T} [GeV]', 'L1 q #upoint p_{T} [GeV]',
            xbins=emu_val_qpt_bins,
            ybins=emu_val_qpt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_qinvpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qinvpt', 'Emulator Validation',
            'GEN q/p_{T} [GeV^{-1}]', 'L1 q/p_{T} [GeV^{-1}]',
            xbins=emu_val_qinvpt_bins,
            ybins=emu_val_qinvpt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_udxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_udxy', 'Emulator Validation',
            'GEN d_{xy} [cm]', 'L1 d_{xy} [cm]',
            xbins=emu_val_udxy_bins,
            ybins=emu_val_udxy_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_udxy_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_udxy_err', 'Emulator Validation',
            'GEN d_{xy} [cm]', 'L1/GEN',
            xbins=emu_val_udxy_bins,
            ybins=uniform_step_axis(0.02, 0, 2),
            min_val=emu_val_mismatch_threshold,
            density=False,
            normalize_by='column',
            logz=True)

    def process_entry(self, event):
        # Loop Muons
        for gen_muon in event.muons:
            # Short-Circuit: Should have a track
            if gen_muon.best_gmt_muon is None:
                continue

            # Get GMT Muon
            gmt_muon = gen_muon.best_gmt_muon

            # Short-Circuit: Include Quality Cut
            min_qual = sm_prompt_min_qual

            if self.displaced_en:
                min_qual = sm_disp_min_qual

            if gmt_muon.qual < min_qual:
                continue

            # Unpack Gen Info
            gen_qpt = gen_muon.q * gen_muon.pt
            gen_qinvpt = gen_muon.q * gen_muon.invpt

            # Get Track Parameters
            if self.displaced_en:
                gmt_pt = gmt_muon.pt_dxy
                gmt_udxy = gmt_muon.dxy

                if gmt_muon.best_emtf_track is not None:
                    gmt_udxy = gmt_muon.best_emtf_track.dxy
            else:
                gmt_pt = gmt_muon.pt
                gmt_udxy = 0

            gmt_qpt = gmt_muon.q * gmt_pt
            gmt_qinvpt = calc_qinvpt_from_qpt(gmt_qpt)
            gmt_invpt = abs(gmt_qinvpt)

            # Short-Circuit: Include Zero-Pt Cut (Pt < 2)
            if abs(gmt_qpt) == 0:
                continue

            # Check how big the differences are
            err_pt = safe_divide(gmt_pt, gen_muon.pt)
            err_invpt = safe_divide(gmt_invpt, gen_muon.invpt)
            err_udxy = safe_divide(gmt_udxy, gen_muon.udxy)

            # Fill correlations
            self.vld_q_plotter.fill(gen_muon.q, gmt_muon.q)

            self.vld_pt_plotter.fill(gen_muon.pt, gmt_pt)
            self.vld_pt_err_plotter.fill(gen_muon.pt, err_pt)

            self.vld_invpt_plotter.fill(gen_muon.invpt, gmt_invpt)
            self.vld_invpt_err_plotter.fill(gen_muon.invpt, err_invpt)

            self.vld_qpt_plotter.fill(gen_qpt, gmt_qpt)
            self.vld_qinvpt_plotter.fill(gen_qinvpt, gmt_qinvpt)

            self.vld_udxy_plotter.fill(gen_muon.udxy, gmt_udxy)
            self.vld_udxy_err_plotter.fill(gen_muon.udxy, err_udxy)

    def merge(self, other):
        self.vld_q_plotter.add(other.vld_q_plotter)
        self.vld_pt_plotter.add(other.vld_pt_plotter)
        self.vld_pt_err_plotter.add(other.vld_pt_err_plotter)
        self.vld_invpt_plotter.add(other.vld_invpt_plotter)
        self.vld_invpt_err_plotter.add(other.vld_invpt_err_plotter)
        self.vld_qpt_plotter.add(other.vld_qpt_plotter)
        self.vld_qinvpt_plotter.add(other.vld_qinvpt_plotter)
        self.vld_udxy_plotter.add(other.vld_udxy_plotter)
        self.vld_udxy_err_plotter.add(other.vld_udxy_err_plotter)

    def post_production(self):
        pass
