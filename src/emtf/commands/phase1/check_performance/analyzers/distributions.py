from emtf.commands.phase1.check_performance.config.distributions_config import disp_trigger, prompt_trigger
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.tools import nz_sign
from emtf.core.commons.variables import *
from emtf.core.plotters.basic import *
from emtf.phase1.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop


class DistributionAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en):
        super().__init__()

        # Flags
        self.displaced_en = displaced_en

        # Gen Stats
        self.gen_q_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_q',
            'GEN Particle',
            'GEN Charge', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.gen_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_pt',
            'GEN Particle',
            'GEN p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            density=True,
            logy=True)

        self.gen_d0_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_d0_sign',
            'GEN Particle',
            'GEN Sgn(d_{0})', 'a.u.',
            xbins=emu_val_d0_sign_bins,
            density=True,
            logy=True)

        self.gen_d0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_d0',
            'Sample Validation',
            'GEN d_{0} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            density=True,
            logy=True)

        self.gen_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lxy',
            'Sample Validation',
            'GEN L_{xy} [cm]', 'a.u.',
            xbins=emu_val_lxy_bins,
            density=True,
            logy=True)

        self.gen_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lz',
            'Sample Validation',
            'GEN L_{z} [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.gen_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi',
            'GEN Particle',
            'GEN #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            logy=True,
            density=True)

        self.gen_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta',
            'GEN Particle',
            'GEN |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.gen_eta_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta_st2',
            'GEN Particle',
            'GEN |#eta_{st2}|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.gen_trk_dR_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_trk_dR',
            'GEN Particle - EMTF Track',
            '#Delta R', 'a.u.',
            xbins=np.linspace(0, 1, 100),
            logy=True,
            density=True)

        self.gen_gmt_dR_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_gmt_dR',
            'GEN Particle - GMT',
            '#Delta R', 'a.u.',
            xbins=np.linspace(0, 1, 100),
            logy=True,
            density=True)

        self.gen_q_vs_d0_plot = self.checkout_plotter(
            CorrelationPlotter,
            'gen_q_vs_d0',
            'Gen Particle',
            'd0', 'q',
            xbins=emu_val_d0_bins,
            ybins=emu_val_q_bins,
            min_val=1e-5,
            logz=True
        )

        # Track Stats
        self.trk_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_pt',
            'EMTF Tracks',
            'L1 p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            logy=True,
            density=True)

        self.trk_pt_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_pt_dxy',
            'EMTF Tracks',
            'L1 p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            logy=True,
            density=True)

        self.trk_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dxy',
            'EMTF Tracks',
            'L1 d_{xy} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            logy=True,
            density=True)

        self.trk_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_phi',
            'EMTF Tracks',
            'L1 #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            logy=True,
            density=True)

        self.trk_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_eta',
            'EMTF Tracks',
            'L1 |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.trk_qual_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_qual',
            'EMTF Tracks',
            'Track Quality', 'a.u.',
            xbins=emu_val_trk_qual_p1_bins,
            density=True,
            logy=True)

    def process_entry(self, event):
        # Get Trigger
        if self.displaced_en:
            trigger = disp_trigger
        else:
            trigger = prompt_trigger

        # Loop GMT Muons
        any_track_passed = False

        for track in event.emtf_tracks:
            # Short-Circuit: Only use valid tracks
            if track.qual == 0:
                continue

            # Short-Circuit: Must be in eta region
            if not (emtf_eta_start <= abs(track.eta) < emtf_eta_stop):
                continue

            # Collect track info
            trk_entry = {
                'trk_qual': track.qual,
                'trk_eta': track.eta,
                'trk_pt': track.pt,
                'trk_pt_dxy': track.pt_dxy,
                'trk_dxy': track.dxy,
            }

            # Short-Circuit: Track didn't pass
            if not trigger(trk_entry):
                continue

            # Fill Track Plots
            any_track_passed = True

            self.trk_pt_plotter.fill(track.pt)
            self.trk_pt_dxy_plotter.fill(track.pt_dxy)
            self.trk_dxy_plotter.fill(track.dxy)
            self.trk_phi_plotter.fill(np.rad2deg(track.phi))
            self.trk_eta_plotter.fill(abs(track.eta))
            self.trk_qual_plotter.fill(track.qual)

        # Short-Circuit: No tracks passed
        if not any_track_passed:
            return

        # Loop Muons
        for gen_muon in event.muons:
            # Fill GEN Plots
            self.gen_q_plotter.fill(gen_muon.q)
            self.gen_pt_plotter.fill(gen_muon.pt)
            self.gen_d0_plotter.fill(gen_muon.d0)
            self.gen_d0_sign_plotter.fill(nz_sign(gen_muon.d0))
            self.gen_lxy_plotter.fill(gen_muon.lxy)
            self.gen_lz_plotter.fill(abs(gen_muon.vz))
            self.gen_phi_plotter.fill(np.rad2deg(gen_muon.phi))
            self.gen_eta_plotter.fill(abs(gen_muon.eta))
            self.gen_eta_st2_plotter.fill(abs(gen_muon.eta_st2))
            self.gen_q_vs_d0_plot.fill(gen_muon.d0, gen_muon.q)

            # Track dR
            if gen_muon.best_emtf_track is not None:
                self.gen_trk_dR_plotter.fill(gen_muon.best_emtf_track_dR)

            # GMT dR
            if gen_muon.best_gmt_muon is not None:
                self.gen_gmt_dR_plotter.fill(gen_muon.best_gmt_muon_dR)

    def merge(self, other):
        self.gen_q_plotter.add(other.gen_q_plotter)
        self.gen_pt_plotter.add(other.gen_pt_plotter)
        self.gen_d0_plotter.add(other.gen_d0_plotter)
        self.gen_d0_sign_plotter.add(other.gen_d0_sign_plotter)
        self.gen_lxy_plotter.add(other.gen_lxy_plotter)
        self.gen_lz_plotter.add(other.gen_lz_plotter)
        self.gen_phi_plotter.add(other.gen_phi_plotter)
        self.gen_eta_plotter.add(other.gen_eta_plotter)
        self.gen_eta_st2_plotter.add(other.gen_eta_st2_plotter)
        self.gen_trk_dR_plotter.add(other.gen_trk_dR_plotter)
        self.gen_gmt_dR_plotter.add(other.gen_gmt_dR_plotter)
        self.gen_q_vs_d0_plot.add(other.gen_q_vs_d0_plot)

        self.trk_pt_plotter.add(other.trk_pt_plotter)
        self.trk_pt_dxy_plotter.add(other.trk_pt_dxy_plotter)
        self.trk_dxy_plotter.add(other.trk_dxy_plotter)
        self.trk_phi_plotter.add(other.trk_phi_plotter)
        self.trk_eta_plotter.add(other.trk_eta_plotter)
        self.trk_qual_plotter.add(other.trk_qual_plotter)

    def post_production(self):
        pass
