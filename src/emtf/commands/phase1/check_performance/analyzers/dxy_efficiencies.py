from ROOT import TCanvas
from ROOT import TLegend
from ROOT import TLine
from ROOT import gPad
from ROOT import kAzure
from ROOT import kBlack
from ROOT import kFullDotLarge

from emtf.commands.phase1.check_performance.config.dxy_config import disp_eff_definitions
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.variables import *
from emtf.core.plotters.basic import *
from emtf.core.root.environment import cm_tab10
from emtf.phase1.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop
from emtf.phase1.commons.emtf_triggers import sm_disp_min_qual, sm_base_trg


class DxyEfficiencyAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en, include_tb_eff, only_matched_eff):
        super().__init__()

        # Flags
        self.displaced_en = displaced_en
        self.include_tb_eff = include_tb_eff
        self.only_matched_eff = only_matched_eff

        # Init pT Eff Plots
        self.eff_vs_pt_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_pt_%s' % eff_definition['name'], '',
                'GEN p_{T} [GeV]', 'Efficiency',
                xbins=pt_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_pt_plots[eff_definition['name']] = eff_plot

        # Init Eta Eff Plots
        self.eff_vs_eta_st2_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_eta_st2_%s' % eff_definition['name'], '',
                'GEN |#eta_{st2}|', 'Efficiency',
                xbins=eta_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_eta_st2_plots[eff_definition['name']] = eff_plot

        # Init Dxy Eff Plots
        self.eff_vs_dxy_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_dxy_%s' % eff_definition['name'], '',
                'GEN d_{xy} [cm]', 'Efficiency',
                xbins=udxy_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_dxy_plots[eff_definition['name']] = eff_plot

        # Init z0 Eff Plots
        self.eff_vs_z0_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_z0_%s' % eff_definition['name'], '',
                'GEN z_{0} [cm]', 'Efficiency',
                xbins=uz0_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_z0_plots[eff_definition['name']] = eff_plot

        # Init Lxy Eff Plots
        self.eff_vs_lxy_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_lxy_%s' % eff_definition['name'], '',
                'GEN L_{xy} [cm]', 'Efficiency',
                xbins=lxy_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_lxy_plots[eff_definition['name']] = eff_plot

        # Init Lz Eff Plots
        self.eff_vs_lz_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_lz_%s' % eff_definition['name'], '',
                'GEN L_{z} [cm]', 'Efficiency',
                xbins=lz_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_lz_plots[eff_definition['name']] = eff_plot

        # Init Lxyz Eff Plots
        self.eff_vs_lxyz_plots = dict()

        for eff_definition in disp_eff_definitions:
            eff_plot = self.checkout_plotter(
                EfficiencyPlotter,
                'dxy_eff_vs_lz_%s' % eff_definition['name'], '',
                'GEN L_{xyz} [cm]', 'Efficiency',
                xbins=lz_eff_bins)

            eff_plot.write_en = False

            self.eff_vs_lxyz_plots[eff_definition['name']] = eff_plot

    def process_entry(self, event):
        # Loop Muons
        for gen_muon in event.muons:
            # Pack Entry
            gen_entry = {
                'gen_bx': 0,
                'gen_pt': gen_muon.pt,
                'gen_d0': gen_muon.d0,
                'gen_eta_st2': gen_muon.eta_st2,
            }

            # Loop Tracks
            any_track_passed = dict()

            # Init State
            if self.include_tb_eff:
                for eff_definition in disp_eff_definitions:
                    # Short-Circuit: Must pass denominator
                    passes_denominator = eff_definition['denominator'](gen_entry)

                    if not passes_denominator:
                        continue

                    # Initialize as false
                    any_track_passed[eff_definition['name']] = False

            # Loop Tracks
            for track in event.gmt_muons:
                # Short-Circuit: Should be from same endcap
                if gen_muon.endcap != track.endcap:
                    continue

                # Short-Circuit: Only use valid tracks
                if track.qual == 0:
                    continue

                # Short-Circuit: Must be in eta region
                if not (emtf_eta_start <= abs(track.eta) < emtf_eta_stop):
                    continue

                if self.only_matched_eff:
                    # Short-Circuit: Has valid dR
                    gmt_dR = math.hypot(
                        gen_muon.phi_st2 - track.phi,
                        gen_muon.eta_st2 - track.eta
                    )

                    if gmt_dR > 0.4:
                        continue

                    # Short-Circuit: Include Quality Cut
                    gmt_qual = track.qual

                    if gmt_qual < sm_disp_min_qual:
                        continue

                # Pack Entry
                trk_entry = {
                    'trk_qual': track.qual,
                    'trk_eta': track.eta,
                    'trk_pt': track.pt,
                    'trk_pt_dxy': track.pt_dxy,
                    'trk_dxy': track.dxy,
                }

                # Fill Efficiency Plots
                for eff_definition in disp_eff_definitions:
                    # Short-Circuit: Must pass denominator
                    passes_denominator = eff_definition['denominator'](gen_entry)

                    if not passes_denominator:
                        continue

                    if self.displaced_en:
                        passes_numerator = sm_base_trg(trk_entry) or \
                                           eff_definition['numerator'](trk_entry)
                    else:
                        passes_numerator = eff_definition['numerator'](trk_entry)

                    # 'OR' numerator passes
                    eff_name = eff_definition['name']
                    trg_state = any_track_passed.get(eff_name, False)
                    any_track_passed[eff_name] = (trg_state or passes_numerator)

            # Fill Plots
            for eff_name, passed in any_track_passed.items():
                # Eff vs pt
                eff_plotter = self.eff_vs_pt_plots[eff_name]
                eff_plotter.fill(passed, gen_muon.pt)

                # Eff vs eta
                eff_plotter = self.eff_vs_eta_st2_plots[eff_name]
                eff_plotter.fill(passed, abs(gen_muon.eta_st2))

                # Eff vs dxy
                eff_plotter = self.eff_vs_dxy_plots[eff_name]
                eff_plotter.fill(passed, gen_muon.udxy)

                # Eff vs z0
                eff_plotter = self.eff_vs_z0_plots[eff_name]
                eff_plotter.fill(passed, abs(gen_muon.z0))

                # Eff vs lxy
                eff_plotter = self.eff_vs_lxy_plots[eff_name]
                eff_plotter.fill(passed, gen_muon.lxy)

                # Eff vs lz
                eff_plotter = self.eff_vs_lz_plots[eff_name]
                eff_plotter.fill(passed, abs(gen_muon.vz))

                # Eff vs lxyz
                eff_plotter = self.eff_vs_lxyz_plots[eff_name]
                eff_plotter.fill(passed, gen_muon.lxyz)

    def merge(self, other):
        for key, plotter in self.eff_vs_pt_plots.items():
            plotter.add(other.eff_vs_pt_plots[key])

        for key, plotter in self.eff_vs_eta_st2_plots.items():
            plotter.add(other.eff_vs_eta_st2_plots[key])

        for key, plotter in self.eff_vs_dxy_plots.items():
            plotter.add(other.eff_vs_dxy_plots[key])

        for key, plotter in self.eff_vs_z0_plots.items():
            plotter.add(other.eff_vs_z0_plots[key])

        for key, plotter in self.eff_vs_lxy_plots.items():
            plotter.add(other.eff_vs_lxy_plots[key])

        for key, plotter in self.eff_vs_lz_plots.items():
            plotter.add(other.eff_vs_lz_plots[key])

        for key, plotter in self.eff_vs_lxyz_plots.items():
            plotter.add(other.eff_vs_lxyz_plots[key])

    def post_production(self):
        self.plot_efficiencies(
            '',
            lambda eff_def: 'denom_eta_st2_ge_1p2_lt_2p4' in eff_def['name'],
            lambda eff_def: eff_def['num_label'],
            dxy_plot_en=True
        )

        if self.only_matched_eff:
            self.plot_efficiencies(
                '',
                lambda eff_def: 'denom_dxy_ge_' in eff_def['name']
                                and '_eta_st2_ge_1p2_lt_2p4' in eff_def['name'],
                lambda eff_def: eff_def['num_label'],
                pt_plot_en=True,
                z0_plot_en=True,
                lxy_plot_en=True,
                lz_plot_en=True,
                lxyz_plot_en=True,
                eta_st2_plot_en=True
            )
        else:
            self.plot_efficiencies(
                '',
                lambda eff_def: 'denom_eta_st2_ge_1p2_lt_2p4' in eff_def['name'],
                lambda eff_def: eff_def['num_label'],
                pt_plot_en=True,
                z0_plot_en=True,
                lxy_plot_en=True,
                lz_plot_en=True,
                lxyz_plot_en=True,
                eta_st2_plot_en=True
            )

        self.plot_efficiencies(
            '_dxy_ge_16cm',
            lambda eff_def: 'denom_eta_st2_' in eff_def['name']
                            and '_eta_st2_ge_1p2_lt_2p4' not in eff_def['name']
                            and 'num_dxy_ge_16' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'L1 d_{xy} #geq 16 cm',
            dxy_plot_en=True,
            legend_columns=2
        )

        self.plot_efficiencies(
            '_dxy_ge_32cm',
            lambda eff_def: 'denom_eta_st2_' in eff_def['name']
                            and '_eta_st2_ge_1p2_lt_2p4' not in eff_def['name']
                            and 'num_dxy_ge_32' in eff_def['name'],
            lambda eff_def: eff_def['denom_label'],
            'L1 d_{xy} #geq 32 cm',
            dxy_plot_en=True,
            legend_columns=2
        )

    # Utils
    def plot_efficiencies(self,
                          suffix,
                          group_pred,
                          label_factory,
                          comment=None,
                          pt_plot_en=False,
                          dxy_plot_en=False,
                          z0_plot_en=False,
                          lxy_plot_en=False,
                          lz_plot_en=False,
                          lxyz_plot_en=False,
                          eta_st2_plot_en=False,
                          legend_columns=4):
        #####################################################################
        # Selection
        #####################################################################
        selected_definitions = []

        for eff_definition in disp_eff_definitions:
            if not group_pred(eff_definition):
                continue

            selected_definitions.append(eff_definition)

        #####################################################################
        # Efficiency vs pT
        #####################################################################
        if pt_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_pt_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(pt_eff_bins[0], 0, pt_eff_bins[-1], 1.15,
                                          'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN p_{T} [GeV]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_pt_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)

                eff_plot.Draw('L P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(pt_eff_bins), 0.9, max(pt_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            perc50_line = TLine(min(pt_eff_bins), 0.5, max(pt_eff_bins), 0.5)
            perc50_line.SetLineStyle(10)
            perc50_line.SetLineWidth(5)
            perc50_line.SetLineColor(kAzure + 2)
            perc50_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_pt' + suffix + '.png')

        #####################################################################
        # Efficiency vs dxy
        #####################################################################
        if dxy_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_dxy_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(udxy_eff_bins[0], 0, udxy_eff_bins[-1], 1.15,
                                          'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN d_{xy} [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_dxy_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)
                eff_plot.Draw('L P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(udxy_eff_bins), 0.9, max(udxy_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_dxy' + suffix + '.png')

        #####################################################################
        # Efficiency vs z0
        #####################################################################
        if z0_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_z0_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_z0_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(uz0_eff_bins[0], 0, uz0_eff_bins[-1], 1.15, 'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN z_{0} [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_z0_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)
                eff_plot.Draw('L P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(uz0_eff_bins), 0.9, max(uz0_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_z0' + suffix + '.png')

        #####################################################################
        # Eff vs lxy
        #####################################################################
        if lxy_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_lxy_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_lxy_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(lxy_eff_bins[0], 0, lxy_eff_bins[-1], 1.15,
                                          'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN L_{xy} [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_lxy_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)
                eff_plot.Draw('L P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(lxy_eff_bins), 0.9, max(lxy_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_lxy' + suffix + '.png')

        #####################################################################
        # Eff vs lz
        #####################################################################
        if lz_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_lz_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_lz_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(lz_eff_bins[0], 0, lz_eff_bins[-1], 1.15,
                                          'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN L_{z} [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_lz_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)
                eff_plot.Draw('L P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(lz_eff_bins), 0.9, max(lz_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_lz' + suffix + '.png')

        #####################################################################
        # Eff vs lxyz
        #####################################################################
        if lxyz_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_lxyz_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_lxyz_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(lxyz_eff_bins[0], 0, lxyz_eff_bins[-1], 1.15,
                                          'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN L_{xyz} [cm]')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_lxyz_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)
                eff_plot.Draw('L P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(lxyz_eff_bins), 0.9, max(lxyz_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_lxyz' + suffix + '.png')

        #####################################################################
        # Eff vs eta
        #####################################################################
        if eta_st2_plot_en:
            plot_canvas = TCanvas('dxy_eff_vs_eta_st2_1d', '')
            plot_canvas.SetLeftMargin(0.175)
            plot_canvas.SetBottomMargin(0.300)
            plot_canvas.cd()

            # Init Eff Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_eta_st2_plots[eff_definition['name']].plot
                eff_plot.Paint("")
                gPad.Update()

            # Draw
            frame = plot_canvas.DrawFrame(eta_eff_bins[0], 0, eta_eff_bins[-1], 1.15,
                                          'Emulator Validation')
            frame.GetXaxis().SetTitle('GEN |#eta_{st2}|')
            frame.GetXaxis().SetTitleOffset(1.350)
            frame.GetYaxis().SetTitle('Efficiency')
            frame.GetYaxis().SetTitleOffset(1.450)
            frame.GetYaxis().SetMaxDigits(3)

            legend_x0 = 0.125
            legend_y0 = 0.025

            legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.750, legend_y0 + 0.100)
            legend.SetNColumns(legend_columns)
            legend.SetMargin(0.250)
            legend.SetFillColorAlpha(kBlack, 0.0)
            legend.SetBorderSize(0)
            legend.SetTextSize(.030)

            cm = iter(cm_tab10)

            # Stack Plots
            for eff_definition in selected_definitions:
                eff_plot = self.eff_vs_eta_st2_plots[eff_definition['name']].plot.GetPaintedGraph()

                color = next(cm)
                eff_plot.SetMarkerColor(color)
                eff_plot.SetMarkerSize(1)
                eff_plot.SetMarkerStyle(kFullDotLarge)
                eff_plot.SetLineColor(color)
                eff_plot.SetLineWidth(2)
                eff_plot.Draw('P SAME')

                legend.AddEntry(eff_plot, label_factory(eff_definition), 'P')

            frame.Draw('SAME AXIS')
            frame.Draw('SAME AXIG')

            # Draw 90% Line
            perc90_line = TLine(min(eta_eff_bins), 0.9, max(eta_eff_bins), 0.9)
            perc90_line.SetLineStyle(10)
            perc90_line.SetLineWidth(5)
            perc90_line.SetLineColor(kBlack)
            perc90_line.Draw('SAME')

            legend.Draw()

            draw_fancy_label(0.215, 0.835)

            if comment is not None:
                draw_fancy_label(0.675, 0.835, text=comment)

            gPad.Modified()
            gPad.Update()

            plot_canvas.SaveAs('dxy_eff_vs_eta_st2' + suffix + '.png')
