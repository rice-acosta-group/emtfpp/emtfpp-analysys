import array
import math

from ROOT import TCanvas
from ROOT import TGraphAsymmErrors
from ROOT import TH1F
from ROOT import TLegend
from ROOT import TLine
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kSpring

from emtf.commands.phase1.check_performance.config.triggers_config import prompt_triggers, disp_triggers, \
    prompt_labels, disp_labels
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.roc import ConfusionMatrix
from emtf.phase1.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop
from emtf.phase1.commons.emtf_triggers import sm_disp_min_qual, sm_prompt_min_qual, sm_base_trg


class IdentityWrapper(object):

    def __init__(self):
        pass

    def __call__(self, x, par):
        return x[0]


class TriggerAnalyzer(AbstractAnalyzer):

    def __init__(self, rates, displaced_en, include_tb_eff, only_matched_eff):
        super().__init__()

        # Flags
        self.rates = rates
        self.displaced_en = displaced_en
        self.include_tb_eff = include_tb_eff
        self.only_matched_eff = only_matched_eff

        # Select Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        # Build Categories
        self.categories = dict()
        self.category_styles = dict()

        for name, trigger in triggers.items():
            self.categories[name] = ConfusionMatrix(include_tb_eff)
            self.category_styles[name] = (trigger['color'], trigger['marker'])

    def process_entry(self, event):
        # Select Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        # Loop Muons
        for gen_muon in event.muons:
            # Pack Entry
            gen_entry = {
                'gen_bx': 0,
                'gen_pt': gen_muon.pt,
                'gen_d0': gen_muon.d0,
                'gen_eta_st2': gen_muon.eta_st2,
            }

            # Process GEN Info
            for category_name, category in self.categories.items():
                gen_pass = triggers[category_name]['gen_cond'](gen_entry)
                category.on_truth(gen_pass)

            # Loop Tracks
            for track in event.gmt_muons:
                # Short-Circuit: Should be from same endcap
                if gen_muon.endcap != track.endcap:
                    continue

                # Short-Circuit: Only use valid tracks
                if track.qual == 0:
                    continue

                # Short-Circuit: Must be in eta region
                if not (emtf_eta_start <= abs(track.eta) < emtf_eta_stop):
                    continue

                if self.only_matched_eff:
                    # Short-Circuit: Has valid dR
                    gmt_dR = math.hypot(
                        gen_muon.phi_st2 - track.phi,
                        gen_muon.eta_st2 - track.eta
                    )

                    if gmt_dR > 0.4:
                        continue

                    # Short-Circuit: Include Quality Cut
                    min_qual = sm_prompt_min_qual

                    if self.displaced_en:
                        min_qual = sm_disp_min_qual

                    trk_qual = track.qual

                    if trk_qual < min_qual:
                        continue

                # Pack Entry
                trk_entry = {
                    'trk_qual': track.qual,
                    'trk_eta': track.eta,
                    'trk_pt': track.pt,
                    'trk_pt_dxy': track.pt_dxy,
                    'trk_dxy': track.dxy,
                }

                # Process Tracks
                for category_name, category in self.categories.items():
                    if self.displaced_en:
                        trk_pass = sm_base_trg(trk_entry) or \
                                   triggers[category_name]['trk_cond'](trk_entry)
                    else:
                        trk_pass = triggers[category_name]['trk_cond'](trk_entry)

                    category.on_prediction(trk_pass)

            # Fill entry
            for category_name, category in self.categories.items():
                category.fill()

    def merge(self, other):
        for category_name, roc_category in self.categories.items():
            roc_category.add(other.categories[category_name])

    def post_production(self):
        # Build TGraphs
        cat_col = list()
        graph_col = list()
        results = list()

        for category_name, category in self.categories.items():
            # True positive rate
            p_events = category.n_truth_pass
            tp_events = category.tp_events

            tpr, tpr_elow, tpr_eup = category.calc_tpr()

            # False positive rate
            n_events = category.n_truth_fail
            fp_events = category.fp_events

            fpr, fpr_elow, fpr_eup = category.calc_fpr()

            results.append({
                'name': category_name,
                'tpr': tpr,
                'tpr_eup': tpr_eup,
                'tpr_elow': tpr_elow,
                'fpr': fpr,
                'fpr_eup': fpr_eup,
                'fpr_elow': fpr_elow,
                'p_events': p_events,
                'tp_events': tp_events,
                'n_events': n_events,
                'fp_events': fp_events,
            })

            # Floor
            if tpr < 1e-2:
                tpr = 1e-2

            # Create Graph
            rate_data = self.rates.get(category_name, None)

            if rate_data is None:
                continue

            rate = rate_data['rate']
            error = rate_data['error']

            cat_col.append(category_name)
            graph_col.append(TGraphAsymmErrors(
                1,
                array.array('d', [rate]), array.array('d', [tpr]),
                array.array('d', [error]), array.array('d', [error]),
                array.array('d', [tpr_elow]), array.array('d', [tpr_eup])
            ))

        # Dump FPR and TPR
        with open('trigger_roc.csv', 'w') as out:
            out.write('trigger, tpr, tpr_elow, tpr_eup, p_evt, tp_evt\n')

            for result in results:
                name = result['name']
                tpr = result['tpr']
                tpr_eup = result['tpr_eup']
                tpr_elow = result['tpr_elow']
                p_events = result['p_events']
                tp_events = result['tp_events']

                bin_line = ('%s, %f, %f, %f, %d, %d\n' % (
                    name, tpr, tpr_elow, tpr_eup,
                    p_events, tp_events
                ))

                # Write to file
                out.write(bin_line)

        # Plot Constants
        min_x = 10.
        max_x = 70.
        min_y = 1e-2
        max_y = 1.
        target_x = 40.
        target_y = 0.9

        # Draw Graphs
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas('roc', '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.300)
        # plot_canvas.SetLogx(1)
        # plot_canvas.SetLogy(1)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            min_x, min_y, max_x, max_y,
            'Emulator Analysis'
        )
        frame.GetXaxis().SetTitle('Rate@200PU [kHz]')
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetRangeUser(min_x, max_x)
        frame.GetYaxis().SetTitle('Signal Acceptance')
        frame.GetYaxis().SetTitleOffset(1.650)
        frame.GetYaxis().SetMaxDigits(3)
        frame.GetYaxis().SetRangeUser(min_y, max_y)

        # Draw Frame
        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        # Draw x Target
        x_target_line = TLine(target_x, min_y, target_x, max_y)
        x_target_line.SetLineStyle(10)
        x_target_line.SetLineWidth(5)
        x_target_line.SetLineColor(kSpring + 4)
        x_target_line.Draw('SAME')

        # Draw y Target
        y_target_line = TLine(min_x, target_y, max_x, target_y)
        y_target_line.SetLineStyle(10)
        y_target_line.SetLineWidth(5)
        y_target_line.SetLineColor(kSpring + 4)
        y_target_line.Draw('SAME')

        # Draw Graphs
        for cat_id in range(len(cat_col)):
            name = cat_col[cat_id]
            graph = graph_col[cat_id]
            color, marker = self.category_styles[name]

            graph.SetMarkerSize(2.25)
            graph.SetMarkerStyle(marker)
            graph.SetMarkerColor(color)
            graph.Draw('SAME P')

        # Draw Legend
        legend_x0 = 0.200
        legend_y0 = 0.025

        legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.650, legend_y0 + 0.125)
        legend.SetNColumns(5)
        legend.SetMargin(0.250)
        legend.SetFillColorAlpha(kBlack, 0.0)
        legend.SetBorderSize(0)
        legend.SetTextSize(.030)

        # Select Labels
        labels = prompt_labels

        if self.displaced_en:
            labels = disp_labels

        # Stack Plots
        label_plots = list()

        for label in labels:
            label_plot = TH1F("", "", 1, 0, 1)
            label_plots.append(label_plot)

            label_plot.SetMarkerColor(label['color'])
            label_plot.SetMarkerSize(2)
            label_plot.SetMarkerStyle(label['marker'])
            label_plot.SetLineColor(label['color'])
            label_plot.SetLineWidth(2)

            legend.AddEntry(label_plot, label['text'], 'P')

        legend.Draw()

        # GPad
        gPad.Modified()
        gPad.Update()

        # Save
        plot_canvas.SaveAs('trigger_roc.png')
