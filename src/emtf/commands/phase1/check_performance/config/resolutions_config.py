from emtf.core.commons.binning import uniform_step_axis

method = 'right_sided_crystalball'

histogram_pt_bins = uniform_step_axis(2, 2, 70)
histogram_udxy_bins = uniform_step_axis(10, 0, 70)

ratio_pt_bins = uniform_step_axis(0.02, 0, 2)
ratio_udxy_bins = uniform_step_axis(0.02, 0, 2)
