from emtf.core.commons.variables import emtf_pt_thresholds
from emtf.phase1.commons.emtf_eta_ranges import emtf_extended_zone_eta_ranges, emtf_eta_start, emtf_eta_stop
from emtf.phase1.commons.emtf_triggers import new_gen_condition, \
    sm_disp_min_qual, sm_prompt_min_qual, sm_disp_min_dxy, new_trk_condition

prompt_eff_definitions = list()
disp_eff_definitions = list()

for eta_range in emtf_extended_zone_eta_ranges:
    eta_low = eta_range[0]
    eta_up = eta_range[1]

    for pt_threshold in emtf_pt_thresholds:
        name = 'num_pt_ge_%d' % pt_threshold
        name += '_denom_eta_st2_ge_%s_lt_%s' % (str(eta_low).replace('.', 'p'), str(eta_up).replace('.', 'p'))

        num_label = 'p_{T} #geq %d GeV' % pt_threshold
        denom_label = '%0.2f #leq |#eta_{st2}| < %0.2f' % (eta_low, eta_up)

        prompt_eff_definitions.append({
            'name': name,
            'num_label': num_label,
            'denom_label': denom_label,
            'numerator': new_trk_condition(
                min_qual=sm_prompt_min_qual,
                min_eta=emtf_eta_start, max_eta=emtf_eta_stop,
                min_pt=pt_threshold),
            'denominator': new_gen_condition(
                min_eta=eta_low, max_eta=eta_up,
                min_pt=0
            )
        })

        disp_eff_definitions.append({
            'name': name,
            'num_label': num_label,
            'denom_label': denom_label,
            'numerator': new_trk_condition(
                min_qual=sm_disp_min_qual,
                min_eta=emtf_eta_start, max_eta=emtf_eta_stop,
                min_pt_dxy=pt_threshold, min_dxy=sm_disp_min_dxy
            ),
            'denominator': new_gen_condition(
                min_eta=eta_low, max_eta=eta_up,
                min_pt=0
            )
        })

    for pt_threshold in emtf_pt_thresholds:
        name = 'num_pt_ge_%d' % pt_threshold
        name += '_denom_pt_ge_%d_eta_st2_ge_%s_lt_%s' % (
            pt_threshold,
            str(eta_low).replace('.', 'p'),
            str(eta_up).replace('.', 'p')
        )

        num_label = 'p_{T} #geq %d GeV' % pt_threshold
        denom_label = '%0.2f #leq |#eta_{st2}| < %0.2f' % (eta_low, eta_up)

        prompt_eff_definitions.append({
            'name': name,
            'num_label': num_label,
            'denom_label': denom_label,
            'numerator': new_trk_condition(
                min_qual=sm_prompt_min_qual,
                min_eta=eta_low, max_eta=eta_up,
                min_pt=pt_threshold
            ),
            'denominator': new_gen_condition(
                min_eta=eta_low, max_eta=eta_up,
                min_pt=(pt_threshold + 4)
            )
        })

        disp_eff_definitions.append({
            'name': name,
            'num_label': num_label,
            'denom_label': denom_label,
            'numerator': new_trk_condition(
                min_qual=sm_disp_min_qual,
                min_eta=eta_low, max_eta=eta_up,
                min_pt_dxy=pt_threshold, min_dxy=sm_disp_min_dxy
            ),
            'denominator': new_gen_condition(
                min_eta=eta_low, max_eta=eta_up,
                min_pt=(pt_threshold + 4)
            )
        })
