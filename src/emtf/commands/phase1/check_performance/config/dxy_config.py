from emtf.core.commons.variables import emtf_dxy_thresholds
from emtf.phase1.commons.emtf_eta_ranges import emtf_extended_zone_eta_ranges, emtf_eta_start, emtf_eta_stop
from emtf.phase1.commons.emtf_triggers import new_trk_condition, new_gen_condition, \
    sm_disp_min_qual, sm_disp_min_pt_dxy

disp_eff_definitions = list()

for eta_range in emtf_extended_zone_eta_ranges:
    eta_low = eta_range[0]
    eta_up = eta_range[1]

    for dxy_threshold in emtf_dxy_thresholds:
        name = 'num_dxy_ge_%d' % dxy_threshold
        name += '_denom_eta_st2_ge_%s_lt_%s' % (str(eta_low).replace('.', 'p'), str(eta_up).replace('.', 'p'))

        num_label = 'd_{xy} #geq %d cm' % dxy_threshold
        denom_label = '%0.2f #leq |#eta_{st2}| < %0.2f' % (eta_low, eta_up)

        disp_eff_definitions.append({
            'name': name,
            'num_label': num_label,
            'denom_label': denom_label,
            'numerator': new_trk_condition(
                min_qual=sm_disp_min_qual,
                min_eta=emtf_eta_start, max_eta=emtf_eta_stop,
                min_pt_dxy=sm_disp_min_pt_dxy, min_dxy=dxy_threshold),
            'denominator': new_gen_condition(
                min_eta=eta_low, max_eta=eta_up,
                min_dxy=0)
        })

    for dxy_threshold in emtf_dxy_thresholds:
        name = 'num_dxy_ge_%d' % dxy_threshold
        name += '_denom_dxy_ge_%d_eta_st2_ge_%s_lt_%s' % (
            dxy_threshold,
            str(eta_low).replace('.', 'p'),
            str(eta_up).replace('.', 'p')
        )

        num_label = 'd_{xy} #geq %d cm' % dxy_threshold
        denom_label = '%0.2f #leq |#eta_{st2}| < %0.2f' % (eta_low, eta_up)

        disp_eff_definitions.append({
            'name': name,
            'num_label': num_label,
            'denom_label': denom_label,
            'numerator': new_trk_condition(
                min_qual=sm_disp_min_qual,
                min_eta=eta_low, max_eta=eta_up,
                min_pt_dxy=sm_disp_min_pt_dxy, min_dxy=dxy_threshold),
            'denominator': new_gen_condition(
                min_eta=eta_low, max_eta=eta_up,
                min_dxy=(dxy_threshold + 4)
            )
        })
