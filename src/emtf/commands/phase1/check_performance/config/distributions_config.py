from emtf.phase1.commons.emtf_triggers import sm_disp_min_qual, \
    sm_disp_min_pt_dxy, sm_disp_min_dxy, sm_prompt_min_qual, sm_prompt_min_pt, new_trk_condition, sm_disp_eta_stop, \
    sm_disp_eta_start, sm_prompt_eta_start, sm_prompt_eta_stop

prompt_trigger = new_trk_condition(
    min_qual=sm_prompt_min_qual,
    min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
    min_pt=sm_prompt_min_pt, min_dxy=None)

disp_trigger = new_trk_condition(
    min_qual=sm_disp_min_qual,
    min_eta=sm_disp_eta_start, max_eta=sm_disp_eta_stop,
    min_pt_dxy=sm_disp_min_pt_dxy, min_dxy=sm_disp_min_dxy)
