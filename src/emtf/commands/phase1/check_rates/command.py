import os

from ROOT import TFile

from emtf.commands.phase1.check_rates.analyzers.probe import ProbeAnalyzer
from emtf.commands.phase1.check_rates.analyzers.rates import RateAnalyzer
from emtf.phase1.analysis.phase1_engine import phase1_run


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--displaced', dest='displaced_en', metavar='DISPLACED_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Enables displaced track support')

    parser.add_argument('--ofreq', dest='orbit_freq', metavar='ORBIT_FREQUENCY',
                        type=float, default=11.246,
                        help='Orbit frequency in kHz')

    parser.add_argument('--ncb', dest='n_coll_bunches', metavar='NUMBER_OF_COLLIDING_BUNCHES',
                        type=float, default=2345,
                        help='Number of colliding bunches')


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events
    displaced_en = args.displaced_en
    orbit_freq = args.orbit_freq
    n_coll_bunches = args.n_coll_bunches

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Build Analysis
    main_analyzers = [
        RateAnalyzer(displaced_en, orbit_freq, n_coll_bunches),
        ProbeAnalyzer(displaced_en, orbit_freq, n_coll_bunches)
    ]

    def analysis_fn():
        return [], [
            RateAnalyzer(displaced_en, orbit_freq, n_coll_bunches),
            ProbeAnalyzer(displaced_en, orbit_freq, n_coll_bunches)
        ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase1_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
