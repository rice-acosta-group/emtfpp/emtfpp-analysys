from emtf.core.commons.variables import emtf_pt_thresholds, emtf_dxy_thresholds
from emtf.phase1.commons.emtf_triggers import sm_disp_min_qual, sm_prompt_min_qual, new_trk_condition, \
    sm_disp_eta_start, sm_disp_eta_stop, sm_prompt_eta_stop, \
    sm_prompt_eta_start

# Prompt Triggers
prompt_triggers = dict()

for pt_threshold in emtf_pt_thresholds:
    name = 'SingleMu%d' % pt_threshold

    prompt_triggers[name] = new_trk_condition(
        min_qual=sm_prompt_min_qual,
        min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
        min_pt=pt_threshold,
    )

# Displaced Triggers
disp_triggers = dict()

for pt_threshold in emtf_pt_thresholds:
    for dxy_threshold in emtf_dxy_thresholds:
        name = 'SingleMu%dDxy%d' % (pt_threshold, dxy_threshold)

        disp_triggers[name] = new_trk_condition(
            min_qual=sm_disp_min_qual,
            min_eta=sm_disp_eta_start, max_eta=sm_disp_eta_stop,
            min_pt_dxy=pt_threshold, min_dxy=dxy_threshold
        )
