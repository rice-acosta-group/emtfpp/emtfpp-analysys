import math

from emtf.commands.phase1.check_rates.config.probe_config import prompt_triggers, disp_triggers
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.tools import safe_divide
from emtf.core.plotters.basic import Rate1DPlotter


class ProbeAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en, orbit_freq, n_coll_bunches):
        super().__init__()

        # Init
        self.displaced_en = displaced_en
        self.orbit_freq = orbit_freq
        self.n_coll_bunches = n_coll_bunches

        # Total Counts
        self.event_count = 0

        # Init Stats
        self.trigger_plotters = dict()
        self.trigger_counts = dict()

        # Load Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        for trigger in triggers:
            name = trigger['name']

            plot = self.checkout_plotter(
                Rate1DPlotter,
                trigger['name'],
                trigger['title'],
                trigger['x_label'], trigger['y_label'],
                xbins=trigger['bins'],
                min_val=trigger['min'],
                max_val=trigger['max'],
                target_val=20,
                logy=trigger['logy'],
                density=False)

            cmt_lines = []
            cut_labels = {'pt': 'L1 p_{T}', 'dxy': 'L1 d_{xy}', 'qual': 'Qual'}

            for cut in ['pt', 'dxy', 'qual']:
                cut_label = cut_labels[cut]
                cut_threshold = trigger['cuts'][cut]

                if cut_threshold is None:
                    continue

                if cut_threshold == 0:
                    cut_threshold = 'Thrs.'
                else:
                    val_fmt = '%d' if (cut in ['pt', 'dxy', 'qual']) else '%0.2f'
                    cut_threshold = val_fmt % cut_threshold

                cmt_lines.append('%s #geq %s' % (cut_label, cut_threshold))

            if len(cmt_lines) == 1:
                cmt = cmt_lines[0]
            else:
                def gen_comment(cmnt, lines, i, n):
                    if (i + 2) == n:
                        return '#splitline{%s}{%s}' % (lines[i], lines[i + 1])
                    else:
                        next = gen_comment(cmnt, lines, i + 1, n)
                        return '#splitline{%s}{%s}' % (lines[i], next)

                cmt = gen_comment('', cmt_lines, 0, len(cmt_lines))

            plot.comment = cmt

            self.trigger_plotters[name] = plot
            self.trigger_counts[name] = 0

    def process_entry(self, event):

        # Select Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        # Count
        self.event_count += 1

        # Loop GMT
        for track in event.emtf_unpacked_tracks:
            # Short-Circuit: Only check tracks in BX=0
            if track.bx != 0:
                continue

            # Collect track info
            trk_entry = {
                'trk_qual': track.qual,
                'trk_eta': track.eta,
                'trk_pt': track.pt,
                'trk_pt_dxy': track.pt_dxy,
                'trk_dxy': 0 if track.dxy < 0 else track.dxy,
            }

            # Loop Triggers
            for trigger in triggers:
                # Short-Circuit: Didn't pass trigger
                trk_pass = trigger['condition'](trk_entry)

                if not trk_pass:
                    continue

                # Feed track info
                trigger['aggregator'].process(trk_entry)

        # Loop Triggers
        for trigger in triggers:
            trigger_name = trigger['name']
            trigger_aggregator = trigger['aggregator']

            plotter = self.trigger_plotters[trigger_name]

            if not trigger_aggregator.is_present:
                continue

            self.trigger_counts[trigger_name] += 1

            plotter.fill(trigger_aggregator.get())
            trigger_aggregator.reset()

    def merge(self, other):
        # Select Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        # Merge
        self.event_count += other.event_count

        for trigger in triggers:
            trigger_name = trigger['name']

            this_plotter = self.trigger_plotters[trigger_name]
            this_count = self.trigger_counts[trigger_name]

            other_plotter = other.trigger_plotters[trigger_name]
            other_count = other.trigger_counts[trigger_name]

            this_plotter.add(other_plotter)
            this_count += other_count

            self.trigger_plotters[trigger_name] = this_plotter
            self.trigger_counts[trigger_name] = this_count

    def post_production(self):
        # Select Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        # Note: Do math using integers, otherwise floating-point will screw up sums
        # Integrate Counts and Scale Counts
        n_zero_bias_events = float(self.event_count)
        conv_factor = self.orbit_freq * safe_divide(self.n_coll_bunches, n_zero_bias_events)

        for trigger in triggers:
            trigger_name = trigger['name']

            plotter = self.trigger_plotters[trigger_name]
            plot = plotter.plot

            # Integrate Bins
            int_count = int(self.trigger_counts[trigger_name])
            int_error2 = int_count

            # Update Bin Counts
            for i in range(plot.GetNcells() - 1):
                bin_id = (i + 1)
                bin_count = int(plot.GetBinContent(bin_id))
                bin_error2 = bin_count

                # Calculate Rate
                bin_rate = int_count * conv_factor
                bin_rate_err = math.sqrt(int_error2) * conv_factor

                # Plot
                plot.SetBinContent(bin_id, bin_rate)
                plot.SetBinError(bin_id, bin_rate_err)

                # Subtract bins
                int_count -= bin_count
                int_error2 -= bin_error2
