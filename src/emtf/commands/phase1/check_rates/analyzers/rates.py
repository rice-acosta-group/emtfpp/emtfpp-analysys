import math

from emtf.commands.phase1.check_rates.config.rate_config import prompt_triggers, disp_triggers
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.tools import safe_divide
from emtf.core.commons.triggers import TriggerMenu
from emtf.phase1.commons.emtf_triggers import sm_base_trg


class RateAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en, orbit_freq, n_coll_bunches):
        super().__init__()

        # Init
        self.displaced_en = displaced_en
        self.orbit_freq = orbit_freq
        self.n_coll_bunches = n_coll_bunches

        # Total Counts
        self.event_count = 0

        # Trigger Counts
        self.passed_base_count = dict()
        self.passed_trg_count = dict()
        self.passed_either_count = dict()
        self.passed_count = dict()

    def process_entry(self, event):

        # Select Triggers
        triggers = prompt_triggers

        if self.displaced_en:
            triggers = disp_triggers

        # Count
        self.event_count += 1

        # Build Trigger Menu
        trigger_menu = TriggerMenu()
        trigger_menu.register('base', sm_base_trg)

        for name, trigger in triggers.items():
            trigger_menu.register(name, trigger)

        # Loop GMT
        for track in event.emtf_unpacked_tracks:
            # Short-Circuit: Only check tracks in BX=0
            if track.bx != 0:
                continue

            # Collect track info
            trk_entry = {
                'trk_qual': track.qual,
                'trk_eta': track.eta,
                'trk_pt': track.pt,
                'trk_pt_dxy': track.pt_dxy,
                'trk_dxy': 0 if track.dxy < 0 else track.dxy,
            }

            # Check if track passed
            trigger_menu.process(trk_entry)

        # Count
        for name in triggers.keys():
            trigger_bit_base = trigger_menu.trigger_states.get('base', False)
            trigger_bit_trg = trigger_menu.trigger_states.get(name, False)

            passes_only_base = trigger_bit_base and (not trigger_bit_trg)
            passes_only_trg = (not trigger_bit_base) and trigger_bit_trg
            passes_either = trigger_bit_base or trigger_bit_trg
            passes_trg = trigger_bit_trg

            count = self.passed_base_count.get(name, 0) + int(passes_only_base)
            self.passed_base_count[name] = count

            count = self.passed_trg_count.get(name, 0) + int(passes_only_trg)
            self.passed_trg_count[name] = count

            count = self.passed_either_count.get(name, 0) + int(passes_either)
            self.passed_either_count[name] = count

            count = self.passed_count.get(name, 0) + int(passes_trg)
            self.passed_count[name] = count

    def merge(self, other):
        self.event_count += other.event_count

        for other_trigger, other_count in other.passed_base_count.items():
            new_count = self.passed_base_count.get(other_trigger, 0) + other_count
            self.passed_base_count[other_trigger] = new_count

        for other_trigger, other_count in other.passed_trg_count.items():
            new_count = self.passed_trg_count.get(other_trigger, 0) + other_count
            self.passed_trg_count[other_trigger] = new_count

        for other_trigger, other_count in other.passed_either_count.items():
            new_count = self.passed_either_count.get(other_trigger, 0) + other_count
            self.passed_either_count[other_trigger] = new_count

        for other_trigger, other_count in other.passed_count.items():
            new_count = self.passed_count.get(other_trigger, 0) + other_count
            self.passed_count[other_trigger] = new_count

    def post_production(self):
        # Calculate Rates
        n_zero_bias_events = float(self.event_count)
        conv_factor = self.orbit_freq * safe_divide(self.n_coll_bunches, n_zero_bias_events)

        pairs = [
            ('triggers_base', self.passed_base_count),
            ('triggers_trg', self.passed_trg_count),
            ('triggers_either', self.passed_either_count),
            ('triggers', self.passed_count)
        ]

        for pair in pairs:
            file_name, counts = pair

            with open(file_name + '.csv', 'w') as out:
                out.write('trigger, rate [kHz], rate_err [kHz], count, total\n')

                for trigger, count in counts.items():
                    rate = count * conv_factor
                    rate_err = math.sqrt(count) * conv_factor

                    trigger_line = ''
                    trigger_line += (
                            '%s, %f, %f, %d, %d\n' % (trigger, rate, rate_err, count, self.event_count)
                    )

                    # Write to file
                    out.write(trigger_line)
