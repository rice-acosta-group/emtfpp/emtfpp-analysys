import math

import numpy as np

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import uniform_step_axis
from emtf.core.commons.kinematics import calc_d0_cmssw, calc_dxy, calc_d0, calc_z0_cmssw, propagate_particle
from emtf.core.commons.tools import nz_sign
from emtf.core.commons.variables import emu_val_d0_bins, emu_val_eta_bins, \
    emu_val_phi_deg_bins, emu_val_lxy_bins, emu_val_mismatch_threshold, emu_val_q_bins, \
    emu_val_dxy_bins, emu_val_lz_bins, emu_val_min_threshold, emu_val_pt_bins, emu_val_z0_bins, emu_val_uz0_bins, emu_val_d0_sign_bins
from emtf.core.plotters.basic import Hist1DPlotter, CorrelationPlotter
from emtf.phase2.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop


class MuonDistributionAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        # Counters
        self.muon_count = 0

        # Plots
        self.pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_pt',
            'Muon Distribution',
            'True p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            density=True,
            logy=True)

        self.lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_lxy',
            'Muon Distribution',
            'True L_{xy} [cm]', 'a.u.',
            xbins=emu_val_lxy_bins,
            density=True,
            logy=True)

        self.lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_lz',
            'Muon Distribution',
            'True L_{z} [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.d0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_d0',
            'Muon Distribution',
            'True d_{0} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            density=True,
            logy=True)

        self.d0_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_d0_sign',
            'Muon Distribution',
            'True Sign', 'a.u.',
            xbins=emu_val_d0_sign_bins,
            density=True,
            logy=True)

        self.z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_z0',
            'Muon Distribution',
            'True z_{0} [cm]', 'a.u.',
            xbins=emu_val_z0_bins,
            density=True,
            logy=True)

        self.lz_vs_z0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_lz_vs_z0', 'Muon Distribution',
            'True z_{0} [cm]', 'True L_{z} [cm]',
            xbins=emu_val_uz0_bins,
            ybins=emu_val_lz_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.lxy_vs_z0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_lxy_vs_z0', 'Muon Distribution',
            'True z_{0} [cm]', 'True L_{xy} [cm]',
            xbins=emu_val_uz0_bins,
            ybins=emu_val_lxy_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.lxy_vs_lz_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_lxy_vs_lz', 'Muon Distribution',
            'True L_{z} [cm]', 'True L_{xy} [cm]',
            xbins=emu_val_lz_bins,
            ybins=emu_val_lxy_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.lxy_vs_d0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_lxy_vs_d0',
            'Muon Distribution',
            'True d_{0} [cm]', 'True L_{xy} [cm]',
            xbins=emu_val_d0_bins,
            ybins=emu_val_lxy_bins,
            min_val=emu_val_mismatch_threshold,
            density=True,
            logz=True
        )

        self.q_vs_d0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_q_vs_d0',
            'Muon Distribution',
            'True d_{0} [cm]', 'True q',
            xbins=emu_val_d0_bins,
            ybins=emu_val_q_bins,
            min_val=emu_val_mismatch_threshold,
            density=True,
            logz=True
        )

        self.cmssw_vs_d0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_cmssw_vs_d0',
            'Muon Distribution',
            'True d_{0} [cm]', 'True CMSSW d_{0} [cm]',
            xbins=emu_val_d0_bins,
            ybins=emu_val_d0_bins,
            min_val=emu_val_mismatch_threshold,
            logz=True
        )

        self.cmssw_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_cmssw_vs_dxy',
            'Muon Distribution',
            'True d_{xy} [cm]', 'True CMSSW d_{0} [cm]',
            xbins=emu_val_d0_bins,
            ybins=emu_val_d0_bins,
            min_val=emu_val_mismatch_threshold,
            logz=True
        )

        self.dxy_vs_d0_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_dxy_vs_d0',
            'Muon Distribution',
            'True d_{0} [cm]', 'True d_{xy} [cm]',
            xbins=emu_val_d0_bins,
            ybins=emu_val_dxy_bins,
            min_val=emu_val_mismatch_threshold,
            logz=True
        )

        self.eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_eta',
            'Muon Distribution',
            'True |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            density=True,
            logy=True)

        self.eta_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_eta_st2',
            'Muon Distribution',
            'True |#eta_{st2}|', 'a.u.',
            xbins=emu_val_eta_bins,
            density=True,
            logy=True)

        self.phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_phi',
            'Muon Distribution',
            'True #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            density=True,
            logy=True)

        self.phi_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_phi_st2',
            'Muon Distribution',
            'True #phi_{st2} [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            density=True,
            logy=True)

        self.n_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_n',
            'Muon Distribution',
            'True n', 'a.u.',
            xbins=uniform_step_axis(1, 0, 10),
            density=True,
            logy=True)

    def process_entry(self, event):
        valid_muon_count = 0

        for muon_info in event:
            gen_qinvpt, gen_eta, gen_phi, gen_vx, gen_vy, gen_vz = muon_info

            gen_q = nz_sign(gen_qinvpt)
            gen_pt = abs(1 / gen_qinvpt)
            gen_dxy = calc_dxy(gen_qinvpt, gen_phi, gen_vx, gen_vy)
            gen_d0 = calc_d0(gen_qinvpt, gen_phi, gen_vx, gen_vy)
            gen_d0_cmssw = calc_d0_cmssw(gen_phi, gen_vx, gen_vy)
            gen_z0_cmssw = calc_z0_cmssw(gen_qinvpt, gen_eta, gen_phi, gen_vx, gen_vy, gen_vz)

            gen_lxy = math.hypot(gen_vx, gen_vy)
            gen_mom_st2, gen_pos_st2 = propagate_particle(
                gen_qinvpt, gen_eta, gen_phi,
                gen_vx, gen_vy, gen_vz
            )
            gen_eta_st2 = gen_pos_st2.Eta()
            gen_phi_st2 = gen_pos_st2.Phi()

            # Check lxy
            if gen_lxy > 300:
                continue

            # Check lz
            if abs(gen_vz) > 500:
                continue

            # Check eta
            if not (emtf_eta_start <= abs(gen_eta_st2) < emtf_eta_stop):
                continue

            # Global Counts
            self.muon_count += 1

            # Local Counts
            valid_muon_count += 1

            # Plot
            self.pt_plotter.fill(gen_pt)
            self.lxy_plotter.fill(gen_lxy)
            self.lz_plotter.fill(abs(gen_vz))
            self.d0_sign_plotter.fill(nz_sign(gen_d0))
            self.d0_plotter.fill(gen_d0)
            self.z0_plotter.fill(gen_z0_cmssw)
            self.lz_vs_z0_plotter.fill(abs(gen_z0_cmssw), abs(gen_vz))
            self.lxy_vs_z0_plotter.fill(abs(gen_z0_cmssw), gen_lxy)
            self.q_vs_d0_plotter.fill(gen_d0, gen_q)
            self.lxy_vs_lz_plotter.fill(abs(gen_vz), gen_lxy)
            self.lxy_vs_d0_plotter.fill(gen_d0, gen_lxy)
            self.cmssw_vs_d0_plotter.fill(gen_d0, gen_d0_cmssw)
            self.cmssw_vs_dxy_plotter.fill(gen_dxy, gen_d0_cmssw)
            self.dxy_vs_d0_plotter.fill(gen_d0, gen_dxy)
            self.eta_plotter.fill(abs(gen_eta))
            self.eta_st2_plotter.fill(abs(gen_eta_st2))
            self.phi_plotter.fill(np.rad2deg(gen_phi))
            self.phi_st2_plotter.fill(np.rad2deg(gen_phi_st2))

        # Plot
        self.n_plotter.fill(valid_muon_count)

    def merge(self, other):
        pass

    def post_production(self):
        print('Total Muon Count: %d' % self.muon_count)
