import math

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import uniform_step_axis
from emtf.core.commons.variables import emu_val_lz_bins, emu_val_min_threshold
from emtf.core.plotters.basic import Hist1DPlotter, CorrelationPlotter
from emtf.phase2.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop


class LLPDistributionAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.h_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'h_mass',
            'LLP Distribution',
            'True m_{H} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 1000),
            density=True,
            logy=True)

        self.llp_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_mass',
            'LLP Distribution',
            'True m_{LLP} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.delta_mass_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'delta_mass',
            'LLP Distribution',
            'True m_{H} - m_{LLP} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 1000),
            density=True,
            logy=True)

        self.llp_mass_vs_h_mass_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'llp_mass_vs_h_mass', 'LLP Distribution',
            'True m_{H} [GeV]', 'True m_{LLP} [GeV]',
            xbins=uniform_step_axis(10, 0, 1000),
            ybins=uniform_step_axis(10, 0, 500),
            min_val=emu_val_min_threshold,
            logz=True)

        self.llp_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_pt',
            'LLP Distribution',
            'True p_{T}^{LLP} [GeV]', 'a.u.',
            xbins=uniform_step_axis(10, 0, 500),
            density=True,
            logy=True)

        self.llp_beta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_beta',
            'LLP Distribution',
            'True #beta', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

        self.llp_ctau_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_ctau',
            'LLP Distribution',
            'True c#tau [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.llp_ctau_vs_llp_mass_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'llp_ctau_vs_llp_mass', 'LLP Distribution',
            'True m_{LLP} [GeV]', 'True c#tau [cm]',
            xbins=uniform_step_axis(10, 0, 500),
            ybins=emu_val_lz_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.llp_ctau_vs_h_mass_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'llp_ctau_vs_h_mass', 'LLP Distribution',
            'True m_{H} [GeV]', 'True c#tau [cm]',
            xbins=uniform_step_axis(10, 0, 1000),
            ybins=emu_val_lz_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.llp_ctau_vs_delta_mass_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'llp_ctau_vs_delta_mass', 'LLP Distribution',
            'True m_{H} - m_{LLP} [GeV]', 'True c#tau [cm]',
            xbins=uniform_step_axis(10, 0, 1000),
            ybins=emu_val_lz_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.llp_ct_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_ct',
            'LLP Distribution',
            'True ct [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.llp_ctp_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_ctp',
            'LLP Distribution',
            'True ct_{proper} [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.llp_4p_balance_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'llp_4p_balance',
            'LLP Distribution',
            '4-Momentum Balanced', 'a.u.',
            xbins=uniform_step_axis(1, 0, 1),
            density=True,
            logy=True)

    def process_entry(self, event):
        for llp_info in event:
            h_m, llp_m, llp_ctau, llp_pt, llp_eta, llp_phi, llp_beta, llp_4p_balance, \
                llp_vx, llp_vy, llp_vz, llp_t, llp_tp = llp_info

            llp_lxy = math.sqrt(llp_vx ** 2 + llp_vy ** 2)

            # Check lxy
            if llp_lxy > 300:
                continue

            # Check lz
            if abs(llp_vz) > 500:
                continue

            # Check eta
            if not (emtf_eta_start <= abs(llp_eta) < emtf_eta_stop):
                continue

            # Plot
            self.h_mass_plotter.fill(h_m)
            self.llp_mass_plotter.fill(llp_m)
            self.delta_mass_plotter.fill(h_m - llp_m)
            self.llp_mass_vs_h_mass_plotter.fill(h_m, llp_m)
            self.llp_pt_plotter.fill(llp_pt)
            self.llp_beta_plotter.fill(llp_beta)
            self.llp_ctau_plotter.fill(llp_ctau * 100)
            self.llp_ctau_vs_llp_mass_plotter.fill(llp_m, llp_ctau * 100)
            self.llp_ctau_vs_h_mass_plotter.fill(h_m, llp_ctau * 100)
            self.llp_ctau_vs_delta_mass_plotter.fill(h_m - llp_m, llp_ctau * 100)
            self.llp_ct_plotter.fill(llp_t)
            self.llp_ctp_plotter.fill(llp_tp)
            self.llp_4p_balance_plotter.fill(llp_4p_balance)

    def merge(self, other):
        pass

    def post_production(self):
        pass
