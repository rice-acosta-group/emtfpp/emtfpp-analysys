import math
import random

from ROOT import TLorentzVector

from emtf.commands.general.gen_vertex.analyzers.llp_distributions import LLPDistributionAnalyzer
from emtf.commands.general.gen_vertex.analyzers.muon_distributions import MuonDistributionAnalyzer

# Constants
MAX_M_H = 1000  # GeV
MIN_M_H = 20  # GeV
MAX_PT_H = 120  # GeV
MIN_PT_H = 1  # GeV
MAX_CTAU_LLP = 5000 / 1000  # m
MIN_CTAU_LLP = 10 / 1000  # m
MAX_ETA_LLP = 3.5
MIN_ETA_LLP = 1e-6

M_MU = 0.105  # GeV
SPEED_OF_LIGHT = 299792458  # m/s


def configure_parser(parser):
    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to generate')


def run(args):
    # Unpack args
    max_events = args.max_events

    # Analyzer
    muon_analyzer = MuonDistributionAnalyzer()
    llp_analyzer = LLPDistributionAnalyzer()

    # Generate Events
    for evt in range(max_events):
        # Calculate Higgs mass
        max_h_mass = MAX_M_H
        min_h_mass = MIN_M_H
        h_mass = random.random() * (max_h_mass - min_h_mass) + min_h_mass
        h_mass = min_h_mass if h_mass < min_h_mass else h_mass
        h_mass = max_h_mass if h_mass > max_h_mass else h_mass
        # h_mass = 125

        # Choose Random ctau
        llp_ctau = random.random() * (MAX_CTAU_LLP - MIN_CTAU_LLP) + MIN_CTAU_LLP
        llp_ctau = MIN_CTAU_LLP if llp_ctau < MIN_CTAU_LLP else llp_ctau
        llp_ctau = MAX_CTAU_LLP if llp_ctau > MAX_CTAU_LLP else llp_ctau
        # llp_ctau = 5000 / 1000

        # Calculate LLP mass
        max_llp_mass = h_mass / 2
        min_llp_mass = 10

        # max_dmass = h_mass - min_llp_mass
        # min_dmass = h_mass - max_llp_mass
        # max_invdmass = 1 / min_dmass
        # min_invdmass = 1 / max_dmass
        # dmass = 1 / (random.random() * (max_invdmass - min_invdmass) + min_invdmass)
        # llp_mass = h_mass - dmass
        # llp_mass = max_llp_mass if llp_mass > max_llp_mass else llp_mass

        llp_mass = random.random() * (max_llp_mass - min_llp_mass) + min_llp_mass
        llp_mass = min_llp_mass if llp_mass < min_llp_mass else llp_mass
        llp_mass = max_llp_mass if llp_mass > max_llp_mass else llp_mass
        # llp_mass = 50

        # Calculate LLP 4-momentum
        h_vtx, llp1_4mom, llp2_4mom = shoot_LLP(h_mass, llp_mass)

        # Calculate Muon 4-momentum
        llp1_vtx, llp1_mu1_4mom, llp1_mu2_4mom, llp1_tp = decay_particle(llp_mass, llp_ctau, llp1_4mom, M_MU)
        llp2_vtx, llp2_mu1_4mom, llp2_mu2_4mom, llp2_tp = decay_particle(llp_mass, llp_ctau, llp2_4mom, M_MU)

        llp1_vtx = h_vtx + llp1_vtx
        llp2_vtx = h_vtx + llp2_vtx

        llp1_4p_balance = abs((llp1_mu1_4mom + llp1_mu2_4mom - llp1_4mom).Mag()) < 1e-6
        llp2_4p_balance = abs((llp2_mu1_4mom + llp2_mu2_4mom - llp2_4mom).Mag()) < 1e-6

        if not llp1_4p_balance:
            print('LLP1 4-momentum not balanced')

        if not llp2_4p_balance:
            print('LLP2 4-momentum not balanced')

        # Analyze LLP
        llps = list()
        llps.append((
            h_mass, llp_mass, llp_ctau, llp1_4mom.Pt(), llp1_4mom.Eta(), llp1_4mom.Phi(), llp1_4mom.Beta(),
            llp1_4p_balance,
            llp1_vtx.X(), llp1_vtx.Y(), llp1_vtx.Z(), llp1_vtx.T(), llp1_tp
        ))
        llps.append((
            h_mass, llp_mass, llp_ctau, llp2_4mom.Pt(), llp2_4mom.Eta(), llp2_4mom.Phi(), llp2_4mom.Beta(),
            llp2_4p_balance,
            llp2_vtx.X(), llp2_vtx.Y(), llp2_vtx.Z(), llp2_vtx.T(), llp2_tp
        ))

        llp_analyzer.process_entry(llps)

        # Analyze Muons
        muons = list()

        mu_q = (-1 if (random.random() < 0.5) else 1)
        muons.append((
            mu_q / llp1_mu1_4mom.Pt(), llp1_mu1_4mom.Eta(), llp1_mu1_4mom.Phi(),
            llp1_vtx.X(), llp1_vtx.Y(), llp1_vtx.Z()
        ))
        muons.append((
            -mu_q / llp1_mu2_4mom.Pt(), llp1_mu2_4mom.Eta(), llp1_mu2_4mom.Phi(),
            llp1_vtx.X(), llp1_vtx.Y(), llp1_vtx.Z()
        ))

        mu_q = (-1 if (random.random() < 0.5) else 1)
        muons.append((
            mu_q / llp2_mu1_4mom.Pt(), llp2_mu1_4mom.Eta(), llp2_mu1_4mom.Phi(),
            llp2_vtx.X(), llp2_vtx.Y(), llp2_vtx.Z()
        ))
        muons.append((
            -mu_q / llp2_mu2_4mom.Pt(), llp2_mu2_4mom.Eta(), llp2_mu2_4mom.Phi(),
            llp2_vtx.X(), llp2_vtx.Y(), llp2_vtx.Z()
        ))

        muon_analyzer.process_entry(muons)

    # Write
    muon_analyzer.write()
    llp_analyzer.write()


def shoot_LLP(h_mass, llp_mass):
    # Calculate width
    h_width = 0.027 * h_mass
    h_ctau = 0.19733e-15 / h_width

    # Calculate h 4-momentum in Lab Frame
    h_endcap = (-1 if (random.random() < 0.5) else 1)
    h_pt = (MAX_PT_H - MIN_PT_H) * random.random() + MIN_PT_H
    h_eta = h_endcap * (random.random() * (MAX_ETA_LLP - MIN_ETA_LLP) + MIN_ETA_LLP)
    h_theta = 2 * math.atan(math.exp(-h_eta))

    h_phi = random.random() * 2 * math.pi - math.pi
    h_p = h_pt / math.sin(h_theta)
    h_e = math.hypot(h_p, h_mass)
    h_px = h_p * math.sin(h_theta) * math.cos(h_phi)
    h_py = h_p * math.sin(h_theta) * math.sin(h_phi)
    h_pz = h_p * math.cos(h_theta)

    h_4mom = TLorentzVector(h_px, h_py, h_pz, h_e)

    # Decay h
    h_vtx, llp1_4mom, llp2_4mom, h_ctp = decay_particle(h_mass, h_ctau, h_4mom, llp_mass)

    return h_vtx, llp1_4mom, llp2_4mom


def decay_particle(parent_mass, parent_ctau, parent_4mom, daughter_mass):
    # Calculate beta
    parent_beta3 = parent_4mom.BoostVector()

    # Calculate daughter momentum in parent frame
    daughter_p = 0.5 * math.sqrt(parent_mass ** 2 - 4 * daughter_mass ** 2)
    daughter_e = parent_mass / 2.
    daughter_theta = random.random() * math.pi - math.pi / 2
    daughter_phi = random.random() * 2 * math.pi - math.pi
    daughter_px = daughter_p * math.sin(daughter_theta) * math.cos(daughter_phi)
    daughter_py = daughter_p * math.sin(daughter_theta) * math.sin(daughter_phi)
    daughter_pz = daughter_p * math.cos(daughter_theta)

    # Boost daughter 4-momentum to lab frame
    daughter1_4mom = TLorentzVector(daughter_px, daughter_py, daughter_pz, daughter_e)
    daughter1_4mom.Boost(parent_beta3)

    daughter2_4mom = TLorentzVector(-daughter_px, -daughter_py, -daughter_pz, daughter_e)
    daughter2_4mom.Boost(parent_beta3)

    # Calculate decay time
    parent_ctp = - parent_ctau * math.log(1 - random.random()) * 100  # cm

    # Boost decay vertex to lab frame
    decay_vtx = TLorentzVector(0, 0, 0, parent_ctp)
    decay_vtx.Boost(parent_beta3)

    return decay_vtx, daughter1_4mom, daughter2_4mom, parent_ctp
