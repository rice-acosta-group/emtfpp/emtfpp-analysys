import numpy as np


def configure_parser(parser):
    parser.add_argument('--displaced', dest='displaced_en', action='store_const',
                        const=True, default=False,
                        help='Displaced')


def run(args):
    # Unpack args
    displaced_en = args.displaced_en

    # Get Activations
    signal_patterns_zone0 = np.load('signal_patterns_zone0.npz', allow_pickle=True)
    signal_patterns_zone1 = np.load('signal_patterns_zone1.npz', allow_pickle=True)
    signal_patterns_zone2 = np.load('signal_patterns_zone2.npz', allow_pickle=True)

    zone0_mask_def = signal_patterns_zone0['mask_definition'].item()
    zone1_mask_def = signal_patterns_zone1['mask_definition'].item()
    zone2_mask_def = signal_patterns_zone2['mask_definition'].item()

    save_luts(0, zone0_mask_def, displaced_en)
    save_luts(1, zone1_mask_def, displaced_en)
    save_luts(2, zone2_mask_def, displaced_en)


def save_luts(zone, mask_definitions, displaced_en):
    print('Begin Zone %d' % zone)

    # Export Pattern LUTs
    # For prompt we do highest pT (3rd pT bin, then, left, right, ..., left+3, right+3)
    pattern_export_order = [(3, 3), (3, 2), (3, 4), (3, 1), (3, 5), (3, 0), (3, 6)]

    if displaced_en:
        pattern_export_order = [(3, 3), (2, 3), (4, 3), (1, 3), (5, 3), (0, 3), (6, 3)]

    pattern_col_start_buffer = ''
    pattern_col_mid_buffer = ''
    pattern_col_stop_buffer = ''

    for pattern_key in pattern_export_order:
        pattern_definition = mask_definitions[pattern_key]

        pattern_emu_line = ''
        pattern_col_start_line = ''
        pattern_col_mid_line = ''
        pattern_col_stop_line = ''

        is_first_row = True

        for pattern_row in pattern_definition:
            begin, center, end = pattern_row[:]

            if is_first_row:
                is_first_row = False
            else:
                pattern_emu_line += ', '

            pattern_emu_line += '{%d, %d, %d}' % (begin, center, end)

            pattern_col_start_line += (str(begin) + ' ')
            pattern_col_mid_line += (str(center) + ' ')
            pattern_col_stop_line += (str(end) + ' ')

        print('{' + pattern_emu_line + '},')

        pattern_col_start_buffer += (pattern_col_start_line + '\n')
        pattern_col_mid_buffer += (pattern_col_mid_line + '\n')
        pattern_col_stop_buffer += (pattern_col_stop_line + '\n')

    # Write Patterns
    with open('pat_col_start_zone_%d.csv' % (zone,), 'w') as pat_f:
        pat_f.write(pattern_col_start_buffer)

    with open('pat_col_mid_zone_%d.csv' % (zone,), 'w') as pat_f:
        pat_f.write(pattern_col_mid_buffer)

    with open('pat_col_stop_zone_%d.csv' % (zone,), 'w') as pat_f:
        pat_f.write(pattern_col_stop_buffer)
