def get_mode_v1(zone, activation):
    if zone in [0, 1]:
        st1_mask = 7
        st2_mask = 24
        st3_mask = 96
        st4_mask = 128
    else:
        st1_mask = 3
        st2_mask = 12
        st3_mask = 48
        st4_mask = 192

    # Calculate Mode
    mode = 0

    if (st1_mask & activation) > 0:
        mode += 8

    if (st2_mask & activation) > 0:
        mode += 4

    if (st3_mask & activation) > 0:
        mode += 2

    if (st4_mask & activation) > 0:
        mode += 1

    return mode


def get_mode_v2(zone, activation):
    cnt_ye11 = calc_cnt_ye11(zone, activation)
    cnt_ye12 = calc_cnt_ye12(zone, activation)
    cnt_ye22 = calc_cnt_ye22(zone, activation)
    cnt_ye23 = calc_cnt_ye23(zone, activation)
    cnt_ye24 = calc_cnt_ye24(zone, activation)
    cnt_ye2a = (cnt_ye22 != 0) + (cnt_ye23 != 0) + (cnt_ye24 != 0)
    cnt_ye2b = (cnt_ye23 != 0) + (cnt_ye24 != 0)
    cnt_me11 = calc_cnt_me11(zone, activation)
    cnt_me12 = calc_cnt_me12(zone, activation)
    cnt_me14 = calc_cnt_me14(zone, activation)
    cnt_me2a = calc_cnt_me2a(zone, activation)

    # SingleMu(12)
    def is_single_mu():
        rule_a_i = (cnt_me12 != 0) and (cnt_me2a >= 1)
        rule_a_ii = (cnt_ye12 != 0) and (cnt_me2a >= 1) and (cnt_ye2a >= 2)
        rule_b_i = (cnt_me11 != 0) and (cnt_me2a >= 1) and (cnt_ye2a >= 2)
        rule_b_ii = (cnt_ye11 != 0) and (cnt_me2a >= 2)
        rule_c_i = (cnt_me14 != 0) and (cnt_me11 != 0) and (cnt_ye2b >= 1)
        rule_c_ii = (cnt_me14 != 0) and (cnt_me2a >= 1) and (cnt_ye2a >= 2)

        if rule_a_i or rule_a_ii or rule_b_i or rule_b_ii or rule_c_i or rule_c_ii:
            return True

        return False

    # DoubleMu(8)
    def is_double_mu():
        rule_a_i = (cnt_me12 != 0) and (cnt_ye2a >= 1)
        rule_a_ii = (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_b_i = (cnt_ye12 != 0) and (cnt_me2a >= 1)
        rule_b_ii = (cnt_ye11 != 0) and (cnt_me2a >= 1)
        rule_c_i = (cnt_me14 != 0) and (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_c_ii = (cnt_me14 != 0) and (cnt_me2a >= 1)

        if rule_a_i or rule_a_ii or rule_b_i or rule_b_ii or rule_c_i or rule_c_ii:
            return True

    # TripleMu(4)
    def is_triple_mu():
        rule_a_i = (cnt_me12 != 0) and (cnt_ye2a >= 1)
        rule_a_ii = (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_b_i = (cnt_ye12 != 0) and (cnt_me2a >= 1)
        rule_b_ii = (cnt_ye11 != 0) and (cnt_me2a >= 1)
        rule_c_i = (cnt_me14 != 0) and (cnt_me11 != 0) and (cnt_ye2a >= 1)
        rule_c_ii = (cnt_me14 != 0) and (cnt_me2a >= 1)
        rule_d = (cnt_me2a >= 2)

        if rule_a_i or rule_a_ii or rule_b_i or rule_b_ii or rule_c_i or rule_c_ii or rule_d:
            return True

        return False

    # Get Mode
    if is_single_mu():
        return 12
    elif is_double_mu():
        return 8
    elif is_triple_mu():
        return 4
    else:
        return 0


def calc_cnt_ye11(zone, activation):
    if zone == 0:
        return ((activation & 4) == 4) + ((activation & 2) == 2)
    elif zone == 1:
        return ((activation & 2) == 2) + ((activation & 1) == 1)

    return 0


def calc_cnt_ye12(zone, activation):
    if zone == 1:
        return ((activation & 4) == 4)
    elif zone == 2:
        return ((activation & 2) == 2) + ((activation & 1) == 1)

    return 0


def calc_cnt_ye22(zone, activation):
    if zone == 0:
        return ((activation & 16) == 16) + ((activation & 8) == 8)
    elif zone == 1:
        return ((activation & 16) == 16) + ((activation & 8) == 8)
    elif zone == 2:
        return ((activation & 8) == 8) + ((activation & 4) == 4)

    return 0


def calc_cnt_ye23(zone, activation):
    if zone == 0:
        return ((activation & 32) == 32) + ((activation & 64) == 64)
    elif zone == 1:
        return ((activation & 32) == 32) + ((activation & 64) == 64)
    elif zone == 2:
        return ((activation & 16) == 16) + ((activation & 32) == 32)

    return 0


def calc_cnt_ye24(zone, activation):
    if zone == 0:
        return ((activation & 128) == 128)
    elif zone == 1:
        return ((activation & 128) == 128)
    elif zone == 2:
        return ((activation & 64) == 64) + ((activation & 128) == 128)

    return 0


def calc_cnt_me11(zone, activation):
    if zone == 0:
        return ((activation & 4) == 4)
    elif zone == 1:
        return ((activation & 2) == 2)

    return 0


def calc_cnt_me12(zone, activation):
    if zone == 1:
        return ((activation & 4) == 4)
    elif zone == 2:
        return ((activation & 1) == 1)

    return 0


def calc_cnt_me14(zone, activation):
    if zone == 0:
        return ((activation & 1) == 1)

    return 0


def calc_cnt_me2a(zone, activation):
    if zone == 0:
        return ((activation & 16) == 16) + ((activation & 32) == 32) + ((activation & 128) == 128)
    elif zone == 1:
        return ((activation & 16) == 16) + ((activation & 32) == 32) + ((activation & 128) == 128)
    elif zone == 2:
        return ((activation & 8) == 8) + ((activation & 16) == 16) + ((activation & 64) == 64)

    return 0
