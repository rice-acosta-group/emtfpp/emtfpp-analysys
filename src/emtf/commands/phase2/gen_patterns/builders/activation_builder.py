from emtf.commands.phase2.gen_patterns.config import *
from emtf.phase2.commons.emtf_model import get_zone_site_to_hm_row_lut


class ActivationHitmapBuilder():

    def __init__(self, zone):
        self.zone = zone
        self.zone_mask = (1 << self.zone)
        self.timezone = 0
        self.timezone_mask = (1 << self.timezone)

        self.sig_hitmap = np.zeros((num_z_vs_phi_hitmap_rows, num_z_vs_phi_hitmap_cols), dtype=np.int32)
        self.bkg_hitmap = np.zeros((num_z_vs_phi_hitmap_rows, num_z_vs_phi_hitmap_cols), dtype=np.int32)
        self.row_has_hit = np.zeros(num_z_vs_phi_hitmap_rows, dtype=np.int32)
        self.first_non_zero_col = -1
        self.last_non_zero_col = -1
        self.is_empty = True

    def add_hit(self, hit, is_signal):
        # Short-Circuit: Only keep valid hits
        if hit.valid != 1:
            return

        # Short-Circuit: Only keep same zone
        if self.zone not in hit.emtf_zones:
            return

        # Short-Circuit: Only keep same timezone
        if (hit.emtf_timezones & self.timezone_mask) != self.timezone_mask:
            return

        # Paint Cell
        hm_row = get_zone_site_to_hm_row_lut()[self.zone, hit.emtf_site]
        hm_col = int((hit.emtf_phi >> 4)) - 27

        if is_signal:
            self.sig_hitmap[hm_row, hm_col] = 1
        else:
            self.bkg_hitmap[hm_row, hm_col] = 1

        # Perform Row Redux
        self.row_has_hit[hm_row] = 1

        if (self.first_non_zero_col == -1) or (hm_col < self.first_non_zero_col):
            self.first_non_zero_col = hm_col

        if (self.last_non_zero_col == -1) or (self.last_non_zero_col < hm_col):
            self.last_non_zero_col = hm_col

        # Flag
        self.is_empty = False
