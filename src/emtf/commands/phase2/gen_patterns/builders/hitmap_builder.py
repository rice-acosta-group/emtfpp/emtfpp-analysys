from emtf.commands.phase2.gen_patterns.config import *
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.emtf import calc_theta_rad_from_int
from emtf.core.commons.kinematics import calc_eta_from_theta_rad
from emtf.phase2.commons.emtf_model import get_zone_site_to_hm_row_lut


class ZVsPhiHitmapBuilder():

    def __init__(self, zone):
        self.zone = zone
        self.zone_mask = (1 << self.zone)
        self.timezone = 0
        self.timezone_mask = (1 << self.timezone)

        self.hitmap = np.zeros((num_z_vs_phi_hitmap_rows, num_z_vs_phi_hitmap_cols), dtype=np.int32)
        self.row_has_hit = np.zeros(num_z_vs_phi_hitmap_rows, dtype=np.int32)
        self.first_non_zero_col = -1
        self.last_non_zero_col = -1
        self.is_empty = True

    def add_hit(self, hit):
        # Short-Circuit: Only keep valid hits
        if hit.valid != 1:
            return

        # Short-Circuit: Only keep same zone
        if self.zone not in hit.emtf_zones:
            return

        # Short-Circuit: Only keep same timezone
        if (hit.emtf_timezones & self.timezone_mask) != self.timezone_mask:
            return

        # Paint Cell
        hm_row = get_zone_site_to_hm_row_lut()[self.zone, hit.emtf_site]
        hm_col = int(hit.emtf_phi >> 4) - 27
        self.hitmap[hm_row, hm_col] = 1

        # Perform Row Redux
        self.row_has_hit[hm_row] = 1

        if (self.first_non_zero_col == -1) or (hm_col < self.first_non_zero_col):
            self.first_non_zero_col = hm_col

        if (self.last_non_zero_col == -1) or (self.last_non_zero_col < hm_col):
            self.last_non_zero_col = hm_col

        # Flag
        self.is_empty = False

    def convolve_and_crop(self, pattern):
        # Get Anchor
        # Pseudo 2D convolution by applying 1D convolution multiple times and summing the results
        # Note the box has to be flipped before 1D convolution
        # Osvaldo Note: If I understand correctly this does a convolution per row,
        # and then adds up the results per column, and selects the column with the highest value.
        convoluted_row_sum = np.add.reduce([
            np.convolve(self.hitmap[i, :], pattern[i, ::-1], mode='same')
            for i in range(self.hitmap.shape[0])
        ])

        anchor_col = np.argmax(convoluted_row_sum)

        # Crop hitmaps
        hitmap_start = max(0, anchor_col - pattern_col_offset)
        hitmap_stop = min(num_z_vs_phi_hitmap_cols, anchor_col + pattern_col_offset + 1)
        pattern_start = max(0, num_z_vs_phi_pattern_cols - hitmap_stop)
        pattern_stop = min(num_z_vs_phi_pattern_cols, num_z_vs_phi_hitmap_cols - hitmap_start)

        cropped_hitmap = np.zeros((num_z_vs_phi_pattern_rows, num_z_vs_phi_pattern_cols), dtype=self.hitmap.dtype)
        cropped_hitmap[:, pattern_start:pattern_stop] = self.hitmap[:, hitmap_start:hitmap_stop]

        # Return
        return cropped_hitmap, anchor_col


class EtaVsPhiHitmapBuilder():

    def __init__(self, zone):
        self.zone = zone
        self.zone_mask = (1 << self.zone)
        self.timezone = 0
        self.timezone_mask = (1 << self.timezone)

        self.hitmap = np.zeros((num_eta_bins, num_z_vs_phi_hitmap_cols), dtype=np.int32)
        self.is_empty = True

    def add_hit(self, hit):
        # Short-Circuit: Only keep valid hits
        if hit.valid != 1:
            return

        # Short-Circuit: Only keep same zone
        if self.zone not in hit.emtf_zones:
            return

        # Short-Circuit: Only keep same timezone
        if (hit.emtf_timezones & self.timezone_mask) != self.timezone_mask:
            return

        # Paint both theta values
        for hit_theta in hit.emtf_thetas:
            # Short-Circuit: Invalid theta value
            if hit_theta == 0:
                continue

            # Convert theta to eta
            hit_theta = calc_theta_rad_from_int(hit_theta)
            hit_eta = calc_eta_from_theta_rad(hit_theta)
            hit_eta = abs(hit_eta)

            # Paint Cell
            hm_row = digitize_inclusive(hit_eta, eta_bins)
            hm_col = int((hit.emtf_phi >> 4)) - 27
            self.hitmap[hm_row, hm_col] += 1

            # Flag
            self.is_empty = False

    def crop(self, anchor_col):
        # Crop hitmaps
        hitmap_start = max(0, anchor_col - pattern_col_offset)
        hitmap_stop = min(num_z_vs_phi_hitmap_cols, anchor_col + pattern_col_offset + 1)
        pattern_start = max(0, num_z_vs_phi_pattern_cols - hitmap_stop)
        pattern_stop = min(num_z_vs_phi_pattern_cols, num_z_vs_phi_hitmap_cols - hitmap_start)

        cropped_hitmap = np.zeros((num_eta_bins, num_z_vs_phi_pattern_cols), dtype=self.hitmap.dtype)
        cropped_hitmap[:, pattern_start:pattern_stop] = self.hitmap[:, hitmap_start:hitmap_stop]

        # Return
        return cropped_hitmap
