from emtf.commands.phase2.gen_patterns.config import *


class ZvsPhiPatternBuilder():

    def __init__(self, anchor_row):
        self.anchor_row = anchor_row
        self.pattern = np.zeros((num_z_vs_phi_pattern_rows, num_z_vs_phi_pattern_cols))

    def add_hitmap(self, hitmap):
        # Get largest column
        anchor_row = hitmap[self.anchor_row]
        anchor_col = np.argmax(anchor_row)

        # Find copy ranges
        hitmap_start = max(0, anchor_col - pattern_col_offset)
        hitmap_stop = min(num_z_vs_phi_hitmap_cols, anchor_col + pattern_col_offset + 1)
        pattern_start = max(0, num_z_vs_phi_pattern_cols - hitmap_stop)
        pattern_stop = min(num_z_vs_phi_pattern_cols, num_z_vs_phi_hitmap_cols - hitmap_start)

        # Normalize hitmap per row
        hitmap_row_sum = np.sum(hitmap, axis=-1)
        hitmap_row_sum[hitmap_row_sum == 0] = 1
        norm_hitmap = hitmap / hitmap_row_sum[:, np.newaxis]

        # Fill Pattern
        self.pattern[:, pattern_start:pattern_stop] += norm_hitmap[:, hitmap_start:hitmap_stop]

    def add_pattern(self, pattern):
        self.pattern += pattern

    def normalize(self, antialias_en=True):
        # Clone pattern
        norm_pattern = self.pattern.copy()

        # Apply anti-aliasing to anchor_row (ME2) and anchor_row-1 (RE2 or GE2)
        if antialias_en:
            kernel = np.array([1., 2., 1.], dtype=self.pattern.dtype)
            kernel /= kernel.sum()

            for row in [self.anchor_row, self.anchor_row - 1]:
                norm_pattern[row, :] = np.convolve(self.pattern[row, :], kernel[::-1], mode='same')
        else:
            kernel = np.array([1., 2., 1.], dtype=self.pattern.dtype)
            kernel /= kernel.sum()

            for row in range(num_z_vs_phi_pattern_rows):
                norm_pattern[row, :] = np.convolve(self.pattern[row, :], kernel[::-1], mode='same')

        # Normalization
        norm = np.sqrt(np.square(norm_pattern).sum(axis=-1))
        norm[norm == 0] = 1
        norm_pattern /= norm[:, np.newaxis]

        # Return
        return norm_pattern

    def activation(self, threshold):
        norm_pattern = self.normalize(antialias_en=False)

        for row in norm_pattern:
            row[:] = row > threshold

        return norm_pattern


class EtaVsPhiPatternBuilder():

    def __init__(self):
        self.pattern = np.zeros((num_eta_bins, num_z_vs_phi_pattern_cols))

    def add_pattern(self, pattern):
        self.pattern += pattern

    def normalize(self):
        # Clone pattern
        norm_pattern = self.pattern.copy()

        # Normalization
        norm = np.sqrt(np.square(norm_pattern).sum())

        if norm == 0:
            norm = 1

        norm_pattern /= norm

        # Return
        return norm_pattern
