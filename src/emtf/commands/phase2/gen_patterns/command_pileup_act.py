import os

import numpy as np
from ROOT import TFile

from emtf.commands.phase2.gen_patterns.analyzers.pileup_activations import PileUpActivationAnalyzer
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.producers.Phase2EndsecEventProducer import Phase2EndsecEventProducer


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Get Patterns
    signal_patterns_zone0 = np.load('signal_patterns_zone0.npz', allow_pickle=True)
    signal_patterns_zone1 = np.load('signal_patterns_zone1.npz', allow_pickle=True)
    signal_patterns_zone2 = np.load('signal_patterns_zone2.npz', allow_pickle=True)

    masks_zone0 = signal_patterns_zone0['mask'].item()
    masks_zone1 = signal_patterns_zone1['mask'].item()
    masks_zone2 = signal_patterns_zone2['mask'].item()

    # Build Pile-Up Analysis
    main_analyzers = [
        PileUpActivationAnalyzer(0, masks_zone0),
        PileUpActivationAnalyzer(1, masks_zone1),
        PileUpActivationAnalyzer(2, masks_zone2)
    ]

    def analysis_fn():
        return [
            Phase2EndsecEventProducer(),
        ], [
            PileUpActivationAnalyzer(0, masks_zone0),
            PileUpActivationAnalyzer(1, masks_zone1),
            PileUpActivationAnalyzer(2, masks_zone2)
        ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
