from emtf.commands.phase2.gen_patterns.builders.hitmap_builder import ZVsPhiHitmapBuilder
from emtf.commands.phase2.gen_patterns.builders.pattern_builder import ZvsPhiPatternBuilder
from emtf.commands.phase2.gen_patterns.config import *
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.collections import checkout
from emtf.core.commons.tools import nz_sign


class TruthPatternAnalyzer(AbstractAnalyzer):

    def __init__(self, zone, displaced_en):
        super().__init__()

        # Zone
        self.zone = zone

        # Flags
        self.displaced_en = displaced_en

        # Displaced
        self.merge_weights = np.array([1., 1., 1., 1., 1., 1., 1.])
        self.merge_weights /= self.merge_weights.sum()

        # Config
        self.config = zones[zone]
        self.num_invpt_bins = len(self.config['invpt_bins']) - 1
        self.num_lz_bins = len(self.config['lz_bins']) - 1

        # Cache
        self.pattern_builders = dict()

    def process_entry(self, event):

        # Loop sub-events
        for tp in event.muons:
            # Short-Circuit: Must match zone
            # https://github.com/jiafulow/user-notebooks/blob/L1MuonTrigger-P2_11_1_7/10-patterns-zone0.ipynb
            if tp.zone != self.zone:
                continue

            # Short-Circuit: At least 1 hit
            # Note: Jia Fu requires at least 1 sim hit; I use inclusive hit.
            # Inclusive hit is any hit that EMTF received regardless whether it was used for track building or not.
            # https://github.com/jiafulow/user-notebooks/blob/L1MuonTrigger-P2_11_1_7/10-patterns-zone0.ipynb
            if len(tp.hits) == 0:
                continue

                # Short-Circuit: Negative d0
            if tp.d0 < 0:
                continue

            # Short-Circuit: pT below allowed threshold
            if abs(tp.invpt) > self.config['invpt_thres']:
                continue

            # Short-Circuit: dxy below allowed threshold
            if self.displaced_en and (abs(tp.d0) < self.config['dxy_thres']):
                continue

            # Short-Circuit: Eta beyond zone boundary
            if abs(tp.eta_st2) > self.config['eta_thres']:
                continue

            # Build hitmaps using all hits received by EMTF
            hitmap_builder = ZVsPhiHitmapBuilder(self.zone)

            for hit in tp.hits:
                hitmap_builder.add_hit(hit)

            # Skip empty Hitmap
            if hitmap_builder.is_empty:
                continue

            # Short-Circuit: Check if this is a valid event for the zone
            if self.zone == 0:
                has_at_least_2st = np.sum([
                    hitmap_builder.row_has_hit[[0, ]].any(),
                    hitmap_builder.row_has_hit[[1, 2, ]].any(),
                    hitmap_builder.row_has_hit[[3, 4, ]].any(),
                    hitmap_builder.row_has_hit[[5, 6, ]].any(),
                    hitmap_builder.row_has_hit[[7, ]].any(),
                ]) >= 2
            elif self.zone == 1:
                has_at_least_2st = np.sum([
                    hitmap_builder.row_has_hit[[0, 1, ]].any(),
                    hitmap_builder.row_has_hit[[2, ]].any(),
                    hitmap_builder.row_has_hit[[3, 4, ]].any(),
                    hitmap_builder.row_has_hit[[5, 6, ]].any(),
                    hitmap_builder.row_has_hit[[7, ]].any(),
                ]) >= 2
            elif self.zone == 2:
                has_at_least_2st = np.sum([
                    hitmap_builder.row_has_hit[[0, 1, ]].any(),
                    hitmap_builder.row_has_hit[[2, 3, ]].any(),
                    hitmap_builder.row_has_hit[[4, 5, ]].any(),
                    hitmap_builder.row_has_hit[[6, 7, ]].any(),
                ]) >= 2

            if not has_at_least_2st:
                continue

            # Short-Circuit: Invalid Anchor
            anchor_valid = hitmap_builder.row_has_hit[self.config['anchor_row']]

            if not anchor_valid:
                continue

            # Build keys
            tp_qinvpt = tp.q * tp.invpt
            # tp_signed_lxy = nz_sign(tp_dxy) * tp_lxy
            tp_signed_lz = nz_sign(tp.dxy) * abs(tp.vz)

            lz_bin = digitize_inclusive(tp_signed_lz, self.config['lz_bins'])
            invpt_bin = digitize_inclusive(tp_qinvpt, self.config['invpt_bins'])
            key = (lz_bin, invpt_bin)

            if self.displaced_en:
                key = (lz_bin, self.num_invpt_bins // 2)

            lz_bin_mirror = (self.num_lz_bins - 1) - key[0]
            invpt_bin_mirror = (self.num_invpt_bins - 1) - key[1]
            mirror_key = (lz_bin_mirror, invpt_bin_mirror)

            # Add Patterns
            weight = 1

            if self.displaced_en:
                weight = self.merge_weights[invpt_bin]

            hitmap = hitmap_builder.hitmap * weight

            pattern_builder = self._checkout_pattern_builder(key)
            pattern_builder.add_hitmap(hitmap)

            pattern_builder = self._checkout_pattern_builder(mirror_key)
            pattern_builder.add_hitmap(hitmap[:, ::-1])

    def merge(self, other):
        # Add other patterns to this object's patterns
        for other_key, other_pattern_builder in other.pattern_builders.items():
            this_pattern_builder = self._checkout_pattern_builder(other_key)
            this_pattern_builder.add_pattern(other_pattern_builder.pattern)

    def post_production(self):
        # Plot and Save Patterns
        norm_patterns = dict()

        for pattern_key, pattern_builder in self.pattern_builders.items():
            # Get Pattern
            norm_pattern = pattern_builder.normalize(antialias_en=True)

            # Collect Patterns
            norm_patterns[pattern_key] = norm_pattern

        # Save to npz
        np.savez_compressed('truth_patterns_zone%d.npz' % self.zone,
                            zone=self.zone,
                            normalized=norm_patterns)

    # Utils
    def _checkout_pattern_builder(self, key):
        return checkout(self.pattern_builders, key, lambda: ZvsPhiPatternBuilder(self.config['anchor_row']))
