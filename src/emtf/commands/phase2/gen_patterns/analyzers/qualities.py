from emtf.commands.phase2.gen_patterns.config import *
from emtf.commands.phase2.gen_patterns.plotters import SNRPlotter, IntegratedSNRPlotter, SNR2DPlotter
from emtf.commands.phase2.gen_patterns.utils import get_mode_v2, get_mode_v1
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import uniform_step_axis
from emtf.core.plotters.basic import Hist2DPlotter
from emtf.phase2.commons.emtf_constants import img_row_labels


class QualityAnalyzer(AbstractAnalyzer):

    def __init__(self):
        super().__init__()

        self.pattern_keys = set()
        self.zone_quality_luts = dict()

    def process_entry(self, data):
        zone_id, signal_activations, pileup_activations = data

        # Collect Keys
        local_pattern_keys = set()

        for key in signal_activations.keys():
            self.pattern_keys.add(key)
            local_pattern_keys.add(key)

        for key in pileup_activations.keys():
            self.pattern_keys.add(key)
            local_pattern_keys.add(key)

        # Collect SNR
        for key in local_pattern_keys:
            self.collect_snr(zone_id, key, signal_activations[key], pileup_activations[key])

        # Calculate Qualities
        self.calculate_qualities(zone_id)

        # Plot Qualities
        for key in local_pattern_keys:
            self.plot_qualities(zone_id, key, signal_activations[key], pileup_activations[key])

        # Write Luts
        self.write_luts(zone_id)

    def post_production(self):
        pass

    def collect_snr(self, zone_id, pattern_key, signal_activations, pileup_activations):
        # Unpack pattern key
        lxy_bin, pt_bin = pattern_key

        # Checkout plots
        snr_vs_pt_bin_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_pt_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'p_{T} Bin', 'a.u.',
            xbins=uniform_step_axis(1, 0, 6),
            logy=True
        )

        snr_vs_lxy_bin_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_lxy_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'L_{xy} Bin', 'a.u.',
            xbins=uniform_step_axis(1, 0, 6),
            logy=True
        )

        snr_vs_layer_count_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_layer_count_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'No. Layers Activated', 'a.u.',
            xbins=uniform_step_axis(1, 1, 8),
            logy=True, find_best_cut=True
        )

        snr_vs_layer_id_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_layer_id_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Layer Id', 'a.u.',
            xbins=uniform_step_axis(1, 0, 7),
            xlabels=img_row_labels[zone_id],
            logy=True
        )

        snr_vs_act_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_act_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Activation', 'a.u.',
            xbins=uniform_step_axis(1, 0, 256),
            logy=True
        )

        snr_vs_mix_plot = self.checkout_plotter(
            SNR2DPlotter,
            'snr_vs_mix_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Layer Count', 'Layer Id',
            z_title='S \over S+B',
            xbins=uniform_step_axis(1, 1, 8),
            ybins=uniform_step_axis(1, 0, 7),
            ylabels=img_row_labels[zone_id],
            min_val=0,
            max_val=1,
        )

        # Fill Plots
        for activation in range(256):
            # Get Activations
            signal_count = signal_activations.get(activation, 0)
            pileup_count = pileup_activations.get(activation, 0)

            # Plot by d0 Bin
            snr_vs_pt_bin_plot.fill(pt_bin, signal_count, pileup_count)
            snr_vs_lxy_bin_plot.fill(lxy_bin, signal_count, pileup_count)
            snr_vs_act_plot.fill(activation, signal_count, pileup_count)

            # Plot by Layer Count
            layer_count = activation_to_layer_count_lut[activation]
            snr_vs_layer_count_plot.fill(layer_count, signal_count, pileup_count)

            # Plot by Layer Id
            for layer_id in range(num_z_vs_phi_hitmap_rows):
                layer_mask = (1 << layer_id)
                has_layer = ((activation & layer_mask) == layer_mask)

                # Add to hist
                if has_layer:
                    snr_vs_layer_id_plot.fill(layer_id, signal_count, pileup_count)
                    snr_vs_mix_plot.fill(layer_count, layer_id, signal_count, pileup_count)

    def calculate_qualities(self, zone_id):
        # Get Plots
        snr_vs_pt_bin_plot = self.plotters['snr_vs_pt_zone_%d' % zone_id]
        snr_vs_lxy_bin_plot = self.plotters['snr_vs_lxy_zone_%d' % zone_id]
        snr_vs_layer_count_plot = self.plotters['snr_vs_layer_count_zone_%d' % zone_id]
        snr_vs_layer_id_plot = self.plotters['snr_vs_layer_id_zone_%d' % zone_id]
        snr_vs_act_plot = self.plotters['snr_vs_act_zone_%d' % zone_id]
        snr_vs_mix_plot = self.plotters['snr_vs_mix_zone_%d' % zone_id]

        # Consolidate
        snr_vs_pt_bin_plot.consolidate()
        snr_vs_lxy_bin_plot.consolidate()
        snr_vs_layer_count_plot.consolidate()
        snr_vs_layer_id_plot.consolidate()
        snr_vs_act_plot.consolidate()
        snr_vs_mix_plot.consolidate()

        # Extract Ratios
        act_ratios = snr_vs_act_plot.ratios

        # Layer Count Weights
        lc_weights = dict()
        lc_weights[0] = 0
        lc_weights[1] = 1
        lc_weights[2] = 2
        lc_weights[3] = 4
        lc_weights[4] = 8
        lc_weights[5] = 16
        lc_weights[6] = 32
        lc_weights[7] = 64

        # Split Activations by Modes
        # Define Model Quality Ranges:
        # ModeV1=15 and ModeV2=SingleMu(12): [52, 63]
        # ModeV1=[11,13,14] and ModeV2=SingleMu(12): [40, 51]
        # ModeV1=[remaining] and ModeV2=SingleMu(12): [32, 39]
        # ModeV2=DoubleMu(8): [16, 31]
        # ModeV2=TripleMu(4): [8, 15]
        # ModeV2=Single Hit(0): [0, 7]
        activation_group = [list() for i in range(6)]

        for activation in range(256):
            # Mode Score
            mode_v1 = get_mode_v1(zone_id, activation)
            mode_v2 = get_mode_v2(zone_id, activation)

            if (mode_v1 > 8) and (mode_v2 == 12):
                # St1 and at least 1 more station
                if mode_v1 == 15:
                    activation_group[0].append(activation)
                elif mode_v1 in [11, 13, 14]:
                    activation_group[1].append(activation)
                else:
                    activation_group[2].append(activation)
            elif (mode_v1 > 8) and (mode_v2 == 8):
                # St1 and at least 1 more station
                activation_group[3].append(activation)
            elif mode_v2 == 4:
                # At least two stations
                activation_group[4].append(activation)
            elif mode_v2 == 0:
                # Single Station
                activation_group[5].append(activation)
            else:
                print(zone_id, mode_v1, mode_v2)

        # Build Quality LUT
        quality_lut = np.zeros((256,))

        def find_qualities(activations, base_quality, n_divisions, quality_lut):
            print(len(activations))

            # Score
            act_scores = dict()

            for activation in activations:
                # Get Mode V1
                mode_v1 = get_mode_v1(zone_id, activation)
                mode_v1_score = mode_v1 / 15.

                # Layer Count Score
                layer_count = activation_to_layer_count_lut[activation]
                layer_score = layer_count / 8.

                # Activation SNR
                snr_score = act_ratios.get(activation, 0)

                # Calculate Score
                score = 0.485 * mode_v1_score + 0.485 * layer_score + 0.01 * snr_score

                # Append
                act_scores[activation] = score

            # Calculate score thresholds for each of the 15 bins
            quality_thresholds = np.asarray(
                [0] + [np.percentile([*act_scores.values()], (i + 1) / (n_divisions + 1) * 100) for i in
                       range(n_divisions)]
            )

            # Find duplicate thresholds
            quality_floor = dict()

            floor_quality = None
            floor_threshold = None

            for quality in range(n_divisions):
                threshold = quality_thresholds[quality]

                if floor_quality is None or floor_threshold < threshold:
                    floor_quality = quality
                    floor_threshold = threshold
                else:
                    quality_floor[quality] = floor_quality

            # Build quality lut
            for activation in activations:
                act_score = act_scores[activation]

                # Short-Circuit: Lowest score is the base quality
                if act_score == 0:
                    quality_lut[activation] = base_quality
                    continue

                # Apply thresholds
                above_threshold = 1 * (quality_thresholds < act_score)
                above_all_thresholds = above_threshold.all()

                # Short-Circuit: Above all thresholds is the highest quality
                if above_all_thresholds:
                    quality_lut[activation] = (base_quality + n_divisions)
                    continue

                # Find the first threshold that wasn't passed
                quality = np.argmin(above_threshold)

                # Short-Circuit: Lowest score is the base quality
                if quality == 0:
                    quality_lut[activation] = base_quality
                    continue

                # Use previous threshold quality (qual - 1)
                quality = quality - 1

                # Floor the quality in case consecutive qualities share the same threshold
                quality = quality_floor.get(quality, quality)

                # Append
                quality_lut[activation] = (base_quality + quality)

        find_qualities(activation_group[0], 52, 11, quality_lut)
        find_qualities(activation_group[1], 40, 11, quality_lut)
        find_qualities(activation_group[2], 32, 7, quality_lut)
        find_qualities(activation_group[3], 16, 15, quality_lut)
        find_qualities(activation_group[4], 8, 7, quality_lut)
        find_qualities(activation_group[5], 0, 7, quality_lut)

        # Collect Lut
        self.zone_quality_luts[zone_id] = quality_lut

        # Checkout plots
        layer_count_plot = self.checkout_plotter(
            Hist2DPlotter,
            'quality_vs_layer_count_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'No. Layers', 'Quality Bin',
            xbins=uniform_step_axis(1, 1, 8),
            ybins=uniform_step_axis(1, 0, 15)
        )

        layer_id_plot = self.checkout_plotter(
            Hist2DPlotter,
            'quality_vs_layer_id_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Layer Id', 'Quality Bin',
            xbins=uniform_step_axis(1, 0, 7),
            ybins=uniform_step_axis(1, 0, 15),
            xlabels=img_row_labels[zone_id]
        )

        mix_plot = self.checkout_plotter(
            Hist2DPlotter,
            'quality_vs_mix_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Layer Count', 'Layer Id',
            z_title='<Quality>',
            xbins=uniform_step_axis(1, 1, 8),
            ybins=uniform_step_axis(1, 0, 7),
            ylabels=img_row_labels[zone_id],
            min_val=-1
        )

        cnt_plot = self.checkout_plotter(
            Hist2DPlotter,
            'cnt_vs_mix_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Layer Count', 'Layer Id',
            z_title='Count',
            xbins=uniform_step_axis(1, 1, 8),
            ybins=uniform_step_axis(1, 0, 7),
            ylabels=img_row_labels[zone_id]
        )

        # Plot Scores
        for activation in range(256):
            quality = quality_lut[activation]
            quality_bin = int(quality / 4)

            layer_count = activation_to_layer_count_lut[activation]
            layer_count_plot.fill(layer_count, quality_bin)

            for layer_id in range(num_z_vs_phi_hitmap_rows):
                layer_mask = (1 << layer_id)
                has_layer = ((activation & layer_mask) == layer_mask)

                if has_layer:
                    layer_id_plot.fill(layer_id, quality_bin)
                    mix_plot.fill(layer_count, layer_id, w=quality)
                    cnt_plot.fill(layer_count, layer_id)

        # Average mix plot
        mix_plot.plot.Divide(cnt_plot.plot)

    def plot_qualities(self, zone_id, pattern_key, signal_activations, pileup_activations):
        # Checkout plots
        snr_vs_qual_bin_all_zones_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_qual',
            'All Zones',
            'Quality', 'a.u.',
            xbins=uniform_step_axis(1, 0, 63),
            logy=True, find_best_cut=True
        )

        snr_vs_qual_bin_plot = self.checkout_plotter(
            SNRPlotter,
            'snr_vs_qual_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Quality', 'a.u.',
            xbins=uniform_step_axis(1, 0, 63),
            logy=True, find_best_cut=True
        )

        s_vs_qual_bin_all_zones_plot = self.checkout_plotter(
            IntegratedSNRPlotter,
            's_vs_qual',
            'All Zones',
            'Quality Threshold', '# Actv. > Thrs. [a.u.]',
            xbins=np.linspace(0, 63, 64),
            logy=True, find_best_cut=True
        )

        s_vs_qual_bin_plot = self.checkout_plotter(
            IntegratedSNRPlotter,
            's_vs_qual_zone_%d' % (zone_id),
            'Zone %d' % (zone_id),
            'Quality Threshold', '# Actv. > Thrs. [a.u.]',
            xbins=np.linspace(0, 63, 64),
            logy=True, find_best_cut=True
        )

        # Qualities
        quality_lut = self.zone_quality_luts[zone_id]

        for activation in range(256):
            quality = quality_lut[activation]

            # Get Activations
            signal_count = signal_activations.get(activation, 0)
            pileup_count = pileup_activations.get(activation, 0)

            # Fill
            snr_vs_qual_bin_all_zones_plot.fill(quality, signal_count, pileup_count)
            snr_vs_qual_bin_plot.fill(quality, signal_count, pileup_count)
            s_vs_qual_bin_all_zones_plot.fill(quality, signal_count, pileup_count)
            s_vs_qual_bin_plot.fill(quality, signal_count, pileup_count)

    def write_luts(self, zone_id):
        print('Begin Zone %d' % zone_id)

        quality_lut = self.zone_quality_luts[zone_id]

        emu_line = ''

        with open('quality_zone_%d.txt' % zone_id, 'w') as qual_f:
            activation = 0

            for quality in quality_lut:
                # Buffer lines
                break_line = ((activation > 0) and (activation % 24) == 0)

                if break_line:
                    emu_line += ',\n'
                elif activation > 0:
                    emu_line += ', '

                emu_line += ('%d' % quality)

                # Write to file
                qual_f.write('%d ' % quality)

                # Increase activation number
                activation += 1

        print(emu_line)
