from emtf.commands.phase2.gen_patterns.builders.activation_builder import ActivationHitmapBuilder
from emtf.commands.phase2.gen_patterns.config import *
from emtf.core.analyzers import AbstractAnalyzer


class PileUpActivationAnalyzer(AbstractAnalyzer):

    def __init__(self, zone, mask_patterns):
        super().__init__()

        # Zone
        self.zone = zone

        # Config
        self.config = zones[zone]

        # Counters
        self.count_events = 0
        self.count_endsec_events = 0

        # Particle Counters
        self.count_particles = dict()

        # Cache
        self.mask_patterns = mask_patterns
        self.pattern_activations = dict()

    def process_entry(self, event):
        # Init Flag
        event_was_used = False

        # Loop sub-events
        particles_seen = set()

        for endsec, endsec_info in event.endsec.items():
            # Unpack Info
            seg_collection = endsec_info['segs']

            # Short-Circuit: At least 2 seg
            if len(seg_collection) < 2:
                continue

            # Build Hitmap
            hitmap_builder = ActivationHitmapBuilder(self.zone)

            for seg in seg_collection:
                # Check the tracking particle attached to hits
                is_muon = False

                for tp_id in seg.sim_tp:
                    tp_idx = event.tp_id_to_idx.get(tp_id, -1)

                    if tp_idx < 0:
                        continue

                    # Get PDGId
                    tp_pdgid = event.trk_part_pdgid[tp_idx]

                    # Collect PDGID if particle hasn't been seen yet
                    if tp_idx not in particles_seen:
                        particles_seen.add(tp_idx)
                        particle_count = self.count_particles.get(tp_pdgid, 0)
                        self.count_particles[tp_pdgid] = (particle_count + 1)

                    # Flag muons
                    if abs(tp_pdgid) == 13:
                        is_muon = True

                # Add to hitmaps
                hitmap_builder.add_hit(seg, is_muon)

            # Skip empty Hitmap
            if hitmap_builder.is_empty:
                continue

            # Increase Event Count
            if not event_was_used:
                event_was_used = True
                self.count_events += 1

            # Increase Count
            self.count_endsec_events += 1

            # Process
            self.collect_activations(hitmap_builder)

    def collect_activations(self, hitmap_builder):
        # Get hitmap begin and end columns
        begin_col = max(0, hitmap_builder.first_non_zero_col)
        end_col = max(0, hitmap_builder.last_non_zero_col)

        begin_col = max(0, begin_col - pattern_col_offset)
        end_col = min(num_z_vs_phi_hitmap_cols - 1, end_col + pattern_col_offset)

        # Collect Activations
        po2_coeffs = (2 ** np.arange(num_z_vs_phi_pattern_rows))
        sig_hitmap = hitmap_builder.sig_hitmap
        bkg_hitmap = hitmap_builder.bkg_hitmap

        for col_id in range(begin_col, end_col + 1):
            max_col_id = min(num_z_vs_phi_hitmap_cols - 1, col_id + pattern_col_offset)
            min_col_id = max(0, col_id - pattern_col_offset)
            window_width = max_col_id - min_col_id + 1

            min_pcol_id = max(0, pattern_col_offset - col_id)
            max_pcol_id = min_pcol_id + window_width - 1

            sig_window = sig_hitmap[:, min_col_id:(max_col_id + 1)]
            bkg_window = bkg_hitmap[:, min_col_id:(max_col_id + 1)]

            for pattern_key, pattern in self.mask_patterns.items():
                # Calculate activation
                pattern_window = pattern[:, min_pcol_id:(max_pcol_id + 1)]
                sig_bits = (sig_window * pattern_window).any(axis=-1)
                bkg_bits = (bkg_window * pattern_window).any(axis=-1)
                all_bits = (sig_bits | bkg_bits)

                activation = (all_bits * po2_coeffs).sum(axis=-1)

                # Short-Circuit: Skip Zero-Activation
                if activation == 0:
                    continue

                sig_sum = (sig_window * pattern_window).sum(axis=-1).sum(axis=-1)
                bkg_sum = (bkg_window * pattern_window).sum(axis=-1).sum(axis=-1)
                norm = (sig_sum + bkg_sum)

                norm = max(1, norm)
                bkg_score = (bkg_sum / norm)

                # Get Activations
                activations = self.pattern_activations.get(pattern_key, None)

                if activations is None:
                    activations = dict()
                    self.pattern_activations[pattern_key] = activations

                # Increase Counters
                activation_count = activations.get(activation, 0)
                self.pattern_activations[pattern_key][activation] = (activation_count + bkg_score)

    def merge(self, other):
        self.count_events += other.count_events
        self.count_endsec_events += other.count_endsec_events

        for pdgid, other_count in other.count_particles.items():
            this_count = self.count_particles.get(pdgid, 0)
            self.count_particles[pdgid] = (this_count + other_count)

        for other_key, other_counters in other.pattern_activations.items():
            this_counters = self.pattern_activations.get(other_key, None)

            if this_counters is None:
                this_counters = dict()
                self.pattern_activations[other_key] = this_counters

            for activation, other_count in other_counters.items():
                activation_count = this_counters.get(activation, 0)
                self.pattern_activations[other_key][activation] = (activation_count + other_count)

    def post_production(self):
        # Log
        print('Event Count Zone %d: %d' % (self.zone, self.count_events))
        print('Endsec Event Count Zone %d: %d' % (self.zone, self.count_endsec_events))
        print('Particles Observed:', self.count_particles)

        # Save to npz
        np.savez_compressed(
            'pileup_activations_zone%d.npz' % self.zone,
            zone=self.zone,
            event_count=self.count_events,
            particle_count=self.count_particles,
            endsec_event_count=self.count_endsec_events,
            activations=self.pattern_activations
        )
