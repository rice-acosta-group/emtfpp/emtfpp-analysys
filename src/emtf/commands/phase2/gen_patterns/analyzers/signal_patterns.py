from emtf.commands.phase2.gen_patterns.builders.hitmap_builder import ZVsPhiHitmapBuilder, EtaVsPhiHitmapBuilder
from emtf.commands.phase2.gen_patterns.builders.pattern_builder import ZvsPhiPatternBuilder, \
    EtaVsPhiPatternBuilder
from emtf.commands.phase2.gen_patterns.config import *
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.binning import uniform_step_axis
from emtf.core.commons.collections import checkout
from emtf.core.commons.tools import nz_sign
from emtf.core.plotters.basic import Hist2DPlotter
from emtf.phase2.commons.emtf_constants import img_row_labels


class SignalPatternAnalyzer(AbstractAnalyzer):

    def __init__(self, zone, truth_patterns, displaced_en):
        super().__init__()

        # Zone
        self.zone = zone

        # Flags
        self.displaced_en = displaced_en

        # Displaced
        self.merge_weights = np.array([1., 1., 1., 1., 1., 1., 1.])
        self.merge_weights /= self.merge_weights.sum()

        # Config
        self.config = zones[zone]
        self.num_invpt_bins = len(self.config['invpt_bins']) - 1
        self.num_lz_bins = len(self.config['lz_bins']) - 1

        # Cache
        self.zp_truth_patterns = truth_patterns
        self.zp_pattern_builders = dict()
        self.ep_pattern_builders = dict()

    def process_entry(self, event):

        # Loop sub-events
        for tp in event.muons:
            # Short-Circuit: Must match zone
            # https://github.com/jiafulow/user-notebooks/blob/L1MuonTrigger-P2_11_1_7/10-patterns-zone0.ipynb
            if tp.zone != self.zone:
                continue

            # Short-Circuit: At least 1 hit
            # Note: Jia Fu requires at least 1 sim hit; I use inclusive hit.
            # Inclusive hit is any hit that EMTF received regardless whether it was used for track building or not.
            # https://github.com/jiafulow/user-notebooks/blob/L1MuonTrigger-P2_11_1_7/10-patterns-zone0.ipynb
            if len(tp.hits) == 0:
                continue

            # Short-Circuit: Negative d0
            if tp.d0 < 0:
                continue

            # Short-Circuit: pT below allowed threshold
            if abs(tp.invpt) > self.config['invpt_thres']:
                continue

            # Short-Circuit: dxy below allowed threshold
            if self.displaced_en and (abs(tp.d0) < self.config['dxy_thres']):
                continue

            # Short-Circuit: Eta beyond zone boundary
            if abs(tp.eta_st2) > self.config['eta_thres']:
                continue

            # Build hitmaps using all hits received by EMTF
            truth_hitmap_builder = ZVsPhiHitmapBuilder(self.zone)

            for seg in tp.hits:
                truth_hitmap_builder.add_hit(seg)

            # Skip empty Hitmap
            if truth_hitmap_builder.is_empty:
                continue

            # Short-Circuit: Check if this is a valid event for the zone
            if self.zone == 0:
                has_at_least_2st = np.sum([
                    truth_hitmap_builder.row_has_hit[[0, ]].any(),
                    truth_hitmap_builder.row_has_hit[[1, 2, ]].any(),
                    truth_hitmap_builder.row_has_hit[[3, 4, ]].any(),
                    truth_hitmap_builder.row_has_hit[[5, 6, ]].any(),
                    truth_hitmap_builder.row_has_hit[[7, ]].any(),
                ]) >= 2
            elif self.zone == 1:
                has_at_least_2st = np.sum([
                    truth_hitmap_builder.row_has_hit[[0, 1, ]].any(),
                    truth_hitmap_builder.row_has_hit[[2, ]].any(),
                    truth_hitmap_builder.row_has_hit[[3, 4, ]].any(),
                    truth_hitmap_builder.row_has_hit[[5, 6, ]].any(),
                    truth_hitmap_builder.row_has_hit[[7, ]].any(),
                ]) >= 2
            elif self.zone == 2:
                has_at_least_2st = np.sum([
                    truth_hitmap_builder.row_has_hit[[0, 1, ]].any(),
                    truth_hitmap_builder.row_has_hit[[2, 3, ]].any(),
                    truth_hitmap_builder.row_has_hit[[4, 5, ]].any(),
                    truth_hitmap_builder.row_has_hit[[6, 7, ]].any(),
                ]) >= 2

            if not has_at_least_2st:
                continue

            # Build hitmap using hits used in track building
            zp_hitmap_builder = ZVsPhiHitmapBuilder(self.zone)
            ep_hitmap_builder = EtaVsPhiHitmapBuilder(self.zone)

            for seg in tp.segs:
                zp_hitmap_builder.add_hit(seg)
                ep_hitmap_builder.add_hit(seg)

            # Skip empty Hitmap
            if zp_hitmap_builder.is_empty:
                continue

            # Build keys
            tp_qinvpt = tp.q * tp.invpt
            # tp_signed_lxy = nz_sign(tp_dxy) * tp_lxy
            tp_signed_lz = nz_sign(tp.dxy) * abs(tp.vz)

            lz_bin = digitize_inclusive(tp_signed_lz, self.config['lz_bins'])
            invpt_bin = digitize_inclusive(tp_qinvpt, self.config['invpt_bins'])
            key = (lz_bin, invpt_bin)

            if self.displaced_en:
                key = (lz_bin, self.num_invpt_bins // 2)

            lz_bin_mirror = (self.num_lz_bins - 1) - key[0]
            invpt_bin_mirror = (self.num_invpt_bins - 1) - key[1]
            mirror_key = (lz_bin_mirror, invpt_bin_mirror)

            # Get Pattern
            truth_zp_pattern = self.zp_truth_patterns.get(key)

            # Short-Circuit: No pattern found
            if truth_zp_pattern is None:
                continue

            # Crop Patterns
            cropped_zp_hitmap, zp_anchor_col = zp_hitmap_builder.convolve_and_crop(truth_zp_pattern)
            cropped_ep_hitmap = ep_hitmap_builder.crop(zp_anchor_col)

            # Weigh Patterns
            weight = 1.

            if self.displaced_en:
                weight = self.merge_weights[invpt_bin]

            cropped_zp_hitmap = cropped_zp_hitmap * weight
            cropped_ep_hitmap = cropped_ep_hitmap * weight

            # Add ZP Patterns
            zp_pattern = self._checkout_zp_pattern_builder(key)
            zp_pattern.add_pattern(cropped_zp_hitmap)

            zp_pattern = self._checkout_zp_pattern_builder(mirror_key)
            zp_pattern.add_pattern(cropped_zp_hitmap[:, ::-1])

            # Add EP Patterns
            ep_pattern = self._checkout_ep_pattern_builder(key)
            ep_pattern.add_pattern(cropped_ep_hitmap)

            ep_pattern = self._checkout_ep_pattern_builder(mirror_key)
            ep_pattern.add_pattern(cropped_ep_hitmap[:, ::-1])

    def merge(self, other):
        # Add other patterns to this object's patterns
        for other_key, other_zp_pattern_builder in other.zp_pattern_builders.items():
            this_zp_pattern_builder = self._checkout_zp_pattern_builder(other_key)
            this_zp_pattern_builder.add_pattern(other_zp_pattern_builder.pattern)

        for other_key, other_ep_pattern_builder in other.ep_pattern_builders.items():
            this_ep_pattern_builder = self._checkout_ep_pattern_builder(other_key)
            this_ep_pattern_builder.add_pattern(other_ep_pattern_builder.pattern)

    def post_production(self):
        # Plot Eta vs Phi Patterns
        for pattern_key, pattern_builder in self.ep_pattern_builders.items():
            norm_pattern = pattern_builder.normalize()

            self._plot_ep_pattern('ep_pattern_signal_%d_%d_%d' %
                                  (self.zone, *pattern_key,),
                                  pattern_key, norm_pattern)

        # Select Threshold
        activation_threshold = self.config['prompt_threshold']

        if self.displaced_en:
            activation_threshold = self.config['disp_threshold']

        # Loop Patterns
        mask_patterns = dict()
        norm_patterns = dict()
        mask_definitions = dict()

        for pattern_key, pattern_builder in self.zp_pattern_builders.items():
            # Get Patterns
            mask_pattern = pattern_builder.activation(activation_threshold)
            norm_pattern = pattern_builder.normalize(antialias_en=False)
            raw_pattern = pattern_builder.pattern

            # Collect Patterns
            mask_patterns[pattern_key] = mask_pattern
            norm_patterns[pattern_key] = norm_pattern

            # Find Ranges
            n_row = raw_pattern.shape[0]
            n_col = raw_pattern.shape[1]

            col_pos = 0.5 + np.arange(n_col)
            mask_definition = np.zeros((n_row, 3), dtype=np.int32)

            for row_id in range(n_row):
                raw_row = raw_pattern[row_id]
                act_row = mask_pattern[row_id]
                act_nz = np.nonzero(act_row)
                act_nz = ([-1] if len(act_nz[0]) == 0 else act_nz)

                center_ws = (raw_row * col_pos).sum()
                center_wt = raw_row.sum()
                center_wt = (1 if center_wt == 0 else center_wt)

                begin = np.min(act_nz)
                center = int(center_ws / center_wt)
                end = np.max(act_nz)

                act_row[:] = 0
                act_row[begin:(end + 1)] = 1

                mask_definition[row_id, 0:3] = begin, center, end

            # Collect Definitions
            mask_definitions[pattern_key] = mask_definition

            # Plot
            self._plot_zp_pattern('pattern_signal_%d_%d_%d' %
                                  (self.zone, *pattern_key,),
                                  pattern_key, norm_pattern)

            self._plot_zp_pattern('pattern_mask_%d_%d_%d' %
                                  (self.zone, *pattern_key,),
                                  pattern_key, mask_pattern)

        # Save to npz
        np.savez_compressed('signal_patterns_zone%d.npz' % self.zone,
                            zone=self.zone,
                            normalized=norm_patterns,
                            mask=mask_patterns,
                            mask_definition=mask_definitions)

    # Utils
    def _checkout_zp_pattern_builder(self, key):
        return checkout(self.zp_pattern_builders, key, lambda: ZvsPhiPatternBuilder(self.config['anchor_row']))

    def _checkout_ep_pattern_builder(self, key):
        return checkout(self.ep_pattern_builders, key, lambda: EtaVsPhiPatternBuilder())

    def _plot_zp_pattern(self, name, pattern_key, pattern):
        nxbins = uniform_step_axis(1, 0, num_z_vs_phi_pattern_cols - 1)
        nybins = uniform_step_axis(1, 0, num_z_vs_phi_pattern_rows - 1)

        plot = self.checkout_plotter(
            Hist2DPlotter,
            name, 'Zone %d L_{z,bin}=%d p_{T,bin}=%d' % (self.zone, *pattern_key,),
            'Column', 'Layer Id',
            xbins=nxbins, ybins=nybins,
            logx=False, logy=False, logz=True,
            floor_min=True, min_val=1e-3, max_val=1,
            ylabels=img_row_labels[self.zone]
        )

        for row in range(pattern.shape[0]):
            for col in range(pattern.shape[1]):
                plot.fill(col, row, pattern[row, col])

    def _plot_ep_pattern(self, name, pattern_key, pattern):
        nxbins = uniform_step_axis(1, 0, num_z_vs_phi_pattern_cols - 1)
        nybins = eta_bins

        plot = self.checkout_plotter(
            Hist2DPlotter,
            name, 'Zone %d L_{z,bin}=%d p_{T,bin}=%d' % (self.zone, *pattern_key,),
            'Column', 'Eta',
            xbins=nxbins, ybins=nybins,
            logx=False, logy=False, logz=True,
            floor_min=True, min_val=1e-3, max_val=1,
        )

        for row in range(pattern.shape[0]):
            for col in range(pattern.shape[1]):
                eta = row * eta_bin_step + emtf_eta_start
                plot.fill(col, eta, pattern[row, col])
