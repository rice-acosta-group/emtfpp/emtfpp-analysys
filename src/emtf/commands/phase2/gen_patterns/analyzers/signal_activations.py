from emtf.commands.phase2.gen_patterns.builders.hitmap_builder import ZVsPhiHitmapBuilder
from emtf.commands.phase2.gen_patterns.config import *
from emtf.core.analyzers import AbstractAnalyzer


class SignalActivationAnalyzer(AbstractAnalyzer):

    def __init__(self, zone, truth_patterns, mask_patterns, displaced_en):
        super().__init__()

        # Zone
        self.zone = zone

        # Flags
        self.displaced_en = displaced_en

        # Config
        self.config = zones[zone]

        # Counters
        self.count_events = 0
        self.count_muons = 0

        # Cache
        self.truth_patterns = truth_patterns
        self.mask_patterns = mask_patterns
        self.pattern_activations = dict()

    def process_entry(self, event):

        # Init Flag
        event_was_used = False

        # Loop sub-events
        for tp in event.muons:
            # Short-Circuit: Must match zone
            if tp.zone != self.zone:
                continue

            # Short-Circuit: At least 1 seg
            if len(tp.segs) == 0:
                continue

            # Short-Circuit: Negative d0
            if tp.d0 < 0:
                continue

            # Short-Circuit: pT below allowed threshold
            if abs(tp.invpt) > self.config['invpt_thres']:
                continue

            # Short-Circuit: dxy below allowed threshold
            if self.displaced_en and (abs(tp.d0) < self.config['dxy_thres']):
                continue

            # Short-Circuit: Eta beyond zone boundary
            if abs(tp.eta_st2) > self.config['eta_thres']:
                continue

            # Build hitmap using hits used in track building
            hitmap_builder = ZVsPhiHitmapBuilder(self.zone)

            for hit in tp.segs:
                hitmap_builder.add_hit(hit)

            # Skip empty Hitmap
            if hitmap_builder.is_empty:
                continue

            # Increase Event Count
            if not event_was_used:
                event_was_used = True
                self.count_events += 1

            # Increase Muon Count
            self.count_muons += 1

            # Get Hitmap begin and end columns
            begin_col = max(0, hitmap_builder.first_non_zero_col)
            end_col = max(0, hitmap_builder.last_non_zero_col)

            begin_col = max(0, begin_col - pattern_col_offset)
            end_col = min(num_z_vs_phi_hitmap_cols - 1, end_col + pattern_col_offset)

            # Collect Activations
            po2_coeffs = (2 ** np.arange(num_z_vs_phi_pattern_rows))
            hitmap = hitmap_builder.hitmap

            for col_id in range(begin_col, end_col + 1):
                max_col_id = min(num_z_vs_phi_hitmap_cols - 1, col_id + pattern_col_offset)
                min_col_id = max(0, col_id - pattern_col_offset)
                window_width = max_col_id - min_col_id + 1

                min_pcol_id = max(0, pattern_col_offset - col_id)
                max_pcol_id = min_pcol_id + window_width - 1

                hitmap_window = hitmap[:, min_col_id:(max_col_id + 1)]

                for pattern_key, pattern in self.mask_patterns.items():
                    # Calculate activation
                    pattern_window = pattern[:, min_pcol_id:(max_pcol_id + 1)]
                    layer_bits = (hitmap_window * pattern_window).any(axis=-1)
                    activation = (layer_bits * po2_coeffs).sum(axis=-1)

                    # Short-Circuit: Skip Zero-Activation
                    if activation == 0:
                        continue

                    # Get Activations
                    pattern_activations = self.pattern_activations.get(pattern_key, None)

                    if pattern_activations is None:
                        pattern_activations = dict()
                        self.pattern_activations[pattern_key] = pattern_activations

                    # Increase Counters
                    activation_count = pattern_activations.get(activation, 0)
                    self.pattern_activations[pattern_key][activation] = (activation_count + 1)

    def merge(self, other):
        self.count_events += other.count_events
        self.count_muons += other.count_muons

        for pattern_key, other_counters in other.pattern_activations.items():
            # Increase
            this_counters = self.pattern_activations.get(pattern_key, None)

            if this_counters is None:
                this_counters = dict()
                self.pattern_activations[pattern_key] = this_counters

            for activation, other_count in other_counters.items():
                activation_count = this_counters.get(activation, 0)
                self.pattern_activations[pattern_key][activation] = (activation_count + other_count)

    def post_production(self):
        # Log
        print('Event Count Zone %d: %d' % (self.zone, self.count_events))
        print('Muon Count Zone %d: %d' % (self.zone, self.count_muons))

        # Save to npz
        np.savez_compressed('signal_activations_zone%d.npz' % self.zone,
                            zone=self.zone,
                            event_count=self.count_events,
                            muon_count=self.count_muons,
                            activations=self.pattern_activations)
