import numpy as np

from emtf.phase2.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop

eta_bin_step = 0.05
eta_bins = np.arange(emtf_eta_start, emtf_eta_stop, eta_bin_step)
num_eta_bins = int(len(eta_bins) - 1)

num_z_vs_phi_hitmap_rows = 8
num_z_vs_phi_hitmap_cols = 288

num_z_vs_phi_pattern_rows = 8
num_z_vs_phi_pattern_cols = 111  # 30 degrees
pattern_col_offset = (num_z_vs_phi_pattern_cols - 1) // 2  # (111-1) / 2 = 55
pattern_col_padding = 36  # ad-hoc, reduces pattern winsz to ~20 deg

# Zone dependent settings
zones = {
    0: {
        'invpt_bins': np.array([-0.5, -0.3, -0.2, -0.1, 0.1, 0.2, 0.3, 0.5]),
        'lz_bins': np.array([-500.0, -292.79, -146.77, -50.72, 50.72, 146.77, 292.79, 500.0]),
        'dxy_thres': 0.,
        'invpt_thres': 0.5,
        'eta_thres': 2.45,
        'anchor_row': 4,  # ME2/1
        'prompt_threshold': 0.06,
        'disp_threshold': 0.06,
    },
    1: {
        'invpt_bins': np.array([-0.5, -0.3, -0.2, -0.1, 0.1, 0.2, 0.3, 0.5]),
        'lz_bins': np.array([-500.0, -275.21, -132.05, -44.47, 44.47, 132.05, 275.21, 500.0]),
        'dxy_thres': 0.,
        'invpt_thres': 0.45,
        'eta_thres': 2.05,
        'anchor_row': 4,  # ME2/1
        'prompt_threshold': 0.06,
        'disp_threshold': 0.06,
    },
    2: {
        'invpt_bins': np.array([-0.5, -0.3, -0.2, -0.1, 0.1, 0.2, 0.3, 0.5]),
        'lz_bins': np.array([-500.0, -248.33, -114.04, -36.44, 36.44, 114.04, 248.33, 500.0]),
        'dxy_thres': 0.,
        'invpt_thres': 0.45,
        'eta_thres': 1.65,
        'anchor_row': 3,  # ME2/2
        'prompt_threshold': 0.05,
        'disp_threshold': 0.05
    }
}

# Activation to Layer Count
activation_to_layer_count_lut = dict()

for activation in range(256):
    layer_count = 0

    for layer_id in range(num_z_vs_phi_hitmap_rows):
        layer_mask = (1 << layer_id)

        # Increase counter
        layer_count += ((activation & layer_mask) == layer_mask)

    activation_to_layer_count_lut[activation] = layer_count
