import math

from ROOT import TCanvas
from ROOT import TH1D
from ROOT import TH2D
from ROOT import TLatex
from ROOT import TLegend
from ROOT import TLine
from ROOT import TPad
from ROOT import gStyle
from ROOT import kBlue
from ROOT import kRed
from ROOT import kSpring
from ROOT import kWhite

from emtf.core.analyzers import AbstractPlotter
from emtf.core.root.labels import draw_fancy_label


class SNRPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 min_val=None, max_val=None,
                 logx=False, logy=False,
                 xlabels=None,
                 find_best_cut=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1
            x_low = min(xbins)
            x_up = max(xbins)

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy
        self.xlabels = xlabels

        if all(v is not None for v in [x_low, x_up]):
            self.sig_plot = TH1D(name + '_num', title, nxbins, x_low, x_up)
            self.bkg_plot = TH1D(name + '_denom', title, nxbins, x_low, x_up)
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.sig_plot = TH1D(name + '_num', title, nxbins, xbins)
            self.bkg_plot = TH1D(name + '_denom', title, nxbins, xbins)
            self.plot = TH1D(name, title, nxbins, xbins)

        self.min_val = min_val
        self.max_val = max_val
        self.find_best_cut = find_best_cut

        self.consolidated = False

        self.ratios = dict()
        self.total_sig = 0
        self.total_bkg = 0
        self.cut_at_bin = -1

    def fill(self, x, sig, bkg):
        self.sig_plot.Fill(x, sig)
        self.bkg_plot.Fill(x, bkg)

    def consolidate(self):
        # Short-Circuit: Don't consolidate again
        if self.consolidated:
            return

        self.consolidated = True

        # Calculate SNR & Integrate
        for bin_id in range(self.plot.GetNcells()):
            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            # Integrate
            self.total_sig += signal
            self.total_bkg += background

            # Calculate SNR
            snr = 0
            snr_error = 0

            total = signal + background

            if total > 0:
                snr = signal / total
                snr_error = math.sqrt((background ** 2) * signal + (signal ** 2) * background) / (total ** 2)

            self.plot.SetBinContent(bin_id, snr)
            self.plot.SetBinError(bin_id, snr_error)

        # Subtract Underflow
        for bin_id in [0]:
            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            self.total_sig -= signal
            self.total_bkg -= background

        # Find best min bin
        if self.find_best_cut:
            best_bin_cut = -1
            best_metric = -1

            right_sig = self.total_sig
            right_bkg = self.total_bkg

            for col_id in range(self.nxbins):
                bin_id = self.plot.GetBin(col_id + 1)

                # Calculate Metric
                right_metric = 0

                bin_sig = self.sig_plot.GetBinContent(bin_id)
                bin_bkg = self.bkg_plot.GetBinContent(bin_id)

                right_sig -= bin_sig
                right_bkg -= bin_bkg

                right_total = right_sig + right_bkg

                if right_total > 0:
                    right_metric = right_sig / math.sqrt(right_total)

                if best_metric < right_metric:
                    best_bin_cut = bin_id
                    best_metric = right_metric

            self.cut_at_bin = best_bin_cut

        # Collect Ratios
        for col_id in range(self.nxbins):
            bin_id = self.plot.GetBin(col_id + 1)
            ratio = self.plot.GetBinContent(bin_id)
            self.ratios[col_id] = ratio

    def write(self):
        # WRITE
        self.plot.Write()

        # Best Thrs.
        best_center = 0

        if self.cut_at_bin > -1:
            best_center = self.plot.GetXaxis().GetBinCenter(self.cut_at_bin)

        # IMAGE
        gStyle.SetOptStat(0)

        # CREATE CANVAS
        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.200)
        plot_canvas.SetRightMargin(0.200)
        plot_canvas.SetBottomMargin(0.200)

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetTopMargin(0.125)
        top_pad.SetLeftMargin(0.175)
        top_pad.SetRightMargin(0.050)
        top_pad.SetBottomMargin(0.300)
        top_pad.SetLogx(1 if self.logx else 0)
        top_pad.SetLogy(1 if self.logy else 0)
        top_pad.Draw()
        top_pad.cd()

        # Draw Before and After
        min_top = min(self.sig_plot.GetMinimum(), self.bkg_plot.GetMinimum())
        max_top = max(self.sig_plot.GetMaximum(), self.bkg_plot.GetMaximum())
        min_top = max(1, min_top * 0.8)

        frame_top = plot_canvas.DrawFrame(
            self.x_low, min_top,
            self.x_up, max_top * 1.2,
            '')

        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)

        frame_top.GetYaxis().SetTitle(self.y_title)
        frame_top.GetYaxis().SetTitleOffset(1.450)

        self.sig_plot.SetFillColorAlpha(kBlue, 0.5)
        self.bkg_plot.SetFillColorAlpha(kRed, 0.5)

        self.bkg_plot.Draw('SAME HIST')
        self.sig_plot.Draw('SAME HIST')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')

        # Draw Best Thrs. Line
        cut_line = TLine(best_center, min_top, best_center, max_top * 1.1)
        cut_line.SetLineStyle(10)
        cut_line.SetLineWidth(3)
        cut_line.SetLineColor(kRed)

        if best_center > 0:
            cut_line.Draw('SAME')

        # Draw Legend
        legend_x0 = 0.235
        legend_y0 = 0.350
        legend_w0 = 0.365
        legend_h0 = legend_w0 / 1.777

        legend = TLegend(legend_x0, legend_y0, legend_x0 + legend_w0, legend_y0 + legend_h0)
        legend.SetMargin(0.250)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)
        legend.SetFillColorAlpha(kWhite, 0.5)

        legend.AddEntry(self.sig_plot, 'Single Muon', 'F')
        legend.AddEntry(self.bkg_plot, 'Pile-Up', 'F')

        legend.Draw()

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("center", "center", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(0.750)
        bottom_pad.SetLeftMargin(0.175)
        bottom_pad.SetRightMargin(0.050)
        bottom_pad.SetBottomMargin(0.160)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw Ratio
        frame_bottom = plot_canvas.DrawFrame(
            self.x_low, 0.08,
            self.x_up, 1.2,
            '')

        frame_bottom.GetXaxis().SetTitle(self.x_title)
        frame_bottom.GetXaxis().SetTitleOffset(1.350)
        frame_bottom.GetYaxis().SetTitle('S \over S + B')
        frame_bottom.GetYaxis().SetNdivisions(203)
        frame_bottom.GetYaxis().SetTitleOffset(1.450)

        if self.xlabels is not None:
            for bin_num in range(self.nxbins):
                bin_id = frame_bottom.GetXaxis().FindBin(bin_num)
                frame_bottom.GetXaxis().SetBinLabel(bin_id, self.xlabels[bin_num])

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')

        # Draw 90% Line
        perc90_line = TLine(self.x_low, 0.9, self.x_up, 0.9)
        perc90_line.SetLineStyle(10)
        perc90_line.SetLineWidth(3)
        perc90_line.SetLineColor(kSpring + 4)
        perc90_line.Draw('SAME')

        # Draw Best Thrs. Line
        cut_line_bot = TLine(best_center, 0.08, best_center, 1.2)
        cut_line_bot.SetLineStyle(10)
        cut_line_bot.SetLineWidth(3)
        cut_line_bot.SetLineColor(kRed)

        if best_center > 0:
            cut_line_bot.Draw('SAME')

        # Draw Points
        self.plot.SetMarkerStyle(8)
        self.plot.SetMarkerSize(0.5)
        self.plot.Draw('SAME P E')

        # Draw Titles
        plot_canvas.cd(0)

        main_title = TLatex(0.045, 0.95, self.title)
        main_title.SetTextAlign(12)
        main_title.Draw()

        if best_center > 0:
            draw_fancy_label(0.65, 0.80, text='Best Thrs. @ %0.2f' % (best_center))

        # Save
        plot_canvas.SaveAs(self.name + '.png')


class SNR2DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title, z_title=None,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None,
                 logx=False, logy=False, logz=False,
                 xlabels=None, ylabels=None,
                 density=False, normalize_by=None,
                 floor_min=True, min_val=None, max_val=None):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.z_title = z_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins
        self.logx = logx
        self.logy = logy
        self.logz = logz
        self.density = density
        self.normalize_by = normalize_by
        self.xlabels = xlabels
        self.ylabels = ylabels

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.sig_plot = TH2D(name + '_num', title, nxbins, x_low, x_up, nybins, y_low, y_up)
            self.bkg_plot = TH2D(name + '_denom', title, nxbins, x_low, x_up, nybins, y_low, y_up)
            self.plot = TH2D(name, title, nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.sig_plot = TH2D(name + '_num', title, nxbins, xbins, nybins, ybins)
            self.bkg_plot = TH2D(name + '_denom', title, nxbins, xbins, nybins, ybins)
            self.plot = TH2D(name, title, nxbins, xbins, nybins, ybins)

        self.floor_min = floor_min
        self.min_val = min_val
        self.max_val = max_val

        self.consolidated = False

        self.ratios = dict()
        self.total_sig = 0
        self.total_bkg = 0

    def fill(self, x, y, sig, bkg):
        self.sig_plot.Fill(x, y, sig)
        self.bkg_plot.Fill(x, y, bkg)

    def consolidate(self):
        # Short-Circuit: Don't consolidate again
        if self.consolidated:
            return

        self.consolidated = True

        # Calculate SNR & Integrate
        for bin_id in range(self.plot.GetNcells()):
            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            # Integrate
            self.total_sig += signal
            self.total_bkg += background

            # Calculate SNR
            snr = 0
            snr_error = 0

            total = signal + background

            if total > 0:
                snr = signal / total
                snr_error = math.sqrt((background ** 2) * signal + (signal ** 2) * background) / (total ** 2)

            self.plot.SetBinContent(bin_id, snr)
            self.plot.SetBinError(bin_id, snr_error)

        # Collect Ratios
        for row_id in range(self.nybins):
            for col_id in range(self.nxbins):
                bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                ratio = self.plot.GetBinContent(bin_id)
                self.ratios[(col_id, row_id)] = ratio

    def write(self):
        # WRITE
        self.plot.Write()

        # Normalize
        if self.normalize_by is None:
            pass
        elif self.normalize_by == 'row':
            for row_id in range(self.nybins):
                norm = 0

                for col_id in range(self.nxbins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    norm += bin_val

                for col_id in range(self.nxbins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    bin_val = bin_val / max(norm, 1)
                    self.plot.SetBinContent(bin_id, bin_val)
        elif self.normalize_by == 'column':
            for col_id in range(self.nxbins):
                norm = 0

                for row_id in range(self.nybins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    norm += bin_val

                for row_id in range(self.nybins):
                    bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                    bin_val = self.plot.GetBinContent(bin_id)
                    bin_val = bin_val / max(norm, 1)
                    self.plot.SetBinContent(bin_id, bin_val)

        # Scale
        if self.density and self.plot.GetSumOfWeights() > 0:
            self.plot.Scale(1 / self.plot.GetSumOfWeights())

        # Floor
        if self.min_val is None:
            floor_val = self.plot.GetMinimum() * 0.1
        else:
            floor_val = self.min_val

        if self.floor_min:
            for bin_id in range(self.plot.GetNcells()):
                val = self.plot.GetBinContent(bin_id)

                if val < floor_val:
                    self.plot.SetBinContent(bin_id, floor_val)

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.150)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.SetLogz(1 if self.logz else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.65)
        self.plot.GetYaxis().SetMaxDigits(3)

        # Set Z-Axis
        if self.z_title is not None:
            self.plot.GetZaxis().SetTitle(self.z_title)
            self.plot.GetZaxis().SetTitleOffset(1.25)
            plot_canvas.SetRightMargin(0.200)

        # Set Labels
        if self.xlabels is not None:
            for col_id in range(self.nxbins):
                bin_id = self.plot.GetXaxis().FindBin(col_id)
                self.plot.GetXaxis().SetBinLabel(bin_id, self.xlabels[col_id])

        if self.ylabels is not None:
            for row_id in range(self.nybins):
                bin_id = self.plot.GetYaxis().FindBin(row_id)
                self.plot.GetYaxis().SetBinLabel(bin_id, self.ylabels[row_id])

        # min/max
        if self.density:
            self.plot.SetMaximum(1.1)
            self.plot.SetMinimum(1e-5)
        else:
            if self.max_val is not None:
                self.plot.SetMaximum(self.max_val)

            if self.min_val is not None:
                self.plot.SetMinimum(self.min_val)

        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')


class IntegratedSNRPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 min_val=None, max_val=None,
                 logx=False, logy=False,
                 xlabels=None,
                 find_best_cut=False):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1
            x_low = min(xbins)
            x_up = max(xbins)

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.nxbins = nxbins
        self.x_low = x_low
        self.x_up = x_up
        self.xbins = xbins
        self.logx = logx
        self.logy = logy
        self.xlabels = xlabels

        if all(v is not None for v in [x_low, x_up]):
            self.sig_plot = TH1D(name + '_num', title, nxbins, x_low, x_up)
            self.bkg_plot = TH1D(name + '_denom', title, nxbins, x_low, x_up)
            self.plot = TH1D(name, title, nxbins, x_low, x_up)
        elif all(v is not None for v in [xbins]):
            self.sig_plot = TH1D(name + '_num', title, nxbins, xbins)
            self.bkg_plot = TH1D(name + '_denom', title, nxbins, xbins)
            self.plot = TH1D(name, title, nxbins, xbins)

        self.min_val = min_val
        self.max_val = max_val
        self.find_best_cut = find_best_cut

        self.consolidated = False

        self.ratios = dict()
        self.total_sig = 0
        self.total_bkg = 0
        self.cut_at_bin = -1

    def fill(self, x, sig, bkg):
        self.sig_plot.Fill(x, sig)
        self.bkg_plot.Fill(x, bkg)

    def consolidate(self):
        # Short-Circuit: Don't consolidate again
        if self.consolidated:
            return

        self.consolidated = True

        # Integrate
        for bin_id in range(self.plot.GetNcells()):
            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            self.total_sig += signal
            self.total_bkg += background

        # Subtract Underflow
        for bin_id in [0]:
            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            self.total_sig -= signal
            self.total_bkg -= background

        # Calculate Sensitivity & SNR
        best_bin_cut = -1
        best_metric = -1

        right_sig = self.total_sig
        right_bkg = self.total_bkg

        for bin_num in range(self.nxbins):
            bin_id = self.plot.GetBin(bin_num + 1)

            # Calculate Sensitivity
            right_sensitivity = 0

            right_snr = 0
            right_snr_error = 0

            bin_sig = self.sig_plot.GetBinContent(bin_id)
            bin_bkg = self.bkg_plot.GetBinContent(bin_id)

            right_sig -= bin_sig
            right_bkg -= bin_bkg

            right_total = right_sig + right_bkg

            if right_total > 0:
                right_sensitivity = right_sig / math.sqrt(right_total)

                right_snr = right_sig / right_total
                right_snr_error = math.sqrt((right_bkg ** 2) * right_sig + (right_sig ** 2) * right_bkg) / (
                        right_total ** 2)

            if best_metric < right_sensitivity:
                best_bin_cut = bin_id
                best_metric = right_sensitivity

            # Update Bins
            self.plot.SetBinContent(bin_id, right_snr)
            self.plot.SetBinError(bin_id, right_snr_error)
            self.sig_plot.SetBinContent(bin_id, right_sig)
            self.bkg_plot.SetBinContent(bin_id, right_bkg)

        self.cut_at_bin = best_bin_cut

        # Collect Ratios
        for bin_num in range(self.nxbins):
            bin_id = self.plot.GetBin(bin_num + 1)
            ratio = self.plot.GetBinContent(bin_id)
            self.ratios[bin_num] = ratio

    def write(self):
        # WRITE
        self.plot.Write()

        # Best Thrs.
        best_cut = 0

        if self.cut_at_bin > -1:
            best_cut = self.plot.GetXaxis().GetBinLowEdge(self.cut_at_bin)

        # IMAGE
        gStyle.SetOptStat(0)

        # CREATE CANVAS
        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.200)
        plot_canvas.SetRightMargin(0.200)
        plot_canvas.SetBottomMargin(0.200)

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetTopMargin(0.125)
        top_pad.SetLeftMargin(0.175)
        top_pad.SetRightMargin(0.050)
        top_pad.SetBottomMargin(0.300)
        top_pad.SetLogx(1 if self.logx else 0)
        top_pad.SetLogy(1 if self.logy else 0)
        top_pad.Draw()
        top_pad.cd()

        # Draw Before and After
        min_top = min(self.sig_plot.GetMinimum(), self.bkg_plot.GetMinimum())
        max_top = max(self.sig_plot.GetMaximum(), self.bkg_plot.GetMaximum())
        min_top = max(1, min_top * 0.8)

        frame_top = plot_canvas.DrawFrame(
            self.x_low, min_top,
            self.x_up, max_top * 1.2,
            '')

        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)

        frame_top.GetYaxis().SetTitle(self.y_title)
        frame_top.GetYaxis().SetTitleOffset(1.450)

        self.sig_plot.SetFillColorAlpha(kBlue, 0.5)
        self.bkg_plot.SetFillColorAlpha(kRed, 0.5)

        self.bkg_plot.Draw('SAME HIST')
        self.sig_plot.Draw('SAME HIST')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')

        # Draw Best Thrs. Line
        cut_line = TLine(best_cut, min_top, best_cut, max_top * 1.1)
        cut_line.SetLineStyle(10)
        cut_line.SetLineWidth(3)
        cut_line.SetLineColor(kRed)

        if best_cut > 0:
            cut_line.Draw('SAME')

        # Draw Legend
        legend_x0 = 0.235
        legend_y0 = 0.350
        legend_w0 = 0.365
        legend_h0 = legend_w0 / 1.777

        legend = TLegend(legend_x0, legend_y0, legend_x0 + legend_w0, legend_y0 + legend_h0)
        legend.SetMargin(0.250)
        legend.SetBorderSize(0)
        legend.SetTextSize(.035)
        legend.SetFillColorAlpha(kWhite, 0.5)

        legend.AddEntry(self.sig_plot, 'Single Muon', 'F')
        legend.AddEntry(self.bkg_plot, 'Pile-Up', 'F')

        legend.Draw()

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("center", "center", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(0.750)
        bottom_pad.SetLeftMargin(0.175)
        bottom_pad.SetRightMargin(0.050)
        bottom_pad.SetBottomMargin(0.160)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw Ratio
        frame_bottom = plot_canvas.DrawFrame(
            self.x_low, 0.08,
            self.x_up, 1.2,
            '')

        frame_bottom.GetXaxis().SetTitle(self.x_title)
        frame_bottom.GetXaxis().SetTitleOffset(1.350)
        frame_bottom.GetYaxis().SetTitle('S \over S + B')
        frame_bottom.GetYaxis().SetNdivisions(203)
        frame_bottom.GetYaxis().SetTitleOffset(1.450)

        if self.xlabels is not None:
            for bin_num in range(self.nxbins):
                bin_id = frame_bottom.GetXaxis().FindBin(bin_num)
                frame_bottom.GetXaxis().SetBinLabel(bin_id, self.xlabels[bin_num])

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')

        # Draw 90% Line
        perc90_line = TLine(self.x_low, 0.9, self.x_up, 0.9)
        perc90_line.SetLineStyle(10)
        perc90_line.SetLineWidth(3)
        perc90_line.SetLineColor(kSpring + 4)
        perc90_line.Draw('SAME')

        # Draw Best Thrs. Line
        cut_line_bot = TLine(best_cut, 0.08, best_cut, 1.2)
        cut_line_bot.SetLineStyle(10)
        cut_line_bot.SetLineWidth(3)
        cut_line_bot.SetLineColor(kRed)

        if best_cut > 0:
            cut_line_bot.Draw('SAME')

        # Draw Points
        self.plot.SetMarkerStyle(8)
        self.plot.SetMarkerSize(0.5)
        self.plot.Draw('SAME P E')

        # Draw Titles
        plot_canvas.cd(0)

        main_title = TLatex(0.045, 0.95, self.title)
        main_title.SetTextAlign(12)
        main_title.Draw()

        if best_cut > 0:
            draw_fancy_label(0.65, 0.80, text='Best Thrs. @ %0.2f' % (best_cut))

        # Save
        plot_canvas.SaveAs(self.name + '.png')


class IntegratedSNR2DPlotter(AbstractPlotter):

    def __init__(self, name, title, x_title, y_title, z_title=None,
                 nxbins=None, x_low=None, x_up=None, xbins=None,
                 nybins=None, y_low=None, y_up=None, ybins=None,
                 logx=False, logy=False, logz=False,
                 xlabels=None, ylabels=None,
                 density=False, normalize_by=None,
                 floor_min=True, min_val=None, max_val=None):
        super().__init__()

        if xbins is not None:
            nxbins = len(xbins) - 1

        if ybins is not None:
            nybins = len(ybins) - 1

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.z_title = z_title
        self.nxbins = nxbins
        self.nybins = nybins
        self.x_low = x_low
        self.x_up = x_up
        self.y_low = y_low
        self.y_up = y_up
        self.xbins = xbins
        self.ybins = ybins
        self.logx = logx
        self.logy = logy
        self.logz = logz
        self.density = density
        self.normalize_by = normalize_by
        self.xlabels = xlabels
        self.ylabels = ylabels

        if all(v is not None for v in [nxbins, x_low, x_up, nybins, y_low, y_up]):
            self.sig_plot = TH2D(name + '_num', title, nxbins, x_low, x_up, nybins, y_low, y_up)
            self.bkg_plot = TH2D(name + '_denom', title, nxbins, x_low, x_up, nybins, y_low, y_up)
            self.plot = TH2D(name, title, nxbins, x_low, x_up, nybins, y_low, y_up)
        elif all(v is not None for v in [xbins, ybins]):
            self.sig_plot = TH2D(name + '_num', title, nxbins, xbins, nybins, ybins)
            self.bkg_plot = TH2D(name + '_denom', title, nxbins, xbins, nybins, ybins)
            self.plot = TH2D(name, title, nxbins, xbins, nybins, ybins)

        self.floor_min = floor_min
        self.min_val = min_val
        self.max_val = max_val

        self.consolidated = False

        self.ratios = dict()
        self.total_sig = 0
        self.total_bkg = 0

    def fill(self, x, y, sig, bkg):
        self.sig_plot.Fill(x, y, sig)
        self.bkg_plot.Fill(x, y, bkg)

    def consolidate(self):
        # Short-Circuit: Don't consolidate again
        if self.consolidated:
            return

        self.consolidated = True

        # Integrate
        for bin_id in range(self.plot.GetNcells()):
            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            self.total_sig += signal
            self.total_bkg += background

        # Subtract Underflow Bins
        for row_id in range(self.nybins):
            bin_id = self.plot.GetBin(0, row_id + 1, 0)

            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            self.total_sig -= signal
            self.total_bkg -= background

        for col_id in range(self.nxbins):
            bin_id = self.plot.GetBin(col_id + 1, 0, 0)

            signal = self.sig_plot.GetBinContent(bin_id)
            background = self.bkg_plot.GetBinContent(bin_id)

            self.total_sig -= signal
            self.total_bkg -= background

        # Calculate Sensitivity & SNR
        best_bin_cut = -1
        best_metric = -1

        right_sig = self.total_sig
        right_bkg = self.total_bkg

        for col_id in range(self.nxbins):
            for row_id in range(self.nybins):
                bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)

                # Calculate Sensitivity
                right_sensitivity = 0

                right_snr = 0
                right_snr_error = 0

                bin_signal = self.sig_plot.GetBinContent(bin_id)
                bin_background = self.bkg_plot.GetBinContent(bin_id)

                right_sig -= bin_signal
                right_bkg -= bin_background

                right_total = right_sig + right_bkg

                if right_total > 0:
                    right_sensitivity = right_sig / math.sqrt(right_total)

                    right_snr = right_sig / right_total
                    right_snr_error = math.sqrt((right_bkg ** 2) * right_sig + (right_sig ** 2) * right_bkg) / (
                            right_total ** 2)

                if best_metric < right_sensitivity:
                    best_bin_cut = (col_id, row_id)
                    best_metric = right_sensitivity

                # Update Bins
                self.plot.SetBinContent(bin_id, right_snr)
                self.plot.SetBinError(bin_id, right_snr_error)
                self.sig_plot.SetBinContent(bin_id, right_sig)
                self.bkg_plot.SetBinContent(bin_id, right_bkg)

        self.cut_at_bin = best_bin_cut

        # Collect Ratios
        for col_id in range(self.nxbins):
            for row_id in range(self.nybins):
                bin_id = self.plot.GetBin(col_id + 1, row_id + 1, 0)
                ratio = self.plot.GetBinContent(bin_id)
                self.ratios[(col_id, row_id)] = ratio

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetRightMargin(0.150)
        plot_canvas.SetBottomMargin(0.160)
        plot_canvas.SetLogx(1 if self.logx else 0)
        plot_canvas.SetLogy(1 if self.logy else 0)
        plot_canvas.SetLogz(1 if self.logz else 0)
        plot_canvas.cd(0)

        self.plot.GetXaxis().SetTitle(self.x_title)
        self.plot.GetYaxis().SetTitle(self.y_title)
        self.plot.GetYaxis().SetTitleOffset(1.65)
        self.plot.GetYaxis().SetMaxDigits(3)

        # Set Z-Axis
        if self.z_title is not None:
            self.plot.GetZaxis().SetTitle(self.z_title)
            self.plot.GetZaxis().SetTitleOffset(1.25)
            plot_canvas.SetRightMargin(0.200)

        # Set Labels
        if self.xlabels is not None:
            for col_id in range(self.nxbins):
                bin_id = self.plot.GetXaxis().FindBin(col_id)
                self.plot.GetXaxis().SetBinLabel(bin_id, self.xlabels[col_id])

        if self.ylabels is not None:
            for row_id in range(self.nybins):
                bin_id = self.plot.GetYaxis().FindBin(row_id)
                self.plot.GetYaxis().SetBinLabel(bin_id, self.ylabels[row_id])

        # min/max
        if self.max_val is not None:
            self.plot.SetMaximum(self.max_val)

        if self.min_val is not None:
            self.plot.SetMinimum(self.min_val)

        self.plot.Draw('COLZ')

        plot_canvas.SaveAs(self.name + '.png')
