import numpy as np

from emtf.commands.phase2.gen_patterns.analyzers.qualities import QualityAnalyzer


def configure_parser(parser):
    pass


def run(args):
    # Get Activations
    signal_activations_zone0 = np.load('signal_activations_zone0.npz', allow_pickle=True)
    signal_activations_zone1 = np.load('signal_activations_zone1.npz', allow_pickle=True)
    signal_activations_zone2 = np.load('signal_activations_zone2.npz', allow_pickle=True)

    pileup_activations_zone0 = np.load('pileup_activations_zone0.npz', allow_pickle=True)
    pileup_activations_zone1 = np.load('pileup_activations_zone1.npz', allow_pickle=True)
    pileup_activations_zone2 = np.load('pileup_activations_zone2.npz', allow_pickle=True)

    signal_activations_zone0 = signal_activations_zone0['activations'].item()
    signal_activations_zone1 = signal_activations_zone1['activations'].item()
    signal_activations_zone2 = signal_activations_zone2['activations'].item()

    pileup_activations_zone0 = pileup_activations_zone0['activations'].item()
    pileup_activations_zone1 = pileup_activations_zone1['activations'].item()
    pileup_activations_zone2 = pileup_activations_zone2['activations'].item()

    # Calculate Qualities
    zones = [
        (0, signal_activations_zone0, pileup_activations_zone0),
        (1, signal_activations_zone1, pileup_activations_zone1),
        (2, signal_activations_zone2, pileup_activations_zone2),
    ]

    for zone in zones:
        analyzer = QualityAnalyzer()
        analyzer.process_entry(zone)
        analyzer.write()
