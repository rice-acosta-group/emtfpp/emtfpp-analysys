import os

from ROOT import TFile

from emtf.commands.phase2.calibrate.analyzers.calibrate_emtf_dxy import EMTFDxyCalibrationAnalyzer
from emtf.commands.phase2.calibrate.analyzers.calibrate_emtf_pt import EMTFPtCalibrationAnalyzer
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.commons.emtf_constants import emtf_pt_eps, emtf_dxy_eps
from emtf.phase2.producers.Phase2MuonEventProducer import Phase2MuonEventProducer
from emtf.phase2.producers.Phase2ParameterAssignment import Phase2ParameterAssignment


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--pt-exact-up-to', dest='pt_exact_up_to', metavar='PT_EXACT_UP_TO',
                        type=float, default=None,
                        help='pt at which the calibration is extrapolated')

    parser.add_argument('--dxy-exact-up-to', dest='dxy_exact_up_to', metavar='DXY_EXACT_UP_TO',
                        type=float, default=None,
                        help='dxy at which the calibration is extrapolated')

    parser.add_argument('--pt-ord', dest='pt_order', metavar='PT_POLYNOMIAL_ORDER',
                        type=int, default=2,
                        help='Order of the polynomial to fit to pt calibration')

    parser.add_argument('--dxy-ord', dest='dxy_order', metavar='DXY_POLYNOMIAL_ORDER',
                        type=int, default=2,
                        help='Order of the polynomial to fit to dxy calibration')

    parser.add_argument('--nmuon', dest='n_muon', metavar='N_MUON',
                        type=int, default=1,
                        help='Number of muons per event')

    parser.add_argument('--prompt-pb', dest='prompt_pbf', metavar='PROMPT_PROTOBUF_FILE',
                        type=str, default=None,
                        help='Prompt Protobuf for re-assigning track parameters')

    parser.add_argument('--disp-pb', dest='disp_pbf', metavar='DISP_PROTOBUF_FILE',
                        type=str, default=None,
                        help='Displaced Protobuf for re-assigning track parameters')

    parser.add_argument('--displaced', dest='displaced_en', metavar='DISPLACED_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Enables displaced track support')


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events
    pt_exact_up_to = args.pt_exact_up_to
    dxy_exact_up_to = args.dxy_exact_up_to
    pt_order = args.pt_order
    dxy_order = args.dxy_order
    n_muon = args.n_muon
    prompt_pbf = args.prompt_pbf
    disp_pbf = args.disp_pbf
    displaced_en = args.displaced_en

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Scale Limits
    if pt_exact_up_to is not None:
        pt_exact_up_to /= emtf_pt_eps

    if dxy_exact_up_to is not None:
        dxy_exact_up_to /= emtf_dxy_eps

    # Build Analysis
    main_analyzers = [
        EMTFPtCalibrationAnalyzer(displaced_en, pt_exact_up_to, pt_order)
    ]

    if displaced_en:
        main_analyzers.append(EMTFDxyCalibrationAnalyzer(dxy_exact_up_to, dxy_order))

    def analysis_fn():
        analyzers = [
            EMTFPtCalibrationAnalyzer(displaced_en, pt_exact_up_to, pt_order)
        ]

        if displaced_en:
            analyzers.append(EMTFDxyCalibrationAnalyzer(dxy_exact_up_to, dxy_order))

        return [
            Phase2MuonEventProducer(n_muon),
            Phase2ParameterAssignment(prompt_pbf, disp_pbf)
        ], analyzers

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
