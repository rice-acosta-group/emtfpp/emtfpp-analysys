import math

import numpy as np

from emtf.core.commons.quantization import quantize_floor, quantize_ceil
from emtf.phase2.commons.emtf_constants import emtf_pt_eps, emtf_dxy_eps

# Considerations
# 1. Full Eta Coverage
# 2. Min Qual = 12: All SingleMuon Track Candidates
# 3. Include v1 Modes 11, 13, 14, 15: Tracks with all 4 stations present
# 4. Include v2 Modes 12: SingleMuon Tracks
sm_prompt_min_qual = 12
sm_prompt_eta_start = 1.2
sm_prompt_eta_stop = 2.4
sm_prompt_modes_v1 = [11, 13, 14, 15]
sm_prompt_modes_v2 = [12]

sm_disp_min_qual = 12
sm_disp_eta_start = 1.2
sm_disp_eta_stop = 2.4
sm_disp_modes_v1 = [11, 13, 14, 15]
sm_disp_modes_v2 = [12]

# Model
hist_prompt_model_pt_bins = np.arange(
    quantize_floor(2, emtf_pt_eps),
    quantize_ceil(60, emtf_pt_eps),
    quantize_ceil(1, emtf_pt_eps),
    dtype=np.float64
)
hist_disp_model_pt_bins = np.arange(
    quantize_floor(2, emtf_pt_eps),
    quantize_ceil(40, emtf_pt_eps),
    quantize_ceil(1, emtf_pt_eps),
    dtype=np.float64
)
hist_model_udxy_bins = np.arange(
    quantize_floor(2, emtf_dxy_eps),
    quantize_ceil(120, emtf_dxy_eps),
    emtf_dxy_eps,
    dtype=np.float64
)

hist_prompt_emtf_pt_bins = np.arange(
    quantize_floor(2, emtf_pt_eps),
    quantize_ceil(25, emtf_pt_eps),
    quantize_ceil(1, emtf_pt_eps),
    dtype=np.float64
)
hist_disp_emtf_pt_bins = np.arange(
    quantize_floor(2, emtf_pt_eps),
    quantize_ceil(25, emtf_pt_eps),
    quantize_ceil(1, emtf_pt_eps),
    dtype=np.float64
)
hist_emtf_udxy_bins = np.arange(
    quantize_floor(2, emtf_dxy_eps),
    quantize_ceil(120, emtf_dxy_eps),
    emtf_dxy_eps,
    dtype=np.float64
)

pt_data_bins = np.arange(
    0, math.ceil(120 / emtf_pt_eps), math.ceil(2 / emtf_pt_eps),
    dtype=np.float64
)
udxy_data_bins = np.arange(
    0, math.ceil(256 / emtf_dxy_eps), 1,
    dtype=np.float64
)
