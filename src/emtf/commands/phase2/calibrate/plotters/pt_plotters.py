import numpy as np
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TGraph
from ROOT import TGraphErrors
from ROOT import TLatex
from ROOT import TPad
from ROOT import gStyle
from ROOT import kAzure
from ROOT import kRed
from ROOT import kSpring

from emtf.core.analyzers import AbstractPlotter
from emtf.core.commons.series import power_series
from emtf.core.commons.tools import safe_divide
from emtf.core.root.labels import draw_fancy_label


class FitFunction(object):

    def __init__(self, order):
        self.order = order

    def __call__(self, x, par):
        npar = [par[i] for i in range(self.order + 1)]
        res, _ = power_series(x[0], npar)
        return res


class CalibrationWrapper(object):

    def __init__(self, params, exact_up_to, sf_en, extrapolate_en):
        self.params = params
        self.exact_up_to = exact_up_to
        self.sf_en = sf_en
        self.extrapolate_en = extrapolate_en

    def __call__(self, x, par):
        # Evaluate
        if self.extrapolate_en and (self.exact_up_to is not None) and self.exact_up_to < x[0]:
            x0 = self.exact_up_to
            y0, slope = power_series(x0, self.params)
            calib = slope * (x[0] - x0) + y0
        else:
            x0 = x[0]
            y0, _ = power_series(x0, self.params)
            calib = y0

        # Round half-up
        calib = round(calib)

        # Return scale factor or calibrated value
        sf = safe_divide(calib, x[0])

        if self.sf_en:
            return sf
        else:
            return calib


class IdentityWrapper(object):

    def __init__(self):
        pass

    def __call__(self, x, par):
        return x[0]


class PtCalibrationPlotter(AbstractPlotter):

    def __init__(self, name, title,
                 x_title, y_title,
                 x_draw_min, x_draw_max,
                 y_draw_min, y_draw_max,
                 logx=False, logy=False,
                 exact_up_to=None,
                 poly_order=2):
        super().__init__()

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.point_count = 0
        self.x_arr = list()
        self.y_arr = list()
        self.x_err_arr = list()
        self.y_err_arr = list()
        self.logx = logx
        self.logy = logy

        self.exact_up_to = exact_up_to
        self.poly_order = poly_order

        self.plot = None
        self.sf_plot = None
        self.calib_function = None
        self.ext_calib_function = None
        self.calib_draw_function = None
        self.ext_calib_draw_function = None
        self.sf_function = None
        self.ext_sf_function = None
        self.sf_draw_function = None
        self.ext_sf_draw_function = None
        self.identity_function = None
        self.identity_draw_function = None

        self.x_draw_min = x_draw_min
        self.x_draw_max = x_draw_max
        self.y_draw_min = y_draw_min
        self.y_draw_max = y_draw_max

        self.consolidated = False

    def fill(self, x, y, x_err=0, y_err=0):
        self.point_count += 1
        self.x_arr.append(x)
        self.y_arr.append(y)
        self.x_err_arr.append(x_err)
        self.y_err_arr.append(y_err)

    def consolidate(self):
        # Short-Circuit: Don't consolidate again
        if self.consolidated:
            return

        self.consolidated = True

        # Create
        x_arr_np = np.asarray(self.x_arr, dtype=float)
        y_arr_np = np.asarray(self.y_arr, dtype=float)
        x_err_arr_np = np.asarray(self.x_err_arr, dtype=float)
        y_err_arr_np = np.asarray(self.y_err_arr, dtype=float)
        sf_arr_np = safe_divide(y_arr_np, x_arr_np)

        self.plot = TGraphErrors(
            self.point_count,
            x_arr_np, y_arr_np,
            x_err_arr_np, y_err_arr_np
        )
        self.plot.SetTitle(self.title)

        # Create SF Plot
        self.sf_plot = TGraph(self.point_count, x_arr_np, sf_arr_np)
        self.sf_plot.SetTitle(self.title)

        # Numpy Fit
        fit_params = np.polyfit(x_arr_np, y_arr_np, self.poly_order)
        print('numpy pt', 'poly order=%d' % self.poly_order, fit_params)

        # Build Fit Functions
        self.ff = FitFunction(self.poly_order)
        self.f1 = TF1('calib', self.ff, self.x_draw_min, self.x_draw_max, self.poly_order + 1)

        # Init Parameters
        self.f1.FixParameter(self.poly_order, 0)
        self.f1.SetParameter(self.poly_order, 0)

        for i in range(self.poly_order):
            self.f1.SetParameter(i, fit_params[i])

        # Fit
        self.plot.Fit(self.f1, 'S Q R 0')

        # Collect Parameters
        fit_params = []

        for i in range(self.poly_order + 1):
            fit_params.append(self.f1.GetParameter(i))

        print('pt', 'poly order=%d' % self.poly_order, fit_params)

        # Init functions
        self.calib_function = CalibrationWrapper(
            fit_params,
            self.exact_up_to,
            False, False
        )
        self.ext_calib_function = CalibrationWrapper(
            fit_params,
            self.exact_up_to,
            False, True
        )
        self.sf_function = CalibrationWrapper(
            fit_params,
            self.exact_up_to,
            True, False
        )
        self.ext_sf_function = CalibrationWrapper(
            fit_params,
            self.exact_up_to,
            True, True
        )
        self.identity_function = IdentityWrapper()

        self.calib_draw_function = TF1(
            self.name + '_calib', self.calib_function,
            self.x_draw_min, self.x_draw_max, 1
        )

        self.ext_calib_draw_function = TF1(
            self.name + '_ext_calib', self.ext_calib_function,
            self.x_draw_min, self.x_draw_max, 1
        )

        self.sf_draw_function = TF1(
            self.name + '_sf', self.sf_function,
            self.x_draw_min, self.x_draw_max, 1
        )

        self.ext_sf_draw_function = TF1(
            self.name + '_ext_sf', self.ext_sf_function,
            self.x_draw_min, self.x_draw_max, 1
        )

        self.identity_draw_function = TF1(
            self.name + '_identity', self.identity_function,
            self.x_draw_min, self.x_draw_max, 1
        )

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        # CREATE CANVAS
        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.200)
        plot_canvas.SetRightMargin(0.200)
        plot_canvas.SetBottomMargin(0.200)

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetTopMargin(0.175)
        top_pad.SetLeftMargin(0.175)
        top_pad.SetRightMargin(0.050)
        top_pad.SetBottomMargin(0.400)
        top_pad.SetLogx(1 if self.logx else 0)
        top_pad.SetLogy(1 if self.logy else 0)
        top_pad.Draw()
        top_pad.cd()

        # Draw Before and After
        frame_top = plot_canvas.DrawFrame(
            self.x_draw_min, self.y_draw_min,
            self.x_draw_max, self.y_draw_max,
            '')

        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)

        frame_top.GetYaxis().SetTitle(self.y_title)
        frame_top.GetYaxis().SetTitleOffset(1.450)

        self.identity_draw_function.SetLineColor(kAzure + 2)
        self.identity_draw_function.SetLineWidth(2)
        self.identity_draw_function.Draw('SAME')

        self.calib_draw_function.SetLineColor(kRed)
        self.calib_draw_function.SetLineWidth(2)
        self.calib_draw_function.Draw('SAME')

        self.ext_calib_draw_function.SetLineColor(kSpring + 4)
        self.ext_calib_draw_function.SetLineWidth(2)
        self.ext_calib_draw_function.Draw('SAME')

        self.plot.SetMarkerStyle(8)
        self.plot.SetMarkerSize(0.5)
        self.plot.Draw('P')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("center", "center", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(0.650)
        bottom_pad.SetLeftMargin(0.175)
        bottom_pad.SetRightMargin(0.050)
        bottom_pad.SetBottomMargin(0.160)
        bottom_pad.SetLogx(1 if self.logx else 0)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw SF
        frame_bottom = plot_canvas.DrawFrame(
            self.x_draw_min, 0.0,
            self.x_draw_max, 2.0,
            '')

        frame_bottom.GetXaxis().SetTitle(self.x_title)
        frame_bottom.GetXaxis().SetTitleOffset(1.350)
        frame_bottom.GetYaxis().SetTitle('SF')
        frame_bottom.GetYaxis().SetNdivisions(203)
        frame_bottom.GetYaxis().SetTitleOffset(1.450)

        self.sf_draw_function.SetLineColor(kRed)
        self.sf_draw_function.SetLineWidth(2)
        self.sf_draw_function.Draw('SAME')

        self.ext_sf_draw_function.SetLineColor(kSpring + 4)
        self.ext_sf_draw_function.SetLineWidth(2)
        self.ext_sf_draw_function.Draw('SAME')

        self.sf_plot.SetMarkerStyle(8)
        self.sf_plot.SetMarkerSize(0.5)
        self.sf_plot.Draw('P')

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')

        # Draw Titles
        plot_canvas.cd(0)

        main_title = TLatex(0.045, 0.95, self.title)
        main_title.SetTextAlign(12)
        main_title.Draw()

        draw_fancy_label(0.045, 0.875)

        # Save
        plot_canvas.SaveAs(self.name + '.png')
