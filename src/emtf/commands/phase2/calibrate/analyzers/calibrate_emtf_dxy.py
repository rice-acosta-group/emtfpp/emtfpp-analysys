from emtf.commands.phase2.calibrate.config import sm_disp_min_qual, sm_disp_modes_v1, \
    sm_disp_eta_start, sm_disp_eta_stop, udxy_data_bins, hist_emtf_udxy_bins, sm_disp_modes_v2
from emtf.commands.phase2.calibrate.plotters.dxy_plotters import DxyCalibrationPlotter
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.arbitrary_precision import convert_uint_to_int, convert_int_to_uint
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.distributions import fit_gaus
from emtf.core.commons.stats import Simple2DBin
from emtf.core.commons.tools import nz_sign
from emtf.core.commons.variables import *
from emtf.core.plotters.basic import GausDist1DPlotter
from emtf.phase2.commons.emtf_constants import emtf_dxy_eps, emtf_dxy_bw
from emtf.phase2.luts.model_luts import dxy_model_lut


class EMTFDxyCalibrationAnalyzer(AbstractAnalyzer):

    def __init__(self, exact_up_to, poly_order):
        super().__init__()

        self.exact_up_to = exact_up_to
        self.poly_order = poly_order

        self.nominal_coverage = 0.9
        self.additional_coverage = 0.015  # additional coverage due to fiducial cuts

        self.hist_bins = hist_emtf_udxy_bins
        self.bins = [Simple2DBin() for i in range(len(self.hist_bins) - 1)]

    def process_entry(self, event):

        # Loop Muons
        for tp in event.muons:
            # Unpack Muon Info
            track = tp.best_disp_track

            # Short-Circuit: Should have a track
            if track is None:
                continue

            # Short-Circuit: Skip invalid v1 modes
            if track.emtf_mode_v1 not in sm_disp_modes_v1:
                continue

            # Short-Circuit: Skip invalid v2 modes
            if track.emtf_mode_v2 not in sm_disp_modes_v2:
                continue

            # Short-Circuit: Include Quality Cut
            if track.emtf_quality < sm_disp_min_qual:
                continue

            # Short-Circuit: Keep valid eta region
            if not (sm_disp_eta_start <= abs(track.model_eta) < sm_disp_eta_stop):
                continue

            # Get GEN Parameters
            gen_model_udxy = tp.udxy / emtf_dxy_eps

            # Get Track Parameters
            trk_qdxy, trk_rels, trk_dxy = track.parameters_model
            trk_model_dxy = trk_dxy / emtf_dxy_eps
            trk_model_udxy = abs(trk_model_dxy)

            # Short-Circuit: Ignore overflow dxy assignment
            if trk_model_dxy in [-64, 63]:
                continue

            # Fill bin
            udxy_bin = digitize_inclusive(tp.udxy, self.hist_bins)
            udxy_bin = self.bins[udxy_bin]
            udxy_bin.append(trk_model_udxy, gen_model_udxy)

    def merge(self, other):
        for i in range(len(self.hist_bins) - 1):
            self.bins[i].add(other.bins[i])

    def post_production(self):
        # Plot Values Before Calibration
        for bin_id in range(len(self.hist_bins) - 1):
            bin_low = self.hist_bins[bin_id]
            bin_up = self.hist_bins[bin_id + 1]
            bin_hist = self.checkout_plotter(
                GausDist1DPlotter,
                'dxy_calib_bin_%d' % bin_id,
                'd_{xy} Calibration - GEN d_{xy} [%0.1f, %0.1f) cm' % (bin_low, bin_up),
                'Before Calibration d_{xy}', 'a.u.',
                xbins=udxy_data_bins
            )

            for x in self.bins[bin_id].x:
                bin_hist.fill(x)

        # Perform Calibration
        calibration_plotter = self.checkout_plotter(
            DxyCalibrationPlotter,
            'dxy_calibration',
            'd_{xy} Calibration', 'Before Calibration d_{xy}', 'After Calibration d_{xy}',
            0, 64,  # Min x, Max x
            0, 64,  # Min y, Max y
            logx=False, logy=False,
            exact_up_to=self.exact_up_to,
            poly_order=self.poly_order
        )

        with open('dxy_calibration_coords.csv', 'w') as output_file:
            for i in range(len(self.hist_bins) - 2):
                bin = self.bins[i]

                # Short-Circuit: No entries
                if bin.n == 0:
                    continue

                # Dxy Before
                udxy_before_calib = np.percentile(
                    bin.x,
                    100. * (1 - self.nominal_coverage - self.additional_coverage)
                )

                udxy_before_calib_err = 0

                # Dxy After
                udxy_after_calib, udxy_after_calib_err = fit_gaus(bin.y, udxy_data_bins)

                # Write to file
                args = self.hist_bins[i], self.hist_bins[i + 1], \
                    udxy_before_calib, udxy_before_calib_err, \
                    udxy_after_calib, udxy_after_calib_err

                output_file.write('%f,%f,%f,%f,%f,%f\n' % args)

                # Add to plotter
                calibration_plotter.fill(
                    udxy_before_calib, udxy_after_calib,
                    udxy_before_calib_err, udxy_after_calib_err)

        calibration_plotter.consolidate()

        # Generate LUT
        calibration_lut_length = 2 ** emtf_dxy_bw
        calibration_lut = np.ones((calibration_lut_length,)) * -1

        # Make LUT for d0
        calibration_function = calibration_plotter.ext_calib_function

        for calibration_lut_bin in range(calibration_lut_length):
            dxy_before_calibration = convert_uint_to_int(calibration_lut_bin, emtf_dxy_bw)
            dxy_before_calibration_sign = nz_sign(dxy_before_calibration)

            dxy_before_calibration = abs(dxy_before_calibration)
            sw_dxy_before_calibration = dxy_before_calibration * emtf_dxy_eps

            if sw_dxy_before_calibration > 0:
                dxy_after_calibration = calibration_function([dxy_before_calibration], None)

                if dxy_before_calibration_sign == -1:
                    dxy_after_calibration = np.clip(dxy_after_calibration, 0, (1 << (emtf_dxy_bw - 1)))
                else:
                    dxy_after_calibration = np.clip(dxy_after_calibration, 0, (1 << (emtf_dxy_bw - 1)) - 1)
            else:
                dxy_after_calibration = 0

            calibration_lut[calibration_lut_bin] = dxy_after_calibration * dxy_before_calibration_sign

        # Clone activation Lut
        activation_lut = dxy_model_lut[:]

        # Calibrate activation Lut
        for nn_address in range(len(activation_lut)):
            # Get dxy
            dxy_before_calibration = activation_lut[nn_address]
            calibration_lut_bin = convert_int_to_uint(dxy_before_calibration, emtf_dxy_bw)

            # Overwrite LUT
            dxy_after_calibration = calibration_lut[calibration_lut_bin]
            activation_lut[nn_address] = dxy_after_calibration

        # Save LUT
        with open('dxy_activation_lut.csv', 'w') as output_file:
            for val in activation_lut:
                output_file.write('%d,' % val)
