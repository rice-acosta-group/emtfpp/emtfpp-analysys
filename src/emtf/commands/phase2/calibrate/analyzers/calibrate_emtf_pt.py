from emtf.commands.phase2.calibrate.config import sm_disp_modes_v2, sm_prompt_modes_v2, sm_prompt_min_qual, \
    sm_disp_min_qual, sm_disp_modes_v1, sm_prompt_modes_v1, sm_prompt_eta_start, sm_prompt_eta_stop, sm_disp_eta_start, \
    sm_disp_eta_stop, pt_data_bins, hist_prompt_emtf_pt_bins, hist_disp_emtf_pt_bins
from emtf.commands.phase2.calibrate.plotters.pt_plotters import PtCalibrationPlotter
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.stats import Simple2DBin
from emtf.core.commons.variables import *
from emtf.core.plotters.basic import GausDist1DPlotter
from emtf.phase2.commons.emtf_constants import emtf_pt_eps, emtf_pt_bw
from emtf.phase2.luts.model_luts import disp_pt_model_lut, prompt_pt_model_lut


class EMTFPtCalibrationAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en, exact_up_to, poly_order):
        super().__init__()

        self.displaced_en = displaced_en
        self.exact_up_to = exact_up_to
        self.poly_order = poly_order

        self.nominal_coverage = 0.9
        self.additional_coverage = 0.015  # additional coverage due to fiducial cuts

        self.hist_bins = hist_prompt_emtf_pt_bins

        if self.displaced_en:
            self.hist_bins = hist_disp_emtf_pt_bins

        self.bins = [Simple2DBin() for i in range(len(self.hist_bins) - 1)]

    def process_entry(self, event):

        # Loop Muons
        for tp in event.muons:
            # Unpack Muon Info
            track = tp.best_prompt_track

            if self.displaced_en:
                track = tp.best_disp_track

            # Short-Circuit: Should have a track
            if track is None:
                continue

            # Short-Circuit: Skip invalid v1 modes
            valid_modes = sm_prompt_modes_v1

            if self.displaced_en:
                valid_modes = sm_disp_modes_v1

            if track.emtf_mode_v1 not in valid_modes:
                continue

            # Short-Circuit: Skip invalid v2 modes
            valid_modes = sm_prompt_modes_v2

            if self.displaced_en:
                valid_modes = sm_disp_modes_v2

            if track.emtf_mode_v2 not in valid_modes:
                continue

            # Short-Circuit: Include Quality Cut
            min_qual = sm_prompt_min_qual

            if self.displaced_en:
                min_qual = sm_disp_min_qual

            if track.emtf_quality < min_qual:
                continue

            # Short-Circuit: Keep valid eta region
            sm_eta_start = sm_prompt_eta_start
            sm_eta_stop = sm_prompt_eta_stop

            if self.displaced_en:
                sm_eta_start = sm_disp_eta_start
                sm_eta_stop = sm_disp_eta_stop

            if not (sm_eta_start <= abs(track.model_eta) < sm_eta_stop):
                continue

            # Get GEN Parameters
            gen_model_pt = tp.pt / emtf_pt_eps

            # Get Track Parameters
            trk_qpt, trk_rels, trk_dxy = track.parameters_model
            trk_model_pt = abs(trk_qpt / emtf_pt_eps)

            # Short-Circuit: Ignore overflow pt assignment
            if trk_model_pt in [8191, 0]:
                continue

            # Fill bin
            pt_bin = digitize_inclusive(tp.pt, self.hist_bins)
            pt_bin = self.bins[pt_bin]
            pt_bin.append(trk_model_pt, gen_model_pt)

    def merge(self, other):
        for i in range(len(self.hist_bins) - 1):
            self.bins[i].add(other.bins[i])

    def post_production(self):
        # Plot Values Before Calibration
        for bin_id in range(len(self.hist_bins) - 1):
            bin_low = self.hist_bins[bin_id]
            bin_up = self.hist_bins[bin_id + 1]
            bin_hist = self.checkout_plotter(
                GausDist1DPlotter,
                'pt_calib_bin_%d' % bin_id,
                'p_{T} Calibration - GEN p_{T} [%0.1f, %0.1f) GeV' % (bin_low, bin_up),
                'Before Calibration p_{T}', 'a.u.',
                xbins=pt_data_bins
            )

            for x in self.bins[bin_id].x:
                bin_hist.fill(x)

        # Perform Calibration
        calibration_plotter = self.checkout_plotter(
            PtCalibrationPlotter,
            'pt_calibration',
            'p_{T} Calibration', 'Before Calibration p_{T}', 'After Calibration p_{T}',
            2 / emtf_pt_eps, 8192,  # Min x, Max x
            2 / emtf_pt_eps, 8192,  # Min y, Max y
            logx=True, logy=True,
            exact_up_to=self.exact_up_to,
            poly_order=self.poly_order
        )

        with open('pt_calibration_coords.csv', 'w') as output_file:
            for i in range(len(self.hist_bins) - 2):
                bin = self.bins[i]

                # Short-Circuit: No entries
                if bin.n == 0:
                    continue

                # Pt Before
                pt_before_calib = np.percentile(
                    bin.x,
                    100. * (1 - self.nominal_coverage - self.additional_coverage)
                )
                pt_before_calib_err = 0

                # Pt After
                pt_after_calib, pt_after_calib_err = bin.get_y_pos()

                # Write to file
                args = self.hist_bins[i], self.hist_bins[i + 1], \
                    pt_before_calib, pt_before_calib_err, \
                    pt_after_calib, pt_after_calib_err

                output_file.write('%f,%f,%f,%f,%f,%f\n' % args)

                # Add to plotter
                calibration_plotter.fill(
                    pt_before_calib, pt_after_calib,
                    pt_before_calib_err, pt_after_calib_err)

        calibration_plotter.consolidate()

        # Generate LUT
        calibration_lut_length = 2 ** emtf_pt_bw
        calibration_lut = np.ones((calibration_lut_length,)) * -1

        # Make LUT for pT
        calibration_function = calibration_plotter.ext_calib_function

        for calibration_lut_bin in range(calibration_lut_length):
            pt_before_calibration = calibration_lut_bin
            sw_pt_before_calibration = pt_before_calibration * emtf_pt_eps

            if sw_pt_before_calibration > 0:
                pt_after_calibration = calibration_function([pt_before_calibration], None)
                pt_after_calibration = np.clip(pt_after_calibration, 0, (1 << emtf_pt_bw) - 1)
            else:
                pt_after_calibration = 0

            calibration_lut[calibration_lut_bin] = pt_after_calibration

        # Select activation LUT
        activation_lut = prompt_pt_model_lut

        if self.displaced_en:
            activation_lut = disp_pt_model_lut

        # Clone activation Lut
        activation_lut = activation_lut[:]

        # Calibrate activation Lut
        for nn_address in range(len(activation_lut)):
            # Get Pt
            pt_before_calibration = activation_lut[nn_address]
            pt_after_calibration = calibration_lut[pt_before_calibration]

            # Overwrite LUT
            activation_lut[nn_address] = pt_after_calibration

        # Save LUT
        with open('pt_activation_lut.csv', 'w') as output_file:
            for val in activation_lut:
                output_file.write('%d,' % val)
