import numpy as np

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.tools import nz_sign
from emtf.core.commons.variables import emu_val_d0_bins, emu_val_qpt_bins, emu_val_phi_deg_bins, emu_val_lxy_bins, \
    emu_val_eta_bins, emu_val_q_bins, emu_val_lz_bins, emu_val_min_threshold, emu_val_z0_bins
from emtf.core.plotters.basic import Hist1DPlotter, CorrelationPlotter


class DistributionAnalyzer(AbstractAnalyzer):

    def __init__(self, zone):
        super().__init__()

        self.zone = zone

        self.muon_count = 0
        self.valid_muon_count = 0

        if zone is None:
            suffix = ''
        else:
            suffix = '_zone%d' % zone

        self.gen_q_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_q%s' % suffix,
            'Sample Validation',
            'True Charge', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.gen_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_pt%s' % suffix,
            'Sample Validation',
            'True p_{T} [GeV]', 'a.u.',
            xbins=np.linspace(0, 120, 200),
            density=True,
            logy=True)

        self.gen_invpt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_invpt%s' % suffix,
            'Sample Validation',
            'True 1/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=np.linspace(0, 0.5, 200),
            density=True,
            logy=True)

        self.gen_qpt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_qpt%s' % suffix,
            'Sample Validation',
            'True q #upoint p_{T} [GeV]', 'a.u.',
            xbins=emu_val_qpt_bins,
            density=True,
            logy=True)

        self.gen_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_lxy%s' % suffix,
            'Sample Validation',
            'True L_{xy} [cm]', 'a.u.',
            xbins=np.linspace(0, 300, 300),
            density=True,
            logy=True)

        self.gen_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_lz%s' % suffix,
            'Sample Validation',
            'True L_{z} [cm]', 'a.u.',
            xbins=np.linspace(0, 500, 500),
            density=True,
            logy=True)

        self.gen_lxy_vs_lz_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_lxy_vs_lz%s' % suffix,
            'Sample Validation',
            'True L_{z} [cm]', 'True L_{xy} [cm]',
            xbins=emu_val_lz_bins,
            ybins=emu_val_lxy_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        self.gen_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_dxy%s' % suffix,
            'Sample Validation',
            'True d_{xy} [cm]', 'a.u.',
            xbins=np.linspace(0, 120, 200),
            density=True,
            logy=True)

        self.gen_d0_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_d0_sign%s' % suffix,
            'Sample Validation',
            'True Sgn(d_{0})', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.gen_d0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_d0%s' % suffix,
            'Sample Validation',
            'True d_{0} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            density=True,
            logy=True)

        self.gen_z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_z0%s' % suffix,
            'Sample Validation',
            'True z_{0} [cm]', 'a.u.',
            xbins=emu_val_z0_bins,
            density=True,
            logy=True)

        self.gen_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_eta%s' % suffix,
            'Sample Validation',
            'True |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            density=True,
            logy=True)

        self.gen_eta_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_eta_st2%s' % suffix,
            'Sample Validation',
            'True |#eta_{st2}|', 'a.u.',
            xbins=emu_val_eta_bins,
            density=True,
            logy=True)

        self.gen_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_phi%s' % suffix,
            'Sample Validation',
            'True #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            density=True,
            logy=True)

        self.gen_eta_vs_lxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_eta_vs_lxy%s' % suffix,
            'Sample Validation',
            'True L_{xy} [cm]', 'True |#eta|',
            xbins=emu_val_lxy_bins,
            ybins=emu_val_eta_bins,
            min_val=1e-5)

        self.gen_eta_st2_vs_lxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_eta_st2_vs_lxy%s' % suffix,
            'Sample Validation',
            'True L_{xy} [cm]', 'True |#eta_{st2}|',
            xbins=emu_val_lxy_bins,
            ybins=emu_val_eta_bins,
            min_val=1e-5)

        self.gen_eta_vs_lz_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_eta_vs_lz%s' % suffix,
            'Sample Validation',
            'True L_{z} [cm]', 'True |#eta|',
            xbins=emu_val_lz_bins,
            ybins=emu_val_eta_bins,
            min_val=1e-5)

        self.gen_eta_st2_vs_lz_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'true_eta_st2_vs_lz%s' % suffix,
            'Sample Validation',
            'True L_{z} [cm]', 'True |#eta_{st2}|',
            xbins=emu_val_lz_bins,
            ybins=emu_val_eta_bins,
            min_val=1e-5)

    def process_entry(self, event):

        # Loop Muons
        for tp in event.muons:
            # Increase Counter
            self.muon_count += 1

            # Short-Circuit: Only Training Quality
            if not tp.is_training_quality:
                continue

            # Short-Circuit: Not in valid zone
            if (self.zone is not None) and (tp.zone != self.zone):
                continue

            # Plot Gen Info
            self.gen_d0_sign_plotter.fill(nz_sign(tp.d0))
            self.gen_d0_plotter.fill(tp.d0)

            # Short-Circuit: Only keep physical muons
            if tp.d0 < 0:
                continue

            # Increase Counter
            self.valid_muon_count += 1

            # Plot Gen Info
            self.gen_q_plotter.fill(tp.q)
            self.gen_pt_plotter.fill(tp.pt)
            self.gen_invpt_plotter.fill(tp.invpt)
            self.gen_qpt_plotter.fill(tp.q * tp.pt)
            self.gen_lxy_plotter.fill(tp.lxy)
            self.gen_lz_plotter.fill(abs(tp.vz))
            self.gen_lxy_vs_lz_plotter.fill(abs(tp.vz), tp.lxy)
            self.gen_dxy_plotter.fill(abs(tp.d0))
            self.gen_z0_plotter.fill(tp.z0)
            self.gen_eta_plotter.fill(abs(tp.eta))
            self.gen_eta_st2_plotter.fill(abs(tp.eta_st2))
            self.gen_phi_plotter.fill(np.rad2deg(tp.phi))
            self.gen_eta_vs_lxy_plotter.fill(tp.lxy, abs(tp.eta))
            self.gen_eta_st2_vs_lxy_plotter.fill(tp.lxy, abs(tp.eta_st2))
            self.gen_eta_vs_lz_plotter.fill(abs(tp.vz), abs(tp.eta))
            self.gen_eta_st2_vs_lz_plotter.fill(abs(tp.vz), abs(tp.eta_st2))

    def merge(self, other):
        self.gen_q_plotter.add(other.gen_q_plotter)
        self.gen_pt_plotter.add(other.gen_pt_plotter)
        self.gen_invpt_plotter.add(other.gen_invpt_plotter)
        self.gen_qpt_plotter.add(other.gen_qpt_plotter)
        self.gen_lxy_plotter.add(other.gen_lxy_plotter)
        self.gen_lz_plotter.add(other.gen_lz_plotter)
        self.gen_lxy_vs_lz_plotter.add(other.gen_lxy_vs_lz_plotter)
        self.gen_dxy_plotter.add(other.gen_dxy_plotter)
        self.gen_d0_sign_plotter.add(other.gen_d0_sign_plotter)
        self.gen_d0_plotter.add(other.gen_d0_plotter)
        self.gen_z0_plotter.add(other.gen_z0_plotter)
        self.gen_eta_plotter.add(other.gen_eta_plotter)
        self.gen_eta_st2_plotter.add(other.gen_eta_st2_plotter)
        self.gen_phi_plotter.add(other.gen_phi_plotter)
        self.gen_eta_vs_lxy_plotter.add(other.gen_eta_vs_lxy_plotter)
        self.gen_eta_st2_vs_lxy_plotter.add(other.gen_eta_st2_vs_lxy_plotter)
        self.gen_eta_vs_lz_plotter.add(other.gen_eta_vs_lz_plotter)
        self.gen_eta_st2_vs_lz_plotter.add(other.gen_eta_st2_vs_lz_plotter)

        self.muon_count += other.muon_count
        self.valid_muon_count += other.valid_muon_count

    def post_production(self):
        pairs = []

        # Invpt
        prob = np.array([0.25, 0.50, 0.75, 1.0], dtype=float)

        quant = np.zeros_like(prob)
        self.gen_invpt_plotter.plot.GetQuantiles(prob.shape[0], quant, prob)
        quant = np.floor(quant * 100) / 100
        quant = np.asarray([*(quant[::-1] * -1.), *quant[:]])
        pairs.append(('invpt', quant.tolist()))

        # Dxy
        prob = np.array([0.25, 0.50, 0.75, 1.0], dtype=float)

        quant = np.zeros_like(prob)
        self.gen_dxy_plotter.plot.GetQuantiles(prob.shape[0], quant, prob)
        quant = np.floor(quant * 100) / 100
        quant = np.asarray([*(quant[::-1] * -1.), *quant[:]])
        pairs.append(('dxy', quant.tolist()))

        # Lxy
        prob = np.array([0.25, 0.50, 0.75, 1.0], dtype=float)

        quant = np.zeros_like(prob)
        self.gen_lxy_plotter.plot.GetQuantiles(prob.shape[0], quant, prob)
        quant = np.floor(quant * 100) / 100
        quant = np.asarray([*(quant[::-1] * -1.), *quant[:]])
        pairs.append(('lxy', quant.tolist()))

        # Lz
        prob = np.array([0.25, 0.50, 0.75, 1.0], dtype=float)

        quant = np.zeros_like(prob)
        self.gen_lz_plotter.plot.GetQuantiles(prob.shape[0], quant, prob)
        quant = np.floor(quant * 100) / 100
        quant = np.asarray([*(quant[::-1] * -1.), *quant[:]])
        pairs.append(('lz', quant.tolist()))

        # Write
        fname = 'pattern_edges.csv'

        if self.zone is not None:
            fname = 'pattern_edges_z%s.csv' % self.zone

        with open(fname, 'w') as out:
            out.write('param, e0, e1, e2, e3, e4, e5, e6, e7\n')

            for pair in pairs:
                param, bins = pair

                bin_line = ''
                bin_line += ('%s, %0.1f, %0.1f, %0.1f, %0.1f, %0.1f, %0.1f, %0.1f, %0.1f\n' % (param, *bins))

                # Write to file
                out.write(bin_line)
