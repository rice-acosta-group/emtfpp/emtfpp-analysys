import os

from ROOT import TFile

from emtf.commands.phase2.check_sample.distributions import DistributionAnalyzer
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.producers.Phase2HaloEventProducer import Phase2HaloEventProducer
from emtf.phase2.producers.Phase2MuonEventProducer import Phase2MuonEventProducer


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--nmuon', dest='n_muon', metavar='N_MUON',
                        type=int, default=1,
                        help='Number of muons per event')


def analysis_fn():
    return [
        Phase2MuonEventProducer(-1),
        Phase2HaloEventProducer()
    ], \
        [
            DistributionAnalyzer(None),
            DistributionAnalyzer(0),
            DistributionAnalyzer(1),
            DistributionAnalyzer(2)
        ]


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events
    n_muon = args.n_muon

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Build Analysis
    main_analyzers = [
        DistributionAnalyzer(None),
        DistributionAnalyzer(0),
        DistributionAnalyzer(1),
        DistributionAnalyzer(2)
    ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
