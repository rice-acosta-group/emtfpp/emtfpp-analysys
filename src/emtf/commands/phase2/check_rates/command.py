import os

from ROOT import TFile

from emtf.commands.phase2.check_rates.analyzers.probe import ProbeAnalyzer
from emtf.commands.phase2.check_rates.analyzers.rates import RateAnalyzer
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.producers.Phase2ParameterAssignment import Phase2ParameterAssignment


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--prompt-pb', dest='prompt_pbf', metavar='PROMPT_PROTOBUF_FILE',
                        type=str, default=None,
                        help='Prompt Protobuf for re-assigning track parameters')

    parser.add_argument('--disp-pb', dest='disp_pbf', metavar='DISP_PROTOBUF_FILE',
                        type=str, default=None,
                        help='Displaced Protobuf for re-assigning track parameters')

    parser.add_argument('--displaced', dest='displaced_en', metavar='DISPLACED_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Enables displaced track support')


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events
    prompt_pbf = args.prompt_pbf
    disp_pbf = args.disp_pbf
    displaced_en = args.displaced_en

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Build Analysis
    main_analyzers = [
        RateAnalyzer(displaced_en),
        ProbeAnalyzer(displaced_en)
    ]

    def analysis_fn():
        return [
            Phase2ParameterAssignment(prompt_pbf, disp_pbf)
        ], [
            RateAnalyzer(displaced_en),
            ProbeAnalyzer(displaced_en)
        ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count() // 2, analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
