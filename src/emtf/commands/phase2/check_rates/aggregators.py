class MaxPtAggregator(object):

    def __init__(self):
        self.is_present = False
        self.max_pt = 0

    def process(self, info):
        pt = info['trk_pt']

        if (not self.is_present) or (abs(self.max_pt) < abs(pt)):
            self.is_present = True
            self.max_pt = pt

    def get(self):
        return self.max_pt

    def reset(self):
        self.is_present = False
        self.max_pt = 0


class MaxDisplacedPtAggregator(object):

    def __init__(self):
        self.is_present = False
        self.max_pt = 0

    def process(self, info):
        pt = info['trk_pt']

        if (not self.is_present) or (abs(self.max_pt) < abs(pt)):
            self.is_present = True
            self.max_pt = pt

    def get(self):
        return self.max_pt

    def reset(self):
        self.is_present = False
        self.max_pt = 0


class MaxRelsAggregator(object):

    def __init__(self):
        self.is_present = False
        self.max_rels = 0

    def process(self, info):
        rels = info['trk_rels']

        if (not self.is_present) or (self.max_rels < rels):
            self.is_present = True
            self.max_rels = rels

    def get(self):
        return self.max_rels

    def reset(self):
        self.is_present = False
        self.max_rels = 0


class MaxUnsignedDxyAggregator(object):

    def __init__(self):
        self.is_present = False
        self.max_udxy = 0

    def process(self, info):
        udxy = abs(info['trk_dxy'])

        if (not self.is_present) or (self.max_udxy < udxy):
            self.is_present = True
            self.max_udxy = udxy

    def get(self):
        return self.max_udxy

    def reset(self):
        self.is_present = False
        self.max_udxy = 0


class MaxQualAggregator(object):

    def __init__(self):
        self.is_present = False
        self.max_qual = 0

    def process(self, info):
        qual = info['trk_qual']

        if (not self.is_present) or (self.max_qual < qual):
            self.is_present = True
            self.max_qual = qual

    def get(self):
        return self.max_qual

    def reset(self):
        self.is_present = False
        self.max_qual = 0
