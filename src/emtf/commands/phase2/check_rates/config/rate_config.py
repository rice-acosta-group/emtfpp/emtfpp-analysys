from emtf.core.commons.variables import emtf_pt_thresholds, emtf_dxy_thresholds, emtf_qual_thresholds, \
    emtf_rels_thresholds
from emtf.phase2.commons.emtf_triggers import new_trk_condition, sm_disp_eta_stop, sm_disp_eta_start, \
    sm_prompt_eta_stop, sm_prompt_eta_start

# Prompt Triggers
prompt_triggers = dict()

for pt_threshold in emtf_pt_thresholds:
    for qual_threshold in emtf_qual_thresholds:
        for rels_threshold in emtf_rels_thresholds:
            name = 'SingleMu%dQual%dRels%d' % (
                pt_threshold, qual_threshold, rels_threshold * 100)

            prompt_triggers[name] = new_trk_condition(
                unconstrained=False, min_qual=qual_threshold,
                min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
                min_pt=pt_threshold, min_rels=rels_threshold,
            )

# Displaced Triggers
disp_triggers = dict()

for pt_threshold in emtf_pt_thresholds:
    for dxy_threshold in emtf_dxy_thresholds:
        for qual_threshold in emtf_qual_thresholds:
            for rels_threshold in emtf_rels_thresholds:
                rels_str = 0

                if rels_threshold is not None:
                    rels_str = rels_threshold * 100

                name = 'SingleMu%dDxy%dQual%dRels%s' % (
                    pt_threshold, dxy_threshold, qual_threshold, rels_str)

                disp_triggers[name] = new_trk_condition(
                    unconstrained=True, min_qual=qual_threshold,
                    min_eta=sm_disp_eta_start, max_eta=sm_disp_eta_stop,
                    min_pt=pt_threshold, min_rels=rels_threshold, min_dxy=dxy_threshold
                )
