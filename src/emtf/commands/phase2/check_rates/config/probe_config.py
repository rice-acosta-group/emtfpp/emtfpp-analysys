import numpy as np

from emtf.commands.phase2.check_rates.aggregators import MaxPtAggregator, MaxQualAggregator, \
    MaxUnsignedDxyAggregator, MaxDisplacedPtAggregator, MaxRelsAggregator
from emtf.phase2.commons.emtf_eta_ranges import emtf_z0_eta_start, emtf_z0_eta_stop, emtf_eta_start, \
    emtf_eta_stop, emtf_z1_eta_start, emtf_z1_eta_stop, emtf_z2_eta_start, emtf_z2_eta_stop
from emtf.phase2.commons.emtf_triggers import new_trk_condition, sm_disp_min_dxy, sm_disp_min_pt, \
    sm_prompt_min_pt, sm_disp_min_qual, sm_prompt_min_qual, get_sm_prompt_eta_stop, \
    get_sm_prompt_eta_start, get_sm_disp_eta_start, get_sm_disp_eta_stop, \
    sm_prompt_min_rels, sm_disp_min_rels

prompt_triggers = [
    # Qual
    {
        'name': 'rate_vs_qual',
        'title': 'Emulator Rates',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': 0, 'rels': sm_prompt_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=0,
            min_eta=get_sm_prompt_eta_start(emtf_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=sm_prompt_min_rels),
        'aggregator': MaxQualAggregator()
    },
    {
        'name': 'rate_vs_qual_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': 0, 'rels': sm_prompt_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=0,
            min_eta=get_sm_prompt_eta_start(emtf_z2_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z2_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=sm_prompt_min_rels),
        'aggregator': MaxQualAggregator()
    },
    {
        'name': 'rate_vs_qual_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': 0, 'rels': sm_prompt_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=0,
            min_eta=get_sm_prompt_eta_start(emtf_z1_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z1_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=sm_prompt_min_rels),
        'aggregator': MaxQualAggregator()
    },
    {
        'name': 'rate_vs_qual_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': 0, 'rels': sm_prompt_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=0,
            min_eta=get_sm_prompt_eta_start(emtf_z0_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z0_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=sm_prompt_min_rels),
        'aggregator': MaxQualAggregator()
    },
    # Pt
    {
        'name': 'rate_vs_pt',
        'title': 'Emulator Rates',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': sm_prompt_min_rels},
        'min': 0.1,
        'max': 3.2e4,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_eta_stop),
            min_pt=0, min_rels=sm_prompt_min_rels),
        'aggregator': MaxPtAggregator()
    },
    {
        'name': 'rate_vs_pt_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': sm_prompt_min_rels},
        'min': 0.1,
        'max': 3.2e4,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_z2_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z2_eta_stop),
            min_pt=0, min_rels=sm_prompt_min_rels),
        'aggregator': MaxPtAggregator()
    },
    {
        'name': 'rate_vs_pt_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': sm_prompt_min_rels},
        'min': 0.1,
        'max': 3.2e4,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_z1_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z1_eta_stop),
            min_pt=0, min_rels=sm_prompt_min_rels),
        'aggregator': MaxPtAggregator()
    },
    {
        'name': 'rate_vs_pt_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'comment': '#splitline{L1 Qual #geq %d}{L1 p_{T} #geq Thrs.}' % sm_prompt_min_qual,
        'cuts': {'pt': 0, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': sm_prompt_min_rels},
        'min': 0.1,
        'max': 3.2e4,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_z0_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z0_eta_stop),
            min_pt=0, min_rels=sm_prompt_min_rels),
        'aggregator': MaxPtAggregator()
    },
    # Relevance
    {
        'name': 'rate_vs_rels',
        'title': 'Emulator Rates',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=0),
        'aggregator': MaxRelsAggregator()
    },
    {
        'name': 'rate_vs_rels_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_z2_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z2_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=0),
        'aggregator': MaxRelsAggregator()
    },
    {
        'name': 'rate_vs_rels_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_z1_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z1_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=0),
        'aggregator': MaxRelsAggregator()
    },
    {
        'name': 'rate_vs_rels_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_prompt_min_pt, 'dxy': None, 'qual': sm_prompt_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=get_sm_prompt_eta_start(emtf_z0_eta_start), max_eta=get_sm_prompt_eta_stop(emtf_z0_eta_stop),
            min_pt=sm_prompt_min_pt, min_rels=0),
        'aggregator': MaxRelsAggregator()
    },
]

disp_triggers = [
    # Qual
    {
        'name': 'rate_vs_qual',
        'title': 'Emulator Rates',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': 0, 'rels': sm_disp_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=0,
            min_eta=get_sm_disp_eta_start(emtf_eta_start), max_eta=get_sm_disp_eta_stop(emtf_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxQualAggregator()
    },
    {
        'name': 'rate_vs_qual_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': 0, 'rels': sm_disp_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=0,
            min_eta=get_sm_disp_eta_start(emtf_z2_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z2_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxQualAggregator()
    },
    {
        'name': 'rate_vs_qual_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': 0, 'rels': sm_disp_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=0,
            min_eta=get_sm_disp_eta_start(emtf_z1_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z1_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxQualAggregator()
    },
    {
        'name': 'rate_vs_qual_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'Quality Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': 0, 'rels': sm_disp_min_rels},
        'bins': np.linspace(0, 16, 17),
        'min': 0.1,
        'max': 1000,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=0,
            min_eta=get_sm_disp_eta_start(emtf_z0_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z0_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxQualAggregator()
    },
    # Pt
    {
        'name': 'rate_vs_pt',
        'title': 'Emulator Rates',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_eta_start), max_eta=get_sm_disp_eta_stop(emtf_eta_stop),
            min_pt=0, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxDisplacedPtAggregator()
    },
    {
        'name': 'rate_vs_pt_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z2_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z2_eta_stop),
            min_pt=0, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxDisplacedPtAggregator()
    },
    {
        'name': 'rate_vs_pt_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z1_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z1_eta_stop),
            min_pt=0, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxDisplacedPtAggregator()
    },
    {
        'name': 'rate_vs_pt_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'L1 p_{T} Threshold [GeV]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': 0, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z0_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z0_eta_stop),
            min_pt=0, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxDisplacedPtAggregator()
    },
    # Relevance
    {
        'name': 'rate_vs_rels',
        'title': 'Emulator Rates',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_eta_start), max_eta=get_sm_disp_eta_stop(emtf_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=0, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxRelsAggregator()
    },
    {
        'name': 'rate_vs_rels_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z2_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z2_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=0, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxRelsAggregator()
    },
    {
        'name': 'rate_vs_rels_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z1_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z1_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=0, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxRelsAggregator()
    },
    {
        'name': 'rate_vs_rels_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'Relevance Threshold',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': sm_disp_min_dxy, 'qual': sm_disp_min_qual, 'rels': 0},
        'bins': np.arange(0, 1.1, 0.01),
        'min': 0.1,
        'max': 500,
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z0_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z0_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=0, min_dxy=sm_disp_min_dxy),
        'aggregator': MaxRelsAggregator()
    },
    # D0
    {
        'name': 'rate_vs_dxy',
        'title': 'Emulator Rates',
        'x_label': 'L1 d_{xy} Threshold [cm]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': 0, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_eta_start), max_eta=get_sm_disp_eta_stop(emtf_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=0),
        'aggregator': MaxUnsignedDxyAggregator()
    },
    {
        'name': 'rate_vs_dxy_zone2',
        'title': 'Emulator Rates - Zone 2',
        'x_label': 'L1 d_{xy} Threshold [cm]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': 0, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z2_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z2_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=0),
        'aggregator': MaxUnsignedDxyAggregator()
    },
    {
        'name': 'rate_vs_dxy_zone1',
        'title': 'Emulator Rates - Zone 1',
        'x_label': 'L1 d_{xy} Threshold [cm]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': 0, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z1_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z1_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=0),
        'aggregator': MaxUnsignedDxyAggregator()
    },
    {
        'name': 'rate_vs_dxy_zone0',
        'title': 'Emulator Rates - Zone 0',
        'x_label': 'L1 d_{xy} Threshold [cm]',
        'y_label': 'Rate [kHz]',
        'cuts': {'pt': sm_disp_min_pt, 'dxy': 0, 'qual': sm_disp_min_qual, 'rels': sm_disp_min_rels},
        'min': 0.1,
        'max': 1000,
        'bins': np.linspace(0, 121, 122),
        'logy': True,
        'condition': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=get_sm_disp_eta_start(emtf_z0_eta_start), max_eta=get_sm_disp_eta_stop(emtf_z0_eta_stop),
            min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=0),
        'aggregator': MaxUnsignedDxyAggregator()
    },
]
