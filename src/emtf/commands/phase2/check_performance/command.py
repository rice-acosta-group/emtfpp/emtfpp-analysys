import os

from ROOT import TFile

from emtf.commands.phase2.check_performance.analyzers.correlations import CorrelationAnalyzer
from emtf.commands.phase2.check_performance.analyzers.distributions import DistributionAnalyzer
from emtf.commands.phase2.check_performance.analyzers.dxy_efficiencies import DxyEfficiencyAnalyzer
from emtf.commands.phase2.check_performance.analyzers.pt_efficiencies import PtEfficiencyAnalyzer
from emtf.commands.phase2.check_performance.analyzers.resolutions import ResolutionAnalyzer
from emtf.commands.phase2.check_performance.analyzers.triggers import TriggerAnalyzer
from emtf.core.commons.readers import load_trigger_rates
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.producers.Phase2MuonEventProducer import Phase2MuonEventProducer
from emtf.phase2.producers.Phase2ParameterAssignment import Phase2ParameterAssignment


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--nmuon', dest='n_muon', metavar='N_MUON',
                        type=int, default=1,
                        help='Number of muons per event')

    parser.add_argument('--prompt-pb', dest='prompt_pbf', metavar='PROMPT_PROTOBUF_FILE',
                        type=str, default=None,
                        help='Prompt Protobuf for re-assigning track parameters')

    parser.add_argument('--disp-pb', dest='disp_pbf', metavar='DISP_PROTOBUF_FILE',
                        type=str, default=None,
                        help='Displaced Protobuf for re-assigning track parameters')

    parser.add_argument('--rates', dest='rates_file', metavar='RATE_FILE',
                        type=str, default=None,
                        help='Rates used to compare triggers')

    parser.add_argument('--displaced', dest='displaced_en', metavar='DISPLACED_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Enables displaced track support')

    parser.add_argument('--include-tb-eff', dest='include_tb_eff', metavar='INCLUDE_TB_EFF_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Includes track building in efficiencies')

    parser.add_argument('--only-matched-eff', dest='only_matched_eff', metavar='ONLY_MATCHED_EFF_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Only considers muons that were matched to tracks in efficiencies')


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events
    n_muon = args.n_muon
    prompt_pbf = args.prompt_pbf
    disp_pbf = args.disp_pbf
    rates_file = args.rates_file
    displaced_en = args.displaced_en
    include_tb_eff = args.include_tb_eff
    only_matched_eff = args.only_matched_eff

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Load Rates
    if rates_file is None:
        rates = dict()
    else:
        rates = load_trigger_rates(rates_file)

    # Build Analysis
    main_analyzers = [
        DistributionAnalyzer(displaced_en),
        ResolutionAnalyzer(displaced_en),
        CorrelationAnalyzer(displaced_en),
        PtEfficiencyAnalyzer(displaced_en, include_tb_eff, only_matched_eff),
        DxyEfficiencyAnalyzer(displaced_en, include_tb_eff, only_matched_eff),
        TriggerAnalyzer(rates, displaced_en, include_tb_eff, only_matched_eff),
    ]

    def analysis_fn():
        return [
            Phase2MuonEventProducer(n_muon),
            Phase2ParameterAssignment(prompt_pbf, disp_pbf)
        ], [
            DistributionAnalyzer(displaced_en),
            ResolutionAnalyzer(displaced_en),
            CorrelationAnalyzer(displaced_en),
            PtEfficiencyAnalyzer(displaced_en, include_tb_eff, only_matched_eff),
            DxyEfficiencyAnalyzer(displaced_en, include_tb_eff, only_matched_eff),
            TriggerAnalyzer(rates, displaced_en, include_tb_eff, only_matched_eff),
        ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
