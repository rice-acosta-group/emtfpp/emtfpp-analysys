from emtf.phase2.commons.emtf_triggers import new_trk_condition, sm_disp_min_qual, \
    sm_disp_min_pt, sm_disp_min_dxy, sm_prompt_min_qual, sm_prompt_min_pt, sm_prompt_eta_stop, sm_prompt_eta_start, \
    sm_disp_eta_start, sm_disp_eta_stop, sm_prompt_min_rels, \
    sm_disp_min_rels

prompt_trigger = new_trk_condition(
    unconstrained=False, min_qual=sm_prompt_min_qual,
    min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
    min_pt=sm_prompt_min_pt, min_rels=sm_prompt_min_rels)

disp_trigger = new_trk_condition(
    unconstrained=True, min_qual=sm_disp_min_qual,
    min_eta=sm_disp_eta_start, max_eta=sm_disp_eta_stop,
    min_pt=sm_disp_min_pt, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy)
