from emtf.core.commons.variables import emtf_pt_thresholds
from emtf.core.root.environment import cm_tab10
from emtf.phase2.commons.emtf_triggers import new_trk_condition, new_gen_condition, sm_prompt_min_qual, \
    sm_prompt_eta_stop, \
    sm_prompt_eta_start, sm_disp_eta_stop, sm_disp_eta_start, sm_prompt_min_rels, \
    sm_disp_min_rels, sm_disp_min_qual, sm_disp_min_dxy

# Markers
solid_marker_map = [33, 34, 39, 41, 43, 45, 47, 49]
open_marker_map = [27, 28, 37, 40, 42, 44, 46, 48]

# Prompt Triggers
prompt_triggers = dict()

pt_id = 0

for pt_threshold in emtf_pt_thresholds:
    rels_str = 0

    if sm_prompt_min_rels is not None:
        rels_str = sm_prompt_min_rels * 100

    name = 'SingleMu%dQual%dRels%d' % (pt_threshold, sm_prompt_min_qual, rels_str)

    prompt_triggers[name] = {
        'color': cm_tab10[pt_id],
        'marker': 33,
        'trk_cond': new_trk_condition(
            unconstrained=False, min_qual=sm_prompt_min_qual,
            min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
            min_pt=pt_threshold, min_rels=sm_prompt_min_rels,
        ),
        'gen_cond': new_gen_condition(
            min_eta=sm_prompt_eta_start, max_eta=sm_prompt_eta_stop,
        )
    }

    pt_id += 1

# Prompt Labels
prompt_labels = list()

for pt_id in range(len(emtf_pt_thresholds)):
    pt_threshold = emtf_pt_thresholds[pt_id]

    prompt_labels.append({
        'color': cm_tab10[pt_id],
        'marker': 21,
        'text': 'p_{T} #geq %d' % pt_threshold
    })

# Displaced Triggers
dxy_thresholds = []

disp_triggers = dict()

pt_id = 0

for pt_threshold in emtf_pt_thresholds:

    rels_str = 0

    if sm_disp_min_rels is not None:
        rels_str = sm_disp_min_rels * 100

    dxy_str = 0 if (sm_disp_min_dxy is None) else sm_disp_min_dxy

    name = 'SingleMu%dDxy%dQual%dRels%d' % (pt_threshold, dxy_str, sm_disp_min_qual, rels_str)

    disp_triggers[name] = {
        'color': cm_tab10[pt_id],
        'marker': 33,
        'trk_cond': new_trk_condition(
            unconstrained=True, min_qual=sm_disp_min_qual,
            min_eta=sm_disp_eta_start, max_eta=sm_disp_eta_stop,
            min_pt=pt_threshold, min_rels=sm_disp_min_rels, min_dxy=sm_disp_min_dxy
        ),
        'gen_cond': new_gen_condition(
            min_eta=sm_disp_eta_start, max_eta=sm_disp_eta_stop
        )
    }

    pt_id += 1

# Displaced Labels
disp_labels = list()

for pt_id in range(len(emtf_pt_thresholds)):
    pt_threshold = emtf_pt_thresholds[pt_id]

    disp_labels.append({
        'color': cm_tab10[pt_id],
        'marker': 21,
        'text': 'p_{T} #geq %d' % pt_threshold
    })
