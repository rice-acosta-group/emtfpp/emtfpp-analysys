from array import array

import numpy as np
from ROOT import TCanvas
from ROOT import TGraphErrors
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlack

from emtf.commands.phase2.check_performance.config.resolutions_config import *
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import digitize_inclusive
from emtf.core.commons.distributions import fit_gaus, fit_landau, fit_right_sided_crystalball
from emtf.core.commons.stats import Simple2DBin, calc_mad
from emtf.core.plotters.basic import RCrystalBallDist1DPlotter
from emtf.phase2.commons.emtf_constants import emtf_dxy_eps
from emtf.phase2.commons.emtf_constants import emtf_pt_eps
from emtf.phase2.commons.emtf_triggers import sm_prompt_min_qual, sm_disp_min_qual, sm_disp_min_rels, sm_prompt_min_rels


class ResolutionAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en):
        super().__init__()

        self.displaced_en = displaced_en

        self.pt_error_stats = [Simple2DBin(y_bins=ratio_pt_bins) for i in range(len(histogram_pt_bins) - 1)]
        self.udxy_error_stats = [Simple2DBin(y_bins=ratio_udxy_bins) for i in range(len(histogram_udxy_bins) - 1)]

    def process_entry(self, event):
        # Loop Muons
        for tp in event.muons:
            # Unpack Muon Info
            track = tp.best_prompt_track

            if self.displaced_en:
                track = tp.best_disp_track

            # Short-Circuit: Should have a track
            if track is None:
                continue

            # Short-Circuit: Include Quality Cut
            min_qual = sm_prompt_min_qual

            if self.displaced_en:
                min_qual = sm_disp_min_qual

            if track.emtf_quality < min_qual:
                continue

            # Short-Circuit: Negative d0
            if tp.d0 < 0:
                continue

            # Get Track Parameters
            trk_qpt, trk_rels, trk_dxy = track.parameters_calib
            trk_pt = abs(trk_qpt)

            # Short-Circuit: Remove tracks with invalid pt
            trk_model_pt = abs(trk_qpt / emtf_pt_eps)

            if trk_model_pt in [8191, 0]:
                continue

            # Short-Circuit: Remove tracks with invalid dxy
            trk_model_dxy = trk_dxy / emtf_dxy_eps

            if trk_model_dxy in [-64, 63]:
                continue

            # Short-Circuit: Include Rels Cut
            min_rels = sm_prompt_min_rels

            if self.displaced_en:
                min_rels = sm_disp_min_rels

            if trk_rels < min_rels:
                continue

            # Find bins
            pt_bin = digitize_inclusive(tp.pt, histogram_pt_bins)
            udxy_bin = digitize_inclusive(tp.udxy, histogram_udxy_bins)

            pt_error_stats = self.pt_error_stats[pt_bin]
            udxy_error_stats = self.udxy_error_stats[udxy_bin]

            # Collect Errors
            pt_error_stats.append(tp.pt, trk_pt / tp.pt)
            udxy_error_stats.append(tp.udxy, abs(trk_dxy / tp.dxy))

    def merge(self, other):
        for i in range(len(histogram_pt_bins) - 1):
            self.pt_error_stats[i].add(other.pt_error_stats[i])

        for i in range(len(histogram_udxy_bins) - 1):
            self.udxy_error_stats[i].add(other.udxy_error_stats[i])

    def post_production(self):
        # Extract residuals
        pt_bin = digitize_inclusive(20, histogram_pt_bins)

        residuals_pt_plotter = self.checkout_plotter(
            # LandauDist1DPlotter,
            RCrystalBallDist1DPlotter,
            'residuals_pt',
            'Residuals for %0.1f #leq GEN p_{T} < %0.1f' % (
                histogram_pt_bins[pt_bin], histogram_pt_bins[pt_bin + 1]),
            'L1/GEN', 'a.u.',
            xbins=ratio_pt_bins
        )

        for rresidual in self.pt_error_stats[pt_bin].y:
            residuals_pt_plotter.fill(rresidual)

        # Plot all errors
        pt_stats = np.zeros((len(histogram_pt_bins) - 1, 4), dtype='d')
        udxy_stats = np.zeros((len(histogram_udxy_bins) - 1, 4), dtype='d')

        def get_vals(stats):
            if stats.n == 0:
                return 0, 0, 0, 0

            avg_x, std_x = stats.get_x_pos()

            if method == 'right_sided_crystalball':
                avg_residual, std_residual = fit_right_sided_crystalball(stats.y, stats.y_bins)
            elif method == 'landau':
                avg_residual, std_residual = fit_landau(stats.y, stats.y_bins, 1e-3)
            elif method == 'gaus':
                avg_residual, std_residual = fit_gaus(stats.y, stats.y_bins)
            elif method == 'median':
                avg_residual, std_residual = calc_mad(stats.y)
            else:
                raise ValueError('Invalid resolution method')

            return avg_x, std_x, avg_residual, std_residual

        for i in range(len(histogram_pt_bins) - 1):
            pt_stats[i] = get_vals(self.pt_error_stats[i])

        for i in range(len(histogram_udxy_bins) - 1):
            udxy_stats[i] = get_vals(self.udxy_error_stats[i])

        self.plot_errors(
            pt_stats,
            'err_pt', 'Prediction Error',
            'GEN p_{T} [GeV]', 'L1/GEN',
            2, 70, 0, 2)
        self.plot_res(
            pt_stats,
            'res_pt', 'Prediction Resolution',
            'GEN p_{T} [GeV]', '#sigma_{L1/GEN}',
            2, 70, 0, 1,
            var_id=3)

        self.plot_errors(
            udxy_stats,
            'err_udxy', 'Prediction Error',
            'GEN d_{xy} [cm]', 'L1/GEN',
            0, 70, 0, 2)
        self.plot_res(
            udxy_stats,
            'res_udxy', 'Prediction Resolution',
            'GEN d_{xy} [cm]', '#sigma_{L1/GEN}',
            0, 70, 0, 1,
            var_id=3)

    def plot_errors(
            self, coords,
            name, title,
            xlabel, ylabel,
            xmin, xmax,
            ymin, ymax,
            log_en=False
    ):
        # Clean
        coords = coords[~np.isnan(coords[:, 3]), :]

        if len(coords) == 0:
            return

        # Draw Graphs
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        # plot_canvas.SetLogy(log_en)
        plot_canvas.SetLogx(log_en)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            xmin, ymin, xmax, ymax,
            title
        )
        frame.GetXaxis().SetTitle(xlabel)
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetRangeUser(xmin, xmax)
        frame.GetYaxis().SetTitle(ylabel)
        frame.GetYaxis().SetTitleOffset(1.650)
        frame.GetYaxis().SetMaxDigits(3)
        frame.GetYaxis().SetRangeUser(ymin, ymax)

        # Draw Frame
        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        # Draw Graphs
        graph = TGraphErrors(
            len(coords),
            array('d', coords[:, 0]), array('d', coords[:, 2]),
            array('d', coords[:, 1]), array('d', coords[:, 3])
        )
        graph.SetMarkerSize(1)
        graph.SetMarkerStyle(8)
        graph.SetMarkerColor(kBlack)
        graph.Draw('SAME P')

        # GPad
        gPad.Modified()
        gPad.Update()

        # Save
        plot_canvas.SaveAs('%s.png' % name)

    def plot_res(
            self, coords,
            name, title,
            xlabel, ylabel,
            xmin, xmax,
            ymin, ymax,
            var_id=3,
            log_en=False
    ):
        # Clean
        coords = coords[~np.isnan(coords[:, 3]), :]

        if len(coords) == 0:
            return

        # Draw Graphs
        gStyle.SetOptStat(0)

        plot_canvas = TCanvas(name, '')
        plot_canvas.SetLeftMargin(0.175)
        plot_canvas.SetTopMargin(0.125)
        plot_canvas.SetBottomMargin(0.160)
        # plot_canvas.SetLogy(log_en)
        plot_canvas.SetLogx(log_en)
        plot_canvas.cd(0)

        frame = plot_canvas.DrawFrame(
            xmin, ymin, xmax, ymax,
            title
        )
        frame.GetXaxis().SetTitle(xlabel)
        frame.GetXaxis().SetTitleOffset(1.350)
        frame.GetXaxis().SetRangeUser(xmin, xmax)
        frame.GetYaxis().SetTitle(ylabel)
        frame.GetYaxis().SetTitleOffset(1.650)
        frame.GetYaxis().SetMaxDigits(3)
        frame.GetYaxis().SetRangeUser(ymin, ymax)

        # Draw Frame
        frame.Draw('SAME AXIS')
        frame.Draw('SAME AXIG')

        # Draw Graphs
        graph = TGraphErrors(
            len(coords),
            array('d', coords[:, 0]), array('d', coords[:, var_id]),
            array('d', coords[:, 1]), array('d', np.zeros_like(coords[:, var_id]))
        )
        graph.SetMarkerSize(1)
        graph.SetMarkerStyle(8)
        graph.SetMarkerColor(kBlack)
        graph.Draw('SAME P')

        # GPad
        gPad.Modified()
        gPad.Update()

        # Save
        plot_canvas.SaveAs('%s.png' % name)
