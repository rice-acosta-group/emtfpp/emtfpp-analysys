from emtf.commands.phase2.check_performance.config.distributions_config import disp_trigger, prompt_trigger
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.aggregators import MinAggregator
from emtf.core.commons.tools import nz_sign
from emtf.core.commons.variables import *
from emtf.core.plotters.basic import *
from emtf.phase2.commons.emtf_constants import site_labels
from emtf.phase2.commons.emtf_eta_ranges import emtf_eta_start, emtf_eta_stop
from emtf.phase2.commons.emtf_triggers import sm_base_trg


class DistributionAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en):
        super().__init__()

        # Flags
        self.displaced_en = displaced_en

        # Gen Stats
        self.gen_q_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_q',
            'Sample Validation',
            'GEN Charge', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.gen_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_pt',
            'GEN Particle',
            'GEN p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            logy=True,
            density=True)

        self.gen_d0_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_d0_sign',
            'Sample Validation',
            'GEN Sgn(d_{0})', 'a.u.',
            xbins=emu_val_d0_sign_bins,
            density=True,
            logy=True)

        self.gen_d0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_d0',
            'Sample Validation',
            'GEN d_{0} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            density=True,
            logy=True)

        self.gen_z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_z0',
            'Sample Validation',
            'GEN z_{0} [cm]', 'a.u.',
            xbins=emu_val_z0_bins,
            density=True,
            logy=True)

        self.gen_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lxy',
            'Sample Validation',
            'GEN L_{xy} [cm]', 'a.u.',
            xbins=emu_val_lxy_bins,
            density=True,
            logy=True)

        self.gen_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_lz',
            'Sample Validation',
            'GEN L_{z} [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.gen_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi',
            'GEN Particle',
            'GEN #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            logy=True,
            density=True)

        self.gen_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta',
            'GEN Particle',
            'GEN |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.gen_eta_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta_st2',
            'GEN Particle',
            'GEN |#eta_{st2}|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.gen_lxy_vs_lz_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'gen_lxy_vs_lz', 'GEN Particle',
            'GEN L_{z} [cm]', 'GEN L_{xy} [cm]',
            xbins=emu_val_lz_bins,
            ybins=emu_val_lxy_bins,
            min_val=emu_val_min_threshold,
            logz=True)

        # Segment Stats
        self.seg_zone_st1_vs_gen_zone_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'seg_zone_st1_vs_gen_zone',
            'Segments',
            'GEN Zone', 'Zone Station 1',
            xbins=uniform_step_axis(1, 0, 2),
            ybins=uniform_step_axis(1, 0, 2),
            normalize_by='column',
            min_val=1e-5,
            logz=True)

        self.seg_zone_st2_vs_gen_zone_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'seg_zone_st2_vs_gen_zone',
            'Segments',
            'GEN Zone', 'Zone Station 2',
            xbins=uniform_step_axis(1, 0, 2),
            ybins=uniform_step_axis(1, 0, 2),
            normalize_by='column',
            min_val=1e-5,
            logz=True)

        self.seg_zone_st3_vs_gen_zone_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'seg_zone_st3_vs_gen_zone',
            'Segments',
            'GEN Zone', 'Zone Station 3',
            xbins=uniform_step_axis(1, 0, 2),
            ybins=uniform_step_axis(1, 0, 2),
            normalize_by='column',
            min_val=1e-5,
            logz=True)

        self.seg_zone_st4_vs_gen_zone_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'seg_zone_st4_vs_gen_zone',
            'Segments',
            'GEN Zone', 'Zone Station 4',
            xbins=uniform_step_axis(1, 0, 2),
            ybins=uniform_step_axis(1, 0, 2),
            normalize_by='column',
            min_val=1e-5,
            logz=True)

        # Track Stats
        self.trk_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_pt',
            'EMTF Tracks',
            'L1 p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            logy=True,
            density=True)

        self.trk_rels_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_rels',
            'EMTF Tracks',
            'L1 Relevance', 'a.u.',
            xbins=emu_val_rels_bins,
            logy=True,
            density=True)

        self.trk_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dxy',
            'EMTF Tracks',
            'L1 d_{xy} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            logy=True,
            density=True)

        self.trk_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_phi',
            'EMTF Tracks',
            'L1 #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            logy=True,
            density=True)

        self.trk_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_eta',
            'EMTF Tracks',
            'L1 |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.trk_pattern_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_pattern',
            'EMTF Tracks',
            'Track Pattern', 'a.u.',
            xbins=emu_val_trk_pattern_bins,
            density=True,
            logy=True)

        self.trk_qual_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_qual',
            'EMTF Tracks',
            'Track Quality', 'a.u.',
            xbins=emu_val_trk_qual_p2_bins,
            density=True,
            logy=True)

        self.trk_mode_v1_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_mode_v1',
            'EMTF Tracks',
            'Track ModeV1', 'a.u.',
            xbins=emu_val_trk_mode_v1_bins,
            density=True,
            logy=True)

        self.trk_mode_v2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_mode_v2',
            'EMTF Tracks',
            'Track ModeV2', 'a.u.',
            xbins=emu_val_trk_mode_v2_bins,
            density=True,
            logy=True)

        self.trk_mode_v1_vs_ideal_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_mode_v1_vs_ideal',
            'Segments',
            'Ideal Mode V1', 'Mode V1',
            xbins=uniform_step_axis(1, 0, 15),
            ybins=uniform_step_axis(1, 0, 15),
            normalize_by='column',
            min_val=1e-5,
            logz=True)

        self.trk_mode_v2_vs_ideal_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_mode_v2_vs_ideal',
            'Segments',
            'Ideal Mode V2', 'Mode V2',
            xbins=uniform_step_axis(1, 0, 12),
            ybins=uniform_step_axis(1, 0, 12),
            normalize_by='column',
            min_val=1e-5,
            logz=True)

        self.trk_site_vs_hits_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_site_vs_hits',
            'EMTF Tracks',
            'Track Hits', 'Track Site',
            xbins=emu_val_trk_hit_count_bins,
            ybins=emu_val_trk_site_bins,
            ylabels=site_labels,
            min_val=1e-2, max_val=1,
            normalize_by='column',
            density=False,
            logz=True)

        self.trk_dtheta_vs_site_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_dtheta_vs_site',
            'EMTF Tracks',
            'Track Site', 'Seg #Delta #theta',
            xbins=emu_val_trk_site_bins,
            ybins=uniform_step_axis(1, -36, 36),
            xlabels=site_labels,
            normalize_by='column',
            min_val=1e-5, max_val=1,
            logz=True)

        self.trk_dtheta_vs_site_plotter_z0 = self.checkout_plotter(
            Hist2DPlotter,
            'trk_dtheta_vs_site_z0',
            'EMTF Tracks - Zone 0',
            'Track Site', 'Seg #Delta #theta',
            xbins=emu_val_trk_site_bins,
            ybins=uniform_step_axis(1, -36, 36),
            xlabels=site_labels,
            normalize_by='column',
            min_val=1e-5, max_val=1,
            logz=True)

        self.trk_dtheta_vs_site_plotter_z1 = self.checkout_plotter(
            Hist2DPlotter,
            'trk_dtheta_vs_site_z1',
            'EMTF Tracks - Zone 1',
            'Track Site', 'Seg #Delta #theta',
            xbins=emu_val_trk_site_bins,
            ybins=uniform_step_axis(1, -36, 36),
            xlabels=site_labels,
            normalize_by='column',
            min_val=1e-5, max_val=1,
            logz=True)

        self.trk_dtheta_vs_site_plotter_z2 = self.checkout_plotter(
            Hist2DPlotter,
            'trk_dtheta_vs_site_z2',
            'EMTF Tracks - Zone 2',
            'Track Site', 'Seg #Delta #theta',
            xbins=emu_val_trk_site_bins,
            ybins=uniform_step_axis(1, -36, 36),
            xlabels=site_labels,
            normalize_by='column',
            min_val=1e-5, max_val=1,
            logz=True)

        # Best Track Stats
        self.best_trk_dR_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'best_trk_dR',
            'Best EMTF Track',
            '#Delta R', 'a.u.',
            xbins=np.linspace(0, 1, 100),
            logy=True,
            density=True)

        self.best_trk_pattern_vs_pt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'best_trk_pattern_vs_pt', 'Best EMTF Track',
            'GEN p_{T} [GeV]', 'Track Pattern',
            xbins=emu_val_pt_bins,
            ybins=emu_val_trk_pattern_bins,
            min_val=emu_val_min_threshold,
            max_val=1.,
            normalize_by='column',
            density=False,
            logz=True)

        self.best_trk_pattern_vs_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'best_trk_pattern_vs_dxy', 'Best EMTF Track',
            'GEN d_{xy} [cm]', 'Track Pattern',
            xbins=emu_val_udxy_bins,
            ybins=emu_val_trk_pattern_bins,
            min_val=emu_val_min_threshold,
            max_val=1.,
            normalize_by='column',
            density=False,
            logz=True)

        self.best_trk_pattern_vs_lxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'best_trk_pattern_vs_lxy', 'Best EMTF Track',
            'GEN L_{xy} [cm]', 'Track Pattern',
            xbins=emu_val_lxy_bins,
            ybins=emu_val_trk_pattern_bins,
            min_val=emu_val_min_threshold,
            max_val=1.,
            normalize_by='column',
            density=False,
            logz=True)

        self.best_trk_pattern_vs_lz_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'best_trk_pattern_vs_lz', 'Best EMTF Track',
            'GEN L_{z} [cm]', 'Track Pattern',
            xbins=emu_val_lz_bins,
            ybins=emu_val_trk_pattern_bins,
            min_val=emu_val_min_threshold,
            max_val=1.,
            normalize_by='column',
            density=False,
            logz=True)

        self.best_trk_pattern_vs_eta_st2_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'best_trk_pattern_vs_eta_st2', 'Best EMTF Track',
            'GEN |#eta_{st2}|', 'Track Pattern',
            xbins=emu_val_eta_bins,
            ybins=emu_val_trk_pattern_bins,
            min_val=emu_val_min_threshold,
            max_val=1.,
            normalize_by='column',
            density=False,
            logz=True)

        self.best_trk_qual_vs_lxy_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'best_trk_qual_vs_lxy', 'Best EMTF Track',
            'GEN L_{xy} [cm]', 'Track Quality',
            xbins=emu_val_lxy_bins,
            ybins=emu_val_trk_qual_p2_bins,
            min_val=1e-3, max_val=1,
            normalize_by='column',
            density=False,
            logz=True
        )

        self.best_trk_mode_v1_vs_lxy_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'best_trk_mode_v1_vs_lxy', 'Best EMTF Track',
            'GEN L_{xy} [cm]', 'Track ModeV1',
            xbins=emu_val_lxy_bins,
            ybins=emu_val_trk_mode_v1_bins,
            min_val=1e-3, max_val=1,
            normalize_by='column',
            density=False,
            logz=True
        )

        self.best_trk_mode_v2_vs_lxy_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'best_trk_mode_v2_vs_lxy', 'Best EMTF Track',
            'GEN L_{xy} [cm]', 'Track ModeV2',
            xbins=emu_val_lxy_bins,
            ybins=emu_val_trk_mode_v2_bins,
            min_val=1e-3, max_val=1,
            normalize_by='column',
            density=False,
            logz=True
        )

        self.best_trk_qual_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'best_trk_qual_vs_lz', 'Best EMTF Track',
            'GEN L_{z} [cm]', 'Track Quality',
            xbins=emu_val_lz_bins,
            ybins=emu_val_trk_qual_p2_bins,
            min_val=1e-3, max_val=1,
            normalize_by='column',
            density=False,
            logz=True
        )

        self.best_trk_mode_v1_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'best_trk_mode_v1_vs_lz', 'Best EMTF Track',
            'GEN L_{z} [cm]', 'Track ModeV1',
            xbins=emu_val_lz_bins,
            ybins=emu_val_trk_mode_v1_bins,
            min_val=1e-3, max_val=1,
            normalize_by='column',
            density=False,
            logz=True
        )

        self.best_trk_mode_v2_vs_lz_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'best_trk_mode_v2_vs_lz', 'Best EMTF Track',
            'GEN L_{z} [cm]', 'Track ModeV2',
            xbins=emu_val_lz_bins,
            ybins=emu_val_trk_mode_v2_bins,
            min_val=1e-3, max_val=1,
            normalize_by='column',
            density=False,
            logz=True
        )

        self.trk_dtheta = [list() for i in range(12)]
        self.trk_dtheta_z0 = [list() for i in range(12)]
        self.trk_dtheta_z1 = [list() for i in range(12)]
        self.trk_dtheta_z2 = [list() for i in range(12)]

    def process_entry(self, event):
        # Get Trigger
        if self.displaced_en:
            trigger = disp_trigger
        else:
            trigger = prompt_trigger

        # Loop Tracks
        any_track_passed = False

        for track in event.emtf_tracks:
            # Short-Circuit: Only use valid tracks
            if not track.valid:
                continue

            # Short-Circuit: Must be in eta region
            if not (emtf_eta_start <= abs(track.model_eta) < emtf_eta_stop):
                continue

            # Get Track Parameters
            trk_qpt, trk_rels, trk_dxy = track.parameters_emtf
            trk_q = nz_sign(trk_qpt)
            trk_pt = abs(trk_qpt)

            # Pack Entry
            trk_entry = {
                'trk_unconstrained': track.unconstrained,
                'trk_qual': track.emtf_quality,
                'trk_q': trk_q,
                'trk_pt': trk_pt,
                'trk_rels': trk_rels,
                'trk_dxy': trk_dxy,
                'trk_phi': track.model_phi,
                'trk_eta': track.model_eta
            }

            ideal_trk_entry = {
                **trk_entry,
                'trk_qual': track.ideal.emtf_quality,
                'trk_mode_v1': track.ideal.emtf_mode_v1,
                'trk_mode_v2': track.ideal.emtf_mode_v2
            }

            # Fill Ideal Track Plots
            if self.displaced_en:
                trk_pass = sm_base_trg(ideal_trk_entry) or trigger(ideal_trk_entry)
            else:
                trk_pass = trigger(ideal_trk_entry)

            if trk_pass:
                self.trk_mode_v1_vs_ideal_plotter.fill(track.ideal.emtf_mode_v1, track.emtf_mode_v1)
                self.trk_mode_v2_vs_ideal_plotter.fill(track.ideal.emtf_mode_v2, track.emtf_mode_v2)

            # Short-Circuit: Track didn't pass
            if self.displaced_en:
                trk_pass = sm_base_trg(trk_entry) or trigger(trk_entry)
            else:
                trk_pass = trigger(trk_entry)

            if not trk_pass:
                continue

            # Fill Track Plots
            any_track_passed = True

            self.trk_pt_plotter.fill(trk_pt)
            self.trk_rels_plotter.fill(trk_rels)
            self.trk_dxy_plotter.fill(trk_dxy)
            self.trk_phi_plotter.fill(track.model_phi)
            self.trk_eta_plotter.fill(abs(track.model_eta))
            self.trk_pattern_plotter.fill(track.model_pattern)
            self.trk_qual_plotter.fill(track.emtf_quality)
            self.trk_mode_v1_plotter.fill(track.emtf_mode_v1)
            self.trk_mode_v2_plotter.fill(track.emtf_mode_v2)

            # Calculate dthetas
            is_ideally_single_mu = (track.ideal.emtf_mode_v2 == 12) and (track.ideal.emtf_mode_v1 in [11, 13, 14, 15])

            for site_id in range(12):
                site_bit = ord(track.site_mask[site_id])
                site_rm_bit = ord(track.site_rm_mask[site_id])

                if site_bit == 1:
                    self.trk_site_vs_hits_plotter.fill(track.nhits, site_id)

                if (site_rm_bit == 1) or (site_bit == 1):
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]

                    min_agg = MinAggregator(0, 0)

                    for theta in hit.emtf_thetas:
                        if theta == 0:
                            continue

                        diff = theta - track.model_theta
                        min_agg.process(abs(diff), diff)

                    if min_agg.present and is_ideally_single_mu:
                        self.trk_dtheta[site_id].append(abs(min_agg.cur_value))
                        self.trk_dtheta_vs_site_plotter.fill(site_id, min_agg.cur_value)

                        if track.zone == 0:
                            self.trk_dtheta_z0[site_id].append(abs(min_agg.cur_value))
                            self.trk_dtheta_vs_site_plotter_z0.fill(site_id, min_agg.cur_value)

                        if track.zone == 1:
                            self.trk_dtheta_z1[site_id].append(abs(min_agg.cur_value))
                            self.trk_dtheta_vs_site_plotter_z1.fill(site_id, min_agg.cur_value)

                        if track.zone == 2:
                            self.trk_dtheta_z2[site_id].append(abs(min_agg.cur_value))
                            self.trk_dtheta_vs_site_plotter_z2.fill(site_id, min_agg.cur_value)

        # Short-Circuit: No tracks passed
        if not any_track_passed:
            return

        # Loop Muons
        for tp in event.muons:
            # Fill GEN Plots
            self.gen_q_plotter.fill(tp.q)
            self.gen_pt_plotter.fill(tp.pt)
            self.gen_d0_sign_plotter.fill(nz_sign(tp.d0))
            self.gen_d0_plotter.fill(tp.d0)
            self.gen_z0_plotter.fill(tp.z0)
            self.gen_lxy_plotter.fill(tp.lxy)
            self.gen_lz_plotter.fill(abs(tp.vz))
            self.gen_phi_plotter.fill(np.rad2deg(tp.phi))
            self.gen_eta_plotter.fill(abs(tp.eta))
            self.gen_eta_st2_plotter.fill(abs(tp.eta_st2))
            self.gen_lxy_vs_lz_plotter.fill(abs(tp.vz), tp.lxy)

            # Track Info
            track = tp.best_prompt_track

            if self.displaced_en:
                track = tp.best_disp_track

            if track is not None:
                # Unpack Track Info
                trk_dR = math.hypot(
                    tp.phi_st2 - np.deg2rad(track.model_phi),
                    tp.eta_st2 - track.model_eta
                )

                self.best_trk_dR_plotter.fill(trk_dR)
                self.best_trk_pattern_vs_pt_plotter.fill(tp.pt, track.model_pattern)
                self.best_trk_pattern_vs_dxy_plotter.fill(abs(tp.d0), track.model_pattern)
                self.best_trk_pattern_vs_lxy_plotter.fill(tp.lxy, track.model_pattern)
                self.best_trk_pattern_vs_lz_plotter.fill(abs(tp.vz), track.model_pattern)
                self.best_trk_pattern_vs_eta_st2_plotter.fill(abs(tp.eta_st2), track.model_pattern)
                self.best_trk_qual_vs_lxy_plotter.fill(abs(tp.lxy), track.emtf_quality)
                self.best_trk_mode_v1_vs_lxy_plotter.fill(abs(tp.lxy), track.emtf_mode_v1)
                self.best_trk_mode_v2_vs_lxy_plotter.fill(abs(tp.lxy), track.emtf_mode_v2)
                self.best_trk_qual_vs_lz_plotter.fill(abs(tp.vz), track.emtf_quality)
                self.best_trk_mode_v1_vs_lz_plotter.fill(abs(tp.vz), track.emtf_mode_v1)
                self.best_trk_mode_v2_vs_lz_plotter.fill(abs(tp.vz), track.emtf_mode_v2)

            # Loop GEN Segments
            st1_hits = list()
            st2_hits = list()
            st3_hits = list()
            st4_hits = list()

            for hit in tp.segs:
                if hit.subsystem != 1:
                    continue

                if hit.station == 1:
                    st1_hits.append(hit)
                elif hit.station == 2:
                    st2_hits.append(hit)
                elif hit.station == 3:
                    st3_hits.append(hit)
                elif hit.station == 4:
                    st4_hits.append(hit)

            for hit in st1_hits:
                min_agg = MinAggregator(0, 0)

                for zone_id in hit.emtf_zones:
                    min_agg.process(abs(zone_id - tp.zone), zone_id)

                if min_agg.present:
                    self.seg_zone_st1_vs_gen_zone_plotter.fill(tp.zone, min_agg.cur_value)

            for hit in st2_hits:
                min_agg = MinAggregator(0, 0)

                for zone_id in hit.emtf_zones:
                    min_agg.process(abs(zone_id - tp.zone), zone_id)

                if min_agg.present:
                    self.seg_zone_st2_vs_gen_zone_plotter.fill(tp.zone, min_agg.cur_value)

            for hit in st3_hits:
                min_agg = MinAggregator(0, 0)

                for zone_id in hit.emtf_zones:
                    min_agg.process(abs(zone_id - tp.zone), zone_id)

                if min_agg.present:
                    self.seg_zone_st3_vs_gen_zone_plotter.fill(tp.zone, min_agg.cur_value)

            for hit in st4_hits:
                min_agg = MinAggregator(0, 0)

                for zone_id in hit.emtf_zones:
                    min_agg.process(abs(zone_id - tp.zone), zone_id)

                if min_agg.present:
                    self.seg_zone_st4_vs_gen_zone_plotter.fill(tp.zone, min_agg.cur_value)

    def merge(self, other):
        self.gen_q_plotter.add(other.gen_q_plotter)
        self.gen_pt_plotter.add(other.gen_pt_plotter)
        self.gen_d0_sign_plotter.add(other.gen_d0_sign_plotter)
        self.gen_d0_plotter.add(other.gen_d0_plotter)
        self.gen_z0_plotter.add(other.gen_z0_plotter)
        self.gen_lxy_plotter.add(other.gen_lxy_plotter)
        self.gen_lz_plotter.add(other.gen_lz_plotter)
        self.gen_phi_plotter.add(other.gen_phi_plotter)
        self.gen_eta_plotter.add(other.gen_eta_plotter)
        self.gen_eta_st2_plotter.add(other.gen_eta_st2_plotter)
        self.gen_lxy_vs_lz_plotter.add(other.gen_lxy_vs_lz_plotter)
        self.seg_zone_st1_vs_gen_zone_plotter.add(other.seg_zone_st1_vs_gen_zone_plotter)
        self.seg_zone_st2_vs_gen_zone_plotter.add(other.seg_zone_st2_vs_gen_zone_plotter)
        self.seg_zone_st3_vs_gen_zone_plotter.add(other.seg_zone_st3_vs_gen_zone_plotter)
        self.seg_zone_st4_vs_gen_zone_plotter.add(other.seg_zone_st4_vs_gen_zone_plotter)
        self.trk_pt_plotter.add(other.trk_pt_plotter)
        self.trk_rels_plotter.add(other.trk_rels_plotter)
        self.trk_dxy_plotter.add(other.trk_dxy_plotter)
        self.trk_phi_plotter.add(other.trk_phi_plotter)
        self.trk_eta_plotter.add(other.trk_eta_plotter)
        self.trk_pattern_plotter.add(other.trk_pattern_plotter)
        self.trk_qual_plotter.add(other.trk_qual_plotter)
        self.trk_mode_v1_plotter.add(other.trk_mode_v1_plotter)
        self.trk_mode_v2_plotter.add(other.trk_mode_v2_plotter)
        self.trk_mode_v1_vs_ideal_plotter.add(other.trk_mode_v1_vs_ideal_plotter)
        self.trk_mode_v2_vs_ideal_plotter.add(other.trk_mode_v2_vs_ideal_plotter)
        self.trk_site_vs_hits_plotter.add(other.trk_site_vs_hits_plotter)
        self.trk_dtheta_vs_site_plotter.add(other.trk_dtheta_vs_site_plotter)
        self.trk_dtheta_vs_site_plotter_z0.add(other.trk_dtheta_vs_site_plotter_z0)
        self.trk_dtheta_vs_site_plotter_z1.add(other.trk_dtheta_vs_site_plotter_z1)
        self.trk_dtheta_vs_site_plotter_z2.add(other.trk_dtheta_vs_site_plotter_z2)
        self.best_trk_dR_plotter.add(other.best_trk_dR_plotter)
        self.best_trk_pattern_vs_pt_plotter.add(other.best_trk_pattern_vs_pt_plotter)
        self.best_trk_pattern_vs_dxy_plotter.add(other.best_trk_pattern_vs_dxy_plotter)
        self.best_trk_pattern_vs_lxy_plotter.add(other.best_trk_pattern_vs_lxy_plotter)
        self.best_trk_pattern_vs_lz_plotter.add(other.best_trk_pattern_vs_lz_plotter)
        self.best_trk_pattern_vs_eta_st2_plotter.add(other.best_trk_pattern_vs_eta_st2_plotter)
        self.best_trk_qual_vs_lxy_plotter.add(other.best_trk_qual_vs_lxy_plotter)
        self.best_trk_mode_v1_vs_lxy_plotter.add(other.best_trk_mode_v1_vs_lxy_plotter)
        self.best_trk_mode_v2_vs_lxy_plotter.add(other.best_trk_mode_v2_vs_lxy_plotter)
        self.best_trk_qual_vs_lz_plotter.add(other.best_trk_qual_vs_lz_plotter)
        self.best_trk_mode_v1_vs_lz_plotter.add(other.best_trk_mode_v1_vs_lz_plotter)
        self.best_trk_mode_v2_vs_lz_plotter.add(other.best_trk_mode_v2_vs_lz_plotter)

        for site_id in range(12):
            self.trk_dtheta[site_id] += other.trk_dtheta[site_id]
            self.trk_dtheta_z0[site_id] += other.trk_dtheta_z0[site_id]
            self.trk_dtheta_z1[site_id] += other.trk_dtheta_z1[site_id]
            self.trk_dtheta_z2[site_id] += other.trk_dtheta_z2[site_id]

    def post_production(self):
        def print_thresholds(zone, dtheta):
            line = ''

            for site_id in range(12):
                if len(dtheta[site_id]) == 0:
                    line += ' 0, '
                else:
                    dtheta_threshold = np.percentile(dtheta[site_id], 99)
                    dtheta_threshold = math.ceil(dtheta_threshold) + 1
                    line += '%2d, ' % dtheta_threshold

            print(zone, line)

        print_thresholds(None, self.trk_dtheta)
        print_thresholds(0, self.trk_dtheta_z0)
        print_thresholds(1, self.trk_dtheta_z1)
        print_thresholds(2, self.trk_dtheta_z2)
