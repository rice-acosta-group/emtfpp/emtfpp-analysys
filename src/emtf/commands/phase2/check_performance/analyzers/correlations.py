from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import uniform_step_axis
from emtf.core.commons.kinematics import calc_qinvpt_from_qpt
from emtf.core.commons.tools import safe_divide, nz_sign
from emtf.core.commons.variables import emu_val_q_bins, emu_val_min_threshold, emu_val_pt_bins, \
    emu_val_mismatch_threshold, emu_val_invpt_bins, emu_val_qpt_bins, emu_val_qinvpt_bins, emu_val_dxy_bins
from emtf.core.plotters.basic import CorrelationPlotter
from emtf.phase2.commons.emtf_constants import emtf_pt_eps, emtf_dxy_eps
from emtf.phase2.commons.emtf_triggers import sm_prompt_min_qual, sm_disp_min_qual, sm_prompt_min_rels, sm_disp_min_rels

err_bins = uniform_step_axis(0.02, 0, 2)


class CorrelationAnalyzer(AbstractAnalyzer):

    def __init__(self, displaced_en):
        super().__init__()

        self.displaced_en = displaced_en

        # Plots
        self.vld_q_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_q', 'Emulator Validation',
            'GEN q', 'L1 q',
            xbins=emu_val_q_bins,
            ybins=emu_val_q_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            show_text=True,
            logz=True)

        self.vld_pt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt', 'Emulator Validation',
            'GEN p_{T} [GeV]', 'L1 p_{T} [GeV]',
            xbins=emu_val_pt_bins,
            ybins=emu_val_pt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_pt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_pt_err', 'Emulator Validation',
            'GEN p_{T} [GeV]', 'L1/GEN',
            xbins=emu_val_pt_bins,
            ybins=err_bins,
            min_val=emu_val_mismatch_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_invpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_invpt', 'Emulator Validation',
            'GEN 1/p_{T} [GeV^{-1}]', 'L1 1/p_{T} [GeV^{-1}]',
            xbins=emu_val_invpt_bins,
            ybins=emu_val_invpt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logx=True, logy=True, logz=True)

        self.vld_invpt_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_invpt_err', 'Emulator Validation',
            'GEN 1/p_{T} [GeV^{-1}]', 'L1/GEN',
            xbins=emu_val_invpt_bins,
            ybins=err_bins,
            min_val=emu_val_mismatch_threshold,
            density=False,
            normalize_by='column',
            logx=True, logy=False, logz=True)

        self.vld_qpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qpt', 'Emulator Validation',
            'GEN q #upoint p_{T} [GeV]', 'L1 q #upoint p_{T} [GeV]',
            xbins=emu_val_qpt_bins,
            ybins=emu_val_qpt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_qinvpt_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_qinvpt', 'Emulator Validation',
            'GEN q/p_{T} [GeV^{-1}]', 'L1 q/p_{T} [GeV^{-1}]',
            xbins=emu_val_qinvpt_bins,
            ybins=emu_val_qinvpt_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_dxy_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_dxy', 'Emulator Validation',
            'GEN d_{xy} [cm]', 'L1 d_{xy} [cm]',
            xbins=emu_val_dxy_bins,
            ybins=emu_val_dxy_bins,
            min_val=emu_val_min_threshold,
            density=False,
            normalize_by='column',
            logz=True)

        self.vld_dxy_err_plotter = self.checkout_plotter(
            CorrelationPlotter,
            'vld_dxy_err', 'Emulator Validation',
            'GEN d_{xy} [cm]', 'L1/GEN',
            xbins=emu_val_dxy_bins,
            ybins=err_bins,
            min_val=emu_val_mismatch_threshold,
            density=False,
            normalize_by='column',
            logz=True)

    def process_entry(self, event):

        # Loop Muons
        for tp in event.muons:
            # Unpack Muon Info
            track = tp.best_prompt_track

            if self.displaced_en:
                track = tp.best_disp_track

            # Short-Circuit: Should have a track
            if track is None:
                continue

            # Short-Circuit: Include Quality Cut
            min_qual = sm_prompt_min_qual

            if self.displaced_en:
                min_qual = sm_disp_min_qual

            if track.emtf_quality < min_qual:
                continue

            # Short-Circuit: Negative d0
            if tp.d0 < 0:
                continue

            # Get Track Parameters
            trk_qpt, trk_rels, trk_dxy = track.parameters_calib
            trk_q = nz_sign(trk_qpt)

            trk_qinvpt = calc_qinvpt_from_qpt(trk_qpt)
            trk_invpt = abs(trk_qinvpt)
            trk_pt = abs(trk_qpt)

            # Short-Circuit: Remove tracks with invalid pt
            trk_model_pt = abs(trk_qpt / emtf_pt_eps)

            if trk_model_pt in [8191]:
                continue

            # Short-Circuit: Remove tracks with invalid dxy
            trk_model_dxy = trk_dxy / emtf_dxy_eps

            if trk_model_dxy in [-64, 63]:
                continue

            # Short-Circuit: Include Rels Cut
            min_rels = sm_prompt_min_rels

            if self.displaced_en:
                min_rels = sm_disp_min_rels

            if trk_rels < min_rels:
                continue

            # Check how big the differences are
            err_pt = safe_divide(trk_pt, tp.pt)
            err_invpt = safe_divide(trk_invpt, tp.invpt)
            err_dxy = safe_divide(trk_dxy, tp.dxy)

            # Fill correlations
            self.vld_q_plotter.fill(tp.q, trk_q)

            self.vld_pt_plotter.fill(tp.pt, trk_pt)
            self.vld_pt_err_plotter.fill(tp.pt, err_pt)

            self.vld_invpt_plotter.fill(tp.invpt, trk_invpt)
            self.vld_invpt_err_plotter.fill(tp.invpt, err_invpt)

            self.vld_qpt_plotter.fill(tp.q * tp.pt, trk_qpt)
            self.vld_qinvpt_plotter.fill(tp.q * tp.invpt, trk_qinvpt)

            self.vld_dxy_plotter.fill(tp.dxy, trk_dxy)
            self.vld_dxy_err_plotter.fill(tp.dxy, err_dxy)

    def merge(self, other):
        self.vld_q_plotter.add(other.vld_q_plotter)
        self.vld_pt_plotter.add(other.vld_pt_plotter)
        self.vld_pt_err_plotter.add(other.vld_pt_err_plotter)
        self.vld_invpt_plotter.add(other.vld_invpt_plotter)
        self.vld_invpt_err_plotter.add(other.vld_invpt_err_plotter)
        self.vld_qpt_plotter.add(other.vld_qpt_plotter)
        self.vld_qinvpt_plotter.add(other.vld_qinvpt_plotter)
        self.vld_dxy_plotter.add(other.vld_dxy_plotter)
        self.vld_dxy_err_plotter.add(other.vld_dxy_err_plotter)

    def post_production(self):
        pass
