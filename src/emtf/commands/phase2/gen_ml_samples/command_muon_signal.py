import os

from ROOT import TFile

from emtf.commands.phase2.gen_ml_samples.analyzers.muon_signal import MuonSignalAnalyzer
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.producers.Phase2MuonEventProducer import Phase2MuonEventProducer


def configure_parser(parser):
    parser.add_argument('--filename', dest='filename', metavar='FILENAME',
                        type=str, default='sig_ml_sample',
                        help='Output File Name')

    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--endcap', dest='endcap', metavar='ENDCAP',
                        type=str, default='positive',
                        help='Endcap to extract')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--nmuon', dest='n_muon', metavar='N_MUON',
                        type=int, default=1,
                        help='Number of muons per event')

    parser.add_argument('--displaced', dest='displaced_en', metavar='DISPLACED_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Enables displaced track support')


def run(args):
    # Unpack args
    filename = args.filename
    input_files = args.input_files
    endcap = args.endcap
    max_events = args.max_events
    n_muon = args.n_muon
    displaced_en = args.displaced_en

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Build Analysis
    main_analyzers = [
        MuonSignalAnalyzer(endcap, filename, displaced_en)
    ]

    def analysis_fn():
        return [
            Phase2MuonEventProducer(n_muon)
        ], [
            MuonSignalAnalyzer(endcap, filename, displaced_en)
        ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
