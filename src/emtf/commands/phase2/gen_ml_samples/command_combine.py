import numpy as np


def configure_parser(parser):
    parser.add_argument('--filename', dest='filename', metavar='FILENAME',
                        type=str, default='sig_ml_sample',
                        help='Output File Name')

    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')


def run(args):
    # Unpack args
    filename = args.filename
    input_files = args.input_files

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Constants
    collections = [
        ('variables_emu', 'parameters_emu'),
        ('variables_train', 'parameters_train'),
        ('variables_quant', 'parameters_quant'),
        ('variables_tune', 'parameters_tune'),
        ('variables_test', 'parameters_test'),
        ('variables_qtest', 'parameters_qtest'),
        ('variables_ttest', 'parameters_ttest'),
    ]

    # Combine Samples
    mdata = np.load(input_files[0], allow_pickle=True)
    mdata = {**mdata}

    for file in input_files[1:]:
        data = np.load(file, allow_pickle=True)

        for collection_pair in collections:
            var_mcol = mdata[collection_pair[0]]
            var_dcol = data[collection_pair[0]]

            par_mcol = mdata[collection_pair[1]]
            par_dcol = data[collection_pair[1]]

            print('merging A', collection_pair, var_dcol.shape, par_dcol.shape)
            print('merging B', collection_pair, var_mcol.shape, par_mcol.shape)

            var_mcol = np.concatenate([var_mcol, var_dcol], axis=0)
            par_mcol = np.concatenate([par_mcol, par_dcol], axis=0)

            # Record
            mdata[collection_pair[0]] = var_mcol
            mdata[collection_pair[1]] = par_mcol

    # Shuffle
    for collection_pair in collections:
        # Get Collections
        var_mcol = mdata[collection_pair[0]]
        par_mcol = mdata[collection_pair[1]]

        # Shuffle
        random_index_array = np.arange(var_mcol.shape[0])
        np.random.shuffle(random_index_array)

        var_mcol = var_mcol[random_index_array, :]
        par_mcol = par_mcol[random_index_array, :]

        # Record
        mdata[collection_pair[0]] = var_mcol
        mdata[collection_pair[1]] = par_mcol

        print('merged_and_shuffle', collection_pair, var_mcol.shape, par_mcol.shape)

    # Save
    np.savez_compressed(filename + '.npz', **mdata)
