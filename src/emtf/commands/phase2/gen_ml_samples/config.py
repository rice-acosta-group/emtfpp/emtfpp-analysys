# Considerations
# 1. Full Eta Coverage
# 2. Min Qual = 12: All SingleMuon Track Candidates
# 3. Include v1 Modes 11, 13, 14, 15: Tracks with all 4 stations present
# 4. Include v2 Modes 12: SingleMuon Tracks
sm_prompt_min_qual = 12
sm_prompt_eta_start = 1.2
sm_prompt_eta_stop = 2.4
sm_prompt_modes_v1 = [11, 13, 14, 15]
sm_prompt_modes_v2 = [12]

sm_disp_min_qual = 12
sm_disp_eta_start = 1.2
sm_disp_eta_stop = 2.4
sm_disp_modes_v1 = [11, 13, 14, 15]
sm_disp_modes_v2 = [12]
