from emtf.commands.phase2.gen_ml_samples.analyzers.hscp_signal import HSCPSignalAnalyzer
from emtf.phase2.analysis.phase2_analysis import Phase2Analysis
from emtf.phase2.producers.Phase2HSCPEventProducer import Phase2HSCPEventProducer


class HSCPSignalSample(Phase2Analysis):

    def __init__(self):
        super().__init__()
        self.branches_required = set()

    def configure_parser(self, parser):
        parser.add_argument('--input', dest='input_files', metavar='INPUT_FILES',
                            type=str, nargs='+', default=None,
                            help='Input File')

        parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                            type=int, default=-1,
                            help='Number of events to analyze per file')

        parser.add_argument('--fevents', dest='event_fraction', metavar='FRACTION_OF_EVENTS',
                            type=float, default=1,
                            help='fraction of events to process')

        parser.add_argument('--endcap', dest='endcap', metavar='ENDCAP',
                            type=str, default='positive',
                            help='Endcap to extract')

        parser.add_argument('--nhscp', dest='n_hscp', metavar='N_HSCP',
                            type=int, default=1,
                            help='Number of hscps per event')

        parser.add_argument('--displaced', dest='displaced_en', metavar='DISPLACED_EN',
                            type=bool, nargs='?', const=True, default=False,
                            help='Enables displaced track support')

        parser.add_argument('--filename', dest='filename', metavar='FILENAME',
                            type=str, default='sig_ml_sample',
                            help='Output File Name')

    def configure(self, args):
        # Unpack args
        self.endcap = args.endcap
        self.n_hscp = args.n_hscp
        self.displaced_en = args.displaced_en
        self.filename = args.filename

    def create_producers(self):
        return [
            Phase2HSCPEventProducer(self.n_hscp)
        ]

    def create_analyzers(self):
        return [
            HSCPSignalAnalyzer(self.endcap, self.filename, self.displaced_en)
        ]

    def consolidate(self):
        for analyzer in self.analyzers:
            analyzer.write()
