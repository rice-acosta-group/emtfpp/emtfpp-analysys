import numpy as np

from emtf.commands.phase2.gen_ml_samples.config import sm_disp_modes_v2, sm_prompt_modes_v2, sm_prompt_min_qual, \
    sm_disp_min_qual, sm_prompt_modes_v1, sm_disp_modes_v1, sm_prompt_eta_start, sm_prompt_eta_stop, sm_disp_eta_start, \
    sm_disp_eta_stop
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.plotting import plot_var_walls
from emtf.core.commons.tools import nz_sign
from emtf.core.commons.variables import emu_val_phi_deg_bins, emu_val_eta_bins, emu_val_trk_mode_v2_bins, \
    emu_val_trk_mode_v1_bins, \
    emu_val_trk_qual_p2_bins, emu_val_trk_site_bins, emu_val_trk_hit_count_bins, emu_val_pt_bins, emu_val_q_bins, \
    emu_val_dxy_bins, emu_val_dxy_sign_bins
from emtf.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from emtf.phase2.commons.emtf_constants import site_labels


class PileUpAnalyzer(AbstractAnalyzer):

    def __init__(self, endcap, filename, displaced_en):
        super().__init__()

        self.endcap = endcap
        self.filename = filename
        self.displaced_en = displaced_en

        self.list_trk_variables = list()
        self.list_trk_parameters = list()
        self.list_gen_parameters = list()

        self.trk_q_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_q_' + filename,
            'Sample Validation',
            'L1 Charge', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.trk_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_pt_' + filename,
            'Sample Validation',
            'L1 p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            logy=True,
            density=True)

        self.trk_dxy_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dxy_sign_' + filename,
            'Sample Validation',
            'L1 Sgn(d_{xy})', 'a.u.',
            xbins=emu_val_dxy_sign_bins,
            density=True,
            logy=True)

        self.trk_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dxy_' + filename,
            'Sample Validation',
            'L1 d_{xy} [cm]', 'a.u.',
            xbins=emu_val_dxy_bins,
            density=True,
            logy=True)

        self.trk_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_phi_' + filename,
            'Sample Validation',
            'L1 #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            logy=True,
            density=True)

        self.trk_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_eta_' + filename,
            'Sample Validation',
            'L1 |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.trk_qual_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_qual_' + filename,
            'Sample Validation',
            'L1 Quality', 'a.u.',
            xbins=emu_val_trk_qual_p2_bins,
            density=True,
            logy=True)

        self.trk_mode_v1_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_mode_v1_' + filename,
            'Sample Validation',
            'Track ModeV1', 'a.u.',
            xbins=emu_val_trk_mode_v1_bins,
            density=True,
            logy=True)

        self.trk_mode_v2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_mode_v2_' + filename,
            'Sample Validation',
            'Track ModeV2', 'a.u.',
            xbins=emu_val_trk_mode_v2_bins,
            density=True,
            logy=True)

        self.trk_site_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_sites_' + filename,
            'Sample Validation',
            'Track Hits', 'Track Site',
            xbins=emu_val_trk_hit_count_bins,
            ybins=emu_val_trk_site_bins,
            ylabels=site_labels,
            normalize_by='column', density=False,
            min_val=1e-5, max_val=1.,
            logz=True)

    def process_entry(self, event):

        # Short-Circuit: Skip events with high energy muons
        has_high_pt_particle = False

        for tp_id, tp_idx in event.tp_id_to_idx.items():
            gen_pt = event.trk_part_pt[tp_idx]

            if gen_pt > 5:
                has_high_pt_particle = True
                break

        if has_high_pt_particle:
            return

        # Loop sub-events
        for endsec, endsec_info in event.endsec.items():
            # Get Endcap: Endsec is 0-5 in positive endcap, 6-11 in negative endcap
            endcap = 1

            if 6 <= endsec < 12:
                endcap = -1

            # Short-Circuit: Keep positive endcap
            if (self.endcap == 'positive') and (endcap == -1):
                continue

            # Short-Circuit: Keep negative endcap
            if (self.endcap == 'negative') and (endcap == 1):
                continue

            # Loop Tracks
            for track in endsec_info['tracks']:
                # Short-Circuit: Only relevant tracks
                if self.displaced_en != track.unconstrained:
                    continue

                # Short-Circuit: Skip invalid v1 modes
                valid_modes = sm_prompt_modes_v1

                if self.displaced_en:
                    valid_modes = sm_disp_modes_v1

                if track.emtf_mode_v1 not in valid_modes:
                    continue

                # Short-Circuit: Skip invalid v2 modes
                valid_modes = sm_prompt_modes_v2

                if self.displaced_en:
                    valid_modes = sm_disp_modes_v2

                if track.emtf_mode_v2 not in valid_modes:
                    continue

                # Short-Circuit: Include Quality Cut
                min_qual = sm_prompt_min_qual

                if self.displaced_en:
                    min_qual = sm_disp_min_qual

                if track.emtf_quality < min_qual:
                    continue

                # Short-Circuit: Keep valid eta region
                sm_eta_start = sm_prompt_eta_start
                sm_eta_stop = sm_prompt_eta_stop

                if self.displaced_en:
                    sm_eta_start = sm_disp_eta_start
                    sm_eta_stop = sm_disp_eta_stop

                if not (sm_eta_start <= abs(track.model_eta) < sm_eta_stop):
                    continue

                # Short-Circuit: Invalid Quality
                if track.features[38] == 0:
                    continue

                # Short-Circuit: Invalid Theta
                if track.features[37] == 0:
                    continue

                # Plot Track Info
                self.trk_q_plotter.fill(track.emtf_q)
                self.trk_pt_plotter.fill(track.model_pt)
                self.trk_dxy_sign_plotter.fill(nz_sign(track.model_dxy))
                self.trk_dxy_plotter.fill(track.model_dxy)
                self.trk_eta_plotter.fill(abs(track.model_eta))
                self.trk_phi_plotter.fill(track.model_phi)
                self.trk_qual_plotter.fill(track.emtf_quality)
                self.trk_mode_v1_plotter.fill(track.emtf_mode_v1)
                self.trk_mode_v2_plotter.fill(track.emtf_mode_v2)

                for site_id in range(12):
                    bit_mask = 1 << site_id
                    has_bit = ((track.hitmode & bit_mask) == bit_mask)

                    if has_bit:
                        self.trk_site_plotter.fill(track.nhits, site_id)

                # Pack Gen Parameters
                gen_parameters = np.asarray([
                    2, 0, 0,
                    0, 0,
                    0, 0,
                    0, 0, 0
                ])

                # Pack Track Parameters
                trk_parameters = np.asarray([
                    track.emtf_q * track.model_pt,
                    track.model_rels,
                    track.model_dxy
                ])

                self.list_trk_variables.append(track.features)
                self.list_trk_parameters.append(trk_parameters)
                self.list_gen_parameters.append(gen_parameters)

    def merge(self, other):
        self.trk_q_plotter.add(other.trk_q_plotter)
        self.trk_pt_plotter.add(other.trk_pt_plotter)
        self.trk_dxy_sign_plotter.add(other.trk_dxy_sign_plotter)
        self.trk_dxy_plotter.add(other.trk_dxy_plotter)
        self.trk_eta_plotter.add(other.trk_eta_plotter)
        self.trk_phi_plotter.add(other.trk_phi_plotter)
        self.trk_qual_plotter.add(other.trk_qual_plotter)
        self.trk_mode_v1_plotter.add(other.trk_mode_v1_plotter)
        self.trk_mode_v2_plotter.add(other.trk_mode_v2_plotter)
        self.trk_site_plotter.add(other.trk_site_plotter)

        self.list_trk_variables += other.list_trk_variables
        self.list_trk_parameters += other.list_trk_parameters
        self.list_gen_parameters += other.list_gen_parameters

    def post_production(self):
        # Convert to numpy arrays
        trk_variables = np.asarray(self.list_trk_variables)
        trk_parameters = np.asarray(self.list_trk_parameters)
        gen_parameters = np.asarray(self.list_gen_parameters)

        # Plot Variables
        plot_var_walls('trk_variables_' + self.filename, trk_variables)
        plot_var_walls('trk_parameters_' + self.filename, trk_parameters)

        # Shuffle
        random_index_array = np.arange(trk_variables.shape[0])
        np.random.shuffle(random_index_array)

        trk_variables = trk_variables[random_index_array]
        trk_parameters = trk_parameters[random_index_array]
        gen_parameters = gen_parameters[random_index_array]

        # Split Datasets
        total_tracks = int(trk_variables.shape[0])
        train_tracks = int(0.60 * total_tracks)
        quant_tracks = int(0.10 * total_tracks)
        tune_tracks = int(0.10 * total_tracks)

        test_tracks = int(0.10 * total_tracks)
        qtest_tracks = int(0.05 * total_tracks)
        ttest_tracks = total_tracks - train_tracks - quant_tracks - tune_tracks - test_tracks - qtest_tracks

        from_entry = 0
        to_entry = from_entry + train_tracks
        variables_train = trk_variables[from_entry:to_entry]
        parameters_train = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + quant_tracks
        variables_quant = trk_variables[from_entry:to_entry]
        parameters_quant = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + tune_tracks
        variables_tune = trk_variables[from_entry:to_entry]
        parameters_tune = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + test_tracks
        variables_test = trk_variables[from_entry:to_entry]
        parameters_test = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + qtest_tracks
        variables_qtest = trk_variables[from_entry:to_entry]
        parameters_qtest = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + ttest_tracks
        variables_ttest = trk_variables[from_entry:to_entry]
        parameters_ttest = gen_parameters[from_entry:to_entry]

        # Log
        print('\n*********************************************************')
        print('Pileup Sample Summary')
        print('*********************************************************')
        print('\nValid Track Count: %d' % len(self.list_trk_variables))
        print('Train Tracks: %d' % train_tracks)
        print('Quantization Tracks: %d' % quant_tracks)
        print('Tuning Tracks: %d' % tune_tracks)
        print('Test Tracks: %d' % test_tracks)
        print('QTest Tracks: %d' % qtest_tracks)
        print('TTest Tracks: %d' % ttest_tracks)

        # Save
        np.savez_compressed(self.filename + '.npz',
                            variables_emu=trk_variables, parameters_emu=trk_parameters,
                            variables_train=variables_train, parameters_train=parameters_train,
                            variables_quant=variables_quant, parameters_quant=parameters_quant,
                            variables_tune=variables_tune, parameters_tune=parameters_tune,
                            variables_test=variables_test, parameters_test=parameters_test,
                            variables_qtest=variables_qtest, parameters_qtest=parameters_qtest,
                            variables_ttest=variables_ttest, parameters_ttest=parameters_ttest)
