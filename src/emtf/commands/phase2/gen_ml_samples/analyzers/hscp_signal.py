import numpy as np

from emtf.commands.phase2.gen_ml_samples.config import sm_disp_eta_stop, sm_disp_eta_start, sm_prompt_eta_stop, sm_prompt_eta_start, sm_disp_min_qual, sm_prompt_min_qual, sm_disp_modes_v2, sm_prompt_modes_v2, sm_disp_modes_v1, sm_prompt_modes_v1
from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.binning import uniform_step_axis
from emtf.core.commons.plotting import plot_var_walls
from emtf.core.commons.tools import safe_divide, nz_sign
from emtf.core.commons.variables import emu_val_phi_deg_bins, emu_val_eta_bins, emu_val_z0_bins, emu_val_dxy_bins, emu_val_d0_bins, emu_val_d0_sign_bins, emu_val_lz_bins, emu_val_lxy_bins, emu_val_qinvpt_bins, emu_val_qpt_bins, emu_val_invpt_bins, emu_val_trk_mode_v1_bins, emu_val_trk_qual_p2_bins, emu_val_trk_mode_v2_bins, emu_val_trk_hit_count_bins, emu_val_trk_site_bins, emu_val_q_bins, emu_val_pt_bins, emu_val_dxy_sign_bins
from emtf.core.plotters.basic import Hist1DPlotter, Hist2DPlotter
from emtf.phase2.commons.emtf_constants import site_labels


class HSCPSignalAnalyzer(AbstractAnalyzer):

    def __init__(self, endcap, filename, displaced_en):
        super().__init__()

        self.endcap = endcap
        self.filename = filename
        self.displaced_en = displaced_en

        self.list_trk_variables = list()
        self.list_trk_parameters = list()
        self.list_gen_parameters = list()

        self.hscp_count = 0
        self.valid_hscp_count = 0

        self.trk_q_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_q_' + filename,
            'Sample Validation',
            'L1 Charge', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.trk_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_pt_' + filename,
            'Sample Validation',
            'L1 p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            logy=True,
            density=True)

        self.trk_dxy_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dxy_sign_' + filename,
            'Sample Validation',
            'L1 Sgn(d_{0})', 'a.u.',
            xbins=emu_val_dxy_sign_bins,
            density=True,
            logy=True)

        self.trk_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dxy_' + filename,
            'Sample Validation',
            'L1 d_{xy} [cm]', 'a.u.',
            xbins=emu_val_dxy_bins,
            density=True,
            logy=True)

        self.trk_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_phi_' + filename,
            'Sample Validation',
            'L1 #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            logy=True,
            density=True)

        self.trk_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_eta_' + filename,
            'Sample Validation',
            'L1 |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            logy=True,
            density=True)

        self.trk_qual_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_qual_' + filename,
            'Sample Validation',
            'Track Quality', 'a.u.',
            xbins=emu_val_trk_qual_p2_bins,
            density=True,
            logy=True)

        self.trk_mode_v1_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_mode_v1_' + filename,
            'Sample Validation',
            'Track ModeV1', 'a.u.',
            xbins=emu_val_trk_mode_v1_bins,
            density=True,
            logy=True)

        self.trk_mode_v2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_mode_v2_' + filename,
            'Sample Validation',
            'Track ModeV2', 'a.u.',
            xbins=emu_val_trk_mode_v2_bins,
            density=True,
            logy=True)

        self.trk_site_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_sites_' + filename,
            'Sample Validation',
            'Track Hits', 'Track Site',
            xbins=emu_val_trk_hit_count_bins,
            ybins=emu_val_trk_site_bins,
            ylabels=site_labels,
            normalize_by='column', density=False,
            min_val=1e-5, max_val=1.,
            logz=True)

        self.gen_q_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_q_' + filename,
            'Sample Validation',
            'True Charge', 'a.u.',
            xbins=emu_val_q_bins,
            density=True,
            logy=True)

        self.gen_pt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_pt_' + filename,
            'Sample Validation',
            'True p_{T} [GeV]', 'a.u.',
            xbins=emu_val_pt_bins,
            density=True,
            logy=True)

        self.gen_beta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_beta_' + filename,
            'Sample Validation',
            'True #beta', 'a.u.',
            xbins=uniform_step_axis(0.01, 0, 1),
            density=True,
            logy=True)

        self.gen_invpt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_invpt_' + filename,
            'Sample Validation',
            'True 1/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=emu_val_invpt_bins,
            density=True,
            logy=True)

        self.gen_qpt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_qpt_' + filename,
            'Sample Validation',
            'True q #upoint p_{T} [GeV]', 'a.u.',
            xbins=emu_val_qpt_bins,
            density=True,
            logy=True)

        self.gen_qinvpt_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_qinvpt_' + filename,
            'Sample Validation',
            'True q/p_{T} [GeV^{-1}]', 'a.u.',
            xbins=emu_val_qinvpt_bins,
            density=True,
            logy=True)

        self.gen_lxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_lxy_' + filename,
            'Sample Validation',
            'True L_{xy} [cm]', 'a.u.',
            xbins=emu_val_lxy_bins,
            density=True,
            logy=True)

        self.gen_lz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_lz_' + filename,
            'Sample Validation',
            'True L_{z} [cm]', 'a.u.',
            xbins=emu_val_lz_bins,
            density=True,
            logy=True)

        self.gen_d0_sign_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_d0_sign_' + filename,
            'Sample Validation',
            'True Sgn(d_{0})', 'a.u.',
            xbins=emu_val_d0_sign_bins,
            density=True,
            logy=True)

        self.gen_d0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_d0_' + filename,
            'Sample Validation',
            'True d_{0} [cm]', 'a.u.',
            xbins=emu_val_d0_bins,
            density=True,
            logy=True)

        self.gen_dxy_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_dxy_' + filename,
            'Sample Validation',
            'True d_{0} [cm]', 'a.u.',
            xbins=emu_val_dxy_bins,
            density=True,
            logy=True)

        self.gen_z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_z0_' + filename,
            'Sample Validation',
            'True z_{0} [cm]', 'a.u.',
            xbins=emu_val_z0_bins,
            density=True,
            logy=True)

        self.gen_eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_eta_' + filename,
            'Sample Validation',
            'True |#eta|', 'a.u.',
            xbins=emu_val_eta_bins,
            density=True,
            logy=True)

        self.gen_eta_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_eta_st2_' + filename,
            'Sample Validation',
            'True |#eta_{st2}|', 'a.u.',
            xbins=emu_val_eta_bins,
            density=True,
            logy=True)

        self.gen_phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'true_phi_' + filename,
            'Sample Validation',
            'True #phi [deg]', 'a.u.',
            xbins=emu_val_phi_deg_bins,
            density=True,
            logy=True)

    def process_entry(self, event):

        # Loop Hscps
        for tp in event.hscps:
            # Increase Counter
            self.hscp_count += 1

            # Short-Circuit: Only Training Quality
            if not tp.is_training_quality:
                continue

            # Short-Circuit: Keep positive endcap
            if (self.endcap == 'positive') and (tp.eta_st2 < 0):
                continue

            # Short-Circuit: Keep negative endcap
            if (self.endcap == 'negative') and (tp.eta_st2 >= 0):
                continue

            # Short-Circuit: Negative d0
            if tp.d0 < 0:
                continue

            # Unpack Track Data
            track = tp.best_prompt_track

            if self.displaced_en:
                track = tp.best_disp_track

            # Short-Circuit: Should have a track
            if track is None:
                continue

            # Short-Circuit: Skip invalid v1 modes
            valid_modes = sm_prompt_modes_v1

            if self.displaced_en:
                valid_modes = sm_disp_modes_v1

            if track.emtf_mode_v1 not in valid_modes:
                continue

            # Short-Circuit: Skip invalid v2 modes
            valid_modes = sm_prompt_modes_v2

            if self.displaced_en:
                valid_modes = sm_disp_modes_v2

            if track.emtf_mode_v2 not in valid_modes:
                continue

            # Short-Circuit: Include Quality Cut
            min_qual = sm_prompt_min_qual

            if self.displaced_en:
                min_qual = sm_disp_min_qual

            if track.emtf_quality < min_qual:
                continue

            # Short-Circuit: Keep valid eta region
            sm_eta_start = sm_prompt_eta_start
            sm_eta_stop = sm_prompt_eta_stop

            if self.displaced_en:
                sm_eta_start = sm_disp_eta_start
                sm_eta_stop = sm_disp_eta_stop

            if not (sm_eta_start <= abs(track.model_eta) < sm_eta_stop):
                continue

            # Short-Circuit: Invalid Quality
            if track.features[38] == 0:
                continue

            # Short-Circuit: Invalid Theta
            if track.features[37] == 0:
                continue

            # Increase Counter
            self.valid_hscp_count += 1

            # Plot Track Info
            self.trk_q_plotter.fill(track.emtf_q)
            self.trk_pt_plotter.fill(track.model_pt)
            self.trk_dxy_sign_plotter.fill(nz_sign(track.model_dxy))
            self.trk_dxy_plotter.fill(track.model_dxy)
            self.trk_eta_plotter.fill(abs(track.model_eta))
            self.trk_phi_plotter.fill(track.model_phi)
            self.trk_qual_plotter.fill(track.emtf_quality)
            self.trk_mode_v1_plotter.fill(track.emtf_mode_v1)
            self.trk_mode_v2_plotter.fill(track.emtf_mode_v2)

            me11_bx = 0
            me12_bx = 0
            me2_bx = 0
            me3_bx = 0
            me4_bx = 0

            re1_subbx = 0
            re2_subbx = 0
            re3_subbx = 0
            re4_subbx = 0

            for site_id in range(12):
                bit_mask = 1 << site_id
                has_bit = ((track.hitmode & bit_mask) == bit_mask)

                # Short-Circuit: Site not present
                if not has_bit:
                    continue

                # Get timing information
                if site_id == 0:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    me11_bx = hit.bx
                if site_id == 1:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    me12_bx = hit.bx
                if site_id == 2:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    me2_bx = hit.bx
                if site_id == 3:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    me3_bx = hit.bx
                if site_id == 4:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    me4_bx = hit.bx
                if site_id == 5:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    re1_subbx = hit.subbx
                if site_id == 6:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    re2_subbx = hit.subbx
                if site_id == 7:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    re3_subbx = hit.subbx
                if site_id == 8:
                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    re4_subbx = hit.subbx

                self.trk_site_plotter.fill(track.nhits, site_id)

            # Plot Gen Info
            self.gen_q_plotter.fill(tp.q)
            self.gen_pt_plotter.fill(tp.pt)
            self.gen_invpt_plotter.fill(tp.invpt)
            self.gen_qpt_plotter.fill(tp.q * tp.pt)
            self.gen_qinvpt_plotter.fill(tp.q * tp.invpt)
            self.gen_beta_plotter.fill(tp.beta)
            self.gen_lxy_plotter.fill(tp.lxy)
            self.gen_lz_plotter.fill(abs(tp.vz))
            self.gen_d0_sign_plotter.fill(nz_sign(tp.d0))
            self.gen_d0_plotter.fill(tp.d0)
            self.gen_dxy_plotter.fill(tp.dxy)
            self.gen_z0_plotter.fill(tp.z0)
            self.gen_eta_plotter.fill(abs(tp.eta))
            self.gen_eta_st2_plotter.fill(abs(tp.eta_st2))
            self.gen_phi_plotter.fill(np.rad2deg(tp.phi))

            # Pack Gen Parameters
            gen_parameters = np.asarray([
                1, tp.q * tp.pt, tp.beta,
                tp.dxy, tp.z0,
                tp.eta_st2, tp.phi_st2,
                tp.vx, tp.vy, tp.vz
            ])

            # Inject Timing Information
            timing = me11_bx, me12_bx, me2_bx, me3_bx, me4_bx, re1_subbx, re2_subbx, re3_subbx, re4_subbx
            trk_features = np.insert(track.features, 36, timing)

            # Pack Track Parameters
            trk_parameters = np.asarray([
                track.emtf_q * track.model_pt,
                track.model_rels,
                track.model_dxy
            ])

            self.list_trk_variables.append(trk_features)
            self.list_trk_parameters.append(trk_parameters)
            self.list_gen_parameters.append(gen_parameters)

    def merge(self, other):
        self.trk_q_plotter.add(other.trk_q_plotter)
        self.trk_pt_plotter.add(other.trk_pt_plotter)
        self.trk_dxy_sign_plotter.add(other.trk_dxy_sign_plotter)
        self.trk_dxy_plotter.add(other.trk_dxy_plotter)
        self.trk_eta_plotter.add(other.trk_eta_plotter)
        self.trk_phi_plotter.add(other.trk_phi_plotter)
        self.trk_qual_plotter.add(other.trk_qual_plotter)
        self.trk_mode_v1_plotter.add(other.trk_mode_v1_plotter)
        self.trk_mode_v2_plotter.add(other.trk_mode_v2_plotter)
        self.trk_site_plotter.add(other.trk_site_plotter)

        self.gen_q_plotter.add(other.gen_q_plotter)
        self.gen_pt_plotter.add(other.gen_pt_plotter)
        self.gen_invpt_plotter.add(other.gen_invpt_plotter)
        self.gen_qpt_plotter.add(other.gen_qpt_plotter)
        self.gen_qinvpt_plotter.add(other.gen_qinvpt_plotter)
        self.gen_beta_plotter.add(other.gen_beta_plotter)
        self.gen_lxy_plotter.add(other.gen_lxy_plotter)
        self.gen_lz_plotter.add(other.gen_lz_plotter)
        self.gen_d0_sign_plotter.add(other.gen_d0_sign_plotter)
        self.gen_d0_plotter.add(other.gen_d0_plotter)
        self.gen_dxy_plotter.add(other.gen_dxy_plotter)
        self.gen_z0_plotter.add(other.gen_z0_plotter)
        self.gen_eta_plotter.add(other.gen_eta_plotter)
        self.gen_eta_st2_plotter.add(other.gen_eta_st2_plotter)
        self.gen_phi_plotter.add(other.gen_phi_plotter)

        self.hscp_count += other.hscp_count
        self.valid_hscp_count += other.valid_hscp_count

        self.list_trk_variables += other.list_trk_variables
        self.list_trk_parameters += other.list_trk_parameters
        self.list_gen_parameters += other.list_gen_parameters

    def post_production(self):
        # Convert to numpy arrays
        trk_variables = np.asarray(self.list_trk_variables)
        trk_parameters = np.asarray(self.list_trk_parameters)
        gen_parameters = np.asarray(self.list_gen_parameters)

        # Plot Variables
        plot_var_walls('trk_variables_' + self.filename, trk_variables)
        plot_var_walls('trk_parameters_' + self.filename, trk_parameters)
        plot_var_walls('gen_parameters_' + self.filename, gen_parameters)

        # Shuffle
        random_index_array = np.arange(trk_variables.shape[0])
        np.random.shuffle(random_index_array)

        trk_variables = trk_variables[random_index_array]
        trk_parameters = trk_parameters[random_index_array]
        gen_parameters = gen_parameters[random_index_array]

        # Split Datasets
        total_tracks = int(trk_variables.shape[0])
        train_tracks = int(0.60 * total_tracks)
        quant_tracks = int(0.10 * total_tracks)
        tune_tracks = int(0.10 * total_tracks)

        test_tracks = int(0.10 * total_tracks)
        qtest_tracks = int(0.05 * total_tracks)
        ttest_tracks = total_tracks - train_tracks - quant_tracks - tune_tracks - test_tracks - qtest_tracks

        from_entry = 0
        to_entry = from_entry + train_tracks
        variables_train = trk_variables[from_entry:to_entry]
        parameters_train = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + quant_tracks
        variables_quant = trk_variables[from_entry:to_entry]
        parameters_quant = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + tune_tracks
        variables_tune = trk_variables[from_entry:to_entry]
        parameters_tune = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + test_tracks
        variables_test = trk_variables[from_entry:to_entry]
        parameters_test = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + qtest_tracks
        variables_qtest = trk_variables[from_entry:to_entry]
        parameters_qtest = gen_parameters[from_entry:to_entry]

        from_entry = to_entry
        to_entry = from_entry + ttest_tracks
        variables_ttest = trk_variables[from_entry:to_entry]
        parameters_ttest = gen_parameters[from_entry:to_entry]

        # Log
        print('\n*********************************************************')
        print('Signal Sample Summary')
        print('*********************************************************')
        print('Hscp Count: %d' % self.hscp_count)
        print('Valid Hscp Count: %d (%0.2f %%)' % (
            self.valid_hscp_count, safe_divide(self.valid_hscp_count, self.hscp_count) * 100))
        print('\nTotal Tracks: %d' % total_tracks)
        print('Train Tracks: %d' % train_tracks)
        print('Quantization Tracks: %d' % quant_tracks)
        print('Tuning Tracks: %d' % tune_tracks)
        print('Test Tracks: %d' % test_tracks)
        print('QTest Tracks: %d' % qtest_tracks)
        print('TTest Tracks: %d' % ttest_tracks)

        # Save
        np.savez_compressed(self.filename + '.npz',
                            variables_emu=trk_variables, parameters_emu=trk_parameters,
                            variables_train=variables_train, parameters_train=parameters_train,
                            variables_quant=variables_quant, parameters_quant=parameters_quant,
                            variables_tune=variables_tune, parameters_tune=parameters_tune,
                            variables_test=variables_test, parameters_test=parameters_test,
                            variables_qtest=variables_qtest, parameters_qtest=parameters_qtest,
                            variables_ttest=variables_ttest, parameters_ttest=parameters_ttest)
