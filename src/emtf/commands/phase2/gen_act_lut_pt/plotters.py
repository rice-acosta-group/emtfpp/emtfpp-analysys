import numpy as np
from ROOT import TCanvas
from ROOT import TF1
from ROOT import TGraph
from ROOT import TLatex
from ROOT import TPad
from ROOT import gStyle
from ROOT import kAzure

from emtf.core.analyzers import AbstractPlotter
from emtf.core.commons.tools import safe_divide
from emtf.core.root.labels import draw_fancy_label


class IdentityWrapper(object):

    def __init__(self):
        pass

    def __call__(self, x, par):
        return x[0]


class ActivationPlotter(AbstractPlotter):

    def __init__(self, name, title,
                 x_title, y_title,
                 x_draw_min, x_draw_max,
                 y_draw_min, y_draw_max,
                 logx=False, logy=False):
        super().__init__()

        self.name = name
        self.title = title
        self.x_title = x_title
        self.y_title = y_title
        self.point_count = 0
        self.x_arr = list()
        self.y_arr = list()
        self.logx = logx
        self.logy = logy

        self.plot = None
        self.rerr_plot = None
        self.identity_function = None
        self.identity_draw_function = None

        self.x_draw_min = x_draw_min
        self.x_draw_max = x_draw_max
        self.y_draw_min = y_draw_min
        self.y_draw_max = y_draw_max

        self.consolidated = False

    def fill(self, x, y):
        self.point_count += 1
        self.x_arr.append(x)
        self.y_arr.append(y)

    def consolidate(self):
        # Short-Circuit: Don't consolidate again
        if self.consolidated:
            return

        self.consolidated = True

        # Create
        x_arr_np = np.asarray(self.x_arr, dtype=float)
        y_arr_np = np.asarray(self.y_arr, dtype=float)
        rerr_arr_np = safe_divide(y_arr_np - x_arr_np, x_arr_np)

        self.plot = TGraph(self.point_count, x_arr_np, y_arr_np)
        self.plot.SetTitle(self.title)

        # Create SF Plot
        self.rerr_plot = TGraph(self.point_count, x_arr_np, rerr_arr_np)
        self.rerr_plot.SetTitle(self.title)

        # Init functions        
        self.identity_function = IdentityWrapper()

        self.identity_draw_function = TF1(
            self.name + '_identity', self.identity_function,
            self.x_draw_min, self.x_draw_max, 1
        )

    def write(self):
        # WRITE
        self.plot.Write()

        # IMAGE
        gStyle.SetOptStat(0)

        # CREATE CANVAS
        plot_canvas = TCanvas(self.name, '')
        plot_canvas.SetLeftMargin(0.200)
        plot_canvas.SetRightMargin(0.200)
        plot_canvas.SetBottomMargin(0.200)

        # Draw Top Canvas
        plot_canvas.cd(0)
        top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
        top_pad.SetTopMargin(0.175)
        top_pad.SetLeftMargin(0.175)
        top_pad.SetRightMargin(0.050)
        top_pad.SetBottomMargin(0.400)
        top_pad.SetLogx(1 if self.logx else 0)
        top_pad.SetLogy(1 if self.logy else 0)
        top_pad.Draw()
        top_pad.cd()

        # Draw Before and After
        frame_top = plot_canvas.DrawFrame(
            self.x_draw_min, self.y_draw_min,
            self.x_draw_max, self.y_draw_max,
            '')

        frame_top.GetXaxis().SetTitle('')
        frame_top.GetXaxis().SetLabelOffset(999)
        frame_top.GetXaxis().SetLabelSize(0)

        frame_top.GetYaxis().SetTitle(self.y_title)
        frame_top.GetYaxis().SetTitleOffset(1.450)

        self.identity_draw_function.SetLineColor(kAzure + 2)
        self.identity_draw_function.SetLineWidth(2)
        self.identity_draw_function.Draw('SAME')

        self.plot.SetMarkerStyle(8)
        self.plot.SetMarkerSize(0.5)
        self.plot.Draw('P')

        frame_top.Draw('SAME AXIS')
        frame_top.Draw('SAME AXIG')

        # Draw Bottom Canvas
        plot_canvas.cd(0)
        bottom_pad = TPad("center", "center", 0.0, 0.0, 1.0, 1.0)
        bottom_pad.SetFillStyle(4000)
        bottom_pad.SetTopMargin(0.650)
        bottom_pad.SetLeftMargin(0.175)
        bottom_pad.SetRightMargin(0.050)
        bottom_pad.SetBottomMargin(0.160)
        bottom_pad.Draw()
        bottom_pad.cd()

        # Draw SF
        frame_bottom = plot_canvas.DrawFrame(
            self.x_draw_min, -1.0,
            self.x_draw_max, 1.0,
            '')

        frame_bottom.GetXaxis().SetTitle(self.x_title)
        frame_bottom.GetXaxis().SetTitleOffset(1.350)
        frame_bottom.GetYaxis().SetTitle('Rel. Err.')
        frame_bottom.GetYaxis().SetNdivisions(204)
        frame_bottom.GetYaxis().SetTitleOffset(1.450)

        self.rerr_plot.SetMarkerStyle(8)
        self.rerr_plot.SetMarkerSize(0.5)
        self.rerr_plot.Draw('P')

        frame_bottom.Draw('SAME AXIS')
        frame_bottom.Draw('SAME AXIG')

        # Draw Titles
        plot_canvas.cd(0)

        main_title = TLatex(0.045, 0.95, self.title)
        main_title.SetTextAlign(12)
        main_title.Draw()

        draw_fancy_label(0.045, 0.875)

        # Save
        plot_canvas.SaveAs(self.name + '.png')
