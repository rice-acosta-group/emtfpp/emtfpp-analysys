import math

import numpy as np
from scipy import stats

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.tools import safe_divide
from emtf.core.plotters.basic import Hist1DPlotter, Hist2DPlotter


def gen_x_vs_z(radii, angles):
    x_vals = []
    z_vals = [850., 1050.]

    for i in range(len(radii)):
        x_vals.append(radii[i] * np.cos(angles[i]))

    return stats.linregress(z_vals, x_vals)


def gen_y_vs_z(radii, angles):
    y_vals = []
    z_vals = [850., 1050.]

    for i in range(len(radii)):
        y_vals.append(radii[i] * np.sin(angles[i]))

    return stats.linregress(z_vals, y_vals)


class GenAnalyzer(AbstractAnalyzer):

    def __init__(self, beamhalo_en):
        super().__init__()

        self.count_muon = 0
        self.count_has_track = 0
        self.beamhalo = beamhalo_en

        self.drdz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_drdz',
            'Beam Halo Study',
            'GEN dr/dz', 'a.u.',
            xbins=np.linspace(-0.5, 0.5, 100),
            density=False,
            logy=False)

        self.dphidz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_dphidz',
            'Beam Halo Study',
            'GEN dphi/dz', 'a.u.',
            xbins=np.linspace(-0.1, 0.1, 100),
            density=False,
            logy=False)

        self.z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_z0',
            'Beam Halo Study',
            'GEN z0', 'a.u.',
            xbins=np.linspace(-10000, 10000, 100),
            density=False,
            logy=True)

        self.rat0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_r_at_0',
            'Beam Halo Study',
            'GEN r at z=0', 'a.u.',
            xbins=np.linspace(0, 700, 100),
            density=False,
            logx=True, logy=True)

        self.xy_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'gen_xy',
            'Beam Halo Study',
            'x at z=0', 'y at z=0',
            xbins=np.linspace(-650, 650, 100),
            ybins=np.linspace(-650, 650, 100),
            density=False)

        self.eta_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta',
            'Beam Halo Study',
            'GEN #eta', 'a.u.',
            xbins=np.linspace(-10, 10, 100),
            density=False,
            logy=True)

        self.eta_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_eta_st2',
            'Beam Halo Study',
            'GEN #eta_{st2}', 'a.u.',
            xbins=np.linspace(-10, 10, 100),
            density=True,
            logy=True)

        self.phi_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi',
            'Beam Halo Study',
            'GEN #phi [deg]', 'a.u.',
            xbins=np.linspace(-180, 180, 100),
            density=True,
            logy=True)

        self.phi_st2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_phi_st2',
            'Beam Halo Study',
            'GEN #phi_{st2} [deg]', 'a.u.',
            xbins=np.linspace(-180, 180, 100),
            density=True,
            logy=True)

        self.vz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'gen_vz',
            'Beam Halo Study',
            'GEN vz', 'a.u.',
            xbins=np.linspace(2200, 2400, 100),
            density=False,
            logy=True)

    def process_entry(self, event):
        if self.beamhalo:
            muons = event.beam_halo_muons
        else:
            muons = event.muons

        for gen in muons:
            # Propagate Particle
            mom_st2, pos_st2 = gen.mom_pos_st2
            mom_st4, pos_st4 = gen.mom_pos_st4

            # Calculate Parameters
            phi_z1, phi_z2 = pos_st2.Phi(), pos_st4.Phi()
            r_z1, r_z2 = pos_st2.Perp(), pos_st4.Perp()

            phi_vals = (phi_z1, phi_z2)
            r_vals = (r_z1, r_z2)

            path_xz = gen_x_vs_z(r_vals, phi_vals)
            path_yz = gen_y_vs_z(r_vals, phi_vals)

            b = path_xz.slope
            a = path_xz.intercept
            d = path_yz.slope
            c = path_yz.intercept
            z_0 = safe_divide(-(b * a + d * c), (b ** 2 + d ** 2))
            r_at_0 = math.hypot(a, c)

            self.count_muon += 1

            if gen.best_disp_track is not None or gen.best_prompt_track is not None:
                self.count_has_track += 1

            self.dphidz_plotter.fill(gen.dphidz)
            self.drdz_plotter.fill(gen.drdz)
            self.z0_plotter.fill(z_0)

            self.rat0_plotter.fill(r_at_0)
            self.xy_plotter.fill(a, c)
            self.eta_plotter.fill(gen.eta)
            self.eta_st2_plotter.fill(gen.eta_st2)
            self.phi_plotter.fill(np.rad2deg(gen.phi))
            self.phi_st2_plotter.fill(np.rad2deg(gen.phi_st2))
            self.vz_plotter.fill(gen.vz)

    def merge(self, other):
        self.count_muon += other.count_muon
        self.count_has_track += other.count_has_track
        self.dphidz_plotter.add(other.dphidz_plotter)
        self.drdz_plotter.add(other.drdz_plotter)
        self.z0_plotter.add(other.z0_plotter)
        self.rat0_plotter.add(other.rat0_plotter)
        self.xy_plotter.add(other.xy_plotter)
        self.eta_plotter.add(other.eta_plotter)
        self.eta_st2_plotter.add(other.eta_st2_plotter)
        self.phi_plotter.add(other.phi_plotter)
        self.phi_st2_plotter.add(other.phi_st2_plotter)
        self.vz_plotter.add(other.vz_plotter)

    def post_production(self):
        print('N Halo Muon', self.count_muon)
        print('N Halo Muon with Track', self.count_has_track)
