import numpy as np

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.plotters.basic import Hist1DPlotter


class HitAnalyzer(AbstractAnalyzer):

    def __init__(self, beamhalo_en):
        super().__init__()

        self.beamhalo = beamhalo_en

        self.trk_bx_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'hit_bx',
            'Beam Halo Study',
            'Hit bx', 'a.u.',
            xbins=np.linspace(-3, 3, 100),
            density=False,
            logy=True)

    def process_entry(self, event):
        if self.beamhalo:
            muons = event.beam_halo_muons
        else:
            muons = event.muons
        for tp in muons:
            # for tp in event.muons:
            tracks = []
            track_prompt = tp.best_prompt_track
            track_disp = tp.best_disp_track

            if track_prompt is not None:
                tracks.append(track_prompt)

            if track_disp is not None:
                tracks.append(track_disp)

            # Loop Tracks
            for track in tracks:
                if not track.valid:
                    continue

                hits = []

                for site_id, site_mask in enumerate(track.site_mask):
                    if ord(site_mask) == 0:
                        continue

                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    hits.append(hit)

                for hit in hits:
                    hit_bx = hit.bx
                    self.trk_bx_plotter.fill(hit_bx)

    def merge(self, other):
        self.trk_bx_plotter.add(other.trk_bx_plotter)

    def post_production(self):
        return
