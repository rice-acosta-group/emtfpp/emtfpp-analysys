import numpy as np
from scipy import stats

from emtf.core.analyzers import AbstractAnalyzer
from emtf.core.commons.tools import safe_divide
from emtf.core.plotters.basic import Hist1DPlotter, Hist2DPlotter


def r_vs_z(hits):
    r_vals = []
    z_vals = []
    for hit in hits:
        r_vals.append(hit.glob_perp)
        z_vals.append(hit.glob_z)
    return stats.linregress(z_vals, r_vals)


def x_vs_z(hits):
    x_vals = []
    z_vals = []
    for hit in hits:
        x_vals.append(hit_x(hit))
        z_vals.append(hit.glob_z)
    return stats.linregress(z_vals, x_vals)


def y_vs_z(hits):
    y_vals = []
    z_vals = []
    for hit in hits:
        y_vals.append(hit_y(hit))
        z_vals.append(hit.glob_z)

    return stats.linregress(z_vals, y_vals)


def hit_x(hit):
    return hit.glob_perp * np.cos(hit.glob_phi / 180 * np.pi)


def hit_y(hit):
    return hit.glob_perp * np.sin(hit.glob_phi / 180 * np.pi)


def dphidz_calc(hits):
    phi_vals = []
    z_vals = []
    for hit in hits:
        phi_vals.append(hit.glob_phi)
        z_vals.append(hit.glob_z)
    regression = stats.linregress(z_vals, phi_vals)
    return regression.slope


class TrackAnalyzer(AbstractAnalyzer):

    def __init__(self, beamhalo_en):
        super().__init__()

        self.count_beamhalo = 0
        self.count_muon = 0
        self.count_rat0 = 0
        self.count_drdz = 0
        self.count_dphidz = 0
        self.count_tracks = 0
        self.count_prompt = 0
        self.count_displaced = 0

        self.beamhalo = beamhalo_en

        self.trk_drdz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_drdz',
            'Beam Halo Study',
            'Track dr/dz', 'a.u.',
            xbins=np.linspace(-0.5, 0.5, 100),
            density=False,
            logy=False)

        self.trk_dphidz_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_dphidz',
            'Beam Halo Study',
            'Track dphi/dz', 'a.u.',
            xbins=np.linspace(-0.1, 0.1, 100),
            density=False,
            logy=False)

        self.trk_rat0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_r_at_0',
            'Beam Halo Study',
            'Track r at z=0', 'a.u.',
            xbins=np.linspace(0, 700, 100),
            density=False, logx=False,
            logy=True)

        self.trk_bx_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_bx',
            'Beam Halo Study',
            'Track bx', 'a.u.',
            xbins=np.linspace(-3, 3, 100),
            density=False,
            logy=True)

        self.trk_z0_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_z0',
            'Beam Halo Study',
            'Track z0', 'a.u.',
            xbins=np.linspace(-10000, 10000, 100),
            density=False,
            logy=True)

        self.trk_modev1_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_modev1',
            'Beam Halo Study',
            'Track modev1', 'a.u.',
            xbins=np.linspace(0, 16, 16),
            density=False,
            logy=True)

        self.trk_modev2_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_modev2',
            'Beam Halo Study',
            'Track modev2', 'a.u.',
            xbins=np.linspace(0, 16, 16),
            density=False,
            logy=True)

        self.trk_xy_plotter = self.checkout_plotter(
            Hist2DPlotter,
            'trk_xy',
            'Beam Halo Study',
            'x at z=0', 'y at z=0',
            xbins=np.linspace(-650, 650, 100),
            ybins=np.linspace(-650, 650, 100),
            density=False)

        self.trk_nhits_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_nhits',
            'Beam Halo Study',
            'Track nhits', 'a.u.',
            xbins=np.linspace(0, 13, 13),
            density=False,
            logy=True)

        self.trk_quality_plotter = self.checkout_plotter(
            Hist1DPlotter,
            'trk_quality',
            'Beam Halo Study',
            'Track quality', 'a.u.',
            xbins=np.linspace(0, 16, 16),
            density=False,
            logy=True)

    def process_entry(self, event):
        if self.beamhalo:
            muons = event.beam_halo_muons
        else:
            muons = event.muons

        for tp in muons:
            tracks = []
            beamhalo = False
            track_prompt = tp.best_prompt_track
            track_disp = tp.best_disp_track

            if track_prompt is not None and track_prompt.emtf_quality >= 8:
                tracks.append(track_prompt)

            if track_disp is not None and track_disp.emtf_quality >= 8:
                tracks.append(track_disp)

            if tracks:
                self.count_muon += 1

            # Loop Tracks
            for track in tracks:
                if not track.valid:
                    continue

                hits = []

                for site_id, site_mask in enumerate(track.site_mask):
                    if ord(site_mask) == 0:
                        continue

                    hit_idx = track.site_hits[site_id]
                    hit = event.emtf_hits[hit_idx]
                    hits.append(hit)

                if len(hits) < 2:
                    continue

                # uncomment continue if looking at displaced tracks
                if track.unconstrained == 0:
                    self.count_prompt += 1
                    # continue

                # uncomment continue if looking at prompt tracks
                if track.unconstrained == 1:
                    self.count_displaced += 1
                    # continue

                trackrz = r_vs_z(hits)
                trackxz = x_vs_z(hits)
                trackyz = y_vs_z(hits)

                drdz = trackrz.slope
                dphidz = dphidz_calc(hits)
                r_at_0 = abs(trackrz.intercept)

                if abs(dphidz) < 0.02 and abs(drdz) < 0.2 and abs(r_at_0) > 200:
                    beamhalo = True

                # trk_entry = {
                #     'trk_unconstrained': track.unconstrained,
                #     'trk_qual': track.emtf_quality,
                #     'trk_q': trk_q,
                #     'trk_pt': trk_pt,
                #     'trk_rels': trk_rels,
                #     'trk_dxy': trk_dxy,
                #     'trk_phi': track.model_phi,
                #     'trk_eta': track.model_eta
                # }

                self.trk_drdz_plotter.fill(drdz)
                self.trk_rat0_plotter.fill(r_at_0)
                self.trk_dphidz_plotter.fill(dphidz)

                mode_v1 = track.emtf_mode_v1
                self.trk_modev1_plotter.fill(mode_v1)

                mode_v2 = track.emtf_mode_v2
                self.trk_modev2_plotter.fill(mode_v2)

                nhits = track.nhits
                self.trk_nhits_plotter.fill(nhits)

                quality = track.emtf_quality
                self.trk_quality_plotter.fill(quality)

                for hit in hits:
                    if hit.station == 4 and hit.subsystem == 1:
                        hit_bx = hit.bx
                        self.trk_bx_plotter.fill(hit_bx)

                b = trackxz.slope
                a = trackxz.intercept
                d = trackyz.slope
                c = trackyz.intercept
                z_0 = safe_divide(-(b * a + d * c), (b ** 2 + d ** 2))

                self.trk_z0_plotter.fill(z_0)
                self.trk_xy_plotter.fill(a, c)

                if abs(dphidz) < 0.02:
                    self.count_dphidz += 1

                if abs(drdz) < 0.2:
                    self.count_drdz += 1

                if abs(r_at_0) > 200:
                    self.count_rat0 += 1
            if beamhalo:
                self.count_beamhalo += 1

    def merge(self, other):
        self.count_prompt += other.count_prompt
        self.count_displaced += other.count_displaced
        self.count_dphidz += other.count_dphidz
        self.count_drdz += other.count_drdz
        self.count_rat0 += other.count_rat0
        self.count_tracks += other.count_tracks
        self.count_beamhalo += other.count_beamhalo
        self.count_muon += other.count_muon

        self.trk_drdz_plotter.add(other.trk_drdz_plotter)
        self.trk_dphidz_plotter.add(other.trk_dphidz_plotter)
        self.trk_rat0_plotter.add(other.trk_rat0_plotter)
        self.trk_bx_plotter.add(other.trk_bx_plotter)
        self.trk_z0_plotter.add(other.trk_z0_plotter)
        self.trk_modev1_plotter.add(other.trk_modev1_plotter)
        self.trk_modev2_plotter.add(other.trk_modev2_plotter)
        self.trk_xy_plotter.add(other.trk_xy_plotter)
        self.trk_nhits_plotter.add(other.trk_nhits_plotter)
        self.trk_quality_plotter.add(other.trk_quality_plotter)

    def post_production(self):
        print('Prompt:', self.count_prompt)
        print('Displaced:', self.count_displaced)
        print('Count of dphi/dz:', self.count_dphidz)
        print('Count of dr/dz:', self.count_drdz)
        print('Count of R(z=0):', self.count_rat0)
        print('Count of muons with tracks:', self.count_muon)
        print('Count of Beam Halo:', self.count_beamhalo)
        print('Ratio:', safe_divide(self.count_beamhalo, self.count_muon))
