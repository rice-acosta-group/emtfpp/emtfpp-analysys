import os

from ROOT import TFile

from emtf.commands.phase2.check_beamhalo.gen import GenAnalyzer
from emtf.commands.phase2.check_beamhalo.hits import HitAnalyzer
from emtf.commands.phase2.check_beamhalo.tracks import TrackAnalyzer
from emtf.phase2.analysis.phase2_engine import phase2_run
from emtf.phase2.producers.Phase2HaloEventProducer import Phase2HaloEventProducer
from emtf.phase2.producers.Phase2MuonEventProducer import Phase2MuonEventProducer


def configure_parser(parser):
    parser.add_argument('--input', dest='input_files', metavar='INPUT_FILE',
                        type=str, nargs='+', default=None,
                        help='Input File')

    parser.add_argument('--nevents', dest='max_events', metavar='MAX_EVENTS',
                        type=int, default=-1,
                        help='Number of events to analyze per file')

    parser.add_argument('--nmuon', dest='n_muon', metavar='N_MUON',
                        type=int, default=1,
                        help='Number of muons per event')

    parser.add_argument('--beamhalo', dest='beamhalo_en', metavar='BEAMHALO_EN',
                        type=bool, nargs='?', const=True, default=False,
                        help='Enables beam halo simulation file support')


def run(args):
    # Unpack args
    input_files = args.input_files
    max_events = args.max_events
    n_muon = args.n_muon
    beamhalo_en = args.beamhalo_en

    # Check inputs
    inputs_consistent = True

    for input_file in input_files:
        if input_file is None:
            inputs_consistent = False
            break

    assert inputs_consistent, "Inputs are invalid. Check you\'re not missing an input file or that they are not empty"

    # Build Analysis
    main_analyzers = [
        TrackAnalyzer(beamhalo_en),
        GenAnalyzer(beamhalo_en),
        HitAnalyzer(beamhalo_en)
    ]

    def analysis_fn():
        return [
            Phase2MuonEventProducer(n_muon),
            Phase2HaloEventProducer()
        ], [
            TrackAnalyzer(beamhalo_en),
            GenAnalyzer(beamhalo_en),
            HitAnalyzer(beamhalo_en)
        ]

    def merge_fn(analyzers_by_worker):
        for analyzers in analyzers_by_worker:
            for analyzer_id in range(len(analyzers)):
                partial_analyzer = analyzers[analyzer_id]
                main_analyzers[analyzer_id].merge(partial_analyzer)

    # Run Analysis
    phase2_run(input_files, max_events, os.cpu_count(), analysis_fn, merge_fn)

    # Write
    output_root_file = TFile.Open('outputs.root', 'RECREATE')

    for main_analyzer in main_analyzers:
        main_analyzer.write()

    output_root_file.Close()
