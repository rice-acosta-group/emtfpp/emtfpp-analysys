from ROOT import TCanvas
from ROOT import TFile
from ROOT import TGraphAsymmErrors
from ROOT import TLatex
from ROOT import TLegend
from ROOT import TPad
from ROOT import gPad
from ROOT import gStyle
from ROOT import kBlack
from ROOT import kBlue
from ROOT import kRed

import emtf.core.root.environment as root_env


def make_eff_ratio_plot(
        res_file='../../out/performance_smu_disp_on_LLP_noOR/outputs.root',
        ref_file='../../out/performance_smu_prompt_on_LLP/outputs.root',
        res_label='Disp.',
        ref_label='Prompt',
        plt_name='pt_eff_vs_pt_num_pt_ge_10_denom_eta_st2_ge_1p2_lt_2p4',
        filename=None,
        title='LLP Gun Efficiency',
        xtitle='True p_{T} [cm]',
        ytitle='Efficiency',
        xmin=0, xmax=400,
        rmin=0, rmax=2
):
    root_env.configure()

    res_root = TFile.Open(res_file)
    ref_root = TFile.Open(ref_file)

    res_eff_plot = getattr(res_root, plt_name)
    ref_eff_plot = getattr(ref_root, plt_name)

    ratio_plot = TGraphAsymmErrors(
        res_eff_plot.GetPassedHistogram(),
        ref_eff_plot.GetPassedHistogram(),
        'pois'
    )

    # Make RTitle
    rtitle = '%s / %s' % (res_label, ref_label)

    # Build Entries
    eff_entries = [
        (ref_eff_plot, ref_label, kRed, 20),
        (res_eff_plot, res_label, kBlue, 22),
    ]

    # IMAGE
    gStyle.SetOptStat(0)

    # CREATE CANVAS
    plot_canvas = TCanvas('eff_comparison', '')
    plot_canvas.SetLeftMargin(0.200)
    plot_canvas.SetRightMargin(0.200)
    plot_canvas.SetBottomMargin(0.200)

    # Draw Top Canvas
    plot_canvas.cd(0)
    top_pad = TPad("top", "top", 0.0, 0.0, 1.0, 1.0)
    top_pad.SetTopMargin(0.125)
    top_pad.SetLeftMargin(0.175)
    top_pad.SetRightMargin(0.050)
    top_pad.SetBottomMargin(0.400)
    top_pad.Draw()
    top_pad.cd()

    # Init Eff Plots
    for plot, label, color, marker in eff_entries:
        plot.Paint("")
        gPad.Update()

    # Draw Before and After
    frame_top = plot_canvas.DrawFrame(
        xmin, 0,
        xmax, 1.15,
        ''
    )
    frame_top.GetXaxis().SetTitle('')
    frame_top.GetXaxis().SetLabelOffset(999)
    frame_top.GetXaxis().SetLabelSize(0)
    frame_top.GetYaxis().SetTitle(ytitle)
    frame_top.GetYaxis().SetTitleOffset(1.450)
    frame_top.GetYaxis().SetMaxDigits(3)

    legend_x0 = 0.650
    legend_y0 = 0.450

    legend = TLegend(legend_x0, legend_y0, legend_x0 + 0.275, legend_y0 + 0.150)
    legend.SetMargin(0.250)
    legend.SetFillColorAlpha(kBlack, 0.1)
    legend.SetBorderSize(0)
    legend.SetTextSize(.035)

    for plot, label, color, marker in eff_entries:
        plot.SetMarkerColor(color)
        plot.SetMarkerSize(1)
        plot.SetMarkerStyle(marker)
        plot.SetLineColor(color)
        plot.SetLineWidth(2)

        plot.Draw('P SAME')

        legend.AddEntry(plot, label, 'P E')

    frame_top.Draw('SAME AXIS')
    frame_top.Draw('SAME AXIG')

    legend.Draw()

    # Draw Bottom Canvas
    plot_canvas.cd(0)
    bottom_pad = TPad("bottom", "bottom", 0.0, 0.0, 1.0, 1.0)
    bottom_pad.SetFillStyle(4000)
    bottom_pad.SetTopMargin(0.650)
    bottom_pad.SetLeftMargin(0.175)
    bottom_pad.SetRightMargin(0.050)
    bottom_pad.SetBottomMargin(0.160)
    bottom_pad.Draw()
    bottom_pad.cd()

    # Draw SF
    frame_bottom = plot_canvas.DrawFrame(
        xmin, rmin,
        xmax, rmax,
        ''
    )

    frame_bottom.GetXaxis().SetTitle(xtitle)
    frame_bottom.GetXaxis().SetTitleOffset(1.350)
    frame_bottom.GetYaxis().SetTitle(rtitle)
    frame_bottom.GetYaxis().SetNdivisions(204)
    frame_bottom.GetYaxis().SetTitleOffset(1.450)

    ratio_plot.SetMarkerStyle(8)
    ratio_plot.SetMarkerSize(0.5)
    ratio_plot.SetLineWidth(2)
    ratio_plot.Draw('P E SAME')

    frame_bottom.Draw('SAME AXIS')
    frame_bottom.Draw('SAME AXIG')

    # Mean
    num = res_eff_plot.GetPassedHistogram().GetSumOfWeights()
    num /= res_eff_plot.GetTotalHistogram().GetSumOfWeights()

    den = ref_eff_plot.GetPassedHistogram().GetSumOfWeights()
    den /= ref_eff_plot.GetTotalHistogram().GetSumOfWeights()

    mean = num / den

    # Draw mean label
    mean_label = TLatex()
    mean_label.SetTextAlign(12)
    mean_label.DrawLatexNDC(0.6, 0.3725, 'Mean(#Delta)=%0.2f %%' % ((mean - 1) * 100))

    # Draw Titles
    plot_canvas.cd(0)

    main_title = TLatex(0.045, 0.95, title)
    main_title.SetTextAlign(12)
    main_title.Draw()

    # Get Filename
    if filename is None:
        filename = plt_name

    # Save
    plot_canvas.SaveAs(filename + '.png')

if __name__ == '__main__':
    root_env.configure()
    make_eff_ratio_plot(
        plt_name='pt_eff_vs_pt_num_pt_ge_10_denom_eta_st2_ge_1p2_lt_2p4',
        title='LLP Gun Efficiency @ L1 p_{T} > 10 GeV', xtitle='GEN p_{T}',
        xmin=0., xmax=120.,
        rmin=0., rmax=1.6
    )
    make_eff_ratio_plot(
        plt_name='pt_eff_vs_dxy_num_pt_ge_10_denom_eta_st2_ge_1p2_lt_2p4',
        title='LLP Gun Efficiency @ L1 p_{T} > 10 GeV', xtitle='GEN d_{xy}',
        xmin=0., xmax=120.,
        rmin=0., rmax=7.
    )
