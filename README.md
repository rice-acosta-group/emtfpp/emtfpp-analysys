# EMTF Analysis

## Prerequisites

* Apptainer: [Installation Walkthrough](docs/apptainer)

## Setup

If it's your first time running this framework you'll want to set up a python virtualenv first.

```bash
# Go to the project root directory
# This is the directory that contains the bin, macros, rsc, runs, and src directories
cd project/root/directory/ddm

# Run Singularity
apptainer run docker://registry.gitlab.com/rice-acosta-group/docker/generic-analysis-docker:latest

# Setup virtualenv inside the singularity image
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Running

```bash
cd project/root/directory/ddm
source venv/bin/activate
mkdir out && cd out
../bin/run -h
```