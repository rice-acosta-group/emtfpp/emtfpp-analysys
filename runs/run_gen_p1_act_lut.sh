wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Displaced
task_name='run3_disp_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-act-lut-pt --bitwidth 10 --signed --eps 0.0001220703125 \
  --param-a 1 --param-b 0 --param-bias -16
cd "$wd"

task_name='run3_disp_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-act-lut-dxy --bitwidth 10 --signed --eps 1 \
  --param-a 1 --param-bias 0
cd "$wd"