wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1
zerobiasfile="../../in/run3/data/EphemeralZeroBias0_Run2024I_v1.root"
h2to2xto2mu2j_ctau0p1mm_file="../../in/run3/data/HTo2LongLivedTo2mu2jets_MH-125_MFF-20_CTau-0p1mm.root"
h2to2xto2mu2j_ctau500mm_file="../../in/run3/data/HTo2LongLivedTo2mu2jets_MH-125_MFF-50_CTau-500mm.root"

# Prompt
gflags=""

task_name='run3_rates_prompt'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command check-p1-rates $gflags --nevents="$n_events" \
  --input "$zerobiasfile" --ncb 2340
cd "$wd"

# Displaced
gflags="--displaced"

task_name='run3_rates_disp'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command check-p1-rates $gflags --nevents="$n_events" \
  --input "$zerobiasfile" --ncb 2340
cd "$wd"

#task_name='run3_performance_SingleMuon_FlatVtx_FlatInvPt'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-p1-performance --include-tb-eff --nevents=-1 --nmuon=-1 \
#  --input "$posflatvtxfile" "$negflatvtxfile"
#cd "$wd"

# Data
#task_name='run3_performance_H2To2XTo2Mu2J_ctau0p1mm'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-p1-performance --include-tb-eff --nevents=-1 --nmuon=-1 \
#  --input "$h2to2xto2mu2j_ctau0p1mm_file"
#cd "$wd"

#task_name='run3_performance_H2To2XTo2Mu2J_ctau500mm'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-p1-performance --displaced --include-tb-eff --nevents=-1 --nmuon=-1 \
#  --input "$h2to2xto2mu2j_ctau500mm_file"  --rates="../run3_rates_disp/triggers.csv"
#cd "$wd"
