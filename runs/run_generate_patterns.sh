wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
pattern_events=-1
sig_activation_events=-1
pu_activation_events=700000

posfile_patgen="../../in/phase2/SingleMuon_PosEnd_FlatInvPt_2to7000GeV_PatGen.root"
negfile_patgen="../../in/phase2/SingleMuon_NegEnd_FlatInvPt_2to7000GeV_PatGen.root"
posd0file_patgen="../../in/phase2/SingleMuon_PosEnd_FlatVtx_FlatInvPt_2to7000GeV_PatGen.root"
negd0file_patgen="../../in/phase2/SingleMuon_NegEnd_FlatVtx_FlatInvPt_2to7000GeV_PatGen.root"
posfile_qualgen="../../in/phase2/SingleMuon_PosEnd_FlatInvPt_2to7000GeV_QualGen.root"
negfile_qualgen="../../in/phase2/SingleMuon_NegEnd_FlatInvPt_2to7000GeV_QualGen.root"
posd0file_qualgen="../../in/phase2/SingleMuon_PosEnd_FlatVtx_FlatInvPt_2to7000GeV_QualGen.root"
negd0file_qualgen="../../in/phase2/SingleMuon_NegEnd_FlatVtx_FlatInvPt_2to7000GeV_QualGen.root"
llpfile_patgen="../../in/phase2/XTo2LongLivedTo4Mu_PatGen.root"
llpfile_qualgen="../../in/phase2/XTo2LongLivedTo4Mu_QualGen.root"
pileupfile="../../in/phase2/SingleNeutrino_PU200_Phase2HLTTDRSummer20_Train.root"

# Prompt
#task_name='prompt_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-truth-patterns --nevents="$pattern_events" \
#  --input "$posfile_patgen" "$negfile_patgen"
#cd "$wd"
#
#task_name='prompt_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-signal-patterns --nevents="$pattern_events" \
#  --input "$posfile_patgen" "$negfile_patgen"
#cd "$wd"

task_name='prompt_patterns'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-pattern-luts
cd "$wd"

#task_name='prompt_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command calculate-signal-activation --nevents="$sig_activation_events" \
#  --input "$posfile_qualgen" "$negfile_qualgen"
#cd "$wd"
#
#task_name='prompt_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command calculate-pileup-activation --nevents="$pu_activation_events" \
#  --input "$pileupfile"
#cd "$wd"

#task_name='prompt_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command calculate-pattern-qualities
#cd "$wd"


# Displaced
task_name='disp_patterns'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-truth-patterns --displaced --nevents="$pattern_events" --nmuon=-1 \
  --input "$llpfile_patgen"
cd "$wd"

task_name='disp_patterns'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-signal-patterns --displaced --nevents="$pattern_events" --nmuon=-1 \
  --input "$llpfile_patgen"
cd "$wd"

#task_name='disp_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-pattern-luts --displaced
#cd "$wd"w

#task_name='disp_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command calculate-signal-activation --displaced --nevents="$sig_activation_events" --nmuon=-1 \
#  --input "$llpfile_qualgen"
#cd "$wd"
#
#task_name='disp_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command calculate-pileup-activation --nevents="$pu_activation_events" \
#  --input "$pileupfile"
#cd "$wd"

#task_name='disp_patterns'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command calculate-pattern-qualities
#cd "$wd"
