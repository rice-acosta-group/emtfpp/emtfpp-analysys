wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=1000000

# Distributions
task_name='llp_decay'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-llp --nevents="$n_events"
cd "$wd"

#task_name='vtx_prompt'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-vertex --distribution='prompt' --nevents="$n_events"
#cd "$wd"
#
#task_name='vtx_flat'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-vertex --distribution='flatVtx' --nevents="$n_events"
#cd "$wd"
#
#task_name='vtx_flat_d0'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-vertex --distribution='flatD0' --nevents="$n_events"
#cd "$wd"
#
#task_name='vtx_flat_one_over_d0'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-vertex --distribution='flatOneOverD0' --nevents="$n_events"
#cd "$wd"