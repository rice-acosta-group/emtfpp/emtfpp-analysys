wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1
posfile="../../in/SingleMuon_PosEnd_FlatPt_120GeV.root"
negfile="../../in/SingleMuon_NegEnd_FlatPt_120GeV.root"
beamhalofile="../../in/phase2/BeamHalo.root"

#task_name='bh_prompt_sample'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-beamhalo --nevents="$n_events" \
#  --input "$posfile" "$negfile"
#cd "$wd"
#
#task_name='bh_llp_sample'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-beamhalo --nevents="$n_events" --nmuon=-1 \
#  --input "$llpfile"
#cd "$wd"

task_name='bh_beamhalo_sample'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command check-beamhalo --nevents="$n_events" --nmuon=-1 --beamhalo \
  --input "$beamhalofile"
cd "$wd"