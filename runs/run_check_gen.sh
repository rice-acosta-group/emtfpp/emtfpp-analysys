wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=50000
posfile="../../in/SingleMuon_PosEnd_FlatPt_120GeV.root"
negfile="../../in/SingleMuon_NegEnd_FlatPt_120GeV.root"
llpfile="../../in/BeamHalo.root"
llp500mmfile="../../in/phase2/HTo2LongLivedTo4mu_MH-125_MFF-50_CTau-500mm.root"
llp5000mmfile="../../in/phase2/HTo2LongLivedTo4mu_MH-125_MFF-50_CTau-5000mm.root"
pileupfile="../../in/phase2/SingleNeutrino_PU200_Phase2HLTTDRSummer20_Validation.root"

task_name='prompt_sample'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command check-sample --nevents="$n_events" \
  --input "$posfile" "$negfile"
cd "$wd"

#task_name='llp_sample'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-sample --nevents="$n_events" --nmuon=-1 \
#  --input "$llpfile"
#cd "$wd"

#task_name='llp_5000mm_sample'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-sample --nevents="$n_events" --nmuon=-1 \
#  --input "$llp5000mmfile"
#cd "$wd"
#
#task_name='llp_500mm_sample'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-sample --nevents="$n_events" --nmuon=-1 \
#  --input "$llp500mmfile"
#cd "$wd"

#task_name='pu_sample'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-sample --nevents="$n_events" --nmuon=-1 \
#  --input "$pileupfile"
#cd "$wd"
