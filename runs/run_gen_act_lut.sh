wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Prompt
task_name='prompt_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-act-lut-pt --bitwidth 10 --signed --eps 0.0001220703125 \
  --param-a 1 --param-bias -16
cd "$wd"

task_name='prompt_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-act-lut-rels --bitwidth 10 --signed --eps 0.0115
cd "$wd"

# Displaced
task_name='disp_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-act-lut-pt --bitwidth 10 --signed --eps 0.0001220703125 \
  --param-a 1 --param-bias -16
cd "$wd"

task_name='disp_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-act-lut-rels --bitwidth 10 --signed --eps 0.0115
cd "$wd"

task_name='disp_act_lut'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-act-lut-d0 --bitwidth 10 --signed --eps 1 \
  --param-a 1 --param-bias 0
cd "$wd"
