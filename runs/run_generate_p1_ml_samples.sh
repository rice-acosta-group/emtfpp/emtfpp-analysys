wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=100000
posfile="../../in/run3/SingleMuon_PosEnd_FlatInvPt_2to1000GeV_Train.root"
negfile="../../in/run3/SingleMuon_NegEnd_FlatInvPt_2to1000GeV_Train.root"
posflatvtxfile="../../in/run3/SingleMuon_PosEnd_FlatVtx_FlatInvPt_2to1000GeV_Train.root"
negflatvtxfile="../../in/run3/SingleMuon_NegEnd_FlatVtx_FlatInvPt_2to1000GeV_Train.root"
zerobiasfile="../../in/run3/data/2023EphZB_run368822.root"

# Prompt
task_name='run3_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-ml-signal --nevents="$n_events" \
  --input "$posfile" --filename='pos_prompt_sample'
cd "$wd"

task_name='run3_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-ml-signal --nevents="$n_events" \
  --input "$negfile" --filename='neg_prompt_sample'
cd "$wd"

# Displaced
task_name='run3_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-ml-signal --nevents="$n_events" \
  --input "$posflatvtxfile" --filename='pos_disp_sample'
cd "$wd"

task_name='run3_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-ml-signal --nevents="$n_events" \
  --input "$negflatvtxfile" --filename='neg_disp_sample'
cd "$wd"

task_name='run3_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-ml-signal --nevents="$n_events" \
  --input "$zerobiasfile" --filename='zerobias_sample' --no-gen
cd "$wd"

#Zero Bias - Keeps all tracks
task_name='run3_ml_samples'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-p1-ml-zerobias --nevents="$n_events" \
  --input "$zerobiasfile" --filename='run368822_zero_bias_sample'
cd "$wd"
