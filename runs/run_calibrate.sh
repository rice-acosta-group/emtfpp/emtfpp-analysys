wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1
posfile="../../in/phase2/SingleMuon_PosEnd_FlatPt_120GeV.root"
negfile="../../in/phase2/SingleMuon_NegEnd_FlatPt_120GeV.root"
llpfile="../../in/phase2/XTo2LongLivedTo4Mu_Validation.root"

# Prompt
#gflags="--prompt-pb ../../in/models/prompt.pb"

task_name='prompt_model_calibration'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command calibrate-model $gflags --nevents="$n_events" \
  --input "$posfile" "$negfile" \
  --pt-ord=4
cd "$wd"

task_name='prompt_emtf_calibration'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command calibrate-emtf $gflags --nevents="$n_events" \
  --input "$posfile" "$negfile" \
  --pt-exact-up-to=30 \
  --pt-ord=2
cd "$wd"

# Displaced
#gflags="--disp-pb ../../in/models/disp.pb"

task_name='llp_model_calibration'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command calibrate-model $gflags --displaced --nevents="$n_events" --nmuon=-1 \
  --input "$llpfile" \
  --pt-ord=4 --dxy-ord=1
cd "$wd"

task_name='llp_emtf_calibration'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command calibrate-emtf $gflags --displaced --nevents="$n_events" --nmuon=-1 \
  --input "$llpfile" \
  --pt-exact-up-to=10 \
  --pt-ord=2 --dxy-ord=3
cd "$wd"
