wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=-1
llpfile="../../in/phase2/XTo2LongLivedTo4HSCP_ML.root"

## Prompt
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-signal --endcap='positive' --nevents="$n_events" \
#  --input "$posfile" --filename='pos_prompt_sample'
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-signal --endcap='negative' --nevents="$n_events" \
#  --input "$negfile" --filename='neg_prompt_sample'
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-pileup --endcap='positive' --nevents="$n_events" \
#  --input "$pufile" --filename='pos_prompt_pu_sample'
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-pileup --endcap='negative' --nevents="$n_events" \
#  --input "$pufile" --filename='neg_prompt_pu_sample'
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-combine \
#  --input "pos_prompt_sample.npz" "pos_prompt_pu_sample.npz" \
#  --filename='pos_prompt_with_pu_sample'
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-combine \
#  --input "neg_prompt_sample.npz" "neg_prompt_pu_sample.npz" \
#  --filename='neg_prompt_with_pu_sample'
#cd "$wd"

# LLP
task_name='ml_samples_hscp'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-ml-signal-hscp --endcap='positive' --nevents="$n_events" --nhscp=-1 \
  --input "$llpfile" --filename='pos_llp_sample' --displaced
cd "$wd"

task_name='ml_samples_hscp'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command generate-ml-signal-hscp --endcap='negative' --nevents="$n_events" --nhscp=-1 \
  --input "$llpfile" --filename='neg_llp_sample' --displaced
cd "$wd"

#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-pileup --endcap='positive' --nevents="$n_events" \
#  --input "$pufile" --filename='pos_disp_pu_sample' --displaced
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-pileup --endcap='negative' --nevents="$n_events" \
#  --input "$pufile" --filename='neg_disp_pu_sample' --displaced
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-combine \
#  --input "pos_llp_sample.npz" "pos_disp_pu_sample.npz" \
#  --filename='pos_llp_with_pu_sample'
#cd "$wd"
#
#task_name='ml_samples'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command generate-ml-combine \
#  --input "neg_llp_sample.npz" "neg_disp_pu_sample.npz" \
#  --filename='neg_llp_with_pu_sample'
#cd "$wd"
