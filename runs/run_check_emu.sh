wd=$(pwd)

gflags=""
run_command="../../bin/run $gflags"

# Inputs
n_events=100000
posfile="../../in/phase2/SingleMuon_PosEnd_FlatPt_120GeV.root"
negfile="../../in/phase2/SingleMuon_NegEnd_FlatPt_120GeV.root"
pileupfile="../../in/phase2/MinBias_PU200_Validation.root"
llp5000mfile="../../in/phase2/HTo2LongLivedTo4mu_MH-125_MFF-50_CTau-5000mm.root"
llp500mfile="../../in/phase2/HTo2LongLivedTo4mu_MH-125_MFF-50_CTau-500mm.root"
llpfile="../../in/phase2/XTo2LongLivedTo4Mu_Validation.root"

# Prompt
gflags="--prompt-pb ../../in/models/prompt.pb"
#gflags=""

#task_name='rates_prompt'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-rates $gflags --nevents="$n_events" \
#  --input "$pileupfile"
#cd "$wd"

#task_name='performance_smu_prompt'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=1 \
#  --input "$posfile" "$negfile" --rates="../rates_prompt/triggers.csv"
#cd "$wd"

#task_name='performance_smu_prompt_matched'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --only-matched-eff --nevents="$n_events" --nmuon=1 \
#  --input "$posfile" "$negfile" --rates="../rates_prompt/triggers.csv"
#cd "$wd"

task_name='performance_smu_prompt_on_LLP'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
  --input "$llpfile" --rates="../rates_prompt/triggers.csv"
cd "$wd"

#task_name='performance_smu_prompt_on_LLP_5000mm'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
#  --input "$llp5000mfile" --rates="../rates_prompt/triggers.csv"
#cd "$wd"
#
#task_name='performance_smu_prompt_on_LLP_500mm'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
#  --input "$llp500mfile" --rates="../rates_prompt/triggers.csv"
#cd "$wd"

# Displaced
gflags="${gflags} --disp-pb ../../in/models/disp.pb --displaced"
#gflags="${gflags} --displaced"

#task_name='rates_disp'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-rates $gflags --nevents="$n_events" \
#  --input "$pileupfile"
#cd "$wd"

#task_name='performance_smu_disp_on_prompt'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
#  --input "$posfile" "$negfile" --rates="../rates_disp/triggers_either.csv"
#cd "$wd"

task_name='performance_smu_disp_on_LLP'
mkdir -p "$task_name" && cd "$wd/$task_name"
$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
  --input "$llpfile" --rates="../rates_disp/triggers_either.csv"
cd "$wd"

#task_name='performance_smu_disp_on_LLP_5000mm'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
#  --input "$llp5000mfile" --rates="../rates_disp/triggers_either.csv"
#cd "$wd"
#
#task_name='performance_smu_disp_on_LLP_500mm'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --include-tb-eff --nevents="$n_events" --nmuon=-1 \
#  --input "$llp500mfile" --rates="../rates_disp/triggers_either.csv"
#cd "$wd"

#task_name='performance_smu_disp_on_LLP_matched'
#mkdir -p "$task_name" && cd "$wd/$task_name"
#$run_command check-performance $gflags --only-matched-eff --nevents="$n_events" --nmuon=-1 \
#  --input "$llpfile" --rates="../rates_disp/triggers_either.csv"
#cd "$wd"
